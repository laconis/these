#!/bin/sh

## Récupération des données yaml du chapitre à transformer
cd contenus
cp $1.md temp.md
awk '/---/{x=++i;next}{if (x == 1){split(FILENAME, A, "."); print > A[1]".yaml";}if (x == 2){print >FILENAME}}' temp.md

title=$(yq r temp.yaml title)
version=$(yq r temp.yaml version)
previous=$(yq r temp.yaml previous)
tagprevious=$(yq r temp.yaml tagprevious)


rm temp.md
rm temp.yaml

cd ..

## Test nouveau chapitre ?
[ -e html/$1 ] && ls html/$1 || mkdir html/$1

## Transformation du chapitre > html
echo "transformation du chapitre $title version $version"

pandoc --standalone --section-divs --ascii --filter pandoc-citeproc --filter pandoc-sidenote --variable=file:$1 --css=../css/chapitre.css --template=./templates/chapitre.html5 --lua-filter=./templates/date.lua --table-of-contents ./contenus/$1.md -o ./html/$1/$1-$version.html

## Production d'un html de diff avec la version précédente
if [ "$version" != "$previous" ]
then
  echo "production des diff des versions $previous et $version, depuis tag $tagprevious"
   git diff --color-words  $tagprevious ./contenus/$1.md > mydiff.diff
   ansifilter -i mydiff.diff -H --encoding=UTF8 -l --no-trailing-nl --wrap=80 --doc-title="$1 diff version $previous ($tagprevious) > $version" --style-ref=../css/diff.css -o html/$1/diff$1_$previous-$version.html
   rm mydiff.diff
else
  echo "comparaison $version / $previous --> pas de diff"
fi

cp -r media ./html/

tree html/$1
