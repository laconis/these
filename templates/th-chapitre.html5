<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="$lang$" xml:lang="$lang$"$if(dir)$ dir="$dir$"$endif$>
<head>
<link type="application/rdf+xml" id="zotero_rdf" href="$id$.rdf" title="RDF Bibliontology" /><!--metadonnées en rdf pour zotero-->
<meta charset="utf-8" />
<meta name="generator" content="pandoc" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="DC.format" content="text/html" />
<meta name="DC.type" content="thesis" />
<meta name="DC.title" content="$if(title-prefix)$$title-prefix$ – $endif$$pagetitle$$if(subtitle)$ : $subtitle$$endif$" />
<meta name="DC.title.Main" content="$title$" />
$if(subtitle)$
<meta name="DC.title.Subtitle" content="$subtitle$" />
$endif$
<meta name="DC.publisher" class="editeur" content="$author$" />
<meta name="DC.language" content="$lang$" />
<meta name="DC.date" class="completeDate" content="$date$" />

$if(url)$
<meta name="url" content="$base-url$/$version$/$url$.html" />
$endif$
$if(url)$
<meta name="DC.identifier" content="$base-url$/$version$/$url$.html" />
$endif$


$for(author-meta)$
  <meta name="author" content="$author-meta$" />
$endfor$
$if(date-meta)$
  <meta name="dcterms.date" content="$date-meta$" />
$endif$
$if(keyword_en)$
  <meta name="keywords" content="$for(keyword_en)$$keyword_en$$sep$, $endfor$" />
$endif$
  <title>$if(title-prefix)$$title-prefix$ – $endif$$pagetitle$$if(subtitle)$ : $subtitle$$endif$ $if(version)$(v$version$)$endif$</title>

  <meta property="dcterm:created" content="$date$" />

<!-- schema citation : Indexation pour Google Scholar  -->

<meta name="citation_publisher" content="$author$" />
<meta name="citation_authors" content="$author$" />
<meta name="citation_book_title" content="$these-title$ – $these-subtitle$" />
<meta name="citation_title" content="$title$" />
$if(subtitle)$
<meta name="citation_subtitle" content="$subtitle$" />
$endif$
<meta name="citation_publication_date" content="$date$" />
<meta name="citation_online_date" content="$date$" />
<meta name="citation_language" content="$lang$" />

<meta name="citation_abstract" xml:lang="fr" lang="fr" content="$abstract_fr$" />
$if(abstract_en)$
<meta name="citation_abstract" xml:lang="en" lang="en" content="$abstract_en$" />
$endif$

$if(keyword_fr)$
<meta name="citation_keywords" xml:lang="fr" lang="fr" content="$for(keyword_fr)$$keyword_fr$$sep$, $endfor$" />
$endif$

$if(keyword_en)$
<meta name="citation_keywords" xml:lang="en" lang="en" content="$for(keyword_en)$$keyword_en$$sep$, $endfor$" />
$endif$


<meta name="citation_pdf_url" content="$base-url$/$version$/pdf/$pdf-url$">

<!--RDFa pour ajout des metadonnees DC-->
<meta property="dc:format" content="text/html" />
<meta property="dc:identifier" content="$base-url$/$version$/$url$.html" />
<meta property="dc:title" content="$if(title-prefix)$$title-prefix$ – $endif$$pagetitle$$if(subtitle)$ : $subtitle$$endif$" />
<meta property="dc:title.Main" content="$title$" />
$if(subtitle)$
<meta property="dc:title.Subtitle" content="$subtitle$" />
$endif$
<meta property="dc:publisher" content="$author$" />
<meta property="dc:language" content="$lang$" />
<meta property="dc:type" content="thesis" />
<meta property="dcterm:created" content="$date$" />
<meta property="dc:rights" content="$license$" />
<meta property="dc:creator" content="$author$" />
<meta property="dcterms:abstract" xml:lang="fr" lang="fr" content="$abstract_fr$" />
$if(abstract_en)$
<meta property="dcterms:abstract" xml:lang="en" lang="en" content="$abstract_en$" />
$endif$

<!-- Social network -->

<meta property="og:type" content="thesis" />
<meta property="og:url" content="$base-url$/$version$/$url$.html" />
<meta property="og:title" content="$if(title-prefix)$$title-prefix$ – $endif$$pagetitle$$if(subtitle)$ : $subtitle$$endif$" />
<meta property="og:description" content="$abstract_fr$ " />
<meta property="og:image" content="$coverurl$" />



  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
$if(quotes)$
      q { quotes: "“" "”" "‘" "’"; }
$endif$
  </style>
$if(highlighting-css)$
  <style type="text/css">
$highlighting-css$
  </style>
$endif$
  <link rel="stylesheet" href="../css/fonts.css" />
$for(css)$
  <link rel="stylesheet" href="$css$" />
$endfor$
  <link rel="stylesheet" href="../css/th.css" />
$if(math)$
  $math$
$endif$
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
$for(header-includes)$
  $header-includes$
$endfor$
</head>
<body>
<div class="header">
$if(title)$
<header id="title-block-header">
<div style="display:none">Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
<a class="home" href="/index.html">
  <img class="icon" src="../utils/geography.svg" alt="Index"/>
</a>
<h1 class="title">$title$ $if(subtitle)$ : <span class="subtitle">$subtitle$$endif$</h1>
<div class="navchap"><a class="chap-prec" href="../$versthese$/chapitreprec.html"><img class="icon-nav" src="../utils/left-arrow.svg" alt="précédent"/>PRECTITLE</a><a class="chap-next" href="../$versthese$/chapitrenext.html">NEXTTITLE<img class="icon-nav" src="../utils/next.svg" alt="suivant"/></a></div>
$for(author)$
<p class="author">$author$</p>
$endfor$
</header>
$endif$
</div>
<div class="row">
<div class="side">
<h1 class="title">$title$ $if(subtitle)$: <span class="subtitle">$subtitle$$endif$</h1>
$if(date)$
<p class="date">[$if(version)$v$version$$endif$ mise-à-jour le $date$] $if(version)$<a href="diff$file$_$previous$-$version$.html">[compare v$previous$]</a>$endif$</p>
$endif$
$if(toc)$
<nav id="TOC">
$table-of-contents$
</nav>
$endif$
</div>
<div class="main">
<article>
$for(include-before)$
$include-before$
$endfor$
$body$
</article>
</div>
</div>
$for(include-after)$
$include-after$
$endfor$

<script src="./vendor/jquery/jquery.min.js"></script>
<script src="./vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="https://hypothes.is/embed.js" async></script>
</body>


</html>
