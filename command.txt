- Pour comparer un fichier avec le dernier commit (Où en est le travail?):

      git difftool -t meld chapitre1.md

- Pour comparer un fichier avec une version taggée (Qu'est ce qui a été rajouté depuis la précedente version livrée):

      git difftool -t meld ch1-0.1 chapitre1.md

- pour utiliser diff2html:

      git diff  ch1-0.1 chapitre1.md > mydiff.diff
      diff2html -s side -i file -- mydiff.diff

- lancer la conversion html de revue20, les versions et les tags de versions (diff) sont maintenant dans le yaml de chaque fichier, et utilisés lors du build

    ./build.sh revue20

- recréer l'index

    ./makeindex.sh
