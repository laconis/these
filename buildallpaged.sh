#!/bin/sh

chapters=('intro' 'partie1' 'revue20' 'conversation' 'conclusion' 'bibliographie' 'annexes')
# chapters=('intro' 'partie1')


echo "transformation de la page de garde - pagedJS"

rm ./paged/temp/*

pandoc -f markdown -t html --ascii ./contenus/garde.md -o ./paged/temp/garde.html
pandoc -f markdown -t html --ascii ./contenus/avertissement.md -o ./paged/temp/avertissement.html
pandoc -f markdown -t html --ascii ./contenus/colophon.md -o ./paged/temp/colophon.html

# pandoc --section-divs --ascii  --css=/css/garde.css --template=./templates/garde.html5 ./contenus/garde.md -o ./paged/temp/garde.html

# mkdir paged/temp

for i in ${!chapters[@]};
do

echo "transformation de ${chapters[i]} - pagedJS"

pandoc --section-divs --ascii --template=./templates/pagedchap.html5 --filter pandoc-citeproc --filter pandoc-sidenote ./contenus/${chapters[i]}.md -o ./paged/temp/${chapters[i]}.html

done

pandoc --section-divs --ascii --css=/css/contentpaged.css -B ./paged/temp/garde.html -B ./paged/temp/avertissement.html -A ./paged/temp/intro.html -A ./paged/temp/partie1.html -A ./paged/temp/revue20.html -A ./paged/temp/conversation.html -A ./paged/temp/conclusion.html -A ./paged/temp/bibliographie.html -A ./paged/temp/annexes.html -A ./paged/temp/colophon.html --template=./templates/pagedjsall.html5 --lua-filter=templates/date.lua  contenus/remerciements.md -o ./paged/these-paged.html

# pandoc --section-divs --ascii --css=/css/contentpaged.css -B ./paged/temp/garde.html -B ./paged/temp/avertissement.html -A ./paged/temp/intro.html -A ./paged/temp/partie1.html --template=./templates/pagedjsall.html5 --lua-filter=templates/date.lua  contenus/remerciements.md -o ./paged/these-paged.html

# annexes: renvoie vers les annexes en ligne
sed -i -e "s/..\/annexes/https:\/\/these.nicolassauret.net\/annexes/g" ./paged/these-paged.html
# liens interne: supprime la structure html mais ne fonctionne pas sur pdf.
# to do: ajouter un élément vide avec l'id du lien interne sous/sur la section ciblée.
sed -i -e "s/..\/vrt\/\w*.html//g" ./paged/these-paged.html

cp -r media ./paged/

cd paged
python3 -m http.server 1337
