#!/bin/sh

echo "transformation de la page de garde - pagedJS"


echo "transformation du $1 - pagedJS"

pandoc --section-divs --ascii --filter pandoc-citeproc --filter pandoc-sidenote --css=/css/contentpaged.css --template=./templates/pagedjs.html5 --lua-filter=templates/date.lua --table-of-contents ./contenus/$1.md -o ./paged/$1.html

cp -r media ./paged/

tree paged
cd paged
python3 -m http.server 1337
