#!/bin/sh

echo "Production du pdf du $1 version $2"

pandoc --standalone --section-divs --ascii --id-prefix=$1-$2- --filter pandoc-citeproc --template=./templates/templateprojetrecherche.latex --lua-filter=templates/date.lua --table-of-contents ./$1.md -o ./pdf/$1-$2.tex
cd pdf
xelatex ./$1-$2.tex
xelatex ./$1-$2.tex


#echo "transformation de l'index"
#pandoc --standalone --section-divs --ascii --id-prefix=index --css=css/chapitre.css --template=./templates/chapitre.html5 index.md -o ./html/index.html

tree pdf
