Ce document récupère les chutes et documente ici leur retrait.

1. «Hypothèse\ : un modèle éditorial conversationnel» :
  - Initialement (23 sept 2019), une introduction à la partie conversation  https://framagit.org/laconis/these/-/commit/56ba9dbe33833131c883f9762c9f52ac4d2915cd
  - un chapeau a été ajouté (29sept -- commit du 3 nov) suite à la lecture du poste de jahjah https://framagit.org/laconis/these/-/commit/27ffe7fa17192f79b0bb241e4bbaf14f59181812
  - il est obsolète et bien mieux exprimé par le préambule.
2. «Version 0. Notes sur le livre numérique» :
   - initialement (fév 2020), une description du booksprint version0
   - déplacé et écrémé dans la partie protocole/version0
3. Publishing Sphere, 3 sous-parties:
  - "On y voyait assez clair": où j'explicite le lien entre PS, commun et GI, en utilisant la note d'intention pour le groupe et un post de blog Behind Instin. (peut-être redondant)
  - Modèle conversationnel: reprise de la présentation ELO, où je fais le lien entre imaginaire, mémoire et interprétation. (peut-être redondant)
  - Intersections: je recoupe les discours avec notamment l'ambassade des communs, depuis l'entretien de claire dehove. permet d'expliciter un peu mieux les liens théoriques qui unissaientt le groupe. Elements pertinents pour élaborer sur le groupe.
4. Tweet Emin Ecridil
5. La Charte de l'Ambassade de la MétaNation
6. quelques notes en fin de partie 3
7. Evaluation et "collégialité"
8. Merzeau par Maurel
9. tweet Calimaq Sortie dossier Communs @romain_lalande
10. le contrat d'écriture
11. le constat que le groupe IF ne fonctionne pas
12. partie1: intro obsolète à la partie "institution et chaine de l'écrit"
13. Citations de Guichard sur l'institution
14. Citation complète de Bourdieu
15. Continuité, episteme, gripper l'innovation
16. Note sur le document pour l'introduction (Pédauque)
17. Dialectique formes et pratiques + OpenEdition
18. materialité performative de Drucker
19. Conclusion - git-thèse
20. rédaction révélateur/conversation catalyseur

## Hypothèse\ : un modèle éditorial conversationnel

La situation actuelle des revues met en lumière la nécessité de repenser un paradigme éditorial adapté aux pratiques d'écriture et de lecture qui ont emergé avec l'avènement de l'environnement numérique.

Dans cette partie prospective sur le futur de la revue, nous souhaitons explorer l'hypothèse que l'institution scientifique est en mesure de concevoir et d'adopter de nouvelles formes éditoriales de communication scientifique, à même d'incarner et d'instaurer une épistémé du numérique. Nous soutiendrons que la revue, en tant que média et tel qu'il s'est institutionnalisé dans le champ des sciences humaines et sociales peut constituer un lieu privilégié pour une telle innovation éditoriale. En effet, si ses formes éditoriales restent aujourd'hui contraintes dans le modèle épistémologique, elle conserve, de part sa position intermédiaire entre l'institution universitaire et ses acteurs (chercheurs, éditeurs), une liberté d'action et de pensée qui pourrait être mise à profit pour expérimenter de nouveaux formats.

À travers plusieurs cas d'études et terrains d'observation et d'expérimentation, je montrerai qu'une orientation possible pour transformer la communication scientifique est celle de la conversation. Nous faisons ainsi l'hypothèse d'un modèle éditorial conversationnel, tel que celui mis en place par le dispositif ENMI en 2012 que Louise Merzeau a pu observer et analyser. Son analyse, qui s'est déployée entre 2013 et 2017 à travers plusieurs écrits programmatiques, pave une réflexion que nous reprendrons et étendrons pour imaginer de nouvelles formes de production et de circulation de connaissance.
La dernière partie de la thèse consistera à confronter le modèle théorique des ENMIs (issu de la pratique, c'est à dire initialement de la conception d'un dispositif.), à plusieurs terrains d'observation (Général Instin, Manifeste CCLA) et plusieurs expérimentations (Booksprint Ecridil, Dossier «\ Écrire les communs\ », Publishing Sphere) réalisées pendant le doctorat. Ces différents terrains nous incitent à penser que la communauté scientifique pourrait  s'inspirer des pratiques et des formats élaborés par des  communautés d'écriture non-académiques. Ces communautés ont en effet mis en oeuvre ce que nous appelerons une «\ écriture-milieu\ » dont l'enjeu n'est plus tant de faire document, mais de faire collectif, rejoignant par là-même le projet de Louise Merzeau qui pressentait dans les pratiques communicationnelles émergentes une «\ rhétorique du nous\ » susceptible de porter l'intelligence collective nécessaire pour adresser la complexité du monde.


[articulation avec une partie sur l'écriture, mais peut-être à retrouver plutôt en partie 1]{.note}

Institution et forme éditoriale se répondent et co-existent dans une relation de transduction. De la même manière l'édition doit se penser en co-existence avec l'écriture. Il est donc nécessaire de regarder de plus près ce qu'est l'écriture numérique.

---

## Version 0. Notes sur le livre numérique

~~Cet ouvrage collectif est né de l'intention initiale des organisateurs du colloque _ÉCRIDIL 2018 – Écrire, éditer, lire à l'ère numérique_ de produire un livre numérique avec «\ l'idée d'ouvrir les problématiques, les réflexions et les corpus du colloque à un public élargi, le plus vite possible -- "à chaud" juste après le colloque\ »[@audet_version_2018, p.9-10]. Cette seconde édition du colloque était consacrée au livre sur le thème «\ Le livre, défi de design -- L'intersection numérique de la création et de l'édition\ », suggérant à ses organisateurs d'imaginer une forme éditoriale originale et alternative à des actes classiques.~~

### Contexte

Un peu plus d'un mois avant l'événement, une première discussion par courriel entre René Audet, Jean-Louis Soubret et Servanne Monjour a eu lieu pour tenter de baliser un cadre pour un objet éditorial encore indéfini. Il ressort de ces discussions [Opportun de publier les échanges courriels en annexe\ ?]{.note} la volonté de ne pas définir trop tôt les principes et la forme de l'ouvrage, et d'ouvrir le processus de conception aux participants du colloque lors d'un «\ atelier d'idéation\ » organisé sur une après-midi pendant le colloque en parallèle de certaines sessions.

Jean-Louis Soubret, principal instigateur de l'atelier a souhaité utiliser ce temps pour faire émerger une ou plusieurs formes éditoriales pertinentes pour répondre aux intentions annoncées. Il exprime notamment cette intention dans un courriel à destination des organisateurs du _booksprint_ \ :

:::focus

Jean-Louis Soubret (27 avril 2018 à 06:45)\ :

> D’après nos échanges, un autre principe fait consensus et nous anime\ : la co-création. C’est aussi pourquoi nous proposons cet atelier. [...] J’en déduis que\ :
>
> - les contraintes doivent être exhaustives et explicites ; tout le reste est ouvert au questionnement et à la créativité,
> - les solutions que nous avons imaginées comme souhaitables ne doivent pas être des points d’arrivée, mais peuvent être des points de départ,
> - les livrables de l’atelier sont autant de briques que nous devrons (ré)assembler pour leur donner de la cohérence\ : en sortie nous disposerons de concepts desquels il nous faudra déduire une proposition de valeur viable à laquelle nous devrons donner de la cohérence interne et du sens pour nos utilisateurs pressentis.
>
>[...]
>
>Je serais pour (ré)ouvri[r] la question dans la 1re partie de l'atelier et ne pas se priver d'idées alternatives qui pourraient émerger.

:::

[Courriels booksprint Ecridil (24/08/2018) [voir en annexe](annexes/ecridil/booksprint-Ecridil-20180424.html)]{.legendeFocus}

Le désaccord qui transparait de ce message concerne l'utilisation du temps de l'atelier et de la contribution des participants au colloque. Pour Soubret, la co-conception de l'ouvrage devait s'effectuer pendant le colloque lors de l'atelier, selon un dispositif de co-design précis. Tandis que notre côté, nous avions déjà mené avec Servanne Monjour une réflexion sur un dispositif éditorial dédié prenant en considération les objectifs que s'étaient fixés les organisateurs. Nous souhaitions utiliser le temps de l'atelier pour mettre les participants à contribution de la réalisation et non de la conception.

:::focus

Nicolas Sauret (25 avril 2018 à 12:52)\ :

> La logistique de l'atelier est très importante. [...] Nous avons identifié 3 "inconnues" ou 3 objectifs à remplir, ou réponse à trouver collectivement pendant l'atelier (et sans doute chronologiquement ou simultanément par groupes de travail, selon l'organisation et le nombre de personnes):
>
>  1. Affiner le concept du livre\ : est ce qu'on valide l'idée d'un index, souhaite t on faire autre chose, si oui, comment faire émerger un concept. Le principe de l'index peut vraiment se décliner de plein de manière, c'est encore très ouvert, d'où le point 2.
>  2. Quel format pour les entrées, combien d'entrées pour le livre (il s'agit de "cerner" les sujets/problématiques du colloque), que met-on dans une entrée (définition, ressource, problématiques soulevées, pistes de réponses, références utiles, controverses du colloque, illustrations, etc.), ...
>  3. le tagging collaboratif. Il existe des méthodes en design thinking pour cela. À nouveau, JL pourra être force de proposition. Il s'agit d'un dialogue entre les fiches synthèses et un jeu de concept, les deux s'alimentant mutuellement. Comme tout étape de catégorisation, c'est la plus intéressante et compliquée d'un point de vue scientifique, mais nous avons l'obligation d'arrêter une liste d'entrées pendant l'atelier d'idéation.
>
> Les trois objectifs sont dépendants entre eux. Donc il y aura nécessairement un temps collectif pour discuter le concept global, puis effectivement on peut envisager des groupes de travail différents.

:::

[Courriels booksprint Ecridil (24/08/2018) [voir en annexe](annexes/ecridil/booksprint-Ecridil-20180424.html)]{.legendeFocus}

<!-- Copier-coller 2 mars -->

Quelques jours avant l'événement, le site du colloque dédiait une page au _booksprint_ qui décrivait le dispositif  à travers une note d'intention, un protocole éditorial, et un _workflow_, ou un déroulé, précisant les étapes de collecte, de sélection, d'écriture et d'édition.

:::focus
En parallèle au colloque ÉCRIDIL, le comité d'organisation organise un booksprint destiné à proposer une synthèse originale de nos échanges.

Le livre que nous souhaitons éditer ne constituera pas des «\ actes\ » à proprement parler et ne redoublera pas non plus l'ensemble des archives (visuelles, sonores, textuelles) produites pendant le colloque. Il s'agira plutôt d'ouvrir nos problématiques, nos réflexions, nos corpus à un public élargi. Nous faisons donc l'hypothèse de produire un index du colloque, qui permettra de rendre compte des concepts et notions les plus importantes, tout en faisant dialoguer les différentes interventions.

Outre l'objet livre qui en sera le résultat concret, ce booksprint est aussi une expérimentation questionnant le sujet du colloque. Nous adopterons ainsi des pratiques éditoriales tournées vers une édition collaborative, continue, susceptible de maintenir un dialogue permanent entre forme et contenu. Or les structures formelles de l'édition et de l'éditorialisation sont en premier lieu de nature scripturale. L'enjeu du dispositif de production réside alors dans l'articulation de ces deux écritures\ : la structure-code et le discours.
:::

[Intentions du _booksprint_ Ecridil 2018]{.legendeFocus}

### Dispositif

Le dispositif n'a cessé d'évoluer et de se mettre à jour, tant dans sa dimension organisationnelle que dans sa dimension technique. Proposé sous le terme de _booksprint_, en référence à la méthode diffusée par la plateforme [FLOSS Manual](https://fr.wikipedia.org/wiki/FLOSS_Manuals), il en reprend certains principes. Le _booksprint_ est défini par Wikipédia comme «\ une session de travail intensif organisée pour l'écriture complète d'un livre, par plusieurs auteurs, réunis ou à distance, souvent encadrés par un facilitateur non rédacteur\ »^[voir sur Wikipédia\ : https://fr.wikipedia.org/wiki/Booksprint]. La méthode s'inspire du _code sprint_, rassemblant dans un temps limité plusieurs programmeurs autour de la réalisation d'un logiciel ou d'une fonctionnalité logicielle, et s'apparente de ce fait aux «\ méthodes agiles\ » préconisées en design et appliquées notamment au design logiciel.

Le _booksprint_ Ecridil s'est déroulé en trois temps\ :

1. le colloque\ : prise de notes et production de fiches de synthèse
2. l'atelier d'idéation\ : émergence d'un concept et d'une forme pour le livre
3. le sprint\ : réalisation du livre par l'équipe de sprinters

![Protocole du _booksprint_ Ecridil 2018](https://ecridil.ex-situ.info/user/pages/04.booksprint/ecridil-booksprint-process.png)

Il serait plus juste d'en ajouter un quatrième en amont du booksprint, à savoir la conception et la préparation du booksprint lui-même. L'analyse que nous avons faite du dispositif ENMI12[^dispoenmi12] faisait une large place au trois mois de préparation, essentiels à la réussite de l'expérience [enmi12 image des trois temps]{.note}. Le dispositif du _booksprint_ d'Ecridil s'apparente à celui des ENMI12 en cela qu'il s'agissait également de procéder à une «\ éditorialisation collaborative d'une évenement\ » [@merzeau_editorialisation_2013], soit une «\ couverture\ » de deux journées de colloque.

[^dispoenmi12]: ![Dispositif ENMI12 - visuel 1](../media/enmi12-dispositif1.png) ![Dispositif ENMI12 - visuel 2](../media/enmi12-dispositif2.png) [Dispositif ENMI12 - visuels]{.legendeImage}

Une différence notable entre les deux dispositifs réside dans la finalité anticipée. Là où les ENMI12 envisageait une documentation et une conversation en temps réel par le biais de l'engagement d'une communauté large sur un archipel de plateformes, le _booksprint_ Ecridil tendait essentiellement à la production d'un ouvrage dont les formes et le fond étaient encore à définir, en partie pendant l'évenement lui-même en intégrant dans la réflexion les participants au colloque. Lors des ENMI12, l'intention était d'ouvrir un espace alternatif à la conférence[^objectifscouverture], tout en produisant sa documentation. L'intention du booksprint, dans la lignée des «\ Catal-actes\ » [voir https://www.cairn.info/design-et-innovation-dans-la-chaine-du-livre--9782130788836-page-11.htm]{.ref} de la première session Ecridil à Nimes en 2016, relevait davantage d'une expérimentation éditoriale vers une forme livresque originale.

[^objectifscouverture]: ![Les objectifs de la couverture ENMI12](../media/enmi12-objectifs.png) [Les objectifs de la couverture ENMI12]{.legendeImage}

L'équipe initiatrice du _booksprint_ mettait en place un protocole éditorial disposant une structure de travail, des temporalités et des espaces dédiés d'écritures et d'échanges. Le travail éditorial lui-même consistait en sept étapes\ :

|   |_étape_    |_description_|
|:-:|:----------|:--------------------|
[1] | Prise de notes collaborative | un pad général pour toute la conférence, structuré par communication |
[2] | Rédaction des fiches synthèses | 1 pad par fiche synthèse (chaque fiche synthèse est associée à une communication) |
[3] | Tagging collaboratif | [a] catégorisation\ : émergence d’une première série de tags <br/> [b] tagging des fragments des fiches synthèses |
[4] | Sélection des entrées | identification des entrées de l’index |
[5] | Requête | on extrait tous les fragments taggés ou en lien avec les entrées sélectionnées |
[6] | Mashup | production des entrées à partir des fragments |
[7] | Édition | édition continue et simultanée à la production des entrées |


![Déroulé du travail éditorial du _booksprint_ Ecridil 2018](https://ecridil.ex-situ.info/user/pages/04.booksprint/ecridil-booksprint-workflow.png)

Ces sept étapes rappellent le découpage des activités de la _newsroom_ des ENMI12[^foncprinc], organisées séquentiellement dans un processus relativement similaire d'assimilation et de transformation.

[^foncprinc]: ![Fonctions principales de la couverture [reference]{.ref}](../media/enmi12-fonctions.png) [Fonctions principales de la couverture]{.legendeImage}

---------------   ----------------------------------------------------------------------------------------------
**ENMI12**        prises de notes, conversation --> agrégation, curation --> archive, éditorialisation
**Booksprint**    prises de notes --> synthèse --> catégorisation --> sélection --> agrégation --> mise en forme
---------------   ----------------------------------------------------------------------------------------------

Alors que le dispositif des ENMI tendait principalement à susciter et à favoriser les conversations autour de l'événement, notamment avec l'intégration de la plateforme _Polemictweet_^[![Capture d'écran de la plateforme Polemictweet lors des ENMI12](../media/enmi12-polemictweet-j1.png) [Capture d'écran de la plateforme _Polemictweet lors des ENMI12_]{.legendeImage}], le dispositif d'écridil a en quelque sorte  instrumentalisé la conversation pour se définir lui-même. C'était l'idée de laisser ouverte une part conséquente de la conception éditoriale aux participants du colloque, lors de l'«\ atelier d'idéation\ » supposé faire émerger la forme et le fond de l'ouvrage.

Les échanges et les idées avancées lors de cet atelier ont été aussi riches que ludiques, mais leur état d'aboutissement ne permettait pas d'entreprendre leur réalisation dans le temps et le cadre impartis. L'atelier a malgré cela été utile pour créer un lien particulier entre les participants au colloque et l'équipe éditoriale qui allait poursuivre le travail d'édition. Ce lien a résulté tout d'abord à la sensibilisation de la trentaine de participants aux objectifs et à la finalité du projet. Par ailleurs, en travaillant de concert avec l'équipe éditoriale sur une série de tâches collaboratives, ils se sont associés à une pratique créative, un _faire ensemble_, élargissant le collectif de pensée. Le dialogue qui a ensuite été mis en scène par le truchement de l'ouvrage est rentré en résonnance avec les échanges issus de l'atelier. Entre le premier, fondé sur l'agrégation et la synthèse visuelle de fragments du colloque, et les seconds, engagés dans une réflexion sur les formes du premier dialogue, on retrouve ce rapport étroit entre le support et la pensée. Précisément lors de cette expérience, les modalités d'écriture et d'édition de l'ouvrage se reflètent dans la conversation des fragments.

<!-- Fin du copier-coller -->

### Réalisation

[Gallery photo de Loup Brun - voir [1) booksprint](https://albums.photographie.loupbrun.ca/#15254806497433), [2) colloque et atelier](https://albums.photographie.loupbrun.ca/#15251447512345)]{.note}

L'ouvrage fini est un format poche de 159 pages structuré en 15 entrées thématiques\ : _Architecture_, _Clôture_, _Collectif_, _Espace (public)_, _Forme_, _Geste d’écriture_, _Jeu_, _Humanités numériques_, _Matérialité_, _Mouvement_, _Parcours_, _Sémiotique_, _Sensorialité_, _Transferts et Typographie_.

Ces 15 thématiques ont émergé des étapes [3] Catégorisation et [4] Sélection des entrées, menées pendant le booksprint avec l'équipe éditoriale.



:::bodywidth

![Pad de mots-clé par intervention<br/>([élargir](../media/ecridil-padmotscles2.png) ou [voir en vidéo](../../../these_ressources/ecridil/ecridil-categorization.ogv))](../media/ecridil-padmotscles2.png){height=250px}

![Pad de l'entrée _Sensualité_ (renommé _Sensorialité_)<br/>([élargir](../media/ecridil-padsensualite.png))](../media/ecridil-padsensualite.png){height=250px}

:::


Ainsi, rassemblés pour deux jours après l'événement dans une salle de rédaction improvisée pour réaliser l'ouvrage, l'équipe éditoriale a travaillé à faire converger les propositions émergentes de l'idéation collective, avec le protocole initialement mis en place. Plusieurs idées ont été retenues ou ont pu inspirer l'équipe éditoriale. Par exemple, sur proposition d'Arthur Perret lors de l'atelier d'idéation, les éditeurs ont travaillé dans un premier temps à une mise en page des contenus sur deux colonnes. Cette mise en page particulière^[J'en ai repris les principes pour l'édition et la mise en page de la présente thèse.] a été conçue et véhiculée par l'informaticien Edward Tufte, spécialiste de la visualisation de données.

> Tufte’s style is known for its simplicity, extensive use of sidenotes, tight integration of graphics with text, and carefully chosen typography.^[Voir sur le site _Tufte CSS_\ : https://edwardtufte.github.io/tufte-css/]

Ce principe nous a semblé particulièrement pertinent pour orchestrer une mise en dialogue des fragments.

La chaîne éditoriale utilisée se base sur l'outil _Pandoc_ pour produire une base HTML de l'ouvrage à partir de contenus édités au format _Markdown_. [développer]{.note}

[aspect processuel]{.note}

# Publishing Sphere

### [On y voyait assez clair]
On y voyait pourtant assez clair avec Sylvia en composant le groupe. Suite à une première journée infructueuse pour faire émerger une modalité d'action, la consigne[^consigne] était donnée le lendemain d'expliciter par écrit «\ avec quoi nous venons\ », avec quelles «\ inquiétudes\ », selon le mot de Camille Louis. Pour ma part lors de cette exercice, je tentais d'exprimer le lien entre la _Publishing Sphere_, le _Général Instin_ et les autres initiatives artistiques et politiques que l'on pourrait qualifier de création institutionnelle.

[^consigne]: ![Protocole de conversation asynchrone\ : écrire individuellement.](../media/protocole-de-conversation-asynchrone-ecrire-individuellement.png) [Protocole de conversation asynchrone\ : écrire individuellement.]{.legendeImage}

> En étudiant le Général Instin avec Servanne Monjour, nous avions identifié dans ce “collectif en train de s’écrire” une manière de faire collectif par l’écriture. J’y vois, de manière très intuitive (seulement), une source d’inspiration pour d’autres communautés, qu’elles soient investies sur des terrains sociaux, politiques, littéraires, pour produire des connaissances. En s’émancipant des canaux traditionnels de publication, le GI en vient à réinventer l’acte de publication. [...]
>
> Le lien à la Publishing Sphere est alors une réinvention des formes de publications et d’écriture, et ses effets sur le monde ou sur les terrains où se produisent ces écritures. j’y vois une production d’espace, qui peut être aussi une réinvention d’un espace public. \
  -- Notes du 24 mai - temps d'écriture individuel
  http://notes.ecrituresnumeriques.ca/1jSmmzxnQ5i8RTc8Bl2oIQ.html#nicolas


Dans cette note rapidement rédigée, la problématique que je pose est on peut se demander «\ si d’autres communautés/collectifs ont également investi des formes d’écriture ou de publication produisant de nouveaux espaces publics\ »

je fais également le pont avec le dossier «\ Écrire les communs. Au-devant de l'irréversible\ », piloté conjointement avec Sylvia Fredriksson,

Alors pour comprendre cette intuition, il est nécessaire de revenir en arrière. je vous propose donc de replonger dans l'analyse qu'on faisait du GI deux ans plus tôt.

>  >  Notre intérêt pour le projet Général Instin provient d’une intuition\ : Instin serait un cas d’étude idéal pour comprendre les formes et les pratiques littéraires contemporaines. La multiplicité comme l’hétérogénéité des médias et des formes d’expression croisés qui sont à l’œuvre dans le projet semblent suggérer un nouveau mode de publication littéraire (le terme «\ publication\ » étant compris au sens de «\ rendre public\ »). L’écriture littéraire, en ce sens, se conçoit autant à travers une communauté de contributeurs qu’une collection de médias. L’acte de publication, s’il perd une part de l’autorité que lui conférait les institutions littéraires, regagne en vitalité, en diversité et en circulation. La publication se libère de son carcan institutionnel, s’autonomise par rapport à lui et s’autorise. La mise en circulation se suffit à elle-même, valorisée par les nouvelles dynamiques qu’elle génère, jusqu’à ré-irriguer au passage les circuits traditionnels comme on peut le voir avec les «\ spins-of\ » de GI.1 […] De même qu’Instin constitue intuitivement un cas d’étude idéal de littérature contemporaine, il pourrait nous éclairer sur la nature d’un environnement-dispositif ouvert à toute forme d’appropriation et de réécriture du Général.\ -- http://nicolassauret.net/behindinstin/2017/04/11/questions-de-recherche.html

[mettre ici "l'analyse dispositive" d'Instin\ ?]{.note}

### Modèle conversationnel

&nbsp;

Renouveler les processus de production du sens\ :

- imaginaire
- mémoire
- interprétation


Instin est emblématique de ce paysage. Ce jeu décomplexé de l'emprunt, du remix, du mashup, voir du plagiat, mais aussi ce jeu de rebond d'un hypertexte à un autre, ou encore l'oralité et la performance réhabilitant la présence et l'échange, témoignent ensemble du modèle conversationnel.

Et puisque son écriture est autant récit que milieu, ce n'est pas simplement la figure littéraire du général qui est ouverte, transparente et appropriable, ce sont également ses principes, son fonctionnement, son milieu-dispositif.

On retrouve ainsi cette même dynamique de réécriture et d'appropriation dans le dispositif décrit par Louise Merzeau en 2013, dont l'objet cette fois-çi n'était pas littéraire, mais scientifique et philosophique. Dans cet article, Louise Merzeau montre qu'un dispositif conversationnel est susceptible de produire des connaissances (une mémoire), de la même manière que le GI génère une production littéraire (un imaginaire).

Dans ces réseaux d'échange, ces «\ réseaux d'intelligence\ », qu'ils soient éphémères ou au long cours, l'enjeu n'est pas tant de stabiliser les idées ou de fixer les textes, mais de les faire circuler, de constamment les réitérer, à travers des pratiques d'écritures qui sont peut-être avant tout des gestes d'édition et de publication, autrement dit des éditorialisations.

En implémentant l'écriture comme mouvement dynamique, les pratiques conversationnelles sont une piste à explorer pour renouveler les processus de fabrication du sens que sont la mémoire, l'imaginaire, et l'interprétation, qui d'après Derrida vont toujours ensemble.

Tout cela nous incite à penser que la valeur de cette littérature n'est plus à rechercher entièrement dans sa littérarité, mais dans ce qu'elle explore en terme d'espace public, de collectif, ...de commun aussi.

C'est en quelque sorte ce que nous poursuivions en mai dernier lors de la Publishing SPhere, dans notre groupe sur l'institution fictionnelle, mais sans jamais vraiment l'attraper.

Car ce que le GI nous apprend c'est que le processus d'institutionnalisation est ailleurs, et surtout **est autre**. Il ne repose plus sur les structures traditionnelles de l'institution, il se fait par l'écriture certes, mais dans une perpétuelle réécriture des structures institutionnelles, autrement dit dans un processus continue, dont la finalité n'est plus l'institution, mais le _faire collectif_..

Instin performe ainsi un autre mode d'existence de l'institution. Cette proposition institutionnelle alternative était effectivement le point de départ de chacun des participants au groupe.

### Intersections


[ICI GIT la performance de Patrick Chatelier déplacée ci-dessus]{.note}

Or, on retrouve cette même idée du collectif dans l'analyse que Claire dehove fait de son projet l'ambassade des communs.

> Les œuvres collectives, collaboratives ou plurielles sont entourées d’un flou juridique mais aussi conceptuel.
>
> Cet impensé a posé un sérieux problème aux Nouveaux commanditaires eux-mêmes et à la Fondation de France par définition. Qui est auteur de quoi\ ? Les idées circulent et sont reprises en permanence. Les promotions d’étudiants se succèdent, des intervenants extérieurs sont invités et mobilisés au plan théorique.
>
> J’estime que l’Ambassade des communs est une œuvre globale dont le corps est une co-existence et se co-définit à long terme.^ [_Ambassade des communs_ - entretien avec Claire Dehove https://via.hypothes.is/https://stylo.ecrituresnumeriques.ca/api/v1/htmlArticle/5c76998ebe99eb0011878b36?preview=true#co-autorat]

On retrouve également ce paradigme dans la constitution des archives de ces institutions fictionnelles\ :

> À chaque projet, à chaque institution fictive correspond son corpus d’anarchives. Pourquoi anarchives\ ? Parce que ce sont des archives qui sont constituées de manière anarchique et dans le temps, et par toute personne qui veut y contribuer, sur n’importe quel support et sous n’importe quelle forme.

L'institution fictive telle que l'Ambassade des communs construit une vision particulière de l'institution.

> L’ambassade, en tant qu’œuvre, peut mourir d’elle-même, comme elle peut aussi être revivifiée en étant portée par exemple par une association étudiante. Tout peut arriver. En tout cas, cela appartient à ceux qui en sont les usagers.^[_Ambassade des communs_ - entretien avec Claire Dehove]

Enfin, du côté du PEROU et de la manière dont Sebastien Thiery décrit ses actions dans la jungle de Calais ou ailleurs, il y a cette posture plus activiste consistant à faire de la création institutionnelle.

![ps-republierlemonde](img/ps-republierlemonde.png)

> **Création institutionnelle** Une commune, 36000 fois institué Pourquoi ne reprenons pas les chemins de ces créations\ ? Reinstaurer l’institution pour les déborder, interroger les modes d’existences
>
> Les forces d’agissement de ces institutions sont leur puissance poétique. Opposer des poésies à d’autres poésies.
>
> **Fictions institutionnelles**
>
> Qu’est-ce qui fait tenir un état, une commune. Reconnaître que ce sont des matériaux poétiques et fictionnels. Il faut le prendre en compte, et alors ca nous permet de travailler autrement, en relation avec tous ces êtres, pour les reconsidérer.
>
> Le PEROU répond à des écritures assassines qui gouvernent. combattre un texte avec d’autres textes.^[Notes d’atelier Sébastien Thiéry - Republier le monde http://notes.ecrituresnumeriques.ca/3D2XP5bjRkuREiZlYW8vkQ.html]

# Tweet d'Emin Youssef - Ecridil

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Maquettes pour le booksprint à venir<br><br> <a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a><a href="https://twitter.com/hashtag/designthinking?src=hash&amp;ref_src=twsrc%5Etfw">#designthinking</a> 🧠<a href="https://twitter.com/hashtag/booksprint?src=hash&amp;ref_src=twsrc%5Etfw">#booksprint</a> 🏁📜📖💻 <a href="https://t.co/oNbnp9dBHd">pic.twitter.com/oNbnp9dBHd</a></p>&mdash; Emin Youssef (@EminYou) <a href="https://twitter.com/EminYou/status/991420709216882688?ref_src=twsrc%5Etfw">May 1, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

# La Charte de l'Ambassade de la MétaNation

:::focus

ici^[[à utiliser plus tard!]{.note}]\
là et \
ailleurs\
la MétaNation \
s’auto-institue elle  \
se définit collectivement\
au cours de son existence\
de ses implantations et de ses activations\
elle échappe à tout statut surtout administratif\
la MétaNation appelle à la co-citoyenneté nomade\
elle cultive la fiction dans la réalité et en extrapole le devenir utopique \
la MétaNation appelle à un mouvement de métamorphose en toute discrétion\
elle formule l’hypothèse que les démarcations sont définitivement non pertinentes\
elle dissémine des valeurs humaines animales végétales et élémentaires\
l’emblème de ses manœuvres est le Renard au doux regard rusé\
elle implante ses ambassades un peu partout sur ou \
sans invitation à l’instar des pirates des mers\
le Document MétaNation obtenu par rituel\
d’intégration donne droit à être\
fier.ère de le détenir\
droit indéfini\
perpétuel\
furtif

:::
[La Charte de l'Ambassade de la MétaNation [@noauthor_ambassade_2019, p.5]]{.legendeFocus}

# Quelques notes en fin de partie 3

:::note

Le collectif qu'elle tente de cerner dans ses écrits émerge directement de l'action, essentiellement celle de la pratique et de la présence individuelle en ligne.

[aller chercher des citations]{.note}

Le profil en ligne constitue en effet pour LM une pratique d'écriture de soi [référence]{.ref} particulière. Ces écritures dites «\ profilaires\ », [définition]{.note} composent ce qu'elle va appeler une «\ rhétorique dispositive\ » [-@merzeau_profil_2016]. Les effets de cette rhétorique est une production collective. [développer à partir du texte]{.note}

J'ai pour ma part introduit l'idée d'«\ écriture dispositive\ » [@sauret_autorite_2016] pour requalifier ces actions dispositives, en considérant que l'espace numérique s'agence entièrement par l'écriture, que celle-ci soit machinique ou humaine, binaire ou symbolique.

On l'a vu avec l'analyse du dispositif ENMI12, ces écritures sont de différentes natures\ : discours, collections, réécritures, catégorisation. Toutes pourtant apposent à l'espace numérique leurs «\ traces\ », témoignant autant d'une _inscription_ que d'une action. L'écriture est ici performative car elle agit autant sur le plan scriptural que sur un plan spatial en participant de l'agencement de l'espace numérique. Ainsi, penser l'espace numérique avec Merzeau et @zacklad_organisation_2012 comme un  «\ environnement-support\ », permet de mieux comprendre la nature environnementale du numérique, et ce lien étroit entre espace et écriture. En jouant de la même adjectivisation que pour l'_action_ ou l'_écriture_ (*dispositives*), ce que j'appelle pour ma part l'_environnement dispositif_ se voit agit autant qu'il agit, il écrit autant qu'il est écrit.

[«\ écriture-milieu\ » --> «\ rhétorique du nous\ »]{.note}

:::

# Sur l'évaluation et sa fausse collégialité

:::note

sur l'évaluation... trop léger et rien à faire là

[0] La légitimation scientifique des connaissances produites s'appuie sur l'évaluation par les pairs, adoptant comme principe directeur de scientificité la vérification de nouvelles affirmations. Avant l'étape d'évaluation, l'écrit scientifique construit sa légitimité par une structure argumentative, une mise en évidence de la méthode scientifique utilisée, des concepts théoriques mobilisés, documentés par des citations et un appareil critique permettant aux pairs et futurs lecteurs de reproduire le cheminement de la pensée. Les évaluateurs, ainsi que les correcteurs et relecteurs engagés dans une édition scientifique, sont engagés dans une forme de collégialité, dans la mesure où des avis sont requis, exprimés et éventuellements suivis par l'auteur. Cependant cette forme de collégialité est marquée par des biais, adoptés consciemment par la communauté académique car considérés comme garant d'une certaine scientificité. C'est le cas par exemple du double ou simple aveugle consistant pour l'auteur et/ou l'évaluateur à ne pas connaître l'identité de celui ou celle qui évalue ou écrit. Cette pratique est initialement introduite pour supprimer le biais de jugement lié à l'individu, afin de n'évaluer que la connaissance, le résultat ou l'écrit. Pour des savoirs strictement objectifs de type sciences naturelles, ou sciences de physique, on comprend aisément que l'intérêt de supprimer un tel biais. Mais pour un savoir strictement subjectif, relevant de la pensée discursive, il faut se demander si l'introduction de l'anonymat n'équivaut pas à implanter un autre biais, plus problématique que le premier.

:::

# Merzeau par Maurel

Louise Merzeau[^louisemaurel],

[^louisemaurel]: [Louise Merzeau qui était si douée pour "régler" un texte et l'amener exactement au juste positionnement, en arrivant à susciter le consensus par la précision des mots]{.cite}

#

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Alors oui, il y a beaucoup à commenter sur ce texte, avec lequel je me trouve en profond désaccord... <a href="https://t.co/aQvM0b9MeN">https://t.co/aQvM0b9MeN</a></p>&mdash; S.I.Lex (@Calimaq) <a href="https://twitter.com/Calimaq/status/1101767005852573702?ref_src=twsrc%5Etfw">March 2, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Mais bon, on a l&#39;outil hypothesis pour faire des commentaires structurés. N&#39;allons pas les éparpiller sur Twitter uniquement et prêtons-nous au jeu proposé par Sens Public.</p>&mdash; S.I.Lex (@Calimaq) <a href="https://twitter.com/Calimaq/status/1101795858666979330?ref_src=twsrc%5Etfw">March 2, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## Le contrat d'écriture

Voir la version de l'intro de la partie 3 introduisant l'idée de contrat d'écriture (voir Jahjah) qui permet de définir ou exprimer ce que pourrait être la nouvelle fonction éditoriale. Potentiellement utile pour conclure cette partie.

> Sur le plan éditorial, nous pouvons envisager cette gouvernance comme l'émergence d'un contrat d'écriture [cf le texte de jahjah sur la conversation]{.note}, au même titre que le contrat de lecture qui unissait les éditeurs et les lecteurs de la presse. Il y a là un nouvel enjeu pour la fonction éditoriale, profondément modifiée dans l'environnement numérique. Ma vision est d'imaginer ce contrat d'écriture comme une nouvelle fonction éditoriale, celle de créer les conditions de possibilités de l'appropriation, c'est-à-dire de l'écriture ou de la réécriture. Ces conditions s'implémentent dans des milieux d'écriture, écosystème vertueux dans lesquels s'établit un mouvement, un processus d'éditorialisation de fragments, par lequel le collectif se réalise. C'est de cette mise en conversation des fragments et des individus qui est à la base du modèle éditorial conversationnel discuté. Mon hypothèse est que des instances éditoriales telles que les revues de SHS pourraient s'en emparer.

## Le constat que le groupe IF ne fonctionne pas

Pour ma part, rompu à différentes expériences de collaboration, sur des projets très court terme, comme des hackathon de quelques jours, ou plus long terme, Je me suis retrouvé dans un groupe incapable de se parler, malgré les efforts des uns et des autres. Un peu desépéré, je rédigeais à la fin de la seconde journée une note intitulée «\ Qu'est ce qui s'est passé ces deux derniers jours\ » :

> Au regard de positions si proches et lointaines, une conversation préalable semblait nécessaire avant d’envisager une forme d’action, d’écriture et de publication.
>
> [...]
>
> Cette seconde phase de la conversation [annnotation mutuelle] ne s’est passée qu’à la marge, au profit d’un débat en présence.
>
> Au fil des dissensus, nous avons tenté d’identifier des protocoles d’action susceptibles d’être mise en œuvre dans le temps de l’évenement.\
> -- Notes du 25 mai - Qu'est ce qui s'est passé ces deux derniers jours http://notes.ecrituresnumeriques.ca/bJ1f1lNVQn-YinY6x-3GrQ.html

##  partie1: intro obsolète à la partie "institution et chaine de l'écrit"

Dans un tel contexte, la question qui est posée est celle d'un renouvellement des modalités d'élaboration du savoir au sein des institutions de savoir, de manière à envisager de nouvelles formes de communication scientifique, mais aussi la prise en compte dans ces formes de savoirs pluriels, non académiques. Nous allons dans un premier temps développer notre vision de l'_ethos_ des institutions de savoir dans leur rapport à l'écriture.

Puis, à partir de nos études de terrains, nous montrerons [voir Partie 3]{.note} en quoi la communauté scientifique gagnerait à s'inspirer des pratiques d'écriture et d'édition que l'on a pu observer dans d'autres communautés d'écriture comme celle des communs ou celle qui s'est constituée autour de la figure du Général Instin. Nous verrons que se dessine au sein de ces communautés un paradigme alternatif pour la production de connaissance, pour lequel ce qui se joue à travers l'écriture est d'abord la production d'un collectif, et non la production de documents. Notre enjeu sera alors de réconcilier dans un modèle conversationnel de communication scientifique une production à la fois sociale (_faire collectif_) et éditoriale (des ressources).

## Citations de Guichard sur l'institution

> Prétendre que ce nouvel ensemble de compétences n’est que «\ technique\ » revient à refuser que la mise en réseau de milliers de travaux de recherche touche au métier, au savoir-faire, à la compétence des chercheurs. Ne pas en avoir conscience consiste à refuser de s’interroger sur la culture implicite du chercheur patenté, faite de «\ petits riens\ », de «\ trucs de cuisine\ », d’intuition, mais aussi de culture érudite et de réseaux sociaux. Tout comme la science du milieu du XXe siècle, et la manière de la faire, n’ont plus de rapport avec le joli catalogue qui décore la grande salle de la Sorbonne, la pratique scientifique d’aujourd’hui, avec les laboratoires, les méthodes, les axes de recherche contemporains, s’écartent de ce qui avait valeur de norme il y a 30 ans. À partir d’une transformation de l’outillage intellectuel du chercheur, on arrive logiquement à une transformation de ses pratiques intellectuelles 2. Et bien sûr, plus, tard, à une évolution de ses thèmes de recherche. [@guichard_linternet_2002, III.5.1]


> Notre trentaine de pionniers^[chercheurs au département de littérature à l'ENS] découvre l’internet assez vite, entre 1991 et 1997. Très vite, pour eux, cet ensemble de protocoles est plus qu’un système de communication ou qu’un objet de consommation. C’est un instrument de travail, qui nécessite un savoir-faire, et à ce titre, ils ressentent le besoin d’acquérir une solide culture informatique, ou, à défaut, de s’entourer d’informaticiens.
> [...]
> C’est là qu’ils prennent le plus de risques: l’apprentissage de l’informatique leur coûte du temps, tout comme le travail de recherche sur les réseaux (web, listes de discussion, etc.) et celui de publication. Ce qui réduit leur participation à l’économie universitaire traditionnelle, dont ils ne respectent plus les règles, en négligeant d’accroître leur capital d’articles imprimés et en proposant d’autres modèles. Leur engagement leur coûte aussi de l’argent: achats d’ordinateurs, de modems, frais téléphoniques, etc. sont souvent engagés sur leur budget personnel. En effet, le plus souvent, leurs institutions ne les suivent pas, quand elles n’exercent pas de farouches résistances qui peuvent menacer la carrière des innovateurs.
> [...]
>  Alors que ces derniers développaient une réflexion sur le fonctionnement de l’enseignement et de la recherche, renouvellaient les thèmes et les méthodes de leurs disciplines suite à leur compréhension des potentialités de l’informatique et de l’internet, se lançaient dans des activités entrepreneuriales —tant pour institutionnaliser leurs pratiques que pour les financer, dans un cadre juridique et économique ad hoc—, et découvraient l’importance des défis pédagogiques à relever —élargissement de la base des «\ apprenants\ » et irruption des entreprises dans le marché éducatif—, leurs collègues en étaient à se familiariser avec le courrier électronique 8.
>
> En fait, les innovateurs restent condamnés à la solitude, principalement parce qu’ils se distinguent de leurs pairs et supérieurs sur un point: ils ont choisi d’adopter une attitude réflexive par rapport à leur outillage mental.
[@guichard_linternet_2002, III.5.3]


> Pour autant, l’usage optimal de l’écriture ne va pas de soi. La maîtrise —et le développement— de l’outillage mental nécessite un long apprentissage, dont on sait qu’il peut prendre toute une vie. Cet apprentissage ne se fait pas en solitaire: il se construit dans un environnement collectif, propre à l’individu, que nous appelons laboratoire; mais il crée aussi des effets de sédimentation plus larges. En effet la transcription des savoirs, leur matérialité, comme leurs effets politiques —grâce aux avantages obtenus suite à une meilleure compréhension du monde— puis économiques, se réalisent dans une temporalité longue, qui est celle de la constitution des structures sociales. Ces effets peuvent aussi avoir des pendants pervers: valorisation d’une écriture archaïsante qui perd tout lien avec la langue parlée, constitution de castes attachées à leurs privilèges au point qu’elles en oublient leur mission de transmission de la connaissance, mise en place de monopoles dans la chaîne de production de l’écrit, etc. [@guichard_linternet_2002, Introduction]

## Citation complète de Bourdieu

> Dans la lutte qui les oppose, les dominants et les prétendants, c'est-à-dire les nouveaux entrants, comme disent les économistes, recourent à des stratégies antagonistes, profondément opposées dans leur logique et dans leur principe\ : les intérêts (au double sens) qui les animent et les moyens qu'ils peuvent mettre en œuvre pour les satisfaire dépendent en effet très étroitement de leur position dans le champ, c'est-à-dire de leur capital scientifique et du pouvoir qu'il leur donne sur le champ de production et de circulation scientifique et sur les profits qu'il produit. **Les dominants sont voués à des stratégies de conservation visant à assurer la perpétuation de l'ordre scientifique établi** avec lequel ils ont partie liée. Cet ordre ne se réduit pas, comme on le croit communément, à **la science officielle, ensemble de ressources scientifiques héritées du passé qui existent à _l'état objectivé_, sous forme d'instruments, d'ouvrages, d'institutions, etc., et à _l'état incorporé_, sous forme d'habitus scientifiques, systèmes de schemes générateurs de perception, d'appréciation et d'action**
> qui sont le produit d'une forme spécifique d'action pédagogique et qui rendent possible le choix des objets, la solution des problèmes et l'évaluation des solutions. Il englobe aussi **l'ensemble des institutions chargées d'assurer la production et la circulation des biens scientifiques en même temps que la reproduction et la circulation des producteurs (ou des reproducteurs) et des consommateurs de ces biens**, c'est-à-dire au premier chef le système d'enseignement, seul capable d'assurer à la science officielle la permanence et la consécration en l'inculquant systématiquement (**habitus scientifiques**) à l'ensemble des destinataires légitimes de l'action pédagogique et, en particulier, à tous les nouveaux entrants dans le champ de production proprement dit. Outre **les instances spécifiquement chargées de la consécration (académies, prix, etc.),** il comprend aussi **les instruments de diffusion, et en particulier les revues scientifiques qui, par la sélection qu'elles opèrent en fonction des critères dominants, consacrent les productions conformes aux principes de la science officielle, offrant ainsi continûment l'exemple de ce qui mérite le nom de science, et exercent une censure de fait sur les productions hérétiques soit en les rejetant expressément, soit en décourageant purement l'intention de publication par la définition du publiable qu'elles proposent** (20).

## Continuité, episteme, gripper l'innovation

C'est en prenant en compte cette histoire longue de l'écriture et de l'édition que l'on peut comprendre la continuité des pratiques éditoriales perpétuant les ancrages épistémologiques [@chartier_crise_2014] dans cette période mouvementée qu'est la transition à une _épistémé_ numérique [@gras_les_2016]. L'attachement à l'évaluation par les pairs en est un exemple parmi d'autres. [voir les exemples de chartier, régime de la "preuve" qui se transforme]{.note} [+ citation d'entretien possible]{.note} Mais une telle continuité vient aussi gripper les possibilités et les opportunités qu’offre l’environnement numérique, pour fluidifier et enrichir la conversation scientifique, ou encore pour imaginer de nouveaux modèles épistémologiques.

## Note sur le document pour l'introduction (Pédauque)

où je prends le contre-pied de cette approche Pedauque qui consiste à regarder le document. Je prends au contraire une approche médiatique. Certes le document a une «fonction communicationnelle», mais c'est surtout le média qui a cette fonction, s'appuyant par ailleurs sur des documents.

::: note
Voir aussi les 3 "couches" du documents, donc la 3eme\ : «\ Enfin, la troisième couche, c’est la fonction communicationnelle du document\ : la trace sociale instituée par lui.» [@broudoux_evelyne_2018]. Repris de @pedauque_document_2003, «\ le document comme médium\ ».
:::

## Dialectique forme et pratique/usage + Open Edition

# La dialectique entre formes et pratiques

[à replacer ailleurs]{.note}

::: note
Pour le moment dans cette partie, je ne fais qu'amener deux cas concrets montrant la relation entre les pratiques d'une communauté et l'institution, soit à travers l'institutionnalisation d'une pratique (qui se traduit par une formalisation de la pratique), soit à travers l'adoption d'une norme/forme soumise par l'institution à la communauté.
:::

Ce qui se joue au 17^e^ siècle avec la normalisation d'un format de communication, c'est l'inscription de règles et de pratiques\ -- d'écriture et d'édition, qui sont celles d'une communauté. Au fil de l'éclatement des sciences et leur cloisonnement dans des disciplines de plus en plus constituées, la communauté scientifique va également se scinder et chaque sous-communauté disciplinaire va progressivement adopter des normes et des pratiques spécifiques.

Existant au travers de leurs pratiques, les communautés s'instituent au travers des modèles éditoriaux qu'elles adoptent. Ce sont en effet les artefacts éditoriaux qui permettent d'amener certaines pratiques vers leur institutionnalisation, c'est-à-dire leur reconnaissance par les institutions légitimantes.

[citer le cas de la TEI, et comment un format est venu structurer l'epistémologie des communautés qui l'utilisent (voir l'étude du chercheur qu'a rencontré Marcello à Utrecht-DH2019)]{.note}

[citer le cas inverse, où l'institution vient prescrire des pratiques, avec l'interview de Stephane sur l'émergence d'OpenEdition dans au début des années 2000 et comment OE a promu la non structuration des données, au profit d'interfaces de lecture]{.note}

<!-- Paragraphe repris de Q1_v2 - ### Formats et usages -->
Il y a là une tension particulière entre format et usage. Les actions de lecture, d'écriture, d'association tendent avec le temps à se formaliser par l'usage (et devenir des _pratiques_), mais nécessitent pour subsister une liberté d'évolution. La formalisation par l'usage ne relève pas de la même contrainte que la formalisation par un format. Par exemple, la grammatisation des langues vernaculaires [@auroux_revolution_1994] n'empêche ni des pratiques particulières ni leurs évolutions naturelles (on parle bien de langues _vivantes_). La grammatisation reste un processus ouvert, accompagnant les pratiques au fil de la mutation du langage. La relation qu'entretiennent usage et format, ou langue et grammaire, relèvent finalement de celle qui conjuge l'homme et la technique [@leroi-gourhan_geste_1964; @simondon_gilbert_1994]. L'indissociabilité de ces couples ne peut être pensée en termes d'opposition, mais de coévolution dont la tension vertueuse fournit à l'un et à l'autre une dynamique vitale.


### [cas de la plateforme OpenEdition]
Cette dialectique n'est cependant pas toujours aussi équilibrée ou bilatérale. Les plateformes peuvent ainsi profondément structurer les pratiques.
Dans un entretien^[Voir annexe [Entretien avec Stéphane Pouyllau](../index.html)] mené en octobre 2018 pour ce mémoire, Stéphane Pouyllau raconte comment Open Edition, la plateforme de publication scientifique, a pris lors sa création une direction radicale, à contre-courant d'un certain paysage informatique à la fin des années 90. Dans les milieux autorisés de l'informatique scientifique française, la tendance est à la base de données documentaires et au développement de moteurs de recherche performants, basés sur une indexation fine des données. [développer avec l'entretien]{.note}
 Marin Dacos, le fondateur d'Open Edition, vient d'un autre groupe de chercheurs, utilisateurs du CMS SPIP [lequel, cf. entretien]{.note}. Il souhaite mettre en place une plateforme de diffusion des revues scientifiques en SHS. Mais au lieu de considérer les articles comme de la donnée, il va d'abord s'intéresser à la mise en forme et à l'affichage des articles scientifiques, en considérant seule une expérience de lecture proche de celle de l'édition papier amènera les éditeurs de revue à passer à l'édition et à la diffusion numérique. Le choix technique pour implémenter la vision de Marin Dacos est celui d'un CMS (Content Management System, ou système de gestion de contenus), SPIP, qui a le mérite de modéliser un modèle éditorial et de le réaliser à l'écran. Pour les spécialistes de la recherche d'information à l'époque, ce choix est une abberation tant il néglige l'indexation des données, essentielle à la survie d'un contenu dans l'océan en expansion du web.

> On n'a pas vu venir google, qui a complètement remis en cause les principes de la recherche d'information, basée à l'époque sur une donnée très structurée. OpenEdition au contraire a fait le choix du CMS, au détriment de la donnée structurée, pour favoriser la présentation de l'article dans un affichage proche de ce que les revues produisaient en papier. [à réécrire avec la transcription de l'entretien]{.note}

Une telle orientation, qui peut sembler avant tout technique, a eu en fait des répercutions épistémologiques très concrètes. Elle a en effet perpétué une séparation des métiers entre l'éditeur et le diffuseur, là où l'environnement numérique portait la promesse d'une nouvelle répartition des fonctions de l'un et l'autre, comme le reconnaitra Marin Dacos, lorsqu'il publie avec Pierre Mounier, «\ L'édition électronique\ » [@dacos_edition_2010].

Fidèle à sa vision d'une plateforme au service des éditeurs avant de penser les usages potentiels d'une édition numérique, la plateforme Revues.org a confirmé l'éditeur de revue dans sa pratique éditoriale classique, se concentrant sur le texte et sa présentation, sans investir dans la production des données, pourtant essentielle dans l'écosystème numérique. C'est ce que nous a montré la série d'entretiens menés auprès des direct·rice·eur·s et secrétaires de rédaction de revues, dans le cadre du projet Revue 2.0.

> [citation ]{.note}

Installé dans cette distribution des rôles et des fonctions, le diffuseur, au contraire, se spécialise dans la production et la structuration des données, nivellant au passage les données [à développer avec la réflexion sur l'interopérabilité]{.note}.

[rappeler qu'openedition était parti sur une solution sans données, version SEO de base, avant de comprendre qu'il fallait structurer les données, et est allé revoir ses amis "data" négligés à l'époque.]{.note}

::: note

développer sur la structuration du paysage de la communication scientifique, ce qui revient à une des problématiques actuelles de revue 2.0, à savoir une pratique éditoriale qui est restée proche du texte et de sa présentation, et qui n'a pas investi dans l'enrichissement des données, pourtant essentielle dans l'écosystème du web. La séparation des métiers considère ainsi que c'est au diffuseur de gérer l'existence, ou la subsistance, des articles sur le web, là où l'éditeur devrait conserver une certaine maîtrise des données. Question de légitimation.

:::

## Matérialité performative de Drucker

besoin de dépasser le format, comme porte d'entrée du conversationnel. Pour cela, aller voir notamment l'analyse de Drucker sur le performative materiality où elle identifie la matérialité littérale (voir sa référence) qui se compose du _forensic_ et du _formal_. Ce dernier rejoint la problématique du format\ : encoded and conventionnalized (voir https://hyp.is/7jJ-rmFaEeq-cG8PgpHw9Q/www.digitalhumanities.org/dhq/vol/7/1/000143/000143.html)

> In Kirschenbaum’s definitions, forensic materiality refers to evidence, while formal materiality refers to the codes and structures of human expression. The forensic elements of a document might include ink, paper, stains, fingerprints, other physical traces, while the formal elements would be the organization of the layout, design, or the style of literary composition, relations between image and text and so on. (https://hyp.is/kv6IpmFbEeq9HL8dP7dVZw/www.digitalhumanities.org/dhq/vol/7/1/000143/000143.html)


## Conclusion - git-thèse


J'ai tenté dans la réalisation de cette thèse de

Dans le troisième chapitre, j'ai mentionné à différentes reprises le protocole _git_ sur lequel se sont appuyés certains des projets et expérimentations présentés. Initialement conçu pour la programmation informatique dont il facilite la gestion de fichiers et le suivi de versions, _git_ se révèle également efficace et adapté à une fabrique éditoriale raisonnée. Il est particulièrement pertinent lorsque le projet éditorial émane d'une écriture collective, traversée par des enjeux de collaboration, d'auctorialité multiple, et finalement d'autorité et de légitimation. C'est dans cet esprit que l'équipe de la CRC-EN explore depuis plusieurs années les possibilités éditoriales de _git_ dans le contexte de l'édition scientifique. Une chaîne de production éditoriale a été imaginée, initialement pour la revue Sens public, et ne cesse de s'améliorer et de se développer au gré des projets et de leurs modalités de réalisation. Pour la rédaction et l'édition de la présente thèse, j'ai repris les deux principaux composants de la chaîne éditoriale, et les ai adaptés à l'exercice bien particulier de la thèse.

L'environnement de travail associe ainsi l'éditeur de texte ou de code _Atom.io_, outillé de différents _packages_ utiles à la rédaction ; le convertisseur de document _Pandoc_, à la base du

Nous élaborons et améliorons ainsi depuis plusieurs années une chaîne de production éditoriale basée sur _git_ pour la gestion de fichiers, et sur l'outil _Pandoc_ pour la production documentaire.


Associé aux plateformes _Gitlab_ ou _Github_ qui l'exploitent et l'augmentent de services et de fonctionnalités sociales, _git_ se révèle un outil extrêmement pertinent pour l'écriture collective, particulièrement lorsque celle-ci est traversée par des enjeux d'autorité et de légitimation. La présente thèse a été entièrement rédigée

Au sein de la CRC-EN et de la fabrique de la revue _Sens public_, nous élaborons et améliorons depuis plusieurs années une chaîne de production éditoriale basée sur le protocole _git_ pour la gestion de fichiers et l'outil _Pandoc_ pour la production documentaire.
sur les trois formats de fichiers _markdown_, _bibtex_ et _yaml_, destinés à respectivement à complémentaires destinés à

 l'association d'un format d'écriture léger, le markdown,

j'ai pu explorer les possibilités du protocole en contexte de production éditoriale



De manière assez classique, le processus de rédaction, dernier dénivelé non moins sinueux de l'ascension, a pu jouer le rôle de révélateur. Mais la course ne serait pas certainement pas arrivée si loin sans la conversation comme catalyseur
