
# Soutenance

question posées par marcello sur la conclusion :

> et la fin de la conclusion est super - avec l'idée de ne pas cristalliser
> peut-être tu devrais dire un mot sur la place de l'institution, pour conclure
> que se passe-t-il si les expérimentations ne sont pas vouées à devenir des modèles stables?
> je veux dire: tu parles d'une nouvelle institutionalisation. mais elle devra probablement prendre en compte de façon plus marquée la pluralité, non?


## Définition de l'écriture nmérique ?

je pars de la mienne, je pars de mon expérience, à la fois comme simple citoyen et consommacteur culturel, comme twitterien, mais aussi comme auteur d'articles scientifiques, et praticiens de l'édition scientifique.
Je parle de ces écritures qui structurent mes actions et mes pensées, je parle de ce que j'inscris dans le web, écritures expertes et profanes, pratiques expertes ou non. Expertise du bricolage, cet apprentissage jamais enseigné.

## Joel

### densification du milieu numérique

les effets du numérique sont clairement violent.

Les formes de socialisation, de politique hors numérique sont évidemment cruciaux, et resteront une source d'inspiration et de resistance.

je crois que ce sont ceux là même qui génèrent un récit pour imaginer de novueaux territoires numériques.

Il y a clairement un spleen du côté des héritiers de JP Barlow depuis que les fondements du cyberespaces se sont vus colonisés un à un.

Mais ces fondements sont toujours là d'une part, et de nombreux signaux persistent ou émergent.

Ils existent également dans le numérique, et savent notamment particulièrement bien jouer de ces écritures. GI.


## Bouchardon

### question de l'écriture numérique :

J'ai bien saisi et pris en considération la notion de l'écriture numérique telle qu'elle est notamment proposée par vous même avec Victor Petit.

pour autant j'élargis ma conceptualisation de l'écriture numérique à une dimension culturelle pour la considérer comme un mouvement, et pour considérer ses effets performatifs sur le réel, qu'il soit numérique ou non. C'est ce que j'ai fait avec la notion d'écriture dispositive susceptible à la fois dh'abiter un milieu, et à la fois de le structurer, de le faire évoluer. C'est à la fois une écriture dans l'espace et une écriture de l'espace.
Mais alors quelle frontière ? ce sont peut être les frontières politiques à nouveau, car plutôt que de décrire l'écriture numérique, la thèse tente de plaider pour une certaine écriture numérique, une écriture en mouvement,

pour autant, cet élargissement ne s'évapore pas, l'écriture reste directement liée à ses matérialités


### Conversation : il existe d'autres dispositifs

en effet, il existe plusieurs initiatives éditoriales qui mettent en oeuvre certains des preceptes que j'ai pu énoncé. on peut citer la plateforme F1000 qui pratique une évaluation par les pairs ouverte, on peut également penser au principe des épijournaux dans le jiron de l'épiscience.
j'aurais sans doute dû mentionner ces initiatives, et peut-être meme les analyser, mais mon sujet était ailleurs, dans le croisement avec des formes et des pratiques extérieures: communs, general instin.
il s'agissait également de réaliser des choses, d'expérimenter avec d'autres, expliquant aussi la nécessité parfois d'accueillir de nouvelles idées plutôt que de survenir avec les siennes.
implémentation par l'expérience, et on l'a vu ces expériences ont impliqué d'autres communautés que la stricte communauté scientifique.

je crois que cette ouverture a aussi été une ouverture théorique me permettant de conceptualiser la conversation autrement : « comme une forme particulière d’éditorialisation de fragments et de ressources, par laquelle se joue une dynamique d'appropriation.


### Collectif : essentialisation ?

il faut penser le collectif non pas comme une entité, mais comme un processus. c'est le résultat d'une dynamique, dont une bonne partie se réalise dans des écritures

il faut donc penser un «faire collectif» qui institue le processus
et à partir de cet infinitif, je trouve très opérant le fait de le conjuguer à la première personne du pluriel «nous faisons collectif». C'est ainsi que nous pouvons en saisir les multiples subjectivités,
aussi pour considérer son actualité, pour témoigner du nous qui en émerge constamment, pour sortir de l'injonction et entrer dans sa performativité déjà à l'oeuvre.

Le collectif qui confronte l'autre en disant "en es tu" passe à côté de sa finalité, mais s'il est capable d'énoncer : «ce que nous pourrons faire si nous nous nouons». Dans ce «nous», s'établit le sujet collectif



Concrètement, ce nous a besoin d'une conscience collective, qui se réalise dans un sentiment d'appartenance, à un temps donné, à un lieu donné. Merzeau a bien montré l'importances des boucles de réflexivité du collectif sur lui-même.

moyen de partager une finalité commune.

### Fonction éditoriale :

garantir la vitalité d'un nous collectif
fonction éditoriale : redistribuer l'autorité et la légitimité au collectif

virtualisation de la revue : coincide avec la virtualisation du collectif (toujours en puissance), lorsque le protocole est lui même mis en conversation (ouverture de la gouvernance)

## Marta

### Conversation ou dialectique

oui l'acceptation commune du terme conversation peut en effet apparaitre trop légère au regard des débats et des controverses

dans ma conception de la conversation, j'intègre évidemment ces pratiques de débat et de controverse.

mais une question reste à résoudre : le monde universitaire a beaucoup à faire pour s'ouvrir à des savoirs alternatifs, à d'autres communautés de savoirs. Et là, il me semble que la position agonistique qui suppose une joute, dans lequel s'exprime une érudition, irait à l'encontre de cette inclusion. Cela ne veut pas dire fermer le débat ou l'éviter, on voit bien en ce moment la difficulté qu'il y a parfois à rouvrir ou à refermer certains débats.

Le terme de conversation n'est qu'un terme. Ce qui se joue réellement ce sont les protocoles qui seront mis en oeuvre, ce sont les modalités des espaces ouverts, les dispositifs. Il n'y a pas un modèle de conversation, de la meme manière qu'il n'y a pas un terrain de recherche : chaque recherche se doit de se situer, dans un territoire, dans un collectif, dans un faisceaux d'inquiétudes que la science ne peut plus ne pas prendre en compte.

Ce que j'appelle conversation sera ce qui découlera des dispositifs ayant mis en oeuvre une pratique alternative de la recherche.

## question socio-technique

l'outil importe peu si ce n'est qu'il est lui aussi le fruit d'un faisceaux de valeurs. et certaines valeurs ne sont pas compatibles. Cela intègre bien l'outil dans un écosystème

Je pense le dispositif de la meme manière, il est un agencement socio-technique, il est partie prenante d'un écosystème dans lequel se joue effectivement des jeux de pouvoir, des autorités, ce principe ne change pas. Par contre il est possible de faire évoluer les modalités du principe.

## Question graphique

j'ai assez peu abordé cet aspect en effet dans la thèse, et je répondra alors par une pirouette, mais qui n'en est pas complètement une :
il s'agit de conception, forme, de design et d'esthétique. Or je crois ce qui serait prioritaire sur une esthétique graphique, serait une esthétique de la confiance, un design du partage et du don. Car ce sont sur ces bases, ces formes, que pourra émerger la bienveillance.
sir je devais encore plaider, je plaiderai pour une esthétique de la bienveillance.

Mais heureusement que d'autres que moi ont entrepris de travailler la question graphique
