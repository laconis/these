
- 22 mars 2018 : mail de Servanne Monjour pour impliquer des membres de l'équipe CRC-EN dans le projet "booksprint"
  La discussion implique René Audet et Jean-Louis Soubret.
- 23 mars 2018 : réponse de René Audet qui cherche à saisir déjà une forme. Il liste quelques idées :

  > - Actes ? (vs 2e publication augmentée dont parle Renée)
  > - État des lieux de la posture du design à l'égard du livre numérique ?
  > - Snippets conclusives basées sur les communications ?
  > - Recueil de meilleures pratiques édition+design en contexte numérique ?
  > - Ouvrage-nostalgie sur l'événement lui-même ?
  > - Objet composite combinant des éléments graphiques (StoryBoard), des présentations de corpus, une trace du programme lui-même... bref une version bonifiée du site web du colloque ?



## 28 avril 2018 - update chaine booksprint

Dans [ce courriel envoyé à SM](annexes/ecridil/update-chaine-booksprint_20180428.html), je réfléchis déjà à une chaine utilisant les pads collaboratifs comme source.

> à ton avis, ca va si on demande aux éditeurs d'écrire du markdown dans un framapad ? pas très confortable du coup.
>
> J'espérais pouvoir utiliser les styles de framapad pour rendre le pad plus lisible, mais en fait les url ou images créent du bordel (un markdown mal formé) quand ca exportent en md. Et un peu peur d'autres surprises.
>
> Une solution serait de trouver un autre éditeur en ligne qui fait vraiment du md et écriture collaborative, mais il faudrait pouvoir télécharger facilement le md avec wget.
