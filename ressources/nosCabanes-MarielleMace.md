## Lecture de Marielle Macé, «Nos cabanes»

> Une noue est un fossé herbeux en pente douce, aménagé ou naturel (l'ancien bras mort d'une rivière par exemple), qui revueille les eaux, permet d'en maîtriser le ruissellement ou l'évaporation, de reconstituer les nappes souterraines et de ménager les tarres. C'est un abri végétal qui limite la pollution, et s'est mis à protéger des inondations les billages qui y sont continûment exposés[...]. [p10-11]

> Il y a toute une science des noues, même s'il n'y a pas de code cartographique pour les identifier ; [p12-13]

rapprochement avec la mémoire collective :
> Les noues, les noës comme autant d'arches, arches d'eaux vives et de pratiques, où conserver non pas des choses mais des forces, où faire monter des inquiétudes, des pensées, des combats.
> Car les noues se souviennent de destructions et des exploitations, elles font accueil aux luttes, rouvrant ces lits de rivières anciennes « où les eaux tendent à revenir en cas de débordements ».
> Ce qui découle des noues en effet déborde. [p15-16]

Les noues comme tiers paysages (Tiers états - Gilles Clément) [p16-17]

_Nouons-nous_ (Emmanuelle Pagarto) [p18-19]
> Nouons-nous; cette formule emporte, entraîne, elle a la justesse du poème, infaillible. On y reçoit le «nous» comme une sorte d'appel : oui, faisons-le, nouons-nous! Le pronom y devient une modalité du verbe, que l'on conjugue de beaucoup de manières : nous-ons, accomplissons des «nous», nouons encore, imaginons d'autres façons d'être à plusieurs, de se lier, de se coucher, peut-être juste de se frôler... On y entend que dans le mot «nous» quelques chose (mais quoi au juste?) se noue, doit se nouer et pourra donc aussi bien se dénouer ; on se dit que «nous» est une affaire de liens, d'arrachements, de mêlements, d'interdépendances et d'attachements, et de démêlements et de dénouements -- plutôt que d'appartenance ou d'identification. On devine que penser et éprouver le «nous» amoureux n'est peut-être pas inutile à pensée du commun, autrement dit que le «nous deux» d'amour (le «nous deux encore» de Michaux) pourrait, si on l'écoute, s'élargir en collectif, s'infinir en politique. (On se dit d'ailleurs aussi qu'avec les Noues «nous» pourrait se décliner au féminin, pour noues les femmes.)[p20]

> Car «nous» ne désigne pas une addition de sujet («je» plus «je» plus «je»...) mais un sujet collectif, dilaté autour de moi qui parle : moi et _du_ non-moi, en partie indéfini, potentiellement illimité, moi et tout ce à quoi je peux ou veux bien me relier. Benveniste le disait, et c'était une surprise : «nous» n'est pas le pluriel de «je», un pluriel dénombrable découpé dans le plus grand ensemble de «tous». Non, ce n'est pas comme ça que le pronom se construit. «Nous» est le résultat d'un «je» qui s'est ouvert (ouvert à ce qu'il n'est pas), qui s'est dilaté, déposé au-dehors, _élargi_.[p20]

> Il ne s'agit pas avec «nous» de dire qui je suis, de ma déclarer; il ne s'agit même pas de dire comme qui je suis; mais ce que nous pourrons faire si nous nous nouons. [p21]

p22-23: attention à l'abus du «nous»
> un «nous» très noué qui se referme alors sur nous comme un enclos et que l'on connaît très bien aujourd'hui.

> Chercher les noues, les nous, les noeuds, les liens mais aussi les déliaisons qu'il faut ; comme Aragon dans le dernier vers de son dernier recueil. Où le pronom «nous» s'institue en lieu d'un dénouage, d'un dénouement, noed de liens qui libèrent, des lignes de vies qu'on laisse filer et laissent partir.

### Nos cabanes (p27-...)

les cabanes fragments (p28)

> Faire des cabanes en tous genres -- inventer, jardiner les possibles ; sans craindre d'appeler «cabanes» des huttes de phrases, de papier, de pensée, d'amitié, des nouvelles façons de se représentr l'espace, le temps, l'action, les liens, les pratiques. Faire des cabanes pour occuper autrement le terrin , c'est-à-dire toujours, aujourd'hui, pour se mettre à plusieurs.

p30-31 passage important sur de nouvelles écritures à reprendre.
> J'écris sous la dictée des plus jeunes -- sous la dictée de leur vie matérielle, par gratitude et admiration pour ce qu'ils tentent. Il faut voir en effet la vigueur, vigueur inquiète mais vigueur tout de même, avec laquelle certains collectifs affrontent la situation aujourd'hui faite en France à la jeunesse; des collectifs artisans, artistiques, politiques, qui s'emploient à imaginer à même leurs pratiques les formes d'une vie à venir : nouvelles écritures, inventions de liens et de formes de travail, politisation des affects, luttes, éco-diplomaties, remobilisations de la pensée... Avec eux l'avenir n'est pas exactement appelé sous la grande figure de l'utopie (justement non, pas du sans-lieu, du hors-sol) mais sous celle, à la fois joyeuse et sans paix, de l'impatience : une impatience à faire, imaginer, être ensemble, inventer des modalités de présence aux luttes de leur temps.

p40-42:
> (Ici s'égrène l'ample collier des verbes, plutôt que le chapelet des noms, des identitités ou des fonctions : la chaîne illimitée de ces infinitifs où s'anime l'intelligence même de la pratique. L'infinitif, ce mode de non personnel et non temporel, qui sert à nommer un procès, un phrasé général de l'action : tiens, on peut aussi faire ça, et ça, et encore ça : l'infinitif, forme syntaxique du possible, de la possibilisation des gestes et des choses, à chaque instant déclose.
> Et ce n'est pas seulement faire, mais faire à plusieurs : vivre à plusieurs, tenter des façons collectives; habiter à plusieurs [...], penser à plusieurs; et très souvent écrire à plusieurs.
> Dans beaucoup de ces groupes en effet on écrit (ou l'on traduit) des textes à plusieurs, des textes souvent somptueux, d'un grand soin, d'une grande verticalité de parole. On écrit à plusieurs pour constituer un «nous»; parfois en l'instituant d'emblée (c'est le comité invisible), parfois sans se hâter à prononcer ce «nous» nis s'y réchauffer trop vite, en explorat ses pentes, ses impatiences, en tentant plusieurs façons de se nouer et de se dénouer. Et l'on est très attentifs, dans chacun de ces collectifs, à rester anonymes (je ne sais pas faire ça, mais j'en mesure l'honneur); pas pour rester souverainement dans l'ombre ou cultiver le mystère mais pour affirmer à quel point cette vie-là reposera sur le fait de s'y mettre à quelques uns.
> Faire avec les autres, parier sur des «nous», se nouer parce qu'on s'est délié par ailleurs -- qu'on s'en est allé, pour déjouer les surveillances ou vivre plus loin («Fugitif, où cours-tu ?», Dénètem Touam Bona).
> Et encore partager, aimer; car la sensibilité et les affects, ici, sont essentiels. L'une des armes de ces luttes est l'amitiés, la joue et la force données par les amis et par l'amour pour les amis.

p48-49: lisière (diversité contaminée)
> [...] de prendre soin des idées de vie qui se phrasent, parfois de façon très ténue, comme autant de petites utopies quodidiennes  oui, on pourrait vivre aussi comme ça.
> Jardiner : il ne s'agit pourtant pas de réserver l'espérance politique à des lisières et des gestes de peu, et d'encourager une frugalité en toutes choses. «Jardiner» revient comme un mot lesté d'une novuelle audace, et le «jardin» excède ici tout précarré. C'es une pratique plus vaste, un grand appel d'air, une réoccupation de l'avenir, un éperon, une chance de se rapporter d'une novuelle amnière à l'existant, dans cette situation très mêlée, indémêlable, de «diversité contaminée» (Anna Tsing). Gilles clément nous a réappris ce que c'est que jardiner : c'est privilégier en tout le vivant, «faire» certes, mais faire moins (ou plutôt : faire le moins possible contre et le plus possible avec), diminuer les actions et pourtant accroitre la connaissance, refaire connaissances (avec le sol, avec ses peuples), faire place à la vie qui s'invente partout, jusque dans les délaissés... On peut agir comme on jardine : ça veut dire favoriser en tout la vie, parier sur ses inventions, croire aux métamorphoses, prendre soin du jardin planétaire; on peut bâtir comme on jardine (cela demande de mêler architecture pérenne et architecture provisoire, de ne pas tout vouloir «installer», de prendre des décisions collectives sur ce que l'on gardera, et ce dont au contraire on accete la disparition). Il ne s'agit pas de désirer peu, de se contenter de peu, mais au contraire d'imaginer davantage, de connaître davantage, de changer de registre d'abondances et d'élévations

annot: De la consommation à la multiplication des gênes, des espèces de la [..] --> multitude

p53:
> Cette co-construction, cette contamination, révèle des interdépendances, des imbrications constantes (entre espèces, entre des espèces et des techniques, entre des vivants et des objets, entre des vivants et des morts, entre des modes de vie, entre des soucis divergents, entre des valeurs disparates...) qui n'autorisent aucune simplications; il ne s'agit par exemple pas de croire que, dans les ruines et à leur faveur, «la nature» pourrait et aurait à «reprendre ses droits» après les saccages industriels et humains, mais de prendre acte de la surprise de ces agencements; sans célébrer ces nouages du pire et des échappées qu'il autorise; mais en cultivant ce qui, dans le désastre, ne relève pas du désastre, afin de préserver quelque chose d'un amour de la vie.
> Le «monde» ici est la terre rendue en même temps à sa vulnérabilité et à ses potentialités. Un monde de «zones» en effet, un territoire de liens, de mélanges, d'incertitudes, fait de tous les sites, à la fois fragiles et féconds, dans cette diversité contaminée qui est encore une multitude.

p66:
> Un récit qui plante des arbres partout où il peut (arbres de mai dans un air rouge), un récit qui «nous» et sait, face à quel «vous», construire ce «nous», sujet collectif indecidé, et non collection de sujets, «nous» qui n'est pas d'appartenance mais d'espoir et d'émancipation. Nous qui habitons vos ruines (et combien sommes-nous ?), nous qui utilisons les accidents du sol, nousqui nous plantons là, dehors, là où ça respire, tenant droit comme troncs, nou s qui transportons les plances, nous qui ouvrons grnad les yeux et les mains, nous «sommes dans ces huttes avec eux, écoutons les enfants, la folie des enfants, dans nos mains, dans l'eau, partout.»
