# R20 Communication Vancouver - préparation mai 2019

(originalement [[revue20_communicationVancouver]])

## Elements saillants

- diversité du modèle éditorial malgré des principes similaires, les autorités et la légitimation ne se construit pas du tout de la même façon d'une revue à l'autre
  - il y a donc une multiplicité de régime épistémologique
- le numérique est identifié avant tout comme un simple moyen de diffusion
- attachement à l'évaluation: processus vertueux
  - pour améliorer l'écriture
  - pour améliore la qualité scientifique, méthodologique, appareil critique
- regarder le protocole éditorial comme un protocole communicationnel très très particulier, ultra filtré.

on pourrait imaginer des axes
  - autonomie des resp. externes VS contrôle (de la revue)
  - évaluation stricte / évaluation négociée
  - financeur / plaisir ou intérêt scientifique / rôle éditorial

conclusion : le projet doit respecter cette diversité éditoriale. quelle est alors notre capacité d'action et de création éditoriale ? à la fois dans la revue (dans la structuration des données) et hors de la revue dans ses usages et pratiques de lectures.
c'est l'orientation que l'on prend en développant les pratiqeus conversationnelles autours des contenus de la revue (voir services isidore.science)

## Lecture & notes des transcripts

### Thématiques abordées

- dossier - interne/externe
  - degré d'autonomie
  - degré de collaboration / collégialité
- évaluation -
  - choix des évaluateurs : proches, lointains
  - pré-évaluation : resp. de dossier, resp de la revue
  - négociations
    - statut/type d'un article
    - suite à expertise problematique
    - intérêt / qualité d'un article
    - intervention/porosité
  - rôle du
    - comité éditorial
    - secrétaire de rédaction
  - anonymat
  - réception des auteurs
  - **validation de l'évaluation**
- critère de temps d'édition et d'accompagnement
- granularité numéro/article (cf les dossiers EF comme des monographies)
- satisfaction/insatisfaction
  - processus éditorial
  - expertises
  - numérique
  - diffusion/diffuseurs
- idéalité / réalité du modèle
- passage au numérique:
  - travail éditorial
  - diffusion
  - papier: légitimation symbolique
- diversité éditoriale et épistémologique
- communauté scientifique
  - large ou étroite
- valeurs
  - rôle éditorial : passeur
- financeurs injonctions, contradiction
- conversation:
  - structurer le champs
  - interne


### Itinéraires

_Christèle Couleau (CC), Magali Nachtergael (MN), François-Xavier Mas (FXM), Nicolas Sauret (NS)_

* autonomie des resp. de dossier externe :

> MN : ils sont autonomes, complètement, sur le choix, la selection, l'évaluation des propositions et des articles qui vont être publiés. 

* présélection :
  - dossier : resp. de dossier (externe)
  - varia : comité éditorial (interne)

* accompagnement des auteurs :

> MN: les responsables de numéro voient si l'article a des chances d'être publié, il y a certains articles, on en avait beaucoup parlé en comité de redaction, qui peuvent nécessiter beaucoup d'accompagnement pour arriver jusqu'à publication, mais on se dit, bah on évalue aussi le temps qu'on va pouvoir consacrer, passer, dans quelle mesure en fait il va être éditable chez nous. Et ça par exemple, parfois il y a des articles qu'on se dit bah non, ça va nécessiter trop de travail, on va pas y arriver, c'est assez rare, quoi qu'il en soit normalement dans les dossiers, numéros, les articles qu'on reçoit dont virtuellement publiables.

* négociation (statut d'un article)

> MN: quand on recevait un article sous la forme d'un projet inachevé de 15 mil signes mais qui n'était pas présenté comme un entretien, comme un essai, enfin qui se présentait comme un article scientifique, là dans ce cas il y avait effectivement des discussions. Après, on accepte aussi des articles qui ne sont pas des articles académiques, entretiens, retour d'expérience, production créative.
> CC: Oui, et donc des fois ça se traduit simplement par un réétiquetage de l'article avec un petit chapeau expliquant son statut. 

* négociation (suite à expertise problématique)
  - prise en compte de la qualité ou non d'une expertise, appel à un troisème évaluateur, discussion et décision parfois informelles, discussion avec le resp de dossier
[transription révisée - 14dec2019]
> 50:08 CC : Puis au moment où on leur [les directeurs de dossier] transmet les expertises et parfois il y a besoin d’un petit accompagnement à la reception des évaluations. 
> 50:18 MN : Oui, parce que nous, les responsables de suivi d'évaluation reçoivent les expertises mais ils sont pas du tout tenus de les donner [inaudible], ils peuvent parfaitement les lire, voir même enlever les choses inutiles, par exemple “trop mal écrits”, là le chargé d'évaluation peut tout à fait reformuler les phrases de façon autre, moi je mets par exemple "voulez-vous reformuler ce paragraphe" à la place de "trop mal écrit". Donc ça ce sont des interventions quand même ...
> 50:58 CC : j’ai le souvenir d’un dossier pour lequel j’avais fait une grosse synthèse, j’avais complètement [inaudible] les originaux qui étaient trop inutilement blessants.
> 51:06 FXM : Ca parait dans notre réponse au questionnaire, nous on roganise les expertises, mais après on les donne au responsables de numéro de manière brute, et c’est au responsable de numéro de se charger...
> 51:17 CC : Non, justement, là c’est pas ça qu’on avait fait... ça dépend
> 51:21 MN : ca nous est arrivé de faire des préfiltres, parce que justement une fois on avait transmis directement, c'était pour le tisseur, un des premiers qu'on avait édité, et il y avait dedans des expertises qui étaient vraiment trop brutales ...
> 51:38 CC : ... et qui avaient été transmises telles quelles par les responsables de numéro, et les  auteurs ne voulaient plus écrire.
> 51:42 MN : Ils étaient vexés, ils étaient blessés
> 51:45 CC : Et c’était dommage parce que c’était des textes qui étaient bons, donc il n’y avait pas lieu de se fâcher, d’avoir des tensions inutiles.
> 51:56 CC: Ça dépend beaucoup si on connaît les responsables de dossier. On sait que ce sont des personnes qui vont justement faire ce travail de filtre et qui n'auront pas de difficulté à prendre le temps de faire ça. On peut leur donner des expertises brutes, et les laisser se débrouiller. Surtout si c’est des gens avec qui on a déjà travaillé, qui ont l’habitude ou qui font partie du comité de rédaction, en revanche, dans des cas où soit les expertises, où il y a beaucoup d’expertises qui posent problème, soit dans des cas où on craint que ce travail ne soit pas fait par le responsable de numéro, à ce moment là, on peut nous le faire. 


* rôle du comité éditorial

> FXM: dans tous les cas les textes sont validés par le comité de redaction avant publication, donc là aussi c'est un travail qui se fait de manière collégiale,

* anonymat : comité interne également dans l'anonymat, mais... pas un secret absolu (secrétaire de rédac au courant, accès au fichier de noms...)

> MN: les responsables du numéro ne connaissent pas les noms des experts, ils ne le connaitront jamais, c'est le secret de fabrication je pense d'un comité, et les auteurs non plus évidement, et nous entre nous pendant le processus d'évaluation ne communique pas non plus au reste du comité. Après ça arrive qu'on demande les chargés du suivi des évaluations, eux les connaissent

* satisfaction par rapport au processus éditorial

* intérêt / qualité d'un article

> FXM: Après dans un même numéro il y a souvent des articles qui sont un peu faibles, un peu plus faibles que d'autres, théoriquement ou méthodologiquement, mais qui par contre sont très intéressants par leur sujet

> MN: il y a des articles qui sont moins bons sur certains aspects, mais qui ont un intérêt, parce qu'ils ouvrent un champ nouveau, c'est pas abouti mais bon il y a quand même des idées intéressantes qu'on souhaite mettre en avant. Ça va bien dans le fil, dans le cadre du volume. 33:58 en complément des autres articles, il apporte quelque chose. J'ai pas une vision forcément toujours essentialiste de la qualité de l'article lui même, bien sûr il faut qu'il ait une qualité minimale, mais il joue aussi avec les autres articles du numéro.

* satisfaction processus éditorial

> CC: Un des plans sur lequel ça marche vraiment bien c'est pour tout ce qui est renforcement de l'aspect théorique d'une bibliographie, il y a des articles qui gagnent vraiment par ce type de remarque, les auteurs vont du coup aller consulter, lire et citer des choses qui vont permettre de donner plus de tenue à leur raisonnement.

* satisfaction évaluation

> MN: Ça tient aussi à la qualité de l'expert, qui fait un bon travail, **bienveillant**, **pédagogique**, et normalement une bonne évaluation aide vraiment l'article à s'améliorer et donc l'auteur est content. En fait c'est un **cercle vertueux** aussi.

* rôle du secrétaire de rédaction : améliorer la qualité éditoriale, négociation avec l'auteur

* **validation de l'évaluation** :

> CC: Si les deux experts ont l'air très content non pas l'air de mauvaise foi, au moment de la validation il n'y a pas forcément une validation de la part de la personne qui est chargée de valider, elle vérifie juste que les expertises ont été prises en compte

* status du texte : édition continue ou pas

> MN: Il nous a arrivé quand même le corriger les articles à posteriori, leur ajouter une référence, pour des articles déjà édités quand ils étaient en ligne.   
> FXM: Ah oui?
> MN: C'est rare, mais ça nous est arrivé quand même. 
> FXM: Bah c'est pas une bonne pratique, ça. 
> MN: Non non, mais ça nous a arrivé de le faire de manière très très ponctuelle, on relit un article parce qu'on a besoin pour nos travaux, et on se rend compte qu'il y a des questions de police, ou quelque chose qui n'est pas bien citée, mais ça nous a arrivé pas de rajouter des références mais de corriger quelque chose comme une date qui n'est pas bonne. Faut pas laisser quelque chose de faux quand même, quoi. 
> FXM: Bah moi je l'aurais pas publié.

* cercle scientifique cohérent vient contre-balancer une évaluation moins légitimante

> MN: C'est à dire que les personnes du comité connaissent la ligne de la revue et savent que la personne du comité qui a fait le dossier connaît aussi la ligne de la revue, finalement on est déjà dans un cercle scientifique cohérent, il n'y a pas de tension entre nous, parce qu'on a déjà beaucoup discuté en plus de ce qu'on voulait etc. Quand on a des dossiers qui viennent de l'extérieur, ça peut être en peu plus difficile d'avoir de la porosité ou bien au contraire, il faut de la porosité à ce moment là parce que nous quelque chose nous a échappé en fait dans la construction du dossier

* idéalisation du modèle

> CC: Je ne suis pas sûre qu'on idéalise le modèle justement. La séparation absolue, on essaye de jouer le jeu autant que possible parce que ça nous semble une façon d'essayer de prendre de la distance et d'objectiver un peu les choses. 39:39 Il y a plein de cas dans lesquels au contraire, arriver à parler ou à s'échanger des points de vue entre ces différentes partie va permettre d'arriver à un meilleur résultat.

* négociation externe-interne : charité épistémique

> MN: c'est pas parce qu'on est responsable de la revue qu'on est responsable scientifique, on a ce qui Marie-Anne Pavot (pas sûre) appelle la "charité épistémique" c'est qu'on considère quand même à priori, par default, que la personne qu'on a en face de nous, qui est chercheur, enseignant-chercheur, est en capacité de faire le numéro de façon autonome intellectuellement, idéologiquement etc. Son travail est fait dans la confiance. Évidement, c'est une confiance mesuré, après il y a tout le processus d'évaluation

* conversation

> FXM: Il y a deux moments où il y a quand même un dialogue entre le responsable de numéro et le comité de redaction, c'est au moment de la définition du projet, donc l'élaboration de l'appel à contributions, et au moment de la validation de l'introduction, c'est le moment où il peut avoir des échanges
> CC: Puis au moment où on retransmet les expertises et parfois il y a un type d'accompagnement

* evaluation rapport d'évaluation : négociation par la réécriture du rapport

* passage au numérique : réussite, remise à plat des procédures, participe de la légitimation de la revue, aussi du point de vue diffusion

> CC: C'est la question de la diffusion qui a été fondamentale pour nous.

> FXM: il y a quand même une question de visibilité puisque c'était pas innocent en fait, le fait de passer sur OpenEdition comme on l’a dit, on a quand même été expertisé, voilà, on prenait pas tout le monde non plus, encore moins aujourd'hui qu'à l'époque, et du coup ça a un tout petit peu valeur de labellisation

* passage au numérique:
  - changement (ou pas) sur le plan éditorial
  - fragilisation / renforcement


> FXM:  Après, dans le travail éditorial finalement ça change assez peu de choses, si ce n'est peut-être que pour la mise en page print je passais quand même un peu plus de temps. Mais sinon tout le reste du travail c'est effectivement la même chose. 

> CC: pour certains numéros particulièrement, le fait de pouvoir être en ligne a été un gros apport pour les numéros sur le numérique, tous les liens entre les textes et les références en ligne peuvent être accedés directement, c'est beaucoup plus agréable pour les lecteurs et ça c'est particulièrement nette pour ce numéro, mais pour l'ensemble des numéros publiés récement de plus en plus on a cette possibilité de naviguer autour de l'article, de rebondir très facilement et ça je pense que c'est quelque chose qui est une façon aussi d'apporter de la qualité à la revue, de l'inscrire comme ça dans tout le bagage des publications scientifiques

> FXM: Donc effectivement ça change le lectorat, l'autorat, la manière d'éditer, la diffusion, la ligne éditoriale.

> MN: Parce que le passage en ligne nous a plutôt fragilisé au début, [...] Donc institutionnellement, ça n'existe pas

> MN:  Le passage au numérique, une fois qu'on est inscrit dans ce système là et vu que la question de savoir si on va trouver du financement (inaudible), du coup ça donne une grande liberté de se dire tel thème est important, on a envie de le faire, on peut le faire. C'est quelque chose de vraiment précieux.

* Légitimation de la revue:

> C: La clarification des procédures a été complètement mise en place à cette occasion-là. 
> F: Les procédures étaient moins transparentes...
> C: Il n'y avait pas l'idée de double expertise anonyme systématique, ça c'était pas quelque chose d'évident. Ça avait peut-être été fait ponctuellement sur certains numéros.

* diversité éditoriale et épistémologique

> MN: Très clairement les enjeux d'Itinéraires, on avait l'impression de pas parler des mêmes processus scientifiques en fait. Eux ils étaient tout papier avec un comité qui se réunissait un peu comme des moines pour lire collectivement les articles ensemble (rires). Il y a avait un coté très anachronique entre les deux processus. On vivait pas dans la même communauté scientifique, on partageait pas les mêmes références.

* légitimation symbolique du papier:
  - plus de recensions si revue papier
  - souhaite tester le PDF et impression à la demande (print on demand POD)
  - "taux citationnel" supérieur pour le papier, et Syllabus pédagogique surtout papier (80%)

* Valeurs :
  - faire avancer la connaissance
  - explorer ("revue comme espace exploratoire")
  - dynamique d'entrainement de sa communauté scientifique
  - plaisir :
    - écosystème des revues qui évolue constamment
    - la fibre de publier
  - rôle éditorial : passeur

> MN: faire évoluer les études littéraires et que c'était quand même un outil d'agentivité des esprits. C'est-à-dire que même si c'est des petits articles, même si c'est des petits volumes, 3 par an, que ça pouvait peut-être changer progressivement l'idée qu'on se faisait des études littéraires et de ce que ça peut apporter globalement à la connaissance.

> MN: Historiquement, les revues ça a toujours été un espace exploratoire, plus que les éditions papier où il y a quelque chose de presque patrimoniale, il y a un coté expérimentale dans la revue qui est possible.

> FXM: Moi je dirais que pour l'instant on est dans ce rôle de passeur, et je dirais que c'est ce qui nous intéressait dans le projet Revue 2.0, c'était de pouvoir aller plus loins justement, la notion de communauté, c'est là-dessus qu'on doit et qu'on peut s'améliorer, c'est constituer une communauté, l'animer, et qu'en tant que revue, être visible dans la conversation - parler en tant que revue, je sais pas si c'est possible, mais voilà. 

> MN: C'est le plaisir, mais c'est l'idée aussi de publier quoi. Tout le monde n'a pas la fibre non plus. 

* Communauté: large ou étroite

> FXM: C'est à la fois notre point fort et notre faiblesse, je dirais, et c'est vrai qu'ils le pointaient (Openeditions) pour eux, le lectorat n'était pas évident comparé à d'autres revues qui ont des sujets beaucoup plus précis, soit qui sont la revue de référence dans un sujet de discipline, voilà le fait que nous on est à la fois sur plusieurs disciplines et objets, c'est un point fort parce que ça nous permet d'accueillir plein de sujets très différents et du coup innover aussi, mais c'est vrai que c'est une faiblesse dans la mesure où on s'inscrit pas dans un champ très précis, une discipline très précise, dans une communauté qui existerait déjà en fait;

* Conversation : ouvrir des espaces de conversation

> MN: Ça crée des cadres imaginaires de conversation dans lesquels on est autorisé à aller, et je pense que ça c'est ouvrir des espaces de conversation possibles, ce qui est quand même fondamental.

* rapport institutionnel:
  - se démarquer
  - la faire exister en interne (université), dans la communauté

> MN: Nous sommes la revue littéraire du 93. On a pris cette option là, on a fait un numéro sur la banlieue qui a porté à beaucoup de discussions d'ailleurs, est-ce que parce qu'on est en banlieue on doit parler de banlieue... Ok, on est marginaux, donc on va créer un espace de réflexion peut-être aussi marginale mais pus libre et plus ouvert avec des approches pas forcément conformes, institutionnels. 
> CC: Avec la possibilité de laisser la parole à de gens qui veulent aborder des territoires nouveaux, ou les faire de manière pas habituelle. Essayer d'être au coeur des questions actuelles, dans ce cas pas trop marginal non plus, mais... 
> MN: La banlieue influence Paname, Paname influence le monde. C'est pas parce qu'on est en marge qu'on n'a pas un pouvoir d'influence. On parle de la marge, mais on ne réduit pas forcément notre perspective.

>  FXM: Après pour toutes les revues comme nous qui sont des revues relativement jeunes, qui sont pas portées par une société savante, il y a toujours ça, continuer à se développer et se faire une place et à se créer une identité, poursuivre la création d'une identité et du coup avoir le lectorat et l'autorat qui va avec.

### Mémoires du livre

_Marie-Pier Luneau (MPL), Anthony Glinoer (AG), Cécile Delbecchi (CD), Joanie (J), Juliette De Maeyer (JDM), Nicolas Sauret (NS)_

* Collaboration : forte implication des responsable de numéro, décision collégiale sur la base d'un tableau excel rassemblant les avis de chacun (évaluation comprise sur la base des abstracts)

* évaluation:
  - choix des évaluateurs, via le respon. de dossier (externe) qui est le plus pertinent pour connaitre le champs et ses acteurs
  - double aveugle relatif : le comité qui discute des articles sait qui a évalué qui.
  - le cas par cas, la politique générale ne fonctionne pas. on revient au texte et au jugement collégial du comité au cas par cas: gestion des évaluation contradictoires, envoi de rapport ou pas, reformulé ou pas.
  - collégialité de la décision (interne+externe) sur la base des évaluations
  - collégialité installe déjà une conversation et crée un espace + bienveillant pour l'auteur
  - minimiser le rôle de l'évaluation et raisons pour la nuancer
  - évaluation de l'évaluation :
    - proposer des solutions
  - interne/externe : plus sévère en interne et jugement sur la quantité de travail à fournir

> MP: Ce serait assez artificiel de dire qu’on essaye de maintenir l’anonymat. Ils sont au courant comme le directeur.trice de la revue sait qui a évalué quoi. Ça nous permet aussi de mieux juger, car au fil des années nous comprenons mieux les standards de certains évaluateurs.

> MP: Au moment où Cécile reçoit les évaluations — deux externes et une interne d’un membre du comité de rédaction — elle les envoie à la direction de la revue et au directeur.trice de numéro en résumant par courriel les points majeurs des rapports. À partir de là la décision s’enclenche de manière collégiale sur ce que l’on fait. On peut nuancer l’évaluation des rapports qui nous apparaît trop sévère où à l’inverse s’il nous semble qu’un évaluateur a manqué quelque chose.

> MP: Dans le courriel que Cécile va envoyer à l’auteur, avec les rapports anonymes et pointer les décisions que les directeurs de numéros et la direction de la revue ont prises. Ça se fait de manière très collégiale, et c’est une étape importante. Depuis qu’on fait ça, les auteurs comprennent mieux et nous avons moins de réactions outrées. On laisse les rapports complets ou presque, on enlève les phrases injurieuses s’il y en a.

> MP: Moduler un peu la parole d’évangile des évaluateurs c’est à force d’avoir des discussions au comité de rédaction. On se réunit une fois par année, parfois plus, et on fait la liste des problèmes rencontrés dans l’année. L’insatisfaction vis-à-vis des rapports revenait souvent, on voyait que c’était un irritant et on s’est rendu compte qu’il fallait nuancer ça

> A: Je dirais à titre personnel que je soutiens beaucoup cette vision-là, qui consiste à arrondir les angles en cas de besoin pour maintenir la cordialité de la conversation, parce qu’elle va permettre d’améliorer tout le processus y compris les retards, la prise en compte des remarques faites dans les évaluations.

> MP: Le bon évaluateur c’est assez celui qui sort du lot, et qui fait deux trois pages. Il y en a qui corrigent l’article, qui corrigent la langue. C’est un évaluateur qui est assez exigent, car l’inverse n’est pas drôle, quand on a un article entre les mains qu’on sait déficient et que l’évaluateur n’a pas fait son travail ou a été trop peu exigent c’est un problème. Celui qui a bien lu l’article, qui est assez exigeant et qui a fourni un bon rapport de deux-trois pages. Dans les dernières années, il me semble qu’on a eu de bons rapports. Des rapports qui demandent des corrections majeures à un auteur qui est très bon déjà, et on n’avait pas trop de scrupule à envoyer ces demandes de modifications majeures, car le rapport proposait tellement de solutions pour régler le problème. Un bon évaluateur c’est quelqu’un coche « modifications majeures » et qui propose des solutions.

> MP: Dans la quasi-totalité des cas, le membre interne est extrêmement sévère, souvent plus que les évaluateurs externes. Ça permet d’écarter des articles que l’on ne veut pas pour plein de raisons, aussi parce qu’ils sont dix fois plus de travail jusqu’à la fin. Souvent quand il y a un problème de fond il y a un problème de forme, donc l’article va devoir être réécrit.

* rôle du comité scientifique: peu impliqué, mais consultatif et ambassadeur de la revue

* importance/rôle de:
  - l'édition
  - des financeurs

> MP: Quand on monte une revue à partir de rien on doit miser sur la qualité des gens qu’on embauche, et c’est un trait de Cécile. Ce n’est pas une politique éditoriale de la revue. Ceci dit, les échos sont positifs vis-à-vis de la correction. Ce qu’on a toujours voulu c’est publié les meilleurs textes possible, avec le moins de fautes. Oui Cécile intervient même dans la syntaxe, mais les auteurs ne se corrigent pas toujours et d’avoir une réviseuse qui se décrit comme interventionniste c’est un gage de qualité. C’est coûteux, mais ce qui compte c’est la qualité de la publication.

> MP: Ce n’est pas un discours [celui sur un protocole d'évaluation defectueux, constamment à moduler et nuancer] qu’on va avoir dans les demandes de subvention, mais dans la réalité il faut toujours se servir du jugement au cas par cas.

> MP: On est limités à reproduire les exigences que les pourvoyeurs de fonds nous demandent. Au niveau du processus d’évaluation, le CRSH va valoriser cette évaluation-là avec au moins deux experts externes à l’aveugle. À moins qu’eux changent leurs règles, on ne pourra pas les changer non plus. La question du libre-accès a toujours été fondamentale, nous n’avons jamais voulu être une revue sous abonnement, et la conséquence c’est que de faire ce que le patron, en l’occurrence le CRSH, nous dit de faire.

* conversation avec les évaluateurs:
  - il y a des demandes
  - pas forcément pour cependant d'un point de vue scientifique
  - communication entre acteur selon un protocole très particulier

> MP: le dialogue avec les évaluateurs ça ne s’est jamais fait de mémoire, c’est une demande que j’ai eue de la part d’un auteur européen qui est lui-même très impliqué dans le monde de la revue. Je pense que c’est une pratique qui va tendre à se répandre, mais nous ne sommes pas encore rendus là. Je ne sais pas si c’est une chose souhaitable, ce sera peut-être discuté en comité de rédaction si les demandes d’auteurs à être mis en contact avec les évaluateurs se répètent.

* légitimation de la revue :
  - préjugé négatif du numérique
  - marcher dans les pas des revues papier pour se légitimer, reproduire le protocole, mais conscience de devoir peut-être évoluer et expérimenter d'autres modèles et protocoles.
  - démarche pour s'imposer dans un environnement naissant

> MP: Quand on a fondé la revue en 2009, il y avait encore un préjugé très fort à l’encontre des revues électroniques. Ce n’était pas un processus aussi rigoureux qu’une revue scientifique papier. Pour se battre contre ça, notre stratégie a été d’appliquer une grande rigidité, beaucoup de rigueur, et c’est pour ça que les évaluations sont au cœur de notre processus. Je pense qu’elles le sont en réalité, parce qu’il y a deux moments d’évaluation, la sélection des propositions d’abord puis la sélection des articles par deux membres externes et un membre interne, la revue a été pensée pour avoir toute cette rigueur et placer l’évaluation au cœur de son processus.

> MP: Il y a ce paradoxe avec lequel on vit et peut-être que l’équipe va devoir se poser des questions et voir ce qu’ils veulent faire avec ça. Cette nouvelle idéologie que les revues peuvent établir un meilleur dialogue entre les auteurs, les évaluateurs, animer la recherche est peut être une deuxième étape dans la vie de la revue, mais je dirais que les 10 premières années on a surtout été occupés à avoir l’air crédible aux yeux des autres.

> MPL: Ça a voulu dire dans un premier temps reproduire les mêmes manières de faire que les revues papier les plus exigeantes, mais le faire dans un contexte numérique, pour justement lutter contre ces préjugés. Ce n’était pas très original au début. Faire la même chose que ce que les revues papier font, sauf que c’est en ligne, c’est gratuit il n’y a pas d’abonnement, mais c’est les mêmes processus de sélection, les mêmes appels de texte, les mêmes comités scientifiques qui viennent donner du capital symbolique, c’est les mêmes pratiques. J’imagine que dans l’avenir il faudra évoluer.

> A : Comme toute instance de légitimation, ce qu’est une revue, elle a besoin d’être et de se sentir suffisamment légitime et légitimée pour pouvoir produire de la légitimation, donc on se nourrit de ce que l’on produit.

> A: C’est exactement ce qu’on espère qui arrive à une revue, particulièrement à celles lancées entre 2005 et 2012, où on était dans le questionnement de la légitimité du format revue électronique, au moment où les plateformes type revue OpenEdition et Érudit étaient pas encore aussi reconnues qu’elles le sont maintenant. On cherchait un peu le modèle, et celles-ci font certainement partie des revues qui ont réussi à s’imposer avec ce modèle-là.

* matérialité du numérique :
  - diffusion large
  - limitation du format (Erudit)
  - rôle d'érudit

> MP: À chaque fois que je fais des demandes de subvention CRSH, je n’en reviens pas en trois ans des bonds que ça a fait en termes de diffusion, il n’y a pas de frontières, le fait d’être bilingue ça nous aide, mais c’est juste formidable. Quand on fait de la recherche, on sait à quel point l’idée de s’en aller à la bibliothèque prendre une revue papier serait complètement anachronique de nos jours. Le format est super, c’était une bonne idée de faire ça en 2009. Pour ce qui est de la diffusion électronique en libre accès sur Érudit, je suis contente des résultats.

> A : En termes de matérialité, étant une revue 100 % électronique, 100 % sur Érudit, on est contraint par les modèles et par les approches visuelles d’Érudit, on n’a pas tant de prise sur ces questions-là.

> MP: Je ne veux pas avoir l’air candide, parce que vis-à-vis d’Érudit on peut avoir des critiques, notamment en ce qui a trait à la matérialité de la chose. Mémoire du livre n’est pas la revue électronique la plus belle, comparée à d’autres revues. Mais notre revue a pu grandir aussi avec Érudit parce qu’Érudit grandissait, et que ça fonctionnait avec la vision qu’il y avait, le libre-accès a toujours été encouragé par Érudit, mais sans le support technique d’Érudit, des professeurs de littérature qui ne connaissent rien à l’informatique, n’auraient pas pu arriver à ça aujourd’hui.

* conversation :

> A: Je crois qu’il y a des petites appréciations différentes de ce que tu appelles grande conversation, moi j’ai souvent le sentiment d’un grand brouhaha, donc essayer d’obtenir une voix dans cette polyphonie infinie, de proposer quelque chose à la fois éclectique et cohérent dans les différentes approches qui sont accueillies. Je pense qu’il y a un sens, plus on se rapproche plus ça a du sens, plus on s’éloigne plus je m’interroge.
> A: Plus on se rapproche de la discipline, de l’expression scientifique, de ce qui est disponible en ligne, enfin dans ce que l’on fait et comment on le fait. Plus on se rapproche de ça et plus on est dedans, plus je suis sensible à la qualité d’un texte produit et à la réussite en tant que telle, et plus on s’éloigne dans le grand ensemble du discours social, et plus c’est un objet parmi d’autres.

### Intermédialités

- évaluation
  - intense travail pour trouver des évaluateurs sans conflit d'intérêt
  - confiance aveugle dans le processus : c'est le processus qui légitime et non le comité.

> F: En fait j'ai une position assez humble de ce point de vue-là, puisque nous sommes une revue tellement interdisciplinaire, je pense que tout ce que nous avons publié, nous l'avons publié dans la mesure où ça respectait des qualités scientifiques, c'était reconnu par les pairs etc., mais je n'ai pas de position tranchée. Une fois que s’est passé par tout ce processus-là, je n'ai pas de regret, même si je pense que certains articles sont moins bons, que certains me plaisent moins, on n'a pas une ligne éditoriale à ce point-là définie pour se dire « Ça on regrette de l'avoir publié ».

- numérique et diffusion
  - perte de la maitrise du lectorat et du réseau institutionnel
  - plus large lectorat, mais perte de son assise institutionnel
  - erudit avec sa barriere mobile, tout n'est pas accessible dès le lancement
  - concurrence des plateformes tierce de diffusion (Academia)
  - «on n'existe plus de la même façons» (usages)
  - limitation éditoriale (article d'artiste) vs liberté éditoriale
  - rôle patrimoniale de la revue

> T: C'est sûr que les gens ne nous lisent plus de la même façon. Ils lisent nos pdfs, ils ne lisent pas le numéro au complet. Le format d'Intermédialités est intéressant, c'est une des revues que je trimballe le plus pour relire les anciens numéros, c'est un petit format et là les gens doivent aller chercher des choses de façon beaucoup plus morcelée qu'avant. On n'existe plus de la même façon.

> F: je trouve qu'il y a du coup des contraintes techniques qui interfèrent lourdement sur les idées à la création.

> T: Et c'est là qu'on pourrait développer l'intermédialité, via internet. Mais pour Érudit c'est compliqué de faire ça, et ils ont un protocole d'édition très fixe et très peu plastique, à chaque fois c'est un peu compliqué. Quand on fait le dépôt d'un numéro par exemple, on ajoute une rubrique, mais moi je ne savais pas comment leur dire, donc je leur ai demandé si je pouvais leur envoyer un formulaire, une fiche avec nos recommandations et ces choses-là, mais ils nous disent pas grand-chose. Ils nous disent de déposer les numéros dans leur état final, mais la discussion se fait ensuite, et on a un peu l'impression qu'on les dérange en leur demandant certaines choses, alors que ce serait tout à fait intéressant pour nous de développer plastiquement nos publications sur internet.

> F:  Sinon le numérique, à part le fait que ça nous ait permis d'inclure des films, on a même carrément des films dont on a la garde en termes d'archives, par exemple on n’envoie jamais à un lecteur un fichier youtube. Tout ce qui est film doit être conservé sur le serveur d'Érudit, avec son protocole.

- processus éditorial
  - rigueur intellectuelle et scientifique du papier

> F: Non, parce qu'on a hérité d'une rigueur intellectuelle et scientifique très forte qui était déjà dans le papier.

- matérialité du papier/numérique
  - rigueur du processus éditorial (papier)
  - meilleure indexation (numérique)
  - environnement numérique facilitateur d'un travail scientifique (vérifications)

> T: Mais dans scientificité on peut peut-être inclure l'indexation, et dans ce sens-là, la revue a peut-être vu sa scientificité augmenter, puisqu'on est d'avantage cité, on apparaît d'avantage.

> F: je n'ai pas vu un changement énorme dans la manière dont on vérifie les choses, dont on veille à ce que les textes se tiennent dans la diffusion. Il ne s'est pas passé grand-chose. Peut-être que c'est plus facile des fois de faire des vérifications, mais ce n'est pas notre revue, c'est le fait qu'il y est beaucoup de choses en ligne, ce qui fait que les vérifications sont plus faciles et plus rapides. Mais ce n'est pas la revue, c'est l'environnement en général.

- relations humaines

- communauté
  - construire la recherche et la communauté que l'on veut

> F: Dans la revue tu entretiens un milieu, tu fais que ce monde-là existe et qu'il existe avec la couleur que tu as envie qu'il ait. Et c'est un instrument magnifique, parce que comme le dit Marcello ça crée des communautés, mais pas une communauté au sens sociologique du terme, ça crée du commun parce qu'il y a eu beaucoup d'échanges, parce que tu te rends compte que des gens avec qui tu n'as jamais échangé aiment la revue, la lisent. C'est plus ce sentiment de fabriquer le monde académique dans lequel on évolue, et que c'est bien qu'une revue comme ça existe. Donc les valeurs dont tu nous parlais tout à l'heure c'est de se demander dans quel monde académique je veux vivre, et qu'est-ce que je contribue à créer dans ce monde-là, au-delà de ma propre contribution de recherche, au-delà des écrits, au-delà de ce que je fais, moi, comme chercheur, c'est comment je crée des éléments de ce monde-là.

- territoire

> F: J'aime ce côté qu'offre la revue parce qu'elle n’est nulle part, c'est un territoire sans concept, appropriable par qui en veut, et par rapport à ce qu'il en fait, c'est là que va se situer l'évaluation. Tu te l'appropries, montre-moi ce que tu en fais.

- conversation
  - pas destinée à tous
  - a une certaine fonction qui ne fonctionnerait pas si on l'ouvre à un trop large public.
  - importance de l'oralité: spontanéité, convivialité jouissance
  - l'édition de la conversation serait chronophage
  - importance d'un espace sûr et sécurisé pour discuter collégialement d'un texte, sans surveillance.
  - la fonction de l'oralité (de la conversation) ne fonctionne pas si elle est surveillée

> F: Donc pour la discussion, au comité on est entre nous, on prend plaisir entre collègues à discuter, de prendre prétexte de ces textes pour être entre nous et discuter. Donc que d'autres viennent, oui pourquoi pas, mais ce n'est pas le but du jeu. Et puis dans ces moments de discussion entre collègues, tout le monde est content d'être là, d'avoir cet espace-là. C'est des espaces de visu, de face à face, et après, ce plaisir qu'on a eu à lire ces auteurs, à discuter, à valoriser tous ces textes se répercute sur la manière qu'on a d'interagir avec les auteurs plus tard. Donc cette idée d'avoir une continuité ne vient pas de nulle part, elle vient parce qu'on agit comme ça depuis le début, parce qu'on traite les gens comme ça depuis le début, on a un souci de leur carrière, on les suit, on regarde ce qu'ils font, on est content de voir leur évolution. Ce n'est pas quelque chose de nécessairement formaté, organisé, mais qui transparaît plus tard.

> F: Mais l'oralité est quelque chose d'agréable en soi, qui a son propre espace. Je ne sais pas le gain qu'on aurait par rapport à la perte. Je pense que l'espace d'oralité est un espace de convivialité intellectuelle, et qu'il doit exister comme tel.

> T: Oui et puis du point de vue pragmatique du travail, multiplier les versions, faire des couches de conversation peut prêter à confusion. Quand on édite on manipule beaucoup de documents mais il ne faut pas se mélanger. Je me dis que si on ajoute des couches entre les couches, déjà je trouve qu'il y en a beaucoup, je ne vois pas trop comment on peut s'en sortir. Ça devient très prenant. Déjà les secrétaires de rédactions trouvent ça prenant de nourrir les échanges, il faut être ponctuels et on a  nos vies à côté, si on ajoute là-dessus la gestion de discussion avec les auteurs, ça me semble compliqué.

> F: je pense que l'institution scientifique mise beaucoup sur l'oralité en dehors de la publication. Dans l'institution scientifique les espaces d'oralité sont énormes, les espaces de laboratoire, rien que les séminaires. Qu'est-ce que c'est un séminaire ? Rien que dans sa version collégiale, c'est des gens qui viennent exposer les premiers les balbutiements de leurs travaux, qui attendent un retour et qui se sentent protégés parce qu'ils sont dans un espace clos. Ces pratiques existent depuis longtemps, et le fait de les mettre dans des structures de surveillance, enfin des structures de traçabilité, de publication et tout ça, ça devient bizarre ou paradoxal. Tu ne peux pas enlever d'internet ces publications, il y a l'idée que tu es en train de produire des traces, et je ne sais pas si ça va avec la logique de l'oralité. C'est vrai que dans les colloques, je demande toujours au public de ne pas enregistrer ma communication, parce que je ne suis pas venue avec quelque chose de fini, je suis venue avec quelque chose que je commence. Je viens tester du matériel, ce n'est pas l'espace pour le faire parce que les colloques sont devenus de plus en plus publics, je sens que je suis en contre-courant. Mais les espaces de séminaire, dans le sens hors-pédagogie du terme, dans des centres de recherche, dans les labos, sont des espaces pour faire ça, pour tester, pour commencer à penser avec, à apporter ça devant d'autres, en profiter pour avoir des retours.

- état du texte
  - l'édition consiste à sélectionner (et oublier les états précédents)

> F: Entre premier texte, le deuxième et celui qui est publié, moi en tant qu'auteur je ne sais pas à quel point il y est des gens qui conservent la trace de toutes ces étapes. On peut les publier à l'interne, mais en même temps l'auteur compte sur nous pour décider quand même. Décider quoi faire quand il y a toutes ces conversations, ces idées qui partent dans tous les sens, et même des fois en discutant au comité on a des opinions contradictoires et les arguments des uns et des autres font que ceux qui avaient un avis changent d'idée.

- rôle de la revue
  - jardiner

> F: J'aurais du mal à exprimer le rôle exact que l'on a. Surtout s'il faut le penser en termes d'amont ou d'aval, ou de leader. Je dirais qu'on entretient un terrain. On ne le laisse pas à l'abandon, on fait en sorte que ça pousse. Après, dans quelle direction ça va aller, on n'en sait trop rien, mais on fait en sorte que ça pousse.

- rapport institutionnel et positionnement
  - erudit est un partenaire à soutenir

> F : Oui, c'est une chose que l'on pourrait avoir, sauf que j'ai toujours voulu défendre Érudit. Moi pour l'instant je suis la politique d'Érudit, parce que je pense que la manière dont Érudit conçoit l'édition savante, le rôle qu'elle a, la manière dont elle va diffuser, créer des partenariats, et qu'on ne laisse pas ça à la loi du marché, je suis tout à fait d'accord. Donc je ne veux pas faire quelque chose dans le dos d'Érudit, mon objectif c'est d'être derrière eux, avec eux, de faire tout ce que je peux pour penser collectif.


- ouverture / distinction sur d'autres formes et modèles éditoriaux

> F: est-ce qu'on voudrait publier des blogs, parce qu'un éditeur scientifique a une mission, une mission de scientificité, il doit veiller au grain que le protocole qui assure la scientificité d'une publication soit respecté et c'est déjà un énorme travail. Est-ce qu'on s'autorise à publier des choses qui ne relèvent pas de ça mais qui sont d'un intérêt quand même pour la communauté ? Oui, on l'a fait, mais ça a été à chaque fois une sorte de pirouette, on ne pouvait pas faire passer des vessies pour des lanternes et on s'est dit que c'était important intéressant, mais pas un article scientifique, donc on va le mettre dans « Notes de labo ». Comme espace d'accueil d'une autre parole, ou d'une parole scientifique mais qui n’a pas la forme d'un article scientifique, on pourrait le développer, on y a été confronté, ce n'est pas un choix systématique, ce n'est pas quelque chose qu'on se sent faire parce qu'on a déjà cette mission d'édition scientifique, déjà très lourde.

> F: un potentiel directeur que j'ai contacté, très proche de la revue, quand je lui ai proposé de le proposer comme prochain directeur, qui n'est pas dans le comité de rédaction mais qui connaît bien la revue, il m'a dit « Non parce que moi j'aime pas trop l'édition scientifique, j'ai plutôt envie de valoriser les formats essais, d'autres formats de publication. » J'ai très bien compris son point. La revue est identifiée comme qui garde une ligne professionnelle, avec des critères précis, et il n'était pas à l'aise avec cela, il ne voulait pas être le gardien de ça ou le garant. Et il est d'ailleurs investit dans d'autre types de publications, qui deviennent partenaires avec des événements. C'est à la fois très intéressant, en hors-champ, ils font des choses partenariats avec des institutions, avec la Cinémathèque Québécoise, avec le programme de Concordia Cinema Politica, ils sont impliqués dans le milieu. Ils ont des publications dans lesquelles des profs et des étudiants écrivent, mais c'est quand même un refuge qui est en lien avec le milieu, au sens événementiel, communautaire. Mais ils n'ont pas le CRSH comme financement. Et j'ai trouvé intéressant qu'il me dise ça.

> F: il se sentait plus à l'aise dans ce genre d'**activisme académique**, que dans celui de la revue qui est celui d'une ligne scientifique traditionnelle. Est-ce qu'on peut faire l'un et l'autre, ou soit l'un soit l'autre, tout en étant content que chacun existe. Nous on a eu des moments de flottement, surtout autour de cet article-là de « Notes de labo »

- Diffusion numérique : perte de capital symbolique

> On a été contraints au numérique pour des questions budgétaires, on n'avait pas eu nos subventions et il fallait survivre. On a eu un très beau papier, avec une qualité d'impression incroyable, mais qui coûtait énormément cher, donc c'était le premier budget qu'il était facile de compresser. Surtout qu'à ce moment-là je pense que la FRQ obligeait les revues à se numériser entièrement, et qu'on ait commencé à voir l'impact de cette transformation sur nos lecteurs. À la fois il y a eu de la contrainte, et à la fois il y avait cette découverte, cet espoir que la numérisation allait nous ouvrir un champ incroyable de nouveaux lecteurs. La seule chose c'est que c'est arrivé à un moment où la revue commençait à se faire bien connaître avec des tas d'abonnements, individuels, avec des bibliothèques, partout dans le monde, Philippe avait fait un travail extraordinaire là-dessus, il y avait beaucoup de bibliothèques et d'universités américaines, européennes qui avaient répondu à nos démarches, et ça, dans le passage au numérique, à Érudit, c'est tout tombé à l'eau. Parce qu'Érudit ne fait que des paniers d'abonnements et ne gère pas des abonnements de revues, d'institutions directement. Donc à la fois on a eu beaucoup plus de lecteurs, mais en termes d'institutions on a eu une perte. On est plus qu'avec Érudit maintenant, ou avec quelques rares bibliothèques qui continuent à nous suivre. Et cette économie là nous est rentrée dans le corps aussi, parce qu'on voit maintenant qu'on est reliés à des logiques qui nous dépassent, en termes d'abonnement, de coût, de rayonnement, tout passe par Érudit, on s'est relié dans le rayonnement d'Érudit elle-même. On a perdu la maîtrise de ça tout en ayant gagné beaucoup de lecteurs. Au niveau de notre rayonnement purement institutionnel on y a perdu. D'autant que ce qui est intéressant dans ce rayonnement institutionnel avec le papier c'est qu'on touchait des universités anglophones, italiennes, espagnoles, qui en s'abonnant à nos numéros papier pouvaient rendre disponible le numéro à leurs lecteurs hors étudiant, alors qu'en passant par Érudit avec le système de barrière mobile, finalement pendant un an tous nos numéros sont inaccessibles aux gens qui n'ont pas accès à ces institutions-là, ou ces gens qui travaillent dans ces institutions parce qu'ils ne se sont pas abonnés à Érudit, ils ne font pas partie du consortium Érudit, et tout ça est très compliqué, et on n'a personne pour s'occuper des abonnements pour les universités qui ne sont pas dans les règlements d'Érudit.

### Etudes Francaises

#### particularités :

1. chaque numéro ressemble à une monographie collective, le dir. de dossier endosse une grosse partie du travail, notamment d'édition. Le dossier est supposé arrivé dans un état presque fini, et le dir. de dossier fait l'interface entre la revue et les auteurs, sauf pour la partie révision. La revue accepte un dossier sur la base d'un abstract de dossier (conséquent) et sur la base des abstracts d'articles. Les articles sollicités sont ensuite soumis au dir. de dossier
2. un travail très conséquent est fait sur le texte pour améliorer le style, l'argumentation. c'est un travail intense qui implique tout le comité éditorial.

- Evaluation:
  - les évaluateurs évaluent l'ensemble du dossier, et donc sa cohérence.
  - prédominance du jugement du comité VS celui des évaluateurs
  - les évaluateurs s'adressent au comité et non aux auteurs

> EN: On a la liste fournie par le coordonnateur du dossier, on en prend un et on en cherche un autre, avec le souci d'être le plus possible à l'international. Pas pour faire plaisir pour faire plaisir aux organismes subventionnaires puisqu'ils ne nous donnent rien, mais parce que ça nous paraît important d'élargir de cette manière-là, que ce soit dans un autre système. On leur demande longtemps à l'avance, on leur donne trois mois à peu près, **on les remercie pour ce qu'ils nous ont dit et on garde ça pour nous**. Si on fait ça, c'est parce qu'**on compte sur l'intervention du comité pour interpréter ces évaluations et les trancher**. Mais on n'a pas d'autres liens avec les évaluateurs.

> JB: J'ai envie de préciser que la relation qui est établie entre nous et les évaluateurs externes met assez rapidement de l'avant le fait que l'évaluation faite n'est pas adressée aux auteurs mais au comité de rédaction et à la direction. C'est à eux, pour leur travail à venir, que l'évaluation est faite, et c'est dans cette logique-là qu'on demande de la penser.

> c'est vrai que les évaluateurs s'adressent à nous pour aider notre travail d'édition. Il faudrait penser dans une toute autre logique, une logique de débat et de discussion, dans laquelle nous ne sommes pas.

- Négociation interne/externe
  - intense discussion
  - travail important pour présenter les retours du comité (Procès Verbal) au dir. de dossier.
  - cas particulier: cacher la négociation («l'échafaudage»)
  - la revue comme instance d'édition (plus qu'une instance de légitimation)
  - évaluation de l'évaluation (rôle du comité)

> EN: C'est un moment de travail intense, à moins que le dossier soit accepter tel qu'il est, ce qui est le cas de 60% des dossiers. Dans les autres cas, ça repose sur la direction de la revue de négocier avec le coordonnateur tout, l'ordre des textes, leur présentation, l'exclusion ou la modification important d'un des article, la suggestion d'aller chercher un autre article dans une direction précise, ça peut être sortir d'un truc qu'on a trouvé trop étroit, aller à l'international, ça peut même être précis, de dire "on ne comprend pas pourquoi sur ce sujet là untel n'est pas là".

> EN: Moi je dois absolument représenter le point de vue que le comité a émis, en même temps je dois négocier entre les exigences du coordonnateur, c'est arrivé que l'exclusion d'un article soit renégociée complètement. C'est à dire que le coordonnateur me dit qu'il n'est pas possible de dire à l'auteur qu'il ne sera pas publié, auquel cas, j'ai demandé un membre de corrections telles que l'auteur s'est finalement retiré, mais ce n'était même pas dans cette intention, c'était vraiment dans une dynamique de renégocier.

> EN: On a négocié jusqu'à la fin, puisqu'à la fin de son article elle a trouvé le moyen de citer l'article du gars qu'on avait refusé, et je lui ai demandé de le retirer afin que l’échafaudage de la revue ne soit pas visible. Je ne sais plus comment ça s'est terminé, je crois avoir fini par lui accorder en demandant toute fois de retirer la phrase stipulant qu'il aurait pu paraître dans son dossier.

> EN: Et c'est l'exemple type aussi d'une négociation pénible, ce n'est jamais très agréable de demander à une collègue de retravailler, mais qui a vraiment donné un bon dossier. On en est très content, ça a augmenté considérablement sa portée théorique. On voit bien que le comité a un rôle scientifique qui n'est pas simplement d'avaliser ou de reconnaître ou de ne pas reconnaître, **c'est plus qu'une instance de légitimation, c'est une instance d'édition**.

> EN: Je fais confiance à cette structure pour qu'au sein du comité on soit capables d'évaluer non seulement le dossier qu'on a sur la table, mais les évaluations qui en ont été faites, ce qui dans la pratique est vrai. Pour le coup on a le nom des gens, et si quelqu'un dans le comité dit "oui c'était une mauvaise idée de l'envoyer à telle personne parce qu'il est dans une optique totalement différente, bon à ce moment-là on est autorisés à passer par-dessus, en partie, cette évaluation. Je caricature, ce n'est pas vraiment arrivé à ce point-là, mais je crois qu'on est dans la logique d'une légitimation par les pairs à l'aveugle, dont les inconvénients évidents sont contrebalancés par le nombre de gens qui interviennent et qui portent un jugement.

- Structuration / mise en forme
  - la structuration (aspect intellectuel) est livrée en même temps que les articles (rassemblés de manière cohérente dans un dossier)
  - dimension de contrôle
  - édition comme gage d'un travail scientifique

> JB: La structuration renvoie à l'aspect intellectuel de la chose, c'est à dire quelle est l'idée derrière et la structure de pensée du dossier, alors que la mise en forme relève plus de l'aspect technique. On demande toujours que soit respecté un certain protocole de rédaction, mise en forme de situation, de références etc., donc la partie cohérence interne du dossier, intellectuelle, est idéalement faite, tandis que la mise en forme est faite de manière inégale selon les coordonnateurs.

> EN: C'est un rôle de contrôle d'une certaine façon. Ce n'est pas un rôle scientifique, comme l'a dit Jean-Benoit, la cohérence intellectuelle est assurée par le coordonnateur, et quand on reçoit le dossier on part du principe que cela a été fait. Donc nous la mise en forme, c'est une espèce de contrôle général de la conformité des textes qu'on reçoit avec le protocole de la revue.

> JB: S'assurer aussi que les références soient faites de manière rigoureuse, pour une question de légitimité. C'est une forme de légitimation d'adapter le contenu à un contenant qui soit stable.

> EN: C'est une étape de polissage mais aussi une étape de levée de problèmes qui n'avaient peut-être pas été perçus à l'évaluation. Ce sont des problèmes de présentation mais qui en cachent généralement d'autre, des problèmes de rigueur : ce n'est pas le bon numéro de revue, de page etc. Ça soulève d'autres questions.

> JB: On parlait de légitimation tout à l'heure, l'état matériel dans lequel nous arrive le dossier constitué par les coordonnateurs en disent très long sur le travail intellectuel qui est fait aussi. C'est à dire que lorsque les articles arrivent avec différentes polices ou tailles de caractères, des systèmes de note différentes, on en arrive à penser que les textes n'ont pas fait l'objet d'une lecture par les coordonnateurs aussi fine et donc ça nous amène à penser que le texte de présentation n'aura pas une portée théorique ou heuristique aussi intéressante. Ça forme un tout, c'est comme un travail qu'on corrige, lorsque la langue est mauvaise souvent on a une argumentation derrière qui est fautive.

* Conversation:
  - inscription de la conversation : oralité du comité éditorial, transcrit en procès verbal
  - engagement du comité
  - plaisir du travail intellectuel lors des comités, mais pas de valeur pour l'extérieur

> N: Donc le comité lit l'ensemble du dossier, et vous produisez des rapports ?
> EN : Non, c'est oral, et on fait de très longs procès-verbaux, parce qu'on résume ce que chaque personne a dit.

> EN: Pour le reste, il me semble que le comité s'implique, plus que je ne le pensais au départ. Je trouve qu'on a un comité qui fait un vrai travail éditorial, c'est à dire que les gens s'expriment rarement autour de la table pour dire oui ou non, surtout non. Les non sont toujours justifiés, et la plupart des opinions émises sont des suggestions pour améliorer les choses. Et des suggestions qui peuvent être extrêmement précises. Ça je crois qu'on ne peut pas trop l'améliorer.

> EN: Les réunions ont une grande valeur scientifique. Ça fait partie des moments de la carrière où j'ai le plus de plaisir à travailler, le plaisir intellectuel est majeur dans ces réunions. Parce qu'on lit ensemble, on se confronte, on discute, les désaccords sortent, comme il faut arriver à quelque chose au bout du compte on est obligés de les analyser, donc les présupposés derrière les désaccords apparaissent. Je pense qu'on fait du vrai travail intellectuel. Maintenant ces PV ils sont rédigés en termes fonctionnels, je ne crois pas qu'ils aient un intérêt hors de la revue. Mais je souhaite à toutes les revues d'avoir des discussions aussi animées, où les gens s'engagent pour de vrai.

* financement
  - pression pour faire un dossier de pauvre qualité

* légitimation
  - des dossiers et des auteurs, mais aussi des problématiques.
  - l'évaluation interne (du dossier) est la plus importante, elle légitime déjà le dossier (avant évaluation ext.)
  - tout le processus et le travail éditorial est de la légitimation
  - processus en double aveugle produit de la légitimation
  - impossible de sortir de la modalité double-aveugle
  - légitimer la discipline et ses problématiques

> EN: On joue un rôle de légitimation du dossier, on fait exister des dossiers, on fonctionne encore beaucoup comme ça. Donc on joue un rôle de légitimation de problématiques. Ce faisant on légitime des auteurs, à commencer par les coordonnateurs qui ont imaginé et monté ce dossier, et puis des auteurs d'articles.

> EN: Pour l'envoyer en évaluation, on estime que le dossier est suffisamment valable pour qu'on l'envoie, à la limite on pourrait imaginer qu'à ce moment-là on dise que ça ne va pas.

> EN: Si vraiment c'est d'une qualité qui pose problème, j'imagine que je me serais retournée vers le comité pour leur demander leur avis, mais que ça ne serait pas parti en évaluation. Donc l'envoyer en évaluation externe, c'est une autre étape de la légitimation. Accepter sa publication aussi. Je pense que tout le travail éditorial qu'on fait, à la fois intellectuel avec le comité, et lorsque je négocie avec les coordonnateurs, et puis tout le travail de relecture etc., c'est aussi de la légitimation.

> EN: Ça nous semble pas fonctionnel parce qu'on n,est pas dans cette logique-là. Ce qui nous ramène à la légitimation. Je pense que pour nous les évaluations internes et externes, c'est vraiment un processus de légitimation. Et ces verrous qui font que les gens ne communiquent pas entre eux, ou à travers nous, sont sensés garantir que le procédé reste anonyme, externe, et que donc les évaluateurs externes évaluent bien des textes, des problématiques et non des gens, et qu'on soit dans un processus de légitimation.

> EN: je crois que notre logique de chacun sa tâche, qui est peut-être un peu raide mais qui a des raisons, fait que c'est très difficile de rendre ce travail accessible sans brouiller les cartes. Si on publiait ce genre de chose, il faudrait sortir de l'évaluation anonyme, parce que les gens se reconnaîtraient dans la discussion sur leur texte, et toute l'idée qu'on s'est fait jusque-là de l'évaluation par les pairs et des garanties qu'elle offre, tomberait. [...]
> JB: C'est un peu le résidu de ce qui reste comme truc final, c'est-à-dire la revue publiée. C'est comme quand on écrit un essai ou un texte ou un roman, les 1500 pages de notes et de lectures qu'on a fait sont en archive quelque part mais ne participent plus du projet final, publié.
> EN: Donc non, je ne crois pas qu'on puisse rendre ça public sans affecter un processus qui est efficace parce qu'il est comme ça.

> EN: De première légitimation. Et c'est vrai que dans la construction des sous-champs qui constituent la discipline, à mon avis on intervient. C'est à dire qu'en autorisant un dossier, en le légitimant et en le rendant le meilleur possible, on légitime des problématiques. On ajoute potentiellement au champ un sous-secteur qui pourra se développer.
> [...]
> Je crois qu'on a un rôle de constitution de la discipline. Je crois que les revues servent à ça. Elles sont les premières occasions de tester de nouvelles approches, de poser de nouvelles questions, d'explorer un nouveau sous-champ. Je crois que c'est dans les revue que la discipline bouge. Pas forcément de façon spectaculaire, mais il y a toujours du mouvement.

* rôle de l'éditeur de la revue
  - la revue subit la pression des injonctions à publier.
  - valeur du travail éditorial
  - qualité vs quantité, le travail bien fait, une position à tenir

> EN: Mais, par exemple dans le dossier qu'on a réussi à remonter et à rendre potable, là à mon avis il y a des gens qui sont complètement sous la loi de l'obligation de publier, et qui, s'ils avaient 6 mois de plus, feraient de bien meilleures choses. Et les revues n'ont pas le choix d'être le réceptacle de ce flot de communications trop rapide.

> EN: Je pense que notre grande valeur réside dans la qualité du texte, parce qu'on pense qu'écrire avec précision et élégance garantie notre rigueur scientifique.
> JB : Ne serait-ce qu'à la révision linguistique, la qualité du texte mais aussi la valorisation du propos. On pourrait ancrer ça sur ce qu'on disait tout à l'heure, de l'organisation subventionnaire qui valorise le contenant plus que le contenu, mais la valeur que représente le travail de la lecture littéraire, le travail que ça représente d'investir un texte et une pensée et de la faire travailler et d'y inscrire sa propre subjectivité. Ça ne se calcule pas, mais ça se perçoit diffracté et multiplié par des valeurs externes, le comité, notre propre travail, le travail matériel que font les PUM sur l'objet papier et numérique. Cette valorisation-là du travail que représente la lecture des œuvres de fiction et des travaux théoriques qui, rassemblés, font des articles et des dossiers, est mise de l'avant quand on ouvre un dossier et une présentation.

> EN: je crois que l'idée qu'en raffinant le processus d'écriture on raffine l'analyse, le travail d'artisan qu'on a beaucoup aimé, dont on s'est fait dire qu'il était inutile à deux reprises par l'organisme subventionnaire, je crois que ça, il n'y a pas de marche d'évolution. Je crois que c'est ce qui produit la revue telle qu'elle est et que ça va bien au-delà d'une facture de propreté au premier degré, ça va plus loin, ça va dans le fait de pousser la réflexion des contributeurs quels qu'ils soient aussi loin que possible, en améliorant leur texte. Ça je ne crois pas qu'il soit souhaitable que ça change. Je crois qu'il faut le porter, rappeler que dans un univers où on nous demande de publier beaucoup, nous on répond qu'on veut publier bien, quel que soit le support, que publier bien ça prend du temps, de la collégialité, ça ne peut se faire tout seul. Il faut tenir cela, même si les choses ne vont pas dans ce sens, même si les dictats plus ou moins implicites qu'on est obligés de déduire des réponses de nos organismes subventionnaires, je crois qu'il faut le tenir, garder ces valeurs qui sont l'identité de la revue.

* numérique:
  - la diffusion numérique amène un bassin de lecteurs mais aussi d'auteurs
  - pas d'incidence du numérique sur le travail éditorial
  - diffusion num. a pour effet l'éclatement du dossier en article
  - la forme numérique n'est pas un sujet

> EN: Je dirais que la forme numérique est plus accessible, et je suis sûre que c'est par le numérique que passe la majeure partie de notre lectorat, et même probablement du lisible. Par exemple le flot d'articles libres que l'on a, c'est des gens qui voient la revue sur Érudit et qui ont envie d'y publier. J'en suis à peu près sûre. Donc il ne fait pas de doute qu'il est nécessaire que nous soyons diffusés sous cette forme, et que c'est la condition d'existence d'Études Françaises dans le champ disciplinaire.

> EN: Que les textes soient papiers ou distribués sur la plateforme Érudit, notre travail est le même.

> JB: Oui, et le mode de consultation est différent aussi. C'est aussi la question du dossier, de la découvrabilité des textes, les effets de parenté, de voisinage, de certains hasards de lorsqu'on consulte la version papier, on voit tout de suite quels textes sont voisins, on voit un texte d'un autre auteur sur un sujet potentiellement similaire qui est dans un dossier sous la direction de quelqu'un d'autre, alors que si mes mots-clefs de recherches Google Scholar sont pas les bons je ne verrais pas ces articles-là, alors qu'ils vont potentiellement m'intéresser.

> EN: Je crois que c'est rentré dans les usages [les usages numériques de la revue]. Quand on a des retours, ce n’est pas sur la forme, et ce n'est pas possible de savoir si le retour se fonde sur la forme dans laquelle les gens ont pris connaissance du dossier, à moins que ce soit explicité, la forme n'a pas d'importance majeure dans les retours. Je crois que chez Études Françaises, ces deux modes de diffusion n'ont jamais été opposés.

> EN: Mais je ne crois pas que la qualité du travail soit différente. C'est beaucoup rentré dans les mœurs. On a été amenés à réfléchir à la différence quand les organismes subventionnaires ont sous-entendu de toutes sortes de manière, que le numérique demanderait moins de travail. Nous on a toujours soutenu que non. La somme de travail d'un article papier ou sur Érudit, c'est la même somme.

* financeur (FRQ)
  - reconnait le travail de diffusion d'erudit, mais pas celui sur le texte

> EN: cette somme de travail [travail d'érudit] est reconnue par le FRQ, alors que la somme de travail sur le contenu n'est pas reconnue par le FRQ. Il reconnaît la somme de travail sur le contenant et fait comme si le contenu était gratuit à produire. En 5 ans on a eu le temps de s'habituer, ça a cessé d'être une découverte.


# Conference

Je vous présente les premiers résultats de l'observation des pratiques éditoriales de 6 revues de SHS menées dans le cadre du projet de recherche Revue 2.0.

Revue 2.0 a été lancé en octobre dernier, avec pour objectif de :

-

La première année est consacrée à l'observation des revues.

## Méthodologie de l'observation

1. récolte de documents génériques et traces du travail éditorial --> retracer la génétique d'un article, c'est-à-dire ses différents états
2. questionnaire sur le protocole éditorial, pour établir pour chaque revue :
  - les différents acteurs,
  - les modalités d'évaluation,
3. entretiens semi-dirigés auprès des direct·rice·eur·s de revue et des secrétaires de rédaction

## Résultats préléminaires

ce que je présente aujourd'hui, sont les premiers résultats de ces entretiens.

## Thématiques

Wordreference :
- éditorial > ?
- évaluation > review
- évaluateur > reviewer,

### Internal/external - fac/close

there is a particularly notable tension between what is considered internal or external to the journal. It plays on different levels and different actors according to the editorial process. From one journal to another, the reviewing experts for instance can be considered close or far from the journal.

The external can be the authors, the reviewers, the issue directors. The internal is then caracterized by the few individuals working in the journal. Contributors are let aside and are not presented as internal.

This gives a rather peculiar image of what is a journal for the one working there. We often hear about the journal community, made of readers, regular authors, reviewers, issue directors, none the less, this community is kept away from the inner family. Often in purpose, with the idea that the fewer porosity there is, better the journal can maintain its scientificity.

The degree of collaboration with the issue director, as well as her degree of autonomy putting the issue together wil caracterize the porosity between the internal and the external.

### Légitimation / Evaluation

où se situe l'autorité ? qu'est ce qui la fabrique ? qu'est ce qui vient, dans le processus, légitimer un article, un auteur, ou même plus largement la revue elle-même.

_Where is the authority located ? what produces it ? what, in the the editorial protocol, legitimates the article, the author, or more generally the journal itself ?_

Les personnes interrogées font consensus sur le fait que la légitimation découle des différentes instances de décision qui ont lieu tout au long du protocole éditorial. Selon la revue, la légitimation va se jouer à des degrés et des niveaux différents.

Par exemple, Etudes Françaises, dont l'objet éditorial principal est le dossier et non l'article, opère une première légitimation lors de la sélection d'un projet de dossier.
Une autre légitimation est évidemment liée à l'évaluation, dont on distingue l'évaluation interne ou externe. Mais cette légitimité de l'évaluation ne prend véritablement sens que lorsqu'on considère qui valide les évaluations, et comment. Et l'on se rend compte ainsi, d'un journal à l'autre, que les évaluations n'ont pas du tout la même valeur de légitimation.
Par exemple, les évaluations externes dans la revue Intermédialités sont généralement prises au pied de la lettre, et l'anonymat des évaluateurs demeurent tout au long du process. Leur valeur de légitimation est alors très haute. Au contraire, sur d'autres revues, le comité va pouvoir contrebalancer les évaluations externes par des discussions internes en comité (le comité peut alors connaitre ou non l'identité des évaluateurs), par des évaluations internes, elles aussi pouvant être soit en simple aveugle (l'auteur est anonymisé), soit de manière ouverte (l'évaluateur connait le nom de l'auteur). Selon la revue, sera considéré comme décisive l'évaluation interne ou encore, la discussion interne du comité.
La décision peut encore se complexifier avec l'intégration du directeur de dossier dans la négociation.

Ainsi, malgré ce schéma constant d'évaluation externe, parfois interne, l'instance de légitimation ne se situe pas au même endroit d'une revue à l'autre.

la légitimation n'est pas une science exacte, elle est le résultat d'une multitude d'acteurs décisionnels.


### Legitimation de la revue

- le papier : s'appuyer sur l'héritage du papier
- les procédures d'évaluations : reproduire les modèles des revues papiers à haute valeur symbolique.
  On voit bien que ce que l'on va chercher c'est avant tout du symbolique. Est ce que ce symbolique introduit une scientificité supplémentaire, ce n'est pas dit. Un degré de scientificité intervient certes sur la rigueur du travail éditorial du texte (rigueur syntaxique, rigueur de construction), de son outillage critique (notes, références précises). Mais on a vu que les évaluations sont largement négociées et sujettes à être remises en cause. Elles sont de l'ordre de la conversation davantage que de la décision.
- Ce sont les financeurs et les diffuseurs (qui s'alignent sur les financeurs) qui produisent ces injonctions à adopter le modèle classique d'évaluation en double aveugle. Ils produisent la quadrature d'un cercle cohérent d'un point épistémologique mais dont le fondement épistémologique est questionnable.

### Evaluation

Les évaluations ont alors valeur d'alerte, même si parfois une évaluation négative va aussi être un signal pour valider l'évaluateur. A l'inverse, deux évaluations externes positives font souvent office de décision

L'évaluation a aussi cette valeur d'accompagnement, d'amélioration de l'article : bienveillance, pédagogie > cercle vertueux

The interviewee are quite consensual about it : the legitimation occurs through the different decisions


## Quelques réflexions

1. que, même au sein des humanités, les modèles éditoriaux sont aussi multiples que les revues. Il n'y a pas une revue équivalente en terme de vision, de mission au sein de la communauté scientifique, mais plus précisemment, en terme de critères de scientificité, de légitimation, de construction de l'autorité, parfois même sur le plan éditorial.
2. que le numérique est considéré avant tout comme un média, un moyen supplémentaire de diffusion, n'ayant eu pour effet, du point de vue des revues en tout cas, que de modifier et/ou d'augmenter le bassin de lecteur. Pour les revues consultées, le travail éditorial n'a pas évolué.
3. les revues sont attachées au principe d'une évaluation fermée (double-aveugle) la légitimation se base  il existe un certain conservatisme

**Identité** (du modèle éditorial à l'identité éditorial)
Une revue n'est pas simplement un propos adressant un champ disciplinaire. Son identité se construit dans chacun des aspects de son processus éditorial. L'identité est donc à la fois méthodologique, discplinaire, éditorial, epistémologique.

Alors, plutôt que de parler de modèle éditorial, je pense que je vais désormais parler d'identité éditorial.

**Négociation**
un autre aspect important est la place de la négociation dans ce processus. l'édition n'est clairement pas une science exacte, et les jugements multiples sur les articles se croisent au travers de différents espaces conversationnels, parfois très normés (par des formulaires, de l'anonymisation, des rapports d'évaluation), parfois très informel (les comités de rédactions), où l'oralité devient un élément essentiel.

  - rapport d'évaluation
  - notes en marge d'articles
  - emails (beaucoup d'emails)
  - PV de comités
  - discussions à l'oral lors de comité
  - Téléphone

Ces lieux sont en fait des lieux de négociations, où les jugements se croisent, se disputent, où se créent un consensus sur l'acceptation d'un article ou non, et le plus souvent, sur la manière d'en améliorer la teneur, le propos, ou la scientificité (références).

**Protocole de communication**
le protocole éditorial peut être vue comme une sorte de protocole de communication, plus ou moins complexe, mais surtout relativement defectueux au regard des principes de communication.

## Questions à se poser pour aller plus loin dans le projet

la second phase du projet consiste à concevoir des expérimentations et à mettre en place des outils et des protocoles différents.
Ce que montre nos premiers résultats, c'est que ces expérimentations ne pourront pas être conçues de manière générique, et que toute intrusion dans le processus éditorial d'une revue doit en fait se faire en adéquation avec l'identité complexe de la revue.
Il ne suffit pas de décréter l'utilisation de tel outil, ou l'instauration d'un modèle d'évaluation différent. Faire bouger ces lignes en interne, suppose d'impliquer l'équipe éditoriale dans une co-conception, afin d'adresser les véritables besoins d'une part, mais aussi d'intégrer véritablement cette identité dans l'expérimentation.

Chaque revue aura d'ailleurs sans doute un besoin à adresser correspondant au mieux à son modèle et à son identité.

### besoins

- limitation des schémas de diffuseur
- rupture entre le travail scientifique éditorial des éditeurs, et le travail d'encodage et de diffusion des diffuseurs
- lien au lectorat (abasourdi par leur méconnaissance de leur lectorat) > dans le sens d'un développement de la conversation, dans des espaces dédiés.
- nouvelles formes de légitimation adaptées à leur identité
