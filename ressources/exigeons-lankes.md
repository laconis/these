# Lecture de exigeons axée conversation

## Dans le glossaire :

### Conversation
conçoit les bibliothécaires comme facilitateurs et facilitatrices de conversations.

> > Les adeptes de la bibliothéconomie nouvelle approchent leur travail en tant que facilitateurs et facilitatrices de conversations. Que ce soit par le biais des pratiques et des politiques, des programmes et des outils qu’ils et elles développent, ces bibliothécaires cherchent à enrichir, enregistrer, conserver et disséminer les conversations qui ont cours dans leurs communautés [@lankes_new_2011, p.2].

> il signale que sa pensée tout entière repose sur la théorie de la conversation développée par le cybernéticien britannique Gordon Pask (1928-1996). Dans l’ouvrage Conversation Theory (Pask 1976), Pask avance que tout apprentissage et toute connaissance sont générés lors d’une situation d’échange langagier visant à parvenir à une compréhension partagée entre ceux et celles qui y participent. [@lankes_exigeons_2018, glossaire.html]

Compréhension partagée > c'est une connaissance : peut être individuelle ou collective.

> chaque conversation a ceci de déterminant qu’elle contribue à informer, voire à transformer, la réalité de ceux et celles qui y prennent part.

### Connaissance

> > Pour les bibliothécaires, ce que j’appelle « connaissance » signifie le réseau de croyances interreliées qui orientent notre comportement. Et ce réseau se construit via les conversations et les actions que nous menons de notre côté, mais également au sein de nos communautés (Lankes 2016, 26).

> Cette définition pragmatique de la connaissance laisse intentionnellement de côté la question de la vérité pour se concentrer sur l’utilité du produit ponctuel d’un processus infini de transformation qui, à chaque fois, invite à l’action.

Cette phrase résume tout.. Contrairement au régime de vérité que l'édition savante est censée garantir, la conversation dont je parle s'insrit dans un processus infini de transformation, invitant à l'action, et susceptible de faire advenir le collectif.

La connaissance est relative (au contexte) et transitoire (liée à l'action). La connaissance est donc une expérience, individuelle ou collective.

les artefacts sont des objets qui résultent d'une activité de connaissance.

La connaissance est donc issue d'une conversation, avec soi-même ou autrui.
On retrouve l'idée que les bibliothécaires, comme les éditeurs, ont pour fonction première de créer les conditions de la conversation/l'appropriation, ou selon ses termes, de «faciliter».

## Chapitre Faciliter la création de connaissances
