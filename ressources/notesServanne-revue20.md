# 24déc2019
Problématiser davantage, notamment en revoyant le plan.

Mettre en avant la conversation dans la Méthodologie

- archives et questionnaire > inscription de la revue
- les entretiens > mise en conversation de la revue.

Déplacement : l'objet c'est davantage les éditeurs que la revue. Ca fait la spécificité de mon travail.
les diffuseurs ont stabilisé les revues dans des formats, en les condammant dans un "silence". L'analyse et les entretiens redonnent une voix.

Résistance non pas au numérique, mais à une forme de numérique, celle des diffuseurs quelles en sont les limites finalement ?

Mise en conversation des revues, avec les éditeurs (ceux qui la font). C'est un objet performatif, et non un objet de stabilisation de la connaissance.

modele idéal "inscrit" VS processus éditorial performé, pragmatique, et finalement conversationnel.

# Desscription du projet

Plus développer chacun des partenaires et problématiser le "corpus". Par exemple, reprendre des éléments de l'entretien avec Stephane P. Faire dialoguer les 3 partenaires principaux

description revues : titre, année de création, discipline/domaine, distinction de la revue, natif numérique ou non, quelle étape, quels besoins exprimés.

# Méthodologie

mieux problématisé avec inscription/conversation

# Entretiens :

mieux structurer : systématiser et appuyer la thèse (sur la conversation).

ne pas me retenir d'en parler (de la conversation)

La varia : ex meme de la conversation qui a été enterré avec l'institutionnalisation de Sens Public dans Erudit.
le fil de l'eau, ca inscrit la pensée des chercheurs dans une forme d'actualité.
le varia comme le degré 0 du travail éditorial : publier juste pour publier. Le dossier a eu tendance à se scléroser.

le fil de l'eau donne du sens au varia, mais le varia perd son sens dans erudit ou dans tout dossier.

----
# 26 février 2020 -  sur la conversation


**free_servanne 15:08** j'aime bcp la fin du passage
> on retrouve ici tous les ingrédients de notre hypothèse, à savoir le fragment écrit et réécrit dans un processus continu (un mouvement) d’éditorialisation. On retrouve cette écriture-milieu dont les variations de fragments sont aussi les variations du milieu lui-même. Rassemblés parfois malgré eux par le biais de leurs traces numériques – qu’elles soient signature visuelle d’une image ou métadonnées accompagnant les fragments – individus et fragments sont indexés, collectionnés et finalement mis en conversation.

j'ai l'impression que tu commences à relier un peu différemment éditorialisation et conversation
ce ne serait plus la conversation  qui changerait / trouverait un régime particulier dans un contexte numérique d'éditorialisation
c'est l,éditorialisation qui devient une forme de conversation
je sais pas si je te surinterprête ou si je comprends enfin là où tu veux en venir

**nicolas 15:07** oui c'est tout à fait ça, mais pas toutes les éditorialisations, tant le concept s'est étendu. Certaines éditorialisations, comme les enmi12, ce qu'avait vu Louise, ou Instin, ou d'autres encore. Il y a là une certaine mise en espace, une bienveillance, qui permet la conversation. et la finalité de cette conversation n'est plus spcifiquement une production documentaire, mais un collectif (en devenir)

**free_servanne 15:08** crois-tu que ça vaudrait la peine de mieux le formaliser dès le début de ce chapitre ? ou alors c'est fait mais je suis passée à côté ?

**nicolas 15:09** non je  l'ai pas vraiment mis en avant au début, mais je peux réécrire l'intro du chapitre avec cette idée là. bon c'est u peu là quand meme, mais plus clair avec le déroulé que j'ai fait

**free_servanne 15:09** je crois en plus que ça pourrait enrichir tes autres parties. Marcello te disait de revenir sur l'éditorialisation, dans les parties précédentes. Ce serait un bonne occasion
pour moi c'était pas clair du tout

**nicolas 15:09** ok, mais ca l'est plus dans la partie "Louise"

**free_servanne 15:10** en fait, tu proposes un modèle d'éditorialisation que tu appelles conversation

**nicolas 15:10** https://laconis.frama.io/these/partie3/partie3-0.1.html#partie3-0.1-vers-une-herm%C3%A9neutique-collective
oui c'est ça
et ca découle directement de mon analyse de Louise

**free_servanne 15:11** ok attends c'est quoi ça ?

**nicolas 15:12** c'est la réponse Q1 à mon champs 1. que j'ai réécris pour l'article Sciences du design.
c'est ce que je pensais mettre juste avant "de l'écriture au collectif"
ici https://laconis.frama.io/these/conversation/conversation-0.1.html#vers-une-herm%C3%A9neutique-collective
c'est une analyse spécifique à Louise, c'est pour ca que ca va un peu vite parfois avec Louise dans ce que j'ai écris ce mois ci

**free_servanne 15:14** est-ce que "De l'éditorialisation conçue comme une conversation" (meilleurs titre à trouver) ne serait pas du coup ton titre, plutôt que "concept"

**nicolas 15:14** l'idée est aussi de s'appuyer sur ce texte

**free_servanne 15:14** oui oui je comprends
mais du coup je l'avais plus en tête donc ça change aussi ma perspective
j'essaie de penser un peu le tout là

**nicolas 15:15** oui, ou meme, de "l'édito à la conversation :  de l'écriture au collectif"
oui c'est ca que j'essaie de faire "penser un peu le tout"

**free_servanne 15:17** je me demande si tu pourrais pas insérer des expériences au fur et à mesure. COmme des sortes d'interludes allant vers des expériences de plus en plus complexes / réussies

### du coup en en parlant :

faire un développement conceptuel, puis un cas d'étude.

Pourquoi la conversation ? > utopie de version 0, irait bien là, ce qui a marché et ce qui n'a pas marché, échec ou pas forcément ?

version0: livre qui se propose come l'amorce d'une conversation, tout le procesus éditorial, arrimé avec des outils et protocoles, sur lequel tente de se greffer sur la conversation.

- Version > protocole
- ENMI > appropriation
- Instin > Milieu, dépasser l'outil pour penser le milieu
- Publishing > collectif, advenir..
- ecrire les communs > gouvernance
- Manifeste CCLA - bienveillance avec  interculturalité, multiplicité linguistique, travail invisible

## 4 mars - sur v0
faire des parties

sous-partie protocole :

la conversation comme modèle d'éditorialisation peut s'appiuyer syr diff. choses :

1. outils protocole : git.

qu'est ce que je veux dire  ? avoir une hypothèse de départ:

apport théorique à faire : la conversation a échoué avec les chercheurs, mais notre équipe éditoriale : il y a un côté pédagogique qui a été très fort. et meme d'appropriation.

revenir sur ce qui a été le collectif : pédagogique : éditeurs en apprentissage.

quel collectif se dessine.

## 7 mars.
Servanne :
en tant qu'enseignante, ca a changé mon rapport aux étudiants : exp. d'enseignement très forte. On balise le terrain pour les étudiants, on sait où on va. Mais là, on leur donne de la responsabilité, relation plus horizontale, ca aide à la professionalisation, c'est déjà là pendant le stage, mais ca change la facon de voir l'enseignement. Les responsabiliser plus. EN cours, puis en stage (c'est bien/pas bien, validation), puis ecridil : ils proposent des élements : ils deviennent des collaborateurs, et non comme étudiants.

bascule après le préalable du stage et du cours sur l'édition.

pour PS:
joanna drucker : matérialité performative : plus appropriée.
lien avec le protocole git.
l'énonciation performative de ruffel vient combler l'éche de la matérialité performative. Drucker parle plus des interfaces.

servanne l'utilisait pour questionner la question de l'agentivité, affordance (texte pour écrire ce qu'elle a dit lors du colloque Invention littéraire des médias, remédiation conçue comme un geste)

point 7 du déroulé ps: important de faire preuve d'humilité pour reconnaitre les limites : clairvoyance sur les limites et les défis. Car thèse très idéologique/idéaliste, mais aussi très pragmatique. Montrer ce qui a marché et ne pas marché : Lucidité. Ces expériences sont un peu folles, ont donné de très belles choses. [Pour l'intro]

## 19 mars

sur PS: la question de la matérialité performative/énonciation.
réponse de servanne très phénoménologique: d'où émerge le mouvment et quel type de mouvement ?

centralisant/centrifuge > l'outil va favoriser une dimension performative (affordance)

qui est ce qui _fait collectif_ ?

le verbe se conjuguent, "nous" nous faisons le collectif

aller voir sur le dissensus (Derrida), la communauté qui vient (Agamben), Rancière.

énonciation, matérialité : _faire_, mais il doit se conjuguer. nous faisons collectif, et là, la problématique est sur le _je_ et le _nous_.

faire collectif est programmatique, car à l'infinitif. il faut le conjuguer !
l'actualiser!

ce qu'il ne s'est pas fait, c'est la conjugaison.

La démocratie est-elle à venir ? (Rancière)
dissensus de Derrida

l'a-venir, l'à-venir de la démocratie. le sujet qui fait comme s'il était le démos.

structure: discuter à la fois drucker/souchier et Lionel

1. à la recherche du collectif : montrer l'échec
montrer les raisons de l'échec  : il n'a pas fait collectif
2. nous : faisons collectif, expliquer cette conjugaison
3. échec, mais il en ressort qlq chose: le GI la création d'une personne, un éditeur : revenir sur la fonction éditoriale, et donc sur la question des revues.

le mythe de l'éditeur :

- [ ] lire sur le dissensus derridien
- [ ] lionel : prend acte de la critique de la notion d'espace public d'habermas, car il y a besoin d'espaces/communautés alternatives
- [ ] le groupe n'est pas un échec car ne pas s'entendre fait aussi parti d'un idéal : débat, dissensus. chacun a eu sa parole. C'était un brouhaha. (VS le choeur, canon, polyphonie du groupe de Giasson)
- [ ] modele conversationnel qui permettent le dissensus
- [ ] l'échec: chatelier en éditeur : entretient des lisières alors que PEROU ne fonctionne pas en lisière : très totalitaire. Pour son projet, sebastien se pose obligatoirement en sauveur, il a besoin de l'opprimé : système très paradoxal qui s'entretient. Statement : sauver l'opprimé. On le retrouve dans la meme critique de Spivak à Foucault.

### run
