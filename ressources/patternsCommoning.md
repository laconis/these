# Lecture de Patterns of commoning (2016)

[alternative à la science :]{.note}
> [This perspective may abandon the universal regularities that conventional science aspires to identify, but it opens up a rich, empirical field of vision for devising innovative solutions to old problems.]{lang=en} [-@bollier_patterns_2016, p.3]

[le nous : ]{.note}
> ["I am because we are", as the Nguni Bantu expression "Ubuntu" puts it.]{lang=en} [-@bollier_patterns_2016, p.5]

> [The commons paradigm helps us recover some elemental human truths -- that we learn to be individuals through relationships and that autonomy itself is _nested within_ a web of relationships. Autonomy can only be learned and lived through relationships, and commons provide an appropriate framework for this to happen.]{lang=en} [@bollier_patterns_2016, p.5]
