# Brainstorm Conclusion


## - méthodologie :
    - p1: tournant numérique d'une médiologie numérique
    - p2: mise en conversation des éditeurs pour produire des connaissances
    - p3: recherche création.
## - reprendre le parcours de la thèse : format, espace (d'autorité), collectif
## - identifier les concepts clé et les retraiter
    - partie1 :
        - format et media : la revue n'est qu'un media, cad est une dynamique qui survient de conjonctures
        - vers une médiologie du numérique ?
    - partie 2:
        - la conversation est partout, les lsh sont des sciences de la controverse
    - partie 3:
        - **ouvrir les protocoles**, les diversifier, appliquer des recettes qui fonctionnent ailleurs
          - transparence et élargissement : intégration des savoirs
          - partager la gouvernance du protocole
        - **faire science autrement** : s'attacher aux problématiques de la société
          - déhiérarchisation des savoirs
          - dynamique collective de production de savoir
          - contester les modes de légitimation (d'évaluation) : pour une «définition plurale, négociée et pragmatique (elle-même évaluée à partir de ses effets) des modes d’évaluation et de valorisation des différents types de recherche»
          - collectif hybride : individus + connaissances
          - slow revue : ouvrir au sein des revues scientifiques des espaces alternatifs et collectifs, accordant aux textes une nouvelle temporalité inscrite dans la durée
          - reprendre la bienveillance
        - «**À travers la conversation**, je tente d’envisager une approche de la pluralité, intégrative des savoirs, en reconnaissant la nécessité de leur diversité que les processus de légitimation institutionnalisés ont malheureusement tendance à écarter pour des critères d’objectivité et de scientificité.»
        - Mouvement et circulation : «remettre en mouvement les ressources et les connaissances en en fluidifiant la circulation»
        - écritures collectives pour des problématiques collectives > écologie politique
## - identifier des limites
    - il ne s'agit pas de mettre l'article à la poubelle, il s'agit d'en imaginer de nouvelles formes d'appropriations et de nouvelles formes de production : collectives, fragmentaires, etc.
- ouvrir sur qlq chose post-limites
    - rhétorique du nous, de Louise M.


# Notes 7 juillet 2020


### inquiétude, illetrisme, gafam, sur la fabrique de l'autorité

De quoi cette thèse est faite ? qu'est ce qui se cache derrière cette vision de refonte de la revue scientifique ?
cette thèse est un parcours en trois temps, un premier temps est celui d'une interrogation, une inquiétude pourrait-on dire en reprenant l'expression _matter of concern_ que Latour articuler avec le _matter of fact_. Cette inquiétude est celle du chercheur et du praticien, celle aussi de l'ingénieur dont l'un des axes d'activité était la contribution sur les données patrimoniales : cad déjà un geste d'ouverture à des communautés non-spécialistes sur des contenus auparavant fortement soumises à une autorité.
L'inquiétude aussi d'une génération qui a pu voir enfant l'arrivée de l'ordinateur personnel et son appropriation par test/erreur successif, l'apprentissage du système de fichier, de la commande, du script. Ces petites choses qui permettent d'opérer la machine et de maintenir ainsi une relation plus saine, maitrisée entre l'homme et sa technique. L'acquisition d'un savoir-faire.
L'inquiétude ainsi de voir les chercheurs dans un illetrisme souvent profond face à la chose informatique. Illetrisme qui s'applique à plusieurs niveaux, y compris culturel. Il devient plus que problématique à l'université censée prendre soin de la chaine de l'écrit. L'allégeance à des sociétés privées. Qui nous a appris à écrire et sur quoi ? Ce n'est pas un hasard si les institutions académiques ont conservé des presses. Les éditeurs de manuel discutaient avec les équipes pédagogiques, et les équipes pédagogiques conservaient la maitrise de l'apprentissage de l'écrit et de ses techniques. Qui n'a pas subi l'injonction des petits carreaux, du stylo à plume, des 4 couleurs, etc ? Aujourd'hui, ce sont les gafams qui nous divulguent comment écrire.
L'inquiétude donc de voir l'institution et ses serviteurs empêtrés soit dans un conservatisme, soit dans l'illetrisme. L'université est aussi l'institution qui abrite des veilleurs.
Cette inquiétude m'a amené à questionner les moyens de production de la connaissance, et en particulier la revue.

Il y a dans cette thèse l'inquiétude que l'institution n'ait pas pris la mesure des changements à l'oeuvre aujourd'hui. Les modalités de la production de l'autorité ont changé, et les nouveaux acteurs, que d'aucun nomme barbares, ont déjà largement la main sur ces modalités de production. Ils structurent aujourd'hui l'espace du savoir, bien davantage qu'un éditeur puissant, ou qu'un recteur d'université. Ils ont su imposer en quelques dizaines d'années parfois en moins d'une demi-douzaine un empire d'écriture qu'aucune institution ne peut concurrencer. Ces écritures entrainent d'autres écritures, et fournissent aux réseaux de neurones qui les traitent une mine inédite d'information. En maîtrisant les tenants et les aboutissants de la fabrique de l'autorité, ces acteurs sont au sommet d'un pouvoir problématique.

### dynamiques contre-hégémoniques

Qu'y pouvons nous ? peut-on y échapper ? Qu'est ce que la revue scientifique peut opposer à un tel pouvoir.

Les dynamiques contre-hégémoniques pavent le terrain. Est-ce le rôle de l'université de leur emboiter le pas ?

### Le deuxième temps

le 2eme temps a été celui de la discussion, parler à ceux dont je parlais, établir un dialogue sur leurs pratiques et leurs visions. La fabrique de l'autorité est au coeur de leurs inquiétudes, pourtant la perte de pouvoir est manifeste. Pouvoir symbolique par exemple, relégué à un brin au sein d'un bouquet. Soumis aux contraintes des diffuseurs : formalisation des contenus, mais aussi à la perte de contrôle sur les formes (cf ch2 - #ruptures-dans-la-chaîne-de-production-de-lédition-scientifique-de-la-perte-de-lénonciation-éditoriale).

# Premier jet fin aout

J'ai introduit cette thèse avec une polémique. De manière récurrente, des chercheurs et des chercheuses se rebellent contre les modalités de la publication scientifique institutionnelle. Dans ces prises de parole radicales, le constat est clair : les revues n'assument plus correctement leur mission de médiation des connaissances, elles n'ont donc plus lieu d'être. Les raisons évoquées sont nombreuses, j'ai dressé à ce propos un premier paysage en effet peu engageant. Il y a tout d'abord un déphasage éditorial lié à la conservation presque à l'identique des formes article et revue, héritées du papier et de l'imprimé alors même que l'ensemble des activités humaines scripturales, documentaires ou communicationnelles se réinventent depuis une vingtaine d'années dans des formes proprement numérique. En lien avec une innovation éditoriale défaillante, j'ai montré que l'institution académique, pourtant garante de la culture écrite, avait en quelque sorte démissionné de ses responsabilités vis-à-vis de la chaîne de production de l'écrit. Elle laisse en effet aux GAFAM le _soin_ presque exclusif des outils et des pratiques scripturales dans l'environnement numérique, y compris au sein de l'université. Autrement dit, les universités et les chercheurs ne seront plus les prescripteurs des techniques intellectuelles de demain. Plus préoccupant encore, ils en perdent l'expertise. Or on l'a vu dans les chapitres 1 et 2, le constat est le même pour les éditeurs scientifique. Face à une telle «\ prolétarisation\ »^[Je me réfère à nouveau à la définition qu'en donne @faure_proletarisation_2009, ou encore _Ars Industrialis_ [-@ars_industrialis_proletarisation_2008]\ : [«\ La prolétarisation est, d’une manière générale, ce qui consiste à priver un sujet (producteur, consommateur, concepteur) de ses savoirs (savoir-faire, savoir-vivre, savoir concevoir et théoriser).\ »]{.cite} Voir [sur Hypothesis](https://hyp.is/eb-0JuIdEeqHkWd_ZQahpw/arsindustrialis.org/prol%C3%A9tarisation).], il est effectivement urgent pour les institutions et pour l'ensemble de la communauté scientifique de _s'enlettrer_, d'acquérir une maîtrise minimale du milieu numérique, tant dans ses techniques que dans ses enjeux sociétaux et philosophiques, pour regagner la maîtrise de ses moyens de production. La littératie numérique constitue l'une des clés pour que les institutions de savoir à nouveau _prennent soin_ des modalités de production et de circulation du savoir. Par ailleurs, un tel chantier institutionnel se joue sur une évolution nécessaire des modalités juridiques et économiques de la publication scientifique, dont j'ai présenté les dérives liées à la marchandisation croissante depuis la moitié du 20^ème^ siècle, exacerbée par la numérisation des contenus. Tout est lié bien entendu, et lorsque Bernard Stiegler rejoue le mythe du _pharmakon_ à propos du milieu numérique, ce n'est pas tant pour en dénoncer la toxicité ou pour en louer les vertus, mais pour nous désigner responsables de son orientation.

L'enjeu sous-jacent au chantier éditorial et institutionnel s'avère être en fait épistémologique. Quels sont les contours du régime d'autorité et de vérité du milieu numérique ? Ou pour être cohérent avec ce principe de responsabilité, quels peuvent être ces contours ? De quel régime voulons nous ? Et quel rôle les revues peuvent-elles jouer dans ce monde à inventer ?

Pour répondre à ces questions, j'ai travaillé dans plusieurs directions.

Le second chapitre présente une enquête de terrain auprès de six revues de LSH, dont les entretiens avec les éditeurs et éditrices ont permis de renouer avec une dimension humaine, essentielle à la résolution de ces problématiques.
Dans ce chapitre, la revue est conceptualisée comme un espace, celui de la fabrique d'une part et celui de la collégialité d'autre part. Les conversations qui s'y tiennent dessinent plus nettement -- ou plus fidèlement -- que les protocoles éditoriaux la véritable structure de l'autorité, c'est-à-dire sa structure spatiale.
Car le travail éditorial se réalise dans une confrontation continuelle entre la subjectivité des textes et des idées et l'objectivité des protocoles éditoriaux. Cette tension ne se résoud que dans sa _négociation_, c'est-à-dire dans la collégialité pragmatique des conversations, qui révèlent in-finé la dimension collective du processus et de la décision éditorial·e.

Ce qui ressort alors de mon enquête est la pluralité des modèles éditoriaux et des modalités de la légitimation scientifique, au détriment du modèle idéal de scientificité telle qu'il est pourtant prescrit par l'injonction institutionnelle. Cette diversité éditoriale et cette pluralité épistémologique ne doivent pas être comprises comme un effondrement scientifique des LSH ou encore comme une crise de vérité. Ces subjectivités sont en fait la nature et la force de ces champs disciplinaires. Elles doivent être à tout prix entretenues. Elles suggèrent des espaces inédits, encore à investir, et la possibilité d'une innovation éditoriale aussi ouverte que celle que l'on peut observer hors du champ scientifique.
À nouveau, chaque éditeur et éditrice poursuit une vision propre et singulière, souvent porteuse d'innovation intellectuelle ou éditoriale, susceptible de réconcilier l'édition scientifique avec les pratiques contemporaines d'écriture. Reste pour ces praticiens à acquérir une littératie nouvelle, celle d'écrire, de lire et d'éditer le milieu numérique pour mieux l'_occuper_.

Dans le troisième chapitre, j'ai cherché à mettre en évidence les formes conversationnelles aux travers de divers dispositifs et protocoles conçus et expérimentés durant le doctorat.
Remises en perspective par des pratiques d'écriture et de publication hors de la sphère académique, l'analyse de ces expérimentations fait émerger une pensée du collectif bien différente de la collégialité pratiquée dans l'édition scientifique. Cette pensée m'a été précieuse pour esquisser une alternative et envisager une remédiation de la revue scientifique l'inscrivant pleinement dans la culture et les savoirs contemporains.
L'hypothèse d'un possible modèle conversationnel de communication scientifique, avancée à la fin du chapitre 1, s'est confirmée au fil des réalisations. D'un côté, la conversation épouse parfaitement les caractéristiques du milieu numérique, par sa nature fragmentaire, anthologique et performative. De ce point de vue, l'écriture conversationnelle procède d'une «écriture dispositive» dont j'ai montré d'une part la capacité à structurer le milieu d'écriture, et d'autre part la performativité vertueuse sur l'avènement d'un collectif. Ce _faire collectif_ déplace l'enjeu épistémologique, en devenant la vocation de la fonction éditoriale.

Pour atteindre cette nouvelle finalité, l'éditeur doit transformer l'injonction _faire collectif_ en performance collective\ : _nous faisons collectif_, en en réunissant les conditions de possibilité. Et c'est justement par la conversation -- par toutes les conversations -- que ce _nous_ devient effectivement _possible_.
Or la conversation dépend de l'ouverture et de la transparence du protocole éditorial. C'est ce dernier qui doit _disposer_ les acteurs, _quels qu'ils soient_ -- protocoles techniques, éditeurs et éditrices, contributeurs, formats, interfaces, outils, etc., dans le sens d'une production collective d'écritures conversationnelles. On l'a vu, l'ouverture et la transparence du protocole ébranlent en profondeur les fondements épistémologiques de la scientificité, car elles supposent de revoir les principes mêmes de l'évaluation. En devenant collective, cette dernière permet de redéfinir la notion de scientificité, en l'émancipant d'une objectivité qui se révèle impraticable.
Mais l'exemple du dossier «Écrire les communs» et les pratiques de la gouvernance des communs m'incitent à penser que l'édition périodique scientifique pourrait encore accentuer ce tournant épistémologique. Car l'ouverture du protocole éditorial peut s'appliquer récursivement à lui-même. Il s'agit d'en partager la gouvernance en le soumettant lui aussi à la conversation. Ainsi, en définissant collectivement les règles, les outils et les modalités du protocole, c'est-à-dire de la conversation, c'est tout le processus de légitimation qui se _virtualise_ en même temps que le collectif. En suivant cette piste, la virtualisation du collectif coïnciderait avec la virtualisation de la revue.
Quel mission alors pour l'éditeur et la revue ? Quelle fonction éditoriale ? Si la finalité que poursuit l'éditeur et de faire advenir le collectif et de le conjuguer à la pluralité du nous, alors sa fonction revient à ouvrir un espace, et à initier les conditions de possibilité d'une co-construction d'un protocole et d'un dispositif éditorial. Cette co-construction ouvre un horizon auctorial et éditorial nouveau, en nous sortant justement des dualismes auteur/éditeur ou auteur/lecteur. À nouveau elle suppose l'acquisition d'une littératie nouvelle du milieu d'écriture. De la même manière qu'un éditeur connaissait son champ disciplinaire, il devra désormais connaître -- c'est-à-dire pratiquer -- le milieu. Par cette pratique, se déploie une créativité autant éditoriale qu'institutionnelle. Car si les institutions doivent effectivement se réinventer, elles ne le feront que lorsque les pratiques s'en seront émancipées.

Ce tournant épistémologique est finalement une proposition pour _faire science autrement_, selon les termes d'Isabelle Stengers. L'attention et le soin au milieu renvoient à son idée d'écologie politique dont l'éditeur ou l'éditrice pourrait faire sien·ne. La conversation, l'ouverture du protocole et l'ouverture de sa gouvernance profilent ensemble la remédiation d'une revue _inscrite_ dans les _inquiétudes_ de son collectif, au-delà des _objets_ de la communauté scientifique.

Une production de savoir collective
Aussi la production de savoir sera collective. Voilà où se tient le régime de vérité à inventer. Voilà la mission des revues.


Le collectif épouserait la géométrie variable des communautés concernées par les problématiques soulevées.

Il me semble, en concluant cette thèse, que renouer avec cette culture numérique dont j'ai montré qu'elle porte en elle les moyens de la pensée contre-hégémonique




[Zuckenberg éditeur ? suggestion de marcello sur revue20]{.note}



- développer sur la bienveillance et la confiance.

# quelques notes nettoyées

:::note
comment sortir des corporatismes\ ?
appartenance VS corporatisme --> ouvrir la science les protocoles.
:::

:::note
Abrupt comme ouverture.

Pierssens\ : cacophonie / brouhaha\ : l'oralité  et le contre-hégémonique.

remettre Où l’on voit que le modèle le plus susceptible de réussir, pour la revue savante de prochaine génération, c’est peut-être le fanzine. pierssens qlq part en fin de partie 3.
:::


:::note
Rhétorique un peu laborieuse dans la premier partie.

éditions avec les éditeurs\ : montrer le paradoxe et la scyzophrénie:  modèle idéal qui en fait au cœur de leur discours, des gens de principes, et en meme temps ils sont en constante négociation. les modèles sont intenables comme le montrent leur pratique.

chiffrin: l'édition sans éditeur (aujourd'hui avec la concentration éditoriale, vision capitaliste et paupérisation des contenus, donc l'éditeur n'est plus éditeurs, il accompagne une fabrication).
les éditeurs à l'ancienne où une certaine pratique demeure.

pb sur les temps verbaux et vocabulaire\ : jouer sur le plan rhétorique. j'ai été amené à, montrer la dimension processuelle.

reprendre le début en fonction de l'intro.

assertif: on peut en conclure, on peut affirmer, etc.

:::

:::note
geste: systématique ; prendre un objet et le retourner pour l'envisager

1er geste: théorique > théorie des médias, apport en terme de théorie des médias (remédiation criticable) : forme médiatique différente.
2eme geste: être allé faire une enquête de terrain, passé documentariste. il se joue qlq chose ici, enquête très ciblée, sur un petit panel, mais a permis d'embrasser des revues avec un vrai vision éditoriale et receuillir des témoignages d'une grande richesse:  très cohérent, car on s'extrait du principe de modélisation qui ne fonctionnait pas. Je montre les conjonctures, aller au delà des singularités, pour extraire des traits communs.
3eme geste: ingold (making) > s'insérer pour apprendre la technologie.

3 démarches très cohérentes, elles fonctionnent de concert et elles permettent de renouveller ce que peut etre la revue.

la thèse a fait vaciller certains partis pris\ : le format, ce qui doit mourrir, ce qui doit rester, la force des éditeurs,

effet de déconstruction de la revue pour mieux la reconstruire.
- déconstruction du format, de la modélisation
- déconstruction des éditeurs dépassés, conservateurs, etc.
- déconstruction du protocole
- déconstruction de la dimension savante\ : de la scientificité, de l'écriture.

numérique: fait émerger des discours hyper polarisés j'ai questionné ces discours et me sui montré sceptique, là où c'est facile d'adopter des postures très catégoriques.

En fait j'ai déplacé mon discours\ : le positivisme numérique vers un optimisme écologique.

préciser ce que je fais dans la conclusion.
:::
