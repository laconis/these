# todo

en fait, voir sur https://demo.codimd.org/9kUvmnlOSj-ZinxR0cHnbQ?both

---

## Mise en pagedJS

- [ ] gestion groupe d'images
- [ ] issue Footnote
- [ ] concaténation html > produire un non-standalone et concaténer
- [ ] pages de garde
- [ ] index / sommaire des figures...
- [ ] annexes...!

## Intro

## Partie 1

- reprendre

## Partie 2

- reprendre annotations Marcello
- reprendre annotations Servanne

## Partie 3

- [ ] reprendre mes annotations v0.4 sur version locale
- [1/2] reprendre le chapeau et pourquoi la conversation pour mieux articuler avec la suite
- [ ] finaliser réécriture partie communs (voir aussi annotations servanne sur v0.4)
  - [ ] conclusion de la sous-partie (voir note todo et [6])
  - [ ] lire patterns of commoning et référencer dans partie communs
- [ ] rédiger dernière partie
  - [ ] lire stengers
  - [ ] voir note todo en fin de partie
- [ ] livrer à Marcello et Manuel

## Conclusion

- [ ] relire et lister des idées pour la conclusion
