# notes

pourquoi la revue ?
parce qu'elle a constitué un lieu d'innovation éditoriale, parce que les communautés scientifiques se sont constituées autour d'elle.

faire une partie sur les revues aujourd'hui : en présentant le projet Revue20

Sandro: pignochi@hotmail.fr



---

## présentation marcello humanités numériques


le format n'est pas neutre. le forme informe le contenu, le structure, et structure ainsi la pensée. Il se tient au fondement de la production de la pensée.

sur le plein texte:

- texte brut: garantie absolue de pérennité et d'accessibilité. Pas de formattage par dessus.
  - une seule couche de médiation entre le caractère et le binaire : encodage des caractères direct en binaire.
  - ascii et unicode
- formattage en markdown : on ajoute des balises, une syntaxe particulière pour signifier certaines marques éditoriales.

Guerre des navigateurs:
internet explorer voulait s'approprier le format : cad que les contenus seraient formatés par microsoft.

Question de modélisation:
modélisation théorique, implémentation dans une fonction mathématique,

la réflexion sur la modélisation doit venir en premier, lorsqu'on souhaite choisir ou fabriquer un outil:
- modéliser mon besoin
- or pour la rédaction scientifique, les besoins sont :
  - fiable, léger (pérennité, universalité, etc.)
  - beau ? mais on n'est pas des graphistes, on doit par contre


- xml : le modèle est prédéfini > on doit définir en amont le modèle épistémologique.
- html : plus de liberté, et moins de rigueur: plus évolutif.

---
## sur les nouvelles pratiques : voir l'article de Beaudry sur la communication directe

implication de ces nouvelles pratiques sur l'évolution de la communication "par sédimentation"

---
## Sur la conversation

voir le dossier C&L sur la conversation et l'article https://www.cairn.info/revue-communication-et-langages1-2011-3-page-63.htm  sur la conversation dans les sites de presse en ligne.

## Discussion sur les contours de l'éditorialisation

voir la discussion sur hypothesis : https://hyp.is/vqRTbNXNEeegqbv17GMkxw/nicolassauret.net/carnet/2017/11/23/l-intelligence-des-traces-de-louise-merzeau/

Où Marcello réaffirme une vision élargie de l'éditorialisation, lorsque Louise pose plutôt l'éditorialisation comme les actions individuelles d'usage (et de traces), et mon commentaire.

## Sur Stengers et la réouverture des imaginaires

Stengers: dispositif génératif : problématiser une rencontre, cad penser un dispositif conversationnel dans lequel les antagonismes vont pouvoir se rencontrer. Il ne s'agit plus d'avoir raison, mais d'entendre l'autre, et d'ouvrir ainsi des imaginaires nouveaux.
problématiser, c'est fabriquer des inconnues de la question, comprendre que nous sommes tous mutilés et malades, mutilés du sens commun.
  - palabre en afrique
  - quaker: rite faire silence, n'oubliez pas que vous pourriez être dans l'erreur : le doute
    - comme une forme de culture politique
  - David Grebers: généalogie entre les réunions quaker et les modes de discussion horizontaux
résurgence des communs: perspective du désastre, partir du désastre et des ruines pour _voir_ le monde

nouveau tissu, penser ce qu'ils font, être ensemble et faire de la politique.

## Sur Stengers et les communs résurgents

Faire attérir le droit

Récemment, Bruno Latour a appelé à un devenir «   terrestre » des pratiques scientifiques, à des modes de traitement dont la vocation première ne soit pas de constituer ce à quoi les scientifiques ont affaire en ressource pour  «  faire  avancer  la  science  »96,  externalisant  le  reste  comme  anecdotique  ou  «  subjectif », mais soient équipés pour suivre la manière dont les actions et les sen-sibilités humaines et non-humaines s’entre-composent selon des modes d’interdé-pendance  inextricables.  De  manière  analogue,  on  pourrait  dire  que  les  commonsréclament un droit « terrestre », capable de s’adresser à la manière dont ils intriquent des pratiques, des sensibilités, des modes de coopération, des coutumes en inter-dépendance étroite. Un droit en devenir, inductif, topique, plutôt qu’un droit posé et abstrait, axiomatique et déductif : un droit qui favoriserait jurisprudence et pratiques comme sources, plus que la loi ou/et la « doctrine ». [p.335]
