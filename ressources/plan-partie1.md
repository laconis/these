Finalisation

# Introduction

    ## La revue media
    ## Prendre soin de la chaine de production de l'écrit

# Au 17ème siècle, la naissance d’un format éditorial

    ## le Journal des Savants : les fondements de la communication savante périodique
    ## La fabrique de l’autorité
    ## De la correspondance à l’article : première formalisation
    ## L'art de la contrefaçon : l’éternel enjeu de la diffusion
    ## Les appropriations du périodique par la communauté savante
    ## Bilan et perspectives du format

# Le moment numérique des revues : enjeux, défis et déphasages

    ## Écritures, techniques, savoirs : le malentendu numérique des LSH
        L'enjeu de la littératie : résistances et illetrisme
        L’enjeu de l’accessibilité : les politiques du libre accès
        Désintermédiation des institutions et nouveaux acteurs
        Hybridation des pratiques et rhétorique « barbare »
    ## Les artefacts académiques contemporains : du déphasage institutionnel au déphasage éditorial
        Un déphasage institutionnel
        Un déphasage éditorial
        Un déphasage épistémologique

# La revue à l’heure de sa remédiation : pour une théorie médiatique de l’écriture savante

    ## Penser la remédiation des revues : l'apport de l'intermédialité
    ## Les matérialités de l’écriture numérique
        ### Texte et écriture
        ### La métaphore trompeuse de la liquidité
        ### De l'invariant textuel à la variabilité du texte numérique
        ~~### Calculabilité et système de référence~~
    ## Du fragment au cristal : la nouvelle granularité de la connaissance
        ### Culture du fragment
        ### Du fragment au cristal
    ## Performativité de l’écriture : l’apport de la théorie de l’éditorialisation
        ### Les facettes de la performativité
        ### Le prisme de l'éditorialisation

# Vers une herméneutique collective

    ## Régime documentaire en environnement numérique
        ### le dispositif d’éditorialisation ENMI12 : contexte et contours
        ### De l’événement à l’analyse : vers la théorisation du dispositif
    ## L’hypothèse d’une herméneutique collective
        ### Bienveillance
        ### Réflexivité
        ### Appropriation
        ### Distance
    ## Design de la conversation : les défis du format conversationnel
        ### La revue comme espace public
        ### Modélisation et spatialisation de la conversation
        ### ~~Spatialisation~~
        ### Le format au défi la performativité
        ### Remédier la revue, dépasser le format

# Conclusion

Proposition pour partie1 v0.4

# Introduction

##    La revue media
##    Prendre soin de la chaine de production de l’écrit : un devoir de l’institution

# Au 17ème siècle, la naissance d’un format éditorial

##    le Journal des Savants : les fondements de la communication savante périodique
##    La fabrique de l’autorité
##    De la correspondance à l’article : première formalisation
##    L’art de la contrefaçon : l’éternel enjeu de la diffusion
##    Appropriations : la fabrique d’une communauté

# Le “moment numérique” des revues: enjeux, défis et déphasages

##    Écritures, techniques, savoirs : le malentendu numérique des LSH
###        Un conservatisme résistant [à revoir]
###        L’enjeu de l’accessibilité : les politiques du libre accès
###        Désintermédiation des institutions et nouveaux acteurs
###        Hybridation des pratiques et rhétorique « barbare »
##    Les artefacts académiques contemporains : du déphasage institutionnel au déphasage éditorial
###        Un déphasage institutionnel
###        Un déphasage éditorial
###        Un déphasage épistémologique

# La revue à l’heure de sa remédiation : pour une théorie médiatique de l’écriture savante

##    Penser la remédiation des revues : pour une approche intermédiale
##    Le milieu de la culture numérique : matérialité de l'écriture
##    Du fragment au cristal : nouvelle granularité de la connaissance
##    Performativité et éditorialisation


# Vers une herméneutique collective

##    Régime documentaire en environnement numérique
##    L’hypothèse d’une herméneutique collective
###        Bienveillance
###        Réflexivité
###        Appropriation
###        Distance
##    Design de la conversation : l’impasse d’un format conversationnel
###        La revue comme espace public
###        Modélisation
###        Spatialisation
###        Performativité de la conversation
###        [Impasse]
##    Conclusion : dépasser le format, vers la remédiation


##    La revue dans sa chaîne éditoriale : de l’édition à l’éditorialisation
##    La dialectique entre formes et pratiques
###        [cas du hashtag twitter]
###        [cas de la plateforme OpenEdition]



----

au tag format0.3

# Introduction

##    La revue media
##    Prendre soin de la chaine de production de l’écrit : un devoir de l’institution

# Au 17ème siècle, la naissance d’un format éditorial

##    le Journal des Savants : les fondements de la communication savante périodique
##    La fabrique de l’autorité
##    De la correspondance à l’article : première formalisation
##    L’art de la contrefaçon : l’éternel enjeu de la diffusion
##    Appropriations : la fabrique d’une communauté

# Le “moment numérique” des revues: enjeux, défis et déphasages

##    Écritures, techniques, savoirs : le malentendu numérique des LSH
###        Un conservatisme résistant [à revoir]
###        L’enjeu de l’accessibilité : les politiques du libre accès
###        Désintermédiation des institutions et nouveaux acteurs
###        Hybridation des pratiques et rhétorique « barbare »
##    Les artefacts académiques contemporains : du déphasage institutionnel au déphasage éditorial
###        Un déphasage institutionnel
###        Un déphasage éditorial
###        Un déphasage épistémologique

# Vers une herméneutique collective

##    Régime documentaire en environnement numérique
##    L’hypothèse d’une herméneutique collective
###        Bienveillance
###        Réflexivité
###        Appropriation
###        Distance
##    Design de la conversation : l’impasse d’un format conversationnel
###        La revue comme espace public
###        Modélisation
###        Spatialisation
###        Performativité de la conversation
###        [Impasse]
##    Conclusion : dépasser le format, vers la remédiation

# La revue à l’heure de sa remédiation : pour une théorie médiatique de l’écriture savante

##    Penser la remédiation des revues : pour une approche intermédiale
##    Le milieu de la culture numérique : du fragment au cristal
###        Le (texte) numérique est un milieu
###        Le fragment, nouvelle granularité de la connaissance
###        Du fragment au cristal
###        Performativité
###        L’éditorialisation
##    La revue dans sa chaîne éditoriale : de l’édition à l’éditorialisation
##    La dialectique entre formes et pratiques
###        [cas du hashtag twitter]
###        [cas de la plateforme OpenEdition]
