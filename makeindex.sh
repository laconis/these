#!/bin/sh


## Transformation de l'index Work In Progress wip.md
echo "transformation de l'index wip"
pandoc --standalone --section-divs --ascii --id-prefix=index --css=css/chapitre.css --lua-filter=./templates/date.lua --template=./templates/chapitre.html5 ./contenus/wip.md -o ./html/wip.html

pandoc ./contenus/sommaire.md -o ./html/sommaire.html

echo "transformation de l'index général"
pandoc --standalone --section-divs --ascii --toc --id-prefix=index --css=css/chapitre.css --css=css/index.css --lua-filter=./templates/date.lua --template=./templates/th-index.html5 ./contenus/these.yaml ./contenus/index.md -o ./html/index.html

  sed -i -e '/PANTOC/r ./html/sommaire.html' ./html/index.html # rajoute le sommaire à l'html
  sed -i -e "s/chapitrenext/th-introduction/g" ./html/index.html
  sed -i -e "s/NEXTTITLE/Introduction/g" ./html/index.html
  sed -i -e 's/PANTOC//g' ./html/index.html

  rm ./html/sommaire.html
