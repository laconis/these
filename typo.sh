#!/bin/sh

# cp $1 test0.md


sed -i -e "s/«\(\w\)/«\\\ \1/g"  contenus/$1.md
sed -i -e "s/« \(\w\)/«\\\ \1/g" contenus/$1.md
sed -i -e "s/\(\w\)»/\1\\\ »/g"  contenus/$1.md
sed -i -e "s/\(\w\) »/\1\\\ »/g" contenus/$1.md
sed -i -e "s/\(\w\) ?/\1\\\ ?/g" contenus/$1.md
sed -i -e "s/\(\w\)\s:/\1\\\ :/g" contenus/$1.md

sed -i -e "s/oe/œ/g" contenus/$1.md

# pour les : besoin de traiter plein de cas particulier..
# sed -i -e "s/\(\w\):/\1\\\ :/g" test0.md
# sed -i -e "s/\(\w\) :/\1\\\ :/g" test0.md

git difftool contenus/$1.md
