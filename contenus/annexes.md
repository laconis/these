---
title: Annexes
subtitle:
date: 2020-08-27
version: 1.0
previous: 0.1
tagprevious: th1.0
date-version: 2020-09-08
link-citations: true
chapitre: unnumbered
lang: fr
---

[L]{.lettrine}es annexes suivantes sont généralement des documents bruts à télécharger. Lorsque cela a été possible, je les propose en HTML.

Cette liste ne correspond qu'aux éléments cités dans la thèse. Une liste complète des archives est disponible sur demande.

**ENMI\ :**

- [Présentation du dispositif ENMI12 (PDF)](../annexes/enmi12/retourDispositifENMI12.pdf)


**Booksprint Ecridil de l'ouvrage «\ version 0\ »\ :**

<!-- - [Courriel du 22 mars 2018 -- Propositions pour livre Ecridil #2 (HTML)](../annexes/ecridil/Propositions-pour-livre-Ecridil-2_20180322-23.html)
- [Courriel du 9 avril 2018 -- Propositions pour livre Ecridil #2 (HTML)](../annexes/ecridil/Propositions-pour-livre-Ecridil-2_20180409.html) -->
- [Courriel du 24 avril 2018 (HTML)](../annexes/ecridil/booksprint-Ecridil-20180424.html)
<!-- - [Courriel du 28 avril 2018 -- update chaine booksprint (HTML)](../annexes/ecridil/update-chaine-booksprint_20180428.html)
- [Critères pour la mise en page d'Ecridil (HTML)](../annexes/ecridil/Criteres-pour-la-mise-en-page-d-ECRIDIL.html) -->



**Publishing Sphere\ :**

- [Programme de la présentation publique du 25 mai 2019 (PDF)](../annexes/publishingsphere/programme_PSv0-1.pdf)

**Dossier «\ Écrire les communs\ »\ :**

- [Courriel -- Appel en commun, 17 avril 2018 - info pratique (HTML)](../annexes/dossiercommuns/Gmail - 03a-Appel en commun, 17 avril 2018 - info pratique.html)
- [Le document (ggdoc) ouvert à tous pour la gestion du dossier (version du 26 avril 2018) (PDF)](../annexes/dossiercommuns/2018 - Dossier Communs sur Sens public – 26 avril 2018 à 08_24 Soumise par mail à la liste Echange.pdf)
- [Courriel -- Revue Sens Public - Dossier sur les communs (Parution dec. 2018) (HTML)](../annexes/dossiercommuns/Gmail - 12-Re%20 [SavoirsCom1] Revue Sens Public - Dossier sur les communs Parution dec. 2018.html)
- [Courriel -- Gmail - 28a-[Sens public] Appel à conversation (HTML)](../annexes/dossiercommuns/Gmail - 28a-[Sens public] Appel à conversation.html)
- [La comparaison (_git diff_) des deux versions du texte de Romain Lalande (HTML)](../annexes/dossiercommuns/mydiff.html)

**Revue 2.0\ :**

- [Chaîne éditoriale de traitement des revues à _Érudit_ (PDF)](../annexes/revue20/erudit-workflow-prod.pdf)
- [Transcript de l'entretien de Stéphane Pouyllau - 26 octobre 2018 (HTML)](../annexes/revue20/transcript_entretienSPouyllau_20181026.html)
- [Tableur des réponses au questionnaire d'observation du protocole editorial des revues (ODS)](../annexes/revue20/questionnaire_observation_du_protocole_editorial_des_revues.ods)

**Liste DH\ :**

- [Archive des conversations sur la liste DH (juillet 2019)](../annexes/listedh/dh-juillet2019.csv)
