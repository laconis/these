---
title: Partie 3
subtitle: La revue émancipée
date: 2019-09-21
version: 0.1
bibliography: chapitre1.bib
link-citations: true
lang: fr
---

##

Nous avançons l'hypothèse d'un modèle éditorial conversationnel, exploitant notamment la culture participative qui a pu être théorisée par Jenkins, que celui-ci considère héritière directe des paradigmes philosophique et juridique adopté par le monde informatique. En s'inscrivant dans cette culture participative, il ne s'agit de verser ni dans l'idéologie souvent employée à tort d'un méta-discours propre au web 2.0, tel que Jahjah en a analysé les rouages [@ref]{.note}. Il ne s'agit pas non plus de verser dans l'injonction à la participation surexploitée par le marketing à partir des années 2010.

Notre conversation s'inscrira plutôt dans la reconnaissance que les sciences humaines et sociales sont fondamentalement une _disputatio_, et que l'explosion des pratiques d'écriture et de lecture, notamment conversationnelles, nous incite à envisager une évoluation radicale des modes de communication scientifique.

À travers les différents cas d'études que nous amènerons, nous verrons que l'enjeu derrière un modèle éditorial conversationnel n'est pas tant la production de nouveaux artefacts ou d'un document de référence. L'enjeu réside davantage dans le caractère processuel de la conversation, dont l'élément le plus tangible est le réseau social qui ne cesse de se former, autrement dit dans le collectif à l'oeuvre. Ce collectif, témoigne à la fois d'une intelligence collective, et d'une redécouverte d'un _nous_, dont la subjectivité prime sur celle de l'individu. Cette inversion ne doit pas être vue comme le retour d'un communisme, mais comme l'opportunité de refermer la parenthèse capitaliste. Cette parenthèse a su appliquer ses principes de propriété jusqu'à naturaliser l'enclosure des idées, détruisant toujours plus insinueusement les collectifs.

L'analyse de l'instrumentalisation de la culture participative dans les sites de presse en ligne nous sera précieuse pour éviter certains des écueils que Louise Merzeau identifiait déjà. Nous verrons qu'une des conditions du collectif est d'articuler à la production de données dûes à la conversation, la gouvernance de ces même données. Nous pouvons envisager cette gouvernance comme l'émergence d'un contrat d'écriture, au même titre que le contrat de lecture qui unissait les éditeurs et les lecteurs de la presse.


# Hypothèse : un modèle éditorial conversationnel

La situation actuelle des revues met en lumière la nécessité de repenser un paradigme éditorial adapté aux pratiques d'écriture et de lecture qui ont emergé avec l'avènement de l'environnement numérique.

Dans cette partie prospective sur le futur de la revue, nous souhaitons explorer l'hypothèse que l'institution scientifique est en mesure de concevoir et d'adopter de nouvelles formes éditoriales de communication scientifique, à même d'incarner et d'instaurer une épistémé du numérique. Nous soutiendrons que la revue, en tant que média et tel qu'il s'est institutionnalisé dans le champ des sciences humaines et sociales peut constituer un lieu privilégié pour une telle innovation éditoriale. En effet, si ses formes éditoriales restent aujourd'hui contraintes dans le modèle épistémologique, elle conserve, de part sa position intermédiaire entre l'institution universitaire et ses acteurs (chercheurs, éditeurs), une liberté d'action et de pensée qui pourrait être mise à profit pour expérimenter de nouveaux formats.

À travers plusieurs cas d'études et terrains d'observation et d'expérimentation, je montrerai qu'une orientation possible pour transformer la communication scientifique est celle de la conversation. Nous faisons ainsi l'hypothèse d'un modèle éditorial conversationnel, tel que celui mis en place par le dispositif ENMI en 2012 que Louise Merzeau a pu observer et analyser. Son analyse, qui s'est déployée entre 2013 et 2017 à travers plusieurs écrits programmatiques, pave une réflexion que nous reprendrons et étendrons pour imaginer de nouvelles formes de production et de circulation de connaissance.
La dernière partie de la thèse consistera à confronter le modèle théorique des ENMIs (issu de la pratique, c'est à dire initialement de la conception d'un dispositif.), à plusieurs terrains d'observation (Général Instin, Manifeste CCLA) et plusieurs expérimentations (Booksprint Ecridil, Dossier «Écrire les communs», Publishing Sphere) réalisées pendant le doctorat. Ces différents terrains nous incitent à penser que la communauté scientifique pourrait  s'inspirer des pratiques et des formats élaborés par des  communautés d'écriture non-académiques. Ces communautés ont en effet mis en oeuvre ce que nous appelerons une «écriture-milieu» dont l'enjeu n'est plus tant de faire document, mais de faire collectif, rejoignant par là-même le projet de Louise Merzeau qui pressentait dans les pratiques communicationnelles émergentes une «rhétorique du nous» susceptible de porter l'intelligence collective nécessaire pour adresser la complexité du monde.


<!-- articulation avec une partie sur l'écriture -->
Institution et forme éditoriale se répondent et co-existent dans une relation de transduction. De la même manière l'édition doit se penser en co-existence avec l'écriture. Il est donc nécessaire de regarder de plus près ce qu'est l'écriture numérique.

# La nature environnementale du numérique

::: note

Repris de mon champ 1. Il y a un travail de sélection des notions, puis de développement des notions sélecitonner.

:::

## Le numérique comme milieu

### Éditorialisation

La revue scientifique «\ nativement numérique\ » constitue un objet  d'étude idéal pour adresser la question de l'éditorialisation telle que définie par @vitali_rosati_quest-ce_2016 qui propose avec l'éditorialisation une conceptualisation des processus de production, de légitimation et de circulation des connaissances dans l'environnement numérique. Ce qui pourrait apparaître comme une théorie de l'édition numérique, dépasse en fait largement l'édition comme pratique ou comme secteur d'activité. L'éditorialisation prend ainsi une portée culturelle en s'appliquant potentiellement à tous les processus d'écriture dans l'environnement numérique, et une portée philosophique en proposant une pensée de l'espace numérique, de sa structuration et consécutivement de l'autorité.

L'éditorialisation me permet de dépasser la notion de dispositif et sa conception foucaldienne. On retrouve cette même conception de l'éditorialisation chez Louise Merzeau dont l'approche héritée de la pensée médiologique considère le numérique comme _milieu_ [@merzeau_ceci_1998; @merzeau_editorialisation_2013]. La notion de milieu nous sera particulièrement utile ici pour aborder ce glissement de valeurs depuis des entités bien identifiées dans le monde pré-numérique vers des entités qui tendent justement à devenir environnementales.

### Écologie médiatique

Cette approche, qui rejoint celle de l'écologie médiatique [@bardini_entre_2016], intégre la nature environnementale du numérique et nous permet en effet de revisiter ces entités, support, dispositif ou encore technique intellectuelle, et de les considérer comme partie prenante de l'environnement ou du milieu.

#### Causalité circulaire

Le milieu nous amène notamment à sortir du déterminisme technologique associé généralement au support technique d'inscription, et à une conception linéaire de la causalité [@bardini_entre_2016] entre par exemple support et pensée, ou entre inscription et technique intellectuelle. De la causalité _formelle_ [@mcluhan_formal_1976] à la causalité _circulaire_ [@merzeau_mediologie_2006] ou _récursive_ [@levy_place_1998], l'écologie médiatique inscrit les supports dans une boucle rétroactive entre technique et usage, permettant d'envisager autrement les interactions entre des entités en partie dissoutes dans le milieu. Or, il se trouve que le numérique, caractérisé notamment par sa récursivité, adhère particulièrement bien à cette conception de la causalité. Parce qu'il est un milieu écrit (codes, protocoles, dispositifs), le numérique intègre de manière récursive des valeurs qui se reproduisent dans les écritures qu'il supporte et génère.

#### Écriture-milieu

Dans ce contexte théorique, l'étude de cas du corpus littéraire contemporain _Général Instin_^[cf. Méthodologie] viendra illustrer comment l'écriture littéraire elle-même devient constitutive du milieu ^[@rongier_general_2017 parle d'«\ écriture-milieu\ »] . Dans cette idée de glissement et de dilution des fonctions et des valeurs, on pourrait qualifier cette écriture de _dispositive_ dans la mesure où elle véhicule tout autant un discours qu'une série de valeurs transmises dans les formes et supports. Dans le cas d'Instin, ce n'est pas strictement le numérique qui produit cette récursivité de l'écriture sur elle-même, mais il la permet. En quelque sorte, il la _prédispose_.

### Fonction éditoriale (dilution)

Avec la dilution du dispositif dans le milieu, s'opère un autre glissement notable, celui de la fonction éditoriale. Traditionnellement assurée par des acteurs humains, maîtrisant des opérations d'identification, de sélection, d'agencement et de fixation du texte, la fonction éditoriale se trouve distribuée dans le milieu numérique et répartie entre différents acteurs. En effet, le support d'écriture et de lecture s'imbrique avec des dispositifs intégrant en eux-mêmes de nouvelles instances de décision\ : les algorithmes, la communauté de lecteurs. Dans le sens d'une co-construction de l'espace de savoir, ces algorithmes de décision viennent outiller les acteurs humains, éditeurs et communautés de lecteurs. Avec la fonction éditoriale, c'est tout le processus de légitimation qui se trouve lui aussi dilué et réparti entre différents acteurs, humains, dispositifs et supports.

## Le (texte) numérique comme milieu

Le glissement de la fonction éditoriale se joue également dans la nature du texte numérique, devenu à la fois support, dispositif et milieu.

Pour @kittler_logiciel_2015, les inscriptions numériques se distinguent des inscriptions sur support physique par le fait qu'elles ne sont plus perceptibles par la vue humaine. Du visible, elles sont passées à un ordre de grandeur nanométrique hors d'atteinte des capacités sensibles de l'être pensant. Nous pouvons interpréter cela dans le sens de la médiologie en disant que le texte s'est simplement dissout dans son milieu, se confondant avec les autres écritures qui produisent ce même milieu (à savoir les codes, les protocoles, les inscriptions sur silicone - circuits imprimés et et puces, qui sont en fait des instructions). C'est en ce sens aussi que l'on peut concevoir le texte numérique comme une écriture-milieu.

Écriture-milieu également dans la dénaturation ultime du signe alphabétique dans un système de signes binaire, lui-même traduit par la machine par des impulsions électriques. Cette déconstruction radicale du texte [@kittler_logiciel_2015] revient à une dissolution de l'unité d'inscription (la lettre) en unités plus petites, le bit. Ces bits de nature binaire _supporte_ à la fois le texte inscrit, le code qui le manipule, la puce qui _traite_ (_process_) le code. Le bit, et son équivalent physique (l'impulsion électrique) constituent finalement les briques élémentaires du milieu numérique.

Pour autant, si l'inscription sort du domaine du visible, l'intelligibilité du texte n'est pas perdue. Sa dissolution est à tout moment réversible grâce au calcul, tant que les conditions sont réunies pour opérer ce calcul. C'est justement le milieu qui assure la faisabilité de ce calcul, où l'on voit bien ici comment le milieu est coproduit à la fois par les couches matérielles (à commencer par la présence d'énergie électrique), logicielles et environnementales^[on peut penser aux protocoles du réseau lorsque le calcul fait appel à des ressources externes]. Cette faisabilité est également dépendante des multiples standards sous-jacents à la production de texte\ : de l'encodage de caractère à l'encodage du texte, mais aussi de la police d'affichage, des feuilles de styles, etc. C'est l'ensemble de ces éléments qui permettent au texte numérique, doublement inintelligible, car encodé et imperceptible, de se laisser voir et lire par l'être humain alphabétisé.

_L'ensemble de ces éléments_ constituent d'après @bachimont_nouvelles_2007 une étape d'«\ interprétation\ » qui se glisse entre l'inscription et son intelligibilité par le lecteur. Cette interprétation calculée^[à considérer comme une médiation technique.] de l'inscription numérique échappe au lecteur, et révèle une autre conséquence directe du caractère computationnel du milieu numérique\ : ces écritures ou inscriptions sont capables de «\ lire et d'écrire par elles-mêmes\ » [@kittler_logiciel_2015, p.30]. Plus exactement, le milieu est susceptible de produire de nouvelles écritures à partir d'écritures existantes. On peut se demander alors s'il est possible (et nécessaire) de distinguer différents niveaux d'écriture selon sa provenance et sa fonction dans le milieu, qu'elle soit machinique ou humaine, mais aussi qu'elle soit code, données, métadonnées, discours, etc.

Le milieu numérique est en effet régi par une succession d'écritures programmatives (logiciels, protocoles, mais aussi pourquoi pas cartes imprimées et puces électroniques) qui élaborent ensemble un espace d'action (et d'écriture). Ces écritures sont en quelque sorte structurelles ou architecturales et procure un cadre rigide et structurée.

### Liquidité

Or, la dissolution du symbole évoque la nature liquide du texte numérique. Cette métaphore de la liquidité vient illustrer l'instabilité de l'inscription numérique, par essence altérable et modifiable à tout moment, que ce soit du fait d'une action humaine ou d'un process informatique. Tout un chacun (humain ou machine) ayant le contrôle sur le processus d'écriture peut _éditer_, au sens de modifier, un texte numérique.
Cette liquidité est une métaphore potentiellement féconde pour penser l'inscription numérique, mais elle doit être nuancée pour plusieurs raisons. D'une part comme on l'a vu avec l'idée d'écritures architecturales, toutes les écritures informatiques n'ont pas le même niveau de liquidité.

C'est ce qui fait que malgré le fait que le texte soit par nature modifiable, les dispositifs d'écriture (éditeurs ou traitements de texte) sont programmés pour contraindre les droits et accès _en écriture_ à certaines inscriptions. Car si le support de mémoire numérique est effectivement réinscriptible à volonté, il n'en demeure pas moins qu'il est strictement contrôlé et maîtrisé par des couches logiciels qui s'assurent que les données soient correctement manipulées, et en premier lieu qu'elles soient conservées intègres.

<!-- [mvr fait remarquer que le milieu est architecturale, structuré et rigide. qu'il n'est pas liquide comme le texte. Il distingue donc les écritures discursives des écritures de code et protocole :) Ce qui me fait plaisir puisque c'est ce que je me tuais à lui dire : si tout est écriture (protocole, software, cartes imprimées, puces, etc.) et éditorialisation, tout n'est pas du même niveau d'écriture et d'éditorialisation. Il est nécessaire de distinguer les diff. formes d'écriture et leurs rôles respectifs dans le milieu, et notamment dans l'écriture du milieu. Ceci étant dit, oui, si les niveaux de fluidité sont moindre sur les écritures architecturales et supérieures pour les écritures discursives, toute deux participent de la construction de l'espace _et_ du milieu : écriture milieu, écriture _dans_ l'espace et _de_ l'espace.][*] [il y a des architectures qui sont très rigides : oui modifiable mais quand meme des infrastructures et archi principales sont définies.][*] -->

### Invariant textuel

On peut avancer que l'invariant textuel [@de_biasi_papier_1997] que lui assurait le support papier demeure dans l'environnement numérique d'une certaine façon. Il n'est certes plus assuré par le support lui-même, mais par le dispositif, c'est-à-dire ici par tous les mécanismes garantissant le traçage des accès au texte notamment _en écriture_. Ce transfert fonctionnel est sans doute l'une des clés de l'épistémé numérique tant il porte à conséquence sur la fonction d'autorité du texte et par extension de son.ses auteur.s. et des institutions qui en sont responsables. Car dans la graphosphère, c'est sur la base de cette stabilité du support (le papier), que pouvait exister la stabilité du dispositif (le livre), sur laquelle reposait la stabilité de l'institution (la bibliothèque), et finalement tout le régime de sens depuis l'imprimerie d'après @de_biasi_papier_1997. Dans l'environnement numérique, les repères de stabilité (ou encore les signes d'autorité) se sont radicalement déplacés, produisant de fait une impression d'instabilité du texte, érodant la notion même de _référence_ et avec lui tout le système bibliographique qui s’est mis en place pour l’institutionnaliser. Or, cette référence et son institutionnalisation sont les conditions du partage d’un socle commun de connaissances au sein d’une communauté de savoir. La possibilité de s’y référer procure au texte stabilisé une autorité et une authenticité nécessaires à une réflexion commune.

### Calculabilité et système de référence

Une autre approche pour questionner cette apparente liquidité est de la considérer comme une fluidification et accélération des processus d'écriture et de réécriture. Cette accélération est permise par le calcul, et c'est par le calcul que se résoud également la complexité de manipulation du texte et de ses états successifs. Il est en effet possible de mettre en place des dispositifs et des protocoles associés capable de gérer cette liquidité apparente du texte numérique, pour recréer des conditions de stabilité, ou tout du moins pour abaisser la complexité native à une complexité appréhendable par la cognition humaine, qu'elle soit individuelle ou collective.

Depuis les instructions informatiques élémentaires de gestion de fichiers, avec son nommage, son extension, son encodage, sa date de création ou de modification, etc., les dispositifs d'édition et de publication n'ont cessé d'améliorer leur gestion du texte numérique et de reproduire un tant soit peu une certaine stabilité, jusqu'à assurer aujourd'hui une panoplie de fonctions qui n'étaient pas envisageables avec le support papier, telles que le _versionning_, le multi-auteur (asynchrone), le collaboratif (synchrone), l'annotation, etc.

Le cas du wiki, donnant accès à toutes les versions antérieures du texte et aux modifications successives par auteur, en est l'exemple le plus emblématique. Github proposent un protocole de contribution différent à partir duquel émergent des dispositifs d’écriture collaborative^[voir notamment l'outil Penflip https://www.penflip.com/] (Burton). On peut également citer le principe de la blockchain^[sur Wikipedia https://fr.wikipedia.org/wiki/Cha%C3%AEne_de_blocs], conçue comme un registre distribué assurant la comptabilité des écritures et de leurs auteurs (machines et humains).

Ces exemples montrent bien qu'il serait possible, en théorie, de reconstruire un système bibliographique, c'est-à-dire un système fiable de référence, dans le sens d'un modèle épistémologoqie embrassant pleinement cette liquidité du texte. Or, on voit bien que les différentes fonctions traditionnellement assurées par l'institution, le dispositif ou le support (respectivement la bibliothèque, le livre ou le papier), ne sont plus distinctes et séparées, mais sont parfois transférées à d'autres entités, ou diluées entre elles, autrement dit, assumées par un milieu tout à la fois support, dispositif et institution.


# Vers une herméneutique collective

## Régime documentaire en environnement numérique

En 2013, Louise Merzeau publie un article analysant «\ l'éditorialisation collaborative d'un évenement\ » et le dispositif mis en place en marge d'une conférence scientifique pour la documenter [@merzeau_editorialisation_2013].

Dans cet article qui apparaît comme un jalon important de la «\ feuille de route\ » [@merzeau_nouvelle_2007] qu'elle s'était précédemment donnée, Louise Merzeau identifie des pistes de réflexion valides pour penser un nouveau régime documentaire propre à l'environnement numérique et à la fragmentation des écrits. La problématique que l'auteure poursuit alors est celle d'une connaissance dynamique caractérisée par un double processus de production documentaire (constitution de corpus) et d'une production mémorielle (remobilisation de ces corpus).

Pour Louise Merzeau, ce dispositif d'éditorialisation serait susceptible d'apporter une réponse aux inquiétudes soulevées dans plusieurs études du régime attentionnel en contexte Web et numérique. Que ce soit @carr_is_2008 qui considère que le «\ médium\ » Internet amenuise les capacités de concentration et de réflexion, ou @cardon_democratie_2010 qui s'inquiète de la prédominance de la logique affinitaire dans la recommandation des contenus, ou encore @rouvroy_gouvernementalite_2013 pour qui la gouvernementalité algorithmique réduit la subjectivité des individus (et leur devenir) à des profils prédictifs, les travaux ne manquent pas pour analyser un régime de communication condamnant «\ toute possibilité d’aménager des espaces communs de la mémoire et de la connaissance[^reff]\ ».

L'hypothèse avancée par Merzeau, et qu'elle ne cessera d'affiner progressivement [@merzeau_entre_2014; @merzeau_profil_2016], est celle d'un dispositif d'éditorialisation collaborative qui, en générant un processus vertueux de circulation et de réécriture, permet du même coup un processus mémoriel reposant «&nbsp;sur une production documentaire affranchie des logiques affinitaires au sein d'un même espace contributif.[^ref]&nbsp;»

[^reff]: voir [@merzeau_editorialisation_2013, p. 116]

[^ref]: _Ibid._

## L'hypothèse d'une herméneutique collective

Nous portons l'hypothèse plus loin\ : selon nous, le processus qu'elle décrit et analyse comme celui d'une redocumentarisation collective, pose en fait les fondements d'une dynamique interprétative, autrement dit d'une herméneutique collective.

L'analyse de Louise Merzeau fait en effet ressortir quatre propriétés constitutives de ce dispositif d'éditorialisation\ : 1)\ sa bienveillance, 2)\ sa réflexivité, 3)\ son appropriabilité et 4)\ sa distance. Ces quatre éléments assurent respectivement les fonctions 1)\ d'établir entre les individus et le dispositif la confiance nécessaire à tout engagement, condition pour ouvrir un espace où penser collectivement, 2)\ d'établir par la visualisation les conditions de l'élaboration d'une finalité commune, 3)\ de favoriser la circulation et la redocumentarisation des contenus catalysant des associations nouvelles, 4)\ d'aménager à l'interstice de ces associations «\ une glose critique et documentaire\ », lieu-même de l'interprétation.

De l'appropriation à l'interprétation, il n'y a qu'un pas. Et si l'appropriation se matérialise par des écritures et des réécritures, alors le dispositif conversationnel peut prétendre relever d'une activité herméneutique.

La question devient alors\ : comment profiter de cette dynamique herméneutique pour produire des connaissances, ou encore, comment formaliser ces réécritures dans un format éditorial favorisant à la fois la conversation et son archivage\ ? Autrement dit, comment réconcilier le régime social et conversationnel du dispositif avec un régime documentaire\ ?

### Bienveillance

Louise Merzeau emprunte à Belin la notion de «\ bienveillance\ » pour caractériser le dispositif analysé. Chez @belin_bienveillance_1999, la notion relève d'un espace transitionnel _entre_ le dehors et le dedans, espace où l'individu se reconnaît et dans lequel il peut placer sa confiance. Pour Belin, le dispositif est cet espace dit «\ potentiel\ », et «\ repose moins sur l'édition d'une loi que sur la mise en place de conditions\ »\ : conditions d'un possible.

Louise Merzeau reprend le concept de bienveillance dispositive à son compte, insistant sur le caractère environnemental du dispositif, ou spatiale en lien avec «\ l'habiter\ »[^habiter] déjà évoqué.

[^habiter]: Ce rapprochement est également employé par Belin qui l'emprunte à «\ l'habiter\ » de Bachelard (Bachelard, Gaston, La poétique de l'espace, Paris, Presses Universitaires de France, 1957, p. 202-203.).

> «\ Pour négocier l’hétérogénéité des sollicitations extérieures, nous avons besoin de les rendre commensurables avec nos compétences mobilisables. C’est à cet arrangement d’un milieu transitionnel que concourt la médiation des dispositifs. Celle-ci autorise en effet "\ une suspension temporaire de la frontière entre l’intérieur et l’extérieur, frontière qui se trouve remplacée par une relation de rappel, d’assortiment ou de reconnaissance\ " (Belin, 1999, p. 256). Condition de _l’habiter_, cet accommodement avec l’environnement suppose que l’outil soit moins vécu comme instrument que comme augmentation prothétique.\ »

La bienveillance du dispositif relève alors de son appropriabilité, voire de son incorporabilité (jusqu'à la prothèse), à moins que ce ne soit le milieu dispositif qui incorpore l'usage.

Enfin, comme condition du possible, Louise Merzeau rapproche la bienveillance dispositive à la contrainte créative, c'est-à-dire propice à la création, telle que pratiquée dans la littérature «\ oulipienne\ »\ :

> «\ En ce sens, si le dispositif n’est plus à entendre ici dans le sens coercitif que lui donnait Foucault (1977), il désigne encore une contrainte. Moins panoptique que pragmatique, celle-ci instrumente l’autonomie des participants, comme une contrainte poétique guide et libère la créativité du poète.\ »

### Réflexivité

Louise Merzeau rapproche le régime attentionnel du dispositif analysé du régime attentionnel de la co-création, que l'on retrouve par exemple dans les espaces de coworking, les hackathons, et finalement dans toute approche de co-design. Il s'agit de maintenir un niveau d'engagement général suffisamment élevé pour atteindre collectivement un objectif. Ce régime fonctionne sur un premier principe de fragmentation des tâches selon les compétences et appétences des participants, respectant également les temporalités et les degrés de participations de chacun. La modularité est en effet une des caractéristiques du dispositif tel que Louise Merzeau le décrit\ :

> «\ Chaque application privilégie un outil, une temporalité (avant, pendant, après), une forme sémiotique (image, texte, oralité) et une modalité participative spécifique (commentaire, annotation, documentation, témoignage, archivage…), sans chercher à les écraser sous une même logique ou les ordonner dans une arborescence unique.\ »

Bien entendu, le risque d'un tel régime est de verser dans un néo-fordisme, et de générer finalement une prolétarisation [@faure_proletarisation_2009] des individus. Louise Merzeau montre que le dispositif évite cet écueil de deux manières\ :

1. par la déhiérarchisation des processus, propre aux approches design.
2. par la capacité du dispositif à offrir des visualisations de son propre processus, jouant le rôle «\ de boussole et de régie\ ».

Autrement dit, ces visualisations permettent aux individus de conserver une _vision_ d'ensemble, de se repérer, se positionner, «\ se régler\ » sur les actions des autres. Elles donnent à voir et à lire les temporalités à l’œuvre, la communauté («\ sociabilité\ »), et finalement la finalité de l'ensemble.

Ce _don_ de données revient à rétablir une certaine symétrie entre la communauté productrice et les plateformes détentrices. Une symétrie dans le sens d'un rééquilibrage (relatif) de pouvoir.

Ces visualisations sont identifiées par Louise Merzeau comme un pivot essentiel du dispositif. D'une part, elles garantissent aux individus un accès et une maîtrise de la finalité partagée par la communauté, et d'autre part, puisqu'élaborées (calculées) par la machine sur la base des traces produites par les participants, elles permettent aux participants d'acquérir une réflexivité sur ces mêmes traces, première condition vers leur interprétation.

Finalement, ces visualisations sont d'un côté vecteur d'engagement (le moyen), et de l'autre vecteur d'interprétation (la fin). L'interprétation fonctionne ici à la fois sur la synthèse visuelle, vecteur réflexif par excellence, et sur l'association de fragments ou de ressources, dans l'interstice de laquelle elle peut se nicher.

### Appropriation

La réflexivité constitue un premier vecteur herméneutique relativement classique. L'analyse de Louise Merzeau fait ressortir un second vecteur, celui de l'appropriation. Cette dernière se concrétise dans le dispositif analysé à travers les «\ actions dispositives\ » des individus lorsqu’ils sélectionnent, organisent et réécrivent les ressources, autrement dit lorsqu'ils les «\ éditorialisent\ » [@merzeau_editorialisation_2013, p111]. On le voit, ces actions s'emparent des fonctions éditoriales classiques\ : la sélection et la structuration des ressources constitutives d'un processus de légitimation ascendant. Les réécritures sont à proprement parler une forme d'appropriation.

En annonçant vouloir «\ interroger les réseaux sociaux [en tant que dispositifs conversationnels] comme agents d'une évolution de la fonction éditoriale et pas seulement comme moyens de socialisation\ », Louise Merzeau anticipait en fait, sans que ce soit explicite dans le texte, cette fonction d'appropriation ou _d'appropriabilisation_. Ce néologisme peut sembler barbare, il est pourtant relativement opérant pour désigner _le processus de mise à disposition en vue d'une appropriation_. Aux fonctions éditoriales traditionnelles de sélection, de mise en forme et de diffusion [@vitali_rosati_ledition_2017], s'ajoute cette fonction d'appropriation consistant pour les institutions et les éditeurs numériques à créer les conditions de possibilité de l'appropriation, c'est-à-dire d'élaborer les dispositifs d'éditorialisation pré_disposant_ les ressources à leur appropriation.

Pour Louise Merzeau, le succès du dispositif analysé en terme d'engagement des contributeur s'explique précisément par le fait qu'il a pleinement assuré cette fonction d'appropriation. C'est l'un des enjeux à poursuivre dans la mise en œuvre de la Conversation en tant que dispositif d'éditorialisation.

### Distance

Louise Merzeau nous montre que, dans le milieu architectural qu'est le numérique [@vitali_rosati_editorialization:_2018, p. 38], l'espace est régi par «\ des repères, des références, des normes (lexicales) et des règles d’intelligibilité\ ». Le dispositif instaure en effet un «\ protocole\ » éditorial précis qui conditionne les réécritures.

Cette capacité des participants à suivre, ou plus exactement à jouer de ce protocole, constitue pour Louise Merzeau le «\ savoir-lire-et-écrire numérique\ », ou encore la translittératie. «\ En jouer\ », tant les actions dispositives sont indissociablement des écritures _dans_ le dispositif (éditorialisation des ressources), que des écritures _du_ dispositif (éditorialisation du dispositif). Un autre indice de translittératie transparaît dans le fait que les principaux participants aient également contribué à la conception même du dispositif, dans un processus amont de «\ médiation\ ».

Écritures et réécritures forment ensemble une «\ glose\ », contrainte vertueusement selon des normes et des règles (le protocole). Si la glose consiste par définition en une production de sens constitutive d'une herméneutique, Louise Merzeau cherche encore à mieux la caractériser, à la rattacher au milieu numérique d'où elle émane, afin sans doute de ne pas complètement l’amalgamer à la glose manuscrite. Négociation nécessaire pour penser le numérique entre rupture et continuité. Elle invoque ainsi la culture anthologique [@doueihi_grande_2008] pour préciser la nature particulière de cette glose, fonctionnant en effet par associations sans cesse reconfigurées de fragments et de ressources\ :

> «\ Au centre  de la «\ compétence numérique\ » [-@doueihi_grande_2008], il faut placer l’aptitude à extraire, transférer et recomposer des contenus au sein  d’agencements collectifs. Dans ce «\ partage anthologique \[…\], sélection subreptice de fragments pour les diffuser sous forme de  recueils signifiants \[…\], le sens dérive largement d’une association  des contenus\ : au lieu d’être nécessairement lié à des auteurs, avec leur identité ou leurs intentions, il l’est plutôt à une catégorisation  flexible\ » (*Ibid*., p. 70). Alors que dans la culture littérale, l’ordonnancement des unités s’opère dans des milieux homogènes (texte,  livre, bibliothèque), il s’effectue dans la culture translittérale à  l’intersection de systèmes hétérogènes interopérables.\ »

Ces _intersections_ sont des zones potentielles d'association, où deux unités en produiront une troisième (1+1=3). Mais l'association n'est pas fusion, et entre les fragments de sens se loge toujours un _espace interstitiel_, matérialisant à la fois le rapprochement, nécessaire à l'interrogation, et la distance, propice à la critique. Finalement, ces interstices sont les lieux mêmes de l'interprétation. Cette distance joue le rôle de _coupe_-circuit, inhibant les comportements réflexes et réhabilitant nos capacités cognitives et interprétatives _court_-circuitées par les logiques affinitaires, algorithmiques et probabilistes des réseaux sociaux\ :

> «\ Cette inquiétude n’est pas tant celle de l’accélération que de l’écrasement des distances sur des proximités toujours plus étroites\ : proximité affinitaire, promue au rang de principe d’autorité par les réseaux sociaux, proximité algorithmique, ramenant tous les  contenus au stade de données statistiquement corrélables, proximité  probabiliste, évacuant tout écart d’incertitude au profit d’une  prédictibilité des comportements.\ »^[@merzeau_editorialisation_2013, p. 115]

De ce point de vue, Louise Merzeau nous livre une critique fine des dispositifs conversationnels des grandes plateformes algorithmiques dont elle pointe elle aussi «\ l'aliénation attentionnelle\ ». À nouveau, loin de s'arrêter au constat largement consensuel [@morozov_save_2013; @cardon_democratie_2010; @ertzscheid_entre_2014], elle nous confie les clés d'un horizon dépassant l'état de fait que nous imposent les GAFAM\ : distance, appropriation, réflexivité et pour accueillir les trois premières, bienveillance.

# Thèse : faire collectif

:::: note
Ce que nous montre le développement de l'hypothèse précédente, c'est un changement de paradigme par lequel la communauté scientifique déplace ses efforts mis dans la production de connaissances vers la production du collectif. C'est ce déplacement du document vers le collectif qui me semble en effet être une piste prometteuse pour réinventer l'université.
::::

# Etudes de cas

:::: note
à venir
::::

# Bibliographie
