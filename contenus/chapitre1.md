---
title: chapitre 1
subtitle: Tentative de balisage de mon champs de recherche
date: 2019-09-06
version: 0.2
bibliography: chapitre1.bib
link-citations: true
lang: fr
---

:::: note
Dans cet exercice, je vais tenter de baliser le champs que va couvrir ma thèse, en introduisant les différents aspects que la thèse viendra traiter et en tentant de les articuler.

Plusieurs éléments méta pourront venir structurer la thèse\ :

  - une méthodologie très axée sur le faire et la praxis qui vient m'ancrer dans une démarche DH, mais qui pourra aussi résonner avec l'écriture et l'édition de la thèse elle-même : une thèse produite comme une expérimentation éditoriale (voir la thèse d'Anthony Masure par exemple).
  - outre l'expérimentation éditoriale, je pourrais également tenter un renouvellement de l'écriture formelle de la thèse, en proposant une thèse écrite sur le mode de la conversation, invitant plusieurs entités à dialoguer et à se répondre. À voir comment cela peut prendre forme.
::::

# Multiplication des espaces et des formes conversationnelles

[Cette partie est un premier constat, celui d'un double déphasage institutionnel et éditorial entre les formes institutionnalisées de communication scientifiques et la diversification des pratiques d'écriture dans la communauté scientifique.]{.chapeau}

  <!-- va introduire ce moment particulier dans lequel nous sommes où les pratiques d'écriture des chercheurs se diversifient, et avec elles les formes de communication scientifique, mais sans que ces formes soient reconnues par l'institution scientifique. -->

## Constat
<!-- reprise de [[phd/humanistica/artHumanistica_1.0]], repris de ns_chp1_Introduction -->

En quelques dizaines d'années, les pratiques de lecture et d'écriture des chercheurs se sont progressivement installées dans l'environnement numérique, au point que ce nouvel environnement de travail s'est largement substitué au support papier dans le quotidien des chercheurs. Les écrans, que ce soient l'ordinateur, la tablette ou encore le smartphone, sont en effet devenus les supports majoritaires pour rechercher, lire ou étudier, référencer, collectionner ou archiver, écrire ou éditer, communiquer et échanger. [étude sur les pratiques d'écriture]{.note} Une exemple emblématique de cette mutation des usages est celui du logiciel _Word_, utilisé à l'université comme outil d'écriture, de note de lecture et finalement pour toute création de document, amalgamant tout aussi bien la rédaction d'articles et l'édition des thèses et mémoires, que les documents pédagogiques, les exercices d'étudiants, les appels à communication, les programmes de colloque, ou encore les formulaires administratifs. Les investissements parfois colossaux des institutions académiques dans la suite Office de Microsoft illustrent bien l'étendue de ses usages à tous les secteurs d'activité de l'Université [trouver des contrats emblématiques + chiffres]{.note}.

Cette lente mutation des pratiques est relativement récente dans les lettres et les sciences humaines et sociales. La thèse d'Eric @guichard_linternet:_2002 nous rappelle que l'introduction du numérique chez les chercheurs et les étudiants ne s'est pas fait sans difficultés. Dans cette thèse sur l'adoption (ou la no-adoption) de l'Internet dans les rangs de l'ENS au tournant du millénaire, Guichard s'intéresse aux chercheurs et étudiants en lettres, car «\ ils ont cette particularité de valoriser et de commenter abondamment les effets secondaires de l’écriture (culture, histoire, patrimoine, identités, etc.) tout en affichant souvent un mépris pour la technique et un faible intérêt pour l’explicitation des relations entre écriture, outil et pensée\ » [@guichard_linternet:_2002, Introduction]. Son constat est éloquent\ :

> L’acquisition d’un ordinateur, souvent pensé comme une machine à écrire, signifie une perte de standing, puisque qu’il témoigne de l’absence d’un ou d’une secrétaire. Et même une fois dépassée cette première attitude de rejet, il est difficile d’admettre qu’une machine puisse s’intégrer dans une quelconque panoplie de techniques intellectuelles.

Et de citer le rapport de Christine @ducourtieux_lusage_1998 \ : «\ que l’ordinateur soit une machine à écrire performante, cela est parfait. Qu’il permette l’accès à d’autres sources de savoir, cela ne peut être toléré. Il ne s’agit pas de ‘blocage’ mais bien plus d’une volonté de résistance\ ».

Si les sciences dures, ou certaines humanités à l'ENS comme les géographes, les sociologues ou les philosophes, avaient d'ors-à-déjà largement adopté l'informatique pour la recherche ou l'édition, l'usage du numérique pour les départements d'histoire, de littérature et langages ne s'est véritablement répandu qu'avec la démocratisation au grand public de l'ordinateur personnel et du web dans la seconde moitié des années 2000.

Une telle méfiance, souvent mal réfléchie voire réactionnaire, a pu quelques années plus tard se voir confortée par des critiques plus documentées comme celle de Nicholas @carr_internet_2011 qui pointe un profond changement dans les modes de lectures\ :

> Depuis toujours, nous survolons plus les journaux que nous ne les lisons, et nous parcourons les livres et les magazines pour en saisir l’essentiel et décider s’ils méritent d’être lus plus avant. Il est tout aussi important d’être capable de lire en diagonale que de lire en profondeur. Mais ce qui est différent, et qui dérange, c’est que le survol est en train de devenir notre principal mode de lecture. C’était naguère un moyen pour arriver à une fin, une façon d’identifier les informations à lire en profondeur ; maintenant, cela devient une fin en soi –, c’est notre méthode préférée pour recueillir et comprendre les informations de toutes sortes.

Pourtant, depuis la sortie très médiatique des idées de Nicholas Carr, son point de vue a été relativisé par des conceptions moins déterministes de la technique. Alain Giffard remarque notamment que Carr «\ n’envisage pas la possibilité que le lecteur, par un régime d’exercices appropriés, puisse conquérir son autonomie par rapport au dispositif technique, voire le détourner\ » [@giffard_critique_2011]. Il affine la thèse de Carr en proposant la notion de «\ lectures industrielles\ » [@giffard_lectures_2009], dont l'élément déterministe sur la cognition ne réside pas dans la technique elle-même, à savoir l'environnement numérique qu'est Internet comme le prétend Carr, mais dans un (et plusieurs en fait) projet industriel «\ qui cantonne la lecture à une activité de communication, et nuit à l’association de la lecture et de la réflexion\ ». Il appelle d'ailleurs à un nouveau projet industriel de la lecture, que nous pourrions étendre à toute technique intellectuelle.

::: note
Autre citation possible pour Guichard:

> [L]a très grande majorité des personnes qui découvraient les ordinateurs [en 1996] et en acceptaient l’image commune de machine à écrire améliorée ne pouvaient concevoir que ces machines à produire du signe avec du signe puissent transformer un tant soit peu leurs pratiques. Elles s’inféodaient à des codages abscons, ne connaissaient pas l’internet, disposaient d’une très faible culture informatique, restaient —souvent isolées— dans leurs laboratoires traditionnels, et prenaient le risque de devenir rapidement illettrées [@guichard_linternet:_2002, Situation en 1996].

:::

En fait, ce que pointent ensemble ces auteurs, c'est un changement profond des modalités de production des connaissances. La question est de savoir quelle est la nature de ce changement, au-delà de projets industriels particuliers, et comment produire encore la connaissance dans l'environnement numérique\ ?

L'histoire longue de l'écriture, qui est aussi celle de ses supports, nous montre que tout changement de propriété du support d'inscription induit un changement des modalités d'écriture, de lecture et finalement une évolution dans les modes de pensée [@goody_raison_1979; @jacob_quest-ce_2014] et de formation des savoirs.

Les institutions directement impliquées dans la formation des savoirs, leur pérennisation ou leur transmission, ont toujours été des lieux d'innovation quand aux modalités de la production des connaissances. Que l'on pense, du côté des bibliothèques, aux différents systèmes de classement et de catégorisation, ou du côté des académies, par exemple à l'établissement de normes d'évaluation des connaissances, les institutions ont en effet joué un rôle important dans l'élaboration des modèles épistémologiques qui régissent les sciences aujourd'hui. C'est ce qu'a permi de montrer l'approche anthropologique de Christian @jacob_quest-ce_2014 lorsque celui-ci _situe_ le développement de pratiques et de gestes caractéristiques de l'activité savante, au sein des institutions académiques (mais pas seulement). En tant que lieu de savoir, l'université s'est naturellement confrontée aux effets induits par l'évolution des supports d'écriture et de lecture. Mais en a-t-elle était le prescripteur ou le moteur\ ?

Face à ce nouvel environnement, certaines institutions du savoir ont amorcé des adaptations conséquentes. Du côté des institutions para-académiques d'édition et de diffusion de la production scientifique, les éditeurs scientifiques, les librairies et les bibliothèques universitaires ont mis en route plusieurs chantiers dans le sens d'une plus grande accessibilité aux connaissances. Dans la continuité de l'informatisation des catalogues de bibliothèque, de la numérisation des imprimés ou encore de l'adoption de formats numériques standardisés, le mouvement de l'Open Access a pu se développer et gagner progressivement un terrain technologiquement plus favorable. Plus précisemment, le terrain technologique s'est développé conjointement avec des pratiques juridiques plus favorables à _l'ouverture_ des contenus, selon les principes du _copyleft_ déjà en cours dans d'autres communautés d'écriture. Les mouvements _Open source_ ou _Free Software_, nés peu avant l'explosion de l'Internet, se sont rassemblés autour du principe de partage des codes sources informatiques.
Afin de se prémunir des effets de clôture^[la langue anglaise propose le terme d'_enclosure_ pour désigner l'établissement de barrières autour d'un bien, matériel ou informationnel] de la propriété intellectuelle et de ses restrictions d'accès et de partage de ces _écritures_ programmatiques, ces communautés de développeurs ont su inverser le principe du copyright en associant aux sources une licence d'utilisation particulière, imposant à la fois le partage et la viralité de la licence. Ainsi, en jouant du droit de la propriété intellectuelle, ces licences en inverse le sens et la nature, transformant le _copyright_, c'est-à-dire le droit de reproduction, en un _copyleft_, c'est-à-dire la cession de ce droit de copie à tout à chacun à la seule condition de propager cette cession aux nouvelles copies. Les licences _Creative Commons_ ont appliqué ce principe du _copyleft_ aux contenus culturels, libérant ainsi tout un pan de la production culturelle à partir des années 2000. Il est important de noter ainsi que l'émergence du Libre Accès dans la communauté scientifique hérite d'un ensemble de valeurs culturelles apparues avec le développement de l'Internet et du Web. Il ne faut pas s'y tromper, ce n'est pas fondamentalement le numérique qui a permis l'apparition de ces valeurs, mais bien des communautés de pratiques engagées dans le développement informatique, puis dans l'établissement des normes et des standard du web et de l'internet, et finalement dans la libre circulation des contenus culturels.
Si la technique n'est pas déterministe ici, il faut noter l'aspect récursif de ces valeurs, d'abord appliquées aux logiciels informatiques et à leurs infrastructures, puis appliquées aux contenus culturels qui y circulent. L'application de l'ouverture et du partage à la production académique n'en est que la suite logique, dans une communauté scientifique traversée, on le verra, par des courants et des pratiques souvent antagonistes.

En France, la plateforme HAL, lancée par le Centre pour la communication scientifique directe (CCSD) du CNRS, a pour objectif le dépôt et la diffusion d'articles, de thèses et plus largement de documents liés à la recherche. Ce type de plateforme destinée au partage des connaissances, associé aux injonctions émises par les organismes subventionnaires pour contraindre les chercheurs ou les projets subventionnés à publier leurs résultats et leurs données en libre accès, ont su améliorer l'accès et la diffusion.
Cependant, de telles mesures à l'initiative d'institutions publiques restent de l'ordre de l'ajustement au regard des potentialités réelles de l'environnement numérique d'une part, et des pratiques émergentes de la communauté académique d'autre part. C'est le constat que faisaient également @mounier_edition_2011 en décrivant le secteur de l'«\ édition numérique\ »\ : «\ Dans l'édition numérique, le réseau n'intervient qu'en fin de chaîne, au niveau de la diffusion des contenus. Il n'y est utilisé que marginalement et dans un seul sens\ : afin de les faire parvenir à ses lecteurs\ ». Finalement les acteurs, notamment de l'édition et de la diffusion, ont en quelque sorte mécanisé et automatisé des services qui existaient déjà. La normalisation des contenus, l'indexation de document, et par conséquent la recherche d'information, se sont améliorés, conjointement à leur accès et leur ouverture. Les acteurs se sont ainsi ajustés au nouvel espace de diffusion qu'est le web, assurant la mission qui leur incombait déjà, mais sans investir le numérique comme une véritable révolution de l'écriture. Les années 2000 et 2010 ont pourtant témoigné d'une extraordinaire explosion des formes d'écriture et de communication, dont on a vu l'impact sur le plan social, politique, économique, et ce tant sur un plan mondial horizontal, que sur le plan social vertical, touchant tous les individus.

<!-- ### Désintermédiation des institutions et nouveaux acteurs -->

En effet, de nouveaux acteurs se sont progressivement _institués_ dans le champs de la production, de la légitimation et de la diffusion de contenus, en proposant des outils et appareils numériques d'écriture, d'édition et de publication susceptibles de supplanter les canaux institutionnels. Ces acteurs viennent directement du secteur privé et plus particulièrement de la Silicon Valley, dominante sur le marché des OS^[_Operating System_, ou système d'exploitation «\ qui dirige l'utilisation des ressources d'un ordinateur par des logiciels applicatifs\ » (Wikipédia.fr).], des logiciels et des algorithmes [@mounier_edition_2011; @vitali-rosati_edition_2016].
Or, si les GAFAM^[du nom des cinq principales entreprises Google, Apple, Facebook, Amazon et Microsoft.] ont pu fournir au monde un écosystème d'écriture, d'édition, de publication et de communication, c'est qu'elles ont pleinement exploité les potentialités du numérique, c’est-à-dire tout à la fois, celle du _processeur_ comme technologie de calcul, du _réseau_ comme technologie de communication, et du _web_ comme technologie de publication.

À titre d'exemple, un service comme _Google Documents_ témoigne de l'extrême agilité de ces sociétés pour continuellement améliorer leurs logiciels sur le plan ergonomique, comme sur un plan systémique, avec une intégration extrêmement poussée techniquement dans l'écosystème numérique, qu'ils ont par ailleurs largement contribué à façonner. _Google Documents_ a ainsi été un des premiers éditeurs de texte à fonctionner en édition collaborative synchrone, à proposer le partage de document, à intégrer un moteur de recherche permettant de récupérer du contenu issu du web, à proposer un service de suivi de modification et de commentaires avec notifications par courriel en temps réel, ou encore la publication continue vers des urls pérennes. La technicité qui entoure le document exploite tout à la fois le calcul, la communication et la publication, et l'implante dans un écosystème d'«\ édition en réseau\ ». Elle concrétise finalement ce qu'anticipaient @mounier_edition_2011 avec «\ des formes d’écriture, de coopération et de construction du savoir qui sont susceptibles de dépasser certaines des apories auxquelles mènent les formes traditionnelles de rapport au savoir\ ».

<!-- ### Hybridation des pratiques -->

Or, le monde académique a largement adopté cet écosystème dans ses pratiques scientifiques, que ce soit pour la recherche d'information et de contenu scientifiques (Google Search, Google Scholar), la consultation de ces contenus (tablettes sous IOS, Android, Windows, navigateurs Chrome, Safari), mais aussi pour la publication et la diffusion de ses travaux (réseaux sociaux spécialisés ResearchGate, Academia), ainsi que pour la communication (Gmail, Facebook) et la collaboration (Google Drive, Microsoft 360).

Les réseaux sociaux sont ainsi utilisés par les chercheurs pour communiquer sur leurs travaux, partager leur veille scientifique, commenter et annoter les travaux de pairs, voire pour nourrir quelques controverses. C'est aussi un moyen d'élargir son cercle d'échange et de s'ouvrir à des communautés non-académiques^[comme l'initiative de [\@EnDirectDuLabo](https://twitter.com/EnDirectDuLabo) sur Twitter.]. Les blogs de chercheurs se sont également multipliés dans une démarche de publication continue des travaux et des réflexions d'un chercheur ou d'une équipe. Cette mise en circulation génère des cercles particulièrement vertueux tant pour la communication, la visibilité que pour la recherche elle-même [@blanchard_ce_2010]. Cette pratique tend d'ailleurs à s'institutionnaliser en sciences humaines avec la plateforme Hypothèses.org^[http://hypotheses.org/] qui accueille essentiellement des carnets de recherche, des carnets de projets, des blogs de laboratoire, de chercheurs, de séminaire, et auxquels la BNF attribue un numéro ISBN^[En tant que numéro d'identification unique, l'ISBN est un marqueur important de l'institutionnalisation d'un document, dans la mesure où il est délivré par une institution dont l'autorité vient légitimer l'entrée du document dans l'écosystème professionnel des publications]. La correspondance par courriel et sur les listes de diffusions est un autre exemple d'écrits parfaitement intégrés au travail de recherche, mais relevant malgré tout d'une hybridation des pratiques des chercheurs, dont une large production échappe ainsi à l'institution. En effet, ces écrits ne constituent pas aux yeux de l'institution des connaissances certifiées, c'est-à-dire issues d'un processus de production soumis au jugement des pairs. Parce que ces écrits ne relèvent pas d'un format éditorial institutionnalisé, comme l'article scientifique ou la monographie, l'institution n'est pas en mesure de l'intégrer dans ses protocoles d'évaluation.[à développer]{.note}

On pourrait penser que la première victime de ce système est la conversation scientifique. Pourtant c’est bien l’institution qui en pâtit le plus puisque la conversation, elle, se porte bien, mais hors de la sphère institutionnelle. Finalement, les lieux et les formes de la controverse et du consensus échappent de plus en plus à cette institution.

Un exemple assez emblématique^[[il y a matière à développer cet exemple, tant sur le plan de son sujet que sur la nature de la discussion et de ses ramifications dans les réseaux]{.note}] serait celui de la discussion intitulée [«\ Projet "carrière"\ »](https://groupes.renater.fr/sympa/arc/dh/2019-07/msg00012.html)[^ann-listeDH] qui s'est tenue sur la «\ Liste francophone de discussion autour des Digital Humanities (DH)\ », rapidement relayée et développée sur les réseaux sociaux, notamment sur Twitter, et qui donna lieu, dans le temps de la discussion à plusieurs posts de blogs. La conversation est partie sur une requête anodine d'un étudiant à la recherche d'un poste, s'interrogeant si son profil pouvait être pertinent auprès de la communauté des humanités numériques. Sa question a mobilisé 36 intervenants et un échange de 68 messages autour d'une problématique bien plus large que la question initiale. La vigueur des discussions témoigne du contexte de questionnement que traverse le champ des humanités numérique, champ encore en gestation sur le plan institutionnel, et qui ne cesse de travailler son auto-critique, comme le montre plusieurs auteurs d'article [@citton_humanites_2015, @granjon_critique_2016] et de blogs [@ruiz_dhiha8_2019, @bertrand_fin_2019, @bertrand_pandora_2019].

[^ann-listeDH]: voir en annexe les données de la discussion.

:::: note
Pour certains, ces écrits relèvent d'un registre de discours qui n'a pas lieu d'être considéré comme de la connaissance. On peut notamment citer les entretiens des directrices de revues menés dans le cadre du projet _Revue 2.0_. [ développer et citer Nardout-lafarge]{.note}

La question que je poserai, est que ces écrits, dont le registre de discours est en effet différent, peuvent participer d'une autre science. Paradigme à changer.

> servanne ([24juil2019](https://hyp.is/_pcILK43Eemcby95JRKKmw/laconis.frama.io/these/chapitre1/chapitre1-0.1.html)): Oui, en effet. J'en profiterai pour insister aussi sur la distinction avec la notion de vulgarisation (dont on a pu dire qu'elle va avec le web). Le registre de discours permet aussi de poser la question : n'est-on pas en train de changer la langue même du savoir ? La langue est le premier lieu de la pensée théorique - on pourrait faire un parallèle, peut-être, avec la fin des thèses écrites en latin au XIX^e^ siècle.

> nicolas (12aout2019): Est-ce la langue ou son milieu qui change. Ou ce milieu étant construit par des écritures (programmatiques), est-ce de cette langue (langage ?) là dont tu parles ? Ou bien, parles tu plutôt d'une rhétorique de réseau, qui modifie les formes de discours "utiles" ou "pertinents", vers un chgt de paradigme ?

::::

## Déphasage
<!-- ### Déphasage des artefacts académiques -->

La transformation radicale du support d'écriture et de lecture a bouleversé tant les processus que les artefacts [@jacob_quest-ce_2014], au cœur d'un modèle épistémologique ancien remontant aux fondements de la science moderne qui se met en place au 17^ème^ et au 18^ème^ siècle.

[détour pour rappeler ce modèle épistémologique]{.note} Roger Chartier rappelle comment les précepts du paradigme de cette «science moderne» s'ancrent dans la culture naissante de l'imprimé à travers une série de conventions collectivement partagées, constitutives d'un modèle éditorial.

> «\ En établissant, non sans conflits ni divergences, des règles partagées, mobilisables pour repérer les textes corrompus et les faux savoirs, les gens du livre tentent de répondre au discrédit durablement attaché tant aux livres imprimés qu’à ceux qui les publient.
>
> L’attention portée aux pratiques collectives qui donnent autorité à l’imprimé inscrit l’histoire de la « print culture » dans le paradigme qui a régi la nouvelle histoire des sciences. Celle-ci, comme on le sait, privilégie trois objets : les négociations qui fixent les conditions de réplication des expériences, permettant ainsi de comparer ou cumuler leurs résultats ; les conventions qui définissent le crédit que l’on peut attribuer, ou refuser, à la certification des découvertes en fonction de la condition des témoins et de leur compétence à dire le vrai ; les controverses qui font s’affronter non seulement des théories antagonistes, mais plus encore des conceptions opposées quant aux conditions sociales et épistémologiques qui doivent gouverner la production des énoncés scientifiques sur le monde naturel. Ce modèle d’intelligibilité rend compte avec pertinence des multiples transactions qui donnent, ou tentent de donner autorité à tous les textes et à tous les livres qui proposent des discours inscrits dans le régime du vrai et du faux.\ » [@chartier_limprime_2016]

 Le tournant numérique a favorisé dans le même temps l'émergence et la diversification de pratiques d'écriture et de publication.
Il y a donc un déphasage grandissant entre les artefacts de communication académique et la réalité des pratiques de communication. Ce déphasage se manifeste sur deux plans simultanés, l'un institutionnel, l'autre éditorial.

:::: note
peutêtre intégrer ici un troisième déphasage d'ordre épistémologique qui mettrait en évidence la vacuité de l'évaluation par les pairs telle qu'elle est institutionnalisée, cad ne favorisant pas la conversation. Je peux y intégrer toutes les conclusions des entretiens R20.
::::

:::: note
v0.2: ajouté le passage ci-dessous, où j'ai introduit le lien entre formalisation, institutionalisation et légitimation. Avec un développement/exemple sur Vittu et la formalisation de l'article. Mais du coup, j'ai emmené l'argumentation trop loin, en tout trop éloigné de l'idée de déphasage ici. Une démonstration ou peut-être une problématisation de l'évaluation par les pairs serait peut-être plus appropriée ici.
::::

Nous verrons que ces deux déphasages en amènent un troisième, d'ordre épistémologique. Il existe un lien très fort entre les processus 1) de formalisation des connaissances, matérialisée dans le modèle éditorial, 2) d'institutionnalisation, _signifiée_ par la reconnaissance par l'institution, qui prend forme dans les critères de financement et d'évaluation de la recherche, et 3) de légitimation. Cette dernière résulte en grande partie des deux autres processus. Elle s'y *inscrit* sous différentes formes qui en retour ont valeur de prescription pour un certain modèle et une certaine _adoption_^[[En passant cela me fait penser qu'il y a sans doute une réflexion à mener entre adoption communautaire et adoption institutionnelle]{.note}].
On peut penser par exemple pour l'édition savante aux consignes aux auteurs pour citer et référencer leurs écrits. Ces consignes contraignent les auteurs dans un format de citation, adopté par une communauté et reflétant une certaine appartenance à un cercle de pairs. Le tout vient à la fois légitimer le texte correctement formaté, mais aussi l'objet éditorial par lequel le texte est diffusé, c'est-à-dire la revue. Cette légitimation du texte et de la revue opère tant au sein de la communauté, qu'aux yeux de l'institution.

Les travaux de Vittu sur la genèse et la formalisation du modèle éditorial de l'article dans le _Journal des Savants_ à la fin du 17^ème^ sont éloquents à ce sujet.

> «\ Notre problématique envisage le _Journal des savants_ comme une forme editoriale dont les conditions de production et la matérialité même suscitèrent des positions stratégiques nouvelles dans les champs du savoir et induisirent des recompositions de leur configuration.\ » [@vittu_formation_2002, p.181]

Les aspects éditoriaux tels que la formalisation du titre, la pagination, la façon de référencer les ouvrages, se développent conjointement avec des pratiques reflétant parfois des besoins de légitimation par une autorité savante, parfois des besoins d'indexation pour faire du _Journal_ un véritable outil de recherche, ou encore des besoins institutionnels.

En ce qui concerne le titre des _mémoires_, on va progressivement y insérer des garants, venant légitimer le texte.

> «\ **Des titres informatifs**. Le premier élément de l'appareil éditorial est constitué par titres qui précèdent et présentent les extraits ou les mémoires. Pour ces derniers, la majeure partie du titre indique leur sujet: «\ expérience singulière\ », «\ machines rares et surprenantes\ », «\ expérience curieuse\ », «\ extrait d'une lettre\ », «\ invention\ », «\ description et figure\ ». Mais comme le montrent ces exemples empruntés aux premiers numéros du _Journal_ de l'année 1682, il s'agit à la fois de désigner un sujet, et de mettre en valeur un texte en jouant sur la curiosité et sur l'attrait pour l'extraordinaire. Le plus souvent ce titre désigne aussi un auteur, qui en général est accompagné d'une garantie savante, comme le nom d'un intermédiaire étali dans la République des Lettres («\ Extrait d'une lettre écrite (...) par M. l'Abbé Boisot à M. l'Abbé Nicaise...\ »), ou l'indication d'un position lettrée («\ Extrait d'une lettre écrite (...) par M. Bohn Professeur en l'Université de Leipsich\ »)\ ; soit le recours à des garants ou à des autorités que nous avons déjà noté pour les intermédiaires composant le «\ bureau informel\ ».\ » [@vittu_quest-ce_2001, p.137]

Il est intéressant de noter le rôle de ce «\ bureau informel\ » qui a été créé sous la pression du lectorat (la communauté de pairs), se plaignant (auprès du Chancelier ayant autorité sur le privilège) d'une trop grande partialité de l'éditeur. Ce dernier s'est vu contraint de s'entourer d'un «\ bureau\ » indépendant pour légitimer la sélection éditoriale du _Journal_ et les commentaires critiques publiés. On y décèle les prémisses de l'évaluation par les pairs qui deviendra un pilier de l'épistémologie des revues savantes, et de surcroît de la science moderne.

La normalisation des titres a pu cependant être motivée par d'autres aspects, plus économique, mais reflétant malgré tout un certain jeu d'autorité entre les acteurs de l'édition savante. Par exemple pour les _extraits_ publiés dans le _Journal_, l'allongement et la formalisation des titres (entre 1665 et 1714 selon son étude), Vittu conclut:

> «\ La présentation des titres des extraits, d'abord inspirée des habitudes propres aux échanges épistolaires entre lettrés informés évolua donc, d'une part, sous l'influence d'un souci bibliographique - celui qui animait à la même époque les Garnier, les Boulliau, les Clément, ou les Marchant-, et d'autre part selon la perspective commerciale qui conduisait les rédacteurs à insérer des avis suggérant aux libraires de leur confier leurs nouveautés.\ » [@vittu_quest-ce_2001, p.139]

Finalement, Vittu remarque à plusieurs reprises que les modalités éditoriales de l'article tel qu'elles se sont formalisées dans les premières années du _Journal_ ont marqué l'activité savante, associant aux périodiques savants un certain modèle épistémologique\ :

> «\ le mot article rend bien compte de cette articulation d'une rhétorique acceptée par la communauté savante et d'un appareil offrant la possibilité d'une lecture aléatoire du journal savant en plus de sa lecture séquentielle.\ » [@vittu_quest-ce_2001, p.148]

[Réarticuler avec l'idée du Déphasage]

### Un paradoxe institutionnel
<!-- ns_fabriqueSP -->

Des échanges épistolaires aux premiers périodiques, la mécanisation de la communication scientifique a été contingente de sa marchandisation [@guedon_lhistoire_2017]. Les modalités juridiques et économiques de la publication savante ont ainsi toujours négocié entre les idéaux de transmission et de partage qui caractérisaient la communauté savante de la République des lettres, et une nécessaire rémunération des métiers de l'édition et de l'imprimé.

Accompagnant une marchandisation croissante du monde au cours du 20^ème^ siècle, l'édition scientifique prend véritablement un tournant avec l'adoption par les institutions universitaires du _Science Citation Index_ de Eugene Garfield [-@garfield_citation_1955; -@garfield_evolution_2007]. En rapportant l'essentiel de l'évaluation de la recherche à une évaluation quantitative des revues, Jean-Claude Guédon [-@guedon_lhistoire_2017] associe ce tournant à une dérive institutionnelle de la communication scientifique, progressivement soumise à des contraintes et des motivations de natures diverses, mais surtout de plus en plus éloignées de sa mission première, l’élévation des connaissances.

D'un côté, les grands éditeurs qui détiennent notamment les _core journals_, ont rapidement saisi l'enjeu économique d'une telle hiérarchisation des revues, surtout lorsque celle-ci est légitimée par les institutions. Les éditeurs vont alors redoubler d'effort pour propriétariser les connaissances. Paradoxalement, alors même que l'essor de l'Internet et du web s'accompagne d'un vent nouveau, porteur des valeurs d'ouverture et de partage, le numérique va permettre d'accentuer cette mainmise des éditeurs sur les connaissances, à travers «\ une révolution silencieuse du cadre légal\ » associé aux revues [@guedon_lhistoire_2017]. Les éditeurs font définitivement entrer les publications scientifiques dans l'âge de l'accès [@rifkin_lage_2000] en introduisant la licence comme principale modalité juridique et économique de transaction avec les bibliothèques universitaires. Les _Big Deals_ qui lient contractuellement les éditeurs aux bibliothèques ont véritablement privé celles-ci de leur catalogue, n'en maîtrisant plus ni le contenu, ni les conditions d'usage.

Cette situation est aujourd'hui largement décriées, en témoignent les désengagements souvent bruyants de bibliothèques universitaires emblématiques pour dénoncer les accords passés. On peut citer à titre d'exemple les annulations d'abonnements de périodiques _Springer_ et _Taylor and Francis_ par l'Université de Montréal à l'été 2019 [@veillette-peclet_annulations_2019; @veillette-peclet_annulations_2019-1; @veillette-peclet_annulation_2019] [traiter ces références en une seule nbp plutôt ?]{.note} [voir le livre de lankes pour d'autres initiatives]{.note}, ou les initiatives en faveur de l'_Open Access_, comme celle de l'Université de Liège qui a décidé en 2015 de tenir compte uniquement des publications diffusées en livre accès pour les évaluations de ses professeurs [@dumont_les_2015], ou encore simplement les pratiques des chercheurs pour _libérer_ certaines publications. [développer sur les pratiques de publications alternatives peut-être dans la partie sur les pratiques.]{.note}

::: note
voir aussi:

- intiative thecostofknowledge.com
- l'Université de Liège, qui ne tient compte que des publications diffusées en libre accès pour les évaluations de ses professeurs. (voir @dumont_les_2015)
- initiative Knowledge Unlatched

:::

De l'autre côté, c'est toute l'évaluation de la recherche qui a été bouleversée. Avec l'injonction faite aux chercheurs et aux laboratoires de publier dans les _core journals_, identifiés par Garfield sur des bases aujourd'hui critiquées[^corejournals], l'institution a directement indexé les carrières personnelles et les financements de recherche sur le rang des revues dans lesquelles ces recherches sont publiées, et non sur la qualité des recherches ^[[ici encore (ailleurs en fait, où ?), on pourra illustrer le fait que l'évaluation par les pairs est un concept très relatif d'une revue à une autre, non pas pour démontrer une supposée scientificité dans des revues considérées plus rigoureuses, mais plutôt pour démontrer que la légitimation scientifique se joue ailleurs -- voir les entretiens R20]{.note}].
C'est bien l'institution qui est en cause, puisqu'elle continue de légitimer et de soutenir un modèle pervers basé sur un leitmotiv\ : _"publish or perish"_ et sur une métrique\ : l'_impact factor_, devenus de fait l'alpha et l'oméga de tout chercheur.
Dans ce système, la fonction des revues revient à un simple _vecteur de capital symbolique_ [@lariviere_oligopoly_2015] au service d'une évaluation quantitative de la recherche.

[^corejournals]: L'universalisme scientifique, sur lequel Garfield légitime la hiérarchisation des revues, repose sur «\ une sorte de convergence tacite universelle entre scientifiques où s’exprimerait l’ensemble des questions importantes en science à un moment de son histoire\ »[@guedon_lhistoire_2017], principe séduisant mais qui ne résiste pas à l'épreuve de l'influence des plus puissants.


### Un paradoxe éditorial

Malgré la multiplication et l'hybridation des pratiques des chercheurs, les circuits de légitimation de l'institution académique reposent encore principalement sur les formes les plus traditionnelles de la publication et de la communication, à savoir\ : la monographie, la communication de colloque et de conférence, et bien entendu l'article publié dans une revue scientifique.

<!-- 24aout2019 - ajout passage avec citations Intermédialités/Et.Franç. -->

Lors de nos entretiens avec les directeur·rice·s de revues savantes, l'une d'elle assurait demander à ses auteurs de ne pas mettre en ligne les versions _preprint_ de leur texte sur leur site ou sur les espaces institutionnels prévus à cet effet (Paryrus au Québec ou HAL en France par exemple). Elle justifiait cette demande par le fait que les consultations du preprint pénaliseraient les statistiques de consultation sur Erudit, la plateforme de diffusion officielle, et mettraient en péril l'évaluation de la revue auprès de ses financeurs, le FRQSC et le CRSH, deux organismes pourtant promotteurs du Libre Accès depuis plusieurs années.

> -- M.T.\ : Une chose aussi qu'on va refuser c'est que les auteurs rendent disponibles leurs pdf sur Academia par exemple, parce que nous il faut qu'on récolte les clics dans Érudit pour les demandes de bourse, donc il faut une centralisation de la lecture qui soit faite sur Érudit, mais en même temps on fournit des pdfs, donc ça c'est un peu difficile à contrôler.
> -- M.F.\ : Et ça voudrait dire qu'il faudrait quelqu'un qui prenne la peine de surveiller tous les profils Academia de tous nos auteurs pour voir la diffusion de tous nos auteurs, c'est trop compliqué.^[Entretien avec Marion Frogier, directrice de la revue _Intermédialités_ et Maude Trottier, secrétaire de rédaction, Montréal, 2 mai 2019[réfléchir sur la façon de citer ces entretiens.]{.note}]

Ce paradoxe est intéressant à plusieurs niveaux. D'une part, il montre que l'injonction institutionnelle peut s'avérer à double tranchant et desservir l'objectif initial d'ouverture des connaissances. D'autre part, il montre la méconnaissance des éditeurs sur les flux de circulation, qui indiquent que plus un contenu circule, plus il circule [référence? quelle est cet article qui indique que l'ouverture favorise la circulation?]{.note}, c'est-à-dire que plus un texte est lu et cité, plus ses différents artefacts seront également lus et cités, notamment sa version "officielle". Cette méfiance face à des circulations non-maîtrisées révèle enfin qu'une libre et étendue circulation de la connaissance n'est pas nécessairement une fin en soi pour les éditeurs[^altmetrics]. Ils restent en effet attachés à une diffusion institutionnelle, mesurée statistiquement et supposée être seule porteuse de légitimité pour la revue elle-même.

La même éditrice déclarait à ce propos regretter la diffusion papier au motif qu'elle lui permettait de connaitre précisemment son lectorat à travers les commandes et les abonnements. Ces données permettaient en effet de connaitre la répartition géographique des lecteurs, mais surtout, selon ses dires, de savoir quelles institutions étaient abonnées.

> -- M.F.\ : Surtout qu'à ce moment-là je pense que la FRQ obligeait les revues à se numériser entièrement, et qu'on ait commencé à voir l'impact de cette transformation sur nos lecteurs. À la fois il y a eu de la contrainte, et à la fois il y avait cette découverte, cet espoir que la numérisation allait nous ouvrir un champ incroyable de nouveaux lecteurs. La seule chose c'est que c'est arrivé à un moment où la revue commençait à se faire bien connaître avec des tas d'abonnements, individuels, avec des bibliothèques, partout dans le monde, Philippe avait fait un travail extraordinaire là-dessus, il y avait beaucoup de bibliothèques et d'universités américaines, européennes qui avaient répondu à nos démarches, et ça, dans le passage au numérique, à Érudit, c'est tout tombé à l'eau. Parce qu'Érudit ne fait que des paniers d'abonnements et ne gère pas des abonnements de revues, d'institutions directement. Donc à la fois on a eu beaucoup plus de lecteurs, mais en termes d'institutions on a eu une perte. On est plus qu'avec Érudit maintenant, ou avec quelques rares bibliothèques qui continuent à nous suivre. Et cette économie là nous est rentrée dans le corps aussi, parce qu'on voit maintenant qu'on est reliés à des logiques qui nous dépassent, en termes d'abonnement, de coût, de rayonnement, tout passe par Érudit, on s'est relié dans le rayonnement d'Érudit elle-même. On a perdu la maîtrise de ça tout en ayant gagné beaucoup de lecteurs. Au niveau de notre rayonnement purement institutionnel on y a perdu.^[Entretien avec Marion Frogier, directrice de la revue _Intermédialités_ et Maude Trottier, secrétaire de rédaction, Montréal, 2 mai 2019]

Ainsi, le prestige de telle ou telle bibliothèque nord-américaine ou de tel institut europpéen s'abonnant à la revue constitue pour ses éditeurs une «\ assise institutionnelle\ » légitimante pour la revue. C'est cette assise institutionnelle qu'ont perdu les revues avec le passage à une diffusion numérique sur Erudit. En effet l'accès institutionnel aux revues se faisant désormais à travers des bouquets de revues, les diffuseurs ou les éditeurs ne sont pas en mesure de distinguer l'attachement intellectuel d'une institution pour telle ou telle revue.

On peut reconnaître à cet endroit une certaine perte de valeur symbolique. Cependant, ce constat doit être relativisé. Dans le modèle papier dont l'abonnement était maîtrisé par l'éditeur, il n'est pas évident qu'une fois achetée et disposée dans une bibliothèque, aussi prestigieuse soit-elle, la revue soit effectivement consultée de manière certaine et soutenue. Les bibliothèques n'auront d'ailleurs pas nécessairement de données de consultation, et ne seront pas supposées les rendre publique en vertu de la protection des données personnelles de leurs usagers. Par ailleurs, il faut noter que la diffusion numérique, si elle a changé les modes de lectures et de consultation des revues, par exemple en favorisant un accès plus granulaire à l'article au dépens du dossier ou du numéro, a également permis un élargissement très conséquent du lectorat, tant sur le plan sociologique, géographique ou temporel. En effet, l'indexation des articles par les plateformes grand public de recherche comme Google les a fait sortir du cadre universitaire, leur a ouvert des zones géographiques qui n'avaient pas ou peu accès aux versions papiers, ou encore leur a donné une nouvelle visibilité dans le temps long, selon le principe de la _longue traine_ [@anderson_long_2004]. Tout cela laisse penser que la diffusion numérique assure sa fonction de circulation plus nettement [reformuler]{.note} que ce qu'une diffusion papier le permet.

[^altmetrics]: Pour répondre aux inquiétudes concernant la diffusion non institutionnelle, des outils de mesures alternatifs ont emergé, comme les [_Altmetrics_](http://altmetrics.org/manifesto/). Si les organismes financeurs ne leur accordent pas encore la même place que les statistiques de consultation du diffuseur officiel, il faut noter que les formulaires de demande de financement du FRQSC et du CRSH permettent aujourd'hui de rendre compte d'autres formes d'impact de la revue, comme les statistiques du site de la revue, ou ses données _altmetrics_ si elle en dispose.

Les processus institutionnels de circulation et de légitimation sont ainsi déterminés par les préceptes techniques, juridiques et économiques des chaînes de production de l'ère pré-numérique. En effet les maisons d'édition perpétuent des pratiques et des savoirs fondés sur les paradigmes du papier et de l’imprimé, ignorant la nature même du texte numérique et les potentialités de son support.

Les éditeurs avec lesquels nous nous sommes entretenus sont très clairs à ce sujet\ : le numérique n'est perçu et compris que dans sa fonction de diffusion des contenus. S'ils reconnaissent que le numérique a transformé les pratiques d'accès et de lecture, il n'aurait affecté ni le modèle, ni le travail éditorial, autrement dit le numérique n'aurait eu aucun impact sur la production des revues. La directrice de la revue _Études françaises_, très attachée à l'idée «\ qu'écrire avec précision et élégance garantie notre rigueur scientifique\ », déclare ainsi\ :

> -- E.N.\ : «\ Mais je ne crois pas que la qualité du travail soit différente. C'est beaucoup rentré dans les mœurs. On a été amenés à réfléchir à la différence quand les organismes subventionnaires ont sous-entendu de toutes sortes de manière, que le numérique demanderait moins de travail. Nous, on a toujours soutenu que non. La somme de travail d'un article papier ou sur Érudit, c'est la même somme.\ »
> [...]
> -- E.N.\ : «\ Que les textes soient papiers ou distribués sur la plateforme Érudit, notre travail est le même.\ »^[Entretien avec Élisabeth Nardout-Lafarge, directrice de la revue _Études françaises_, et Jean-Benoît Cormier Landry, secrétaire de rédaction, Montréal, 7 mai 2019].

Ces propos sont révélateurs d'une certaine inertie, notamment institutionnelle, sur la production et les formes de la communication scientifique. Ainsi, la remarque très juste de la directrice d'_Études françaises_ dans la suite de l'entretien\ :

> -- E.N.\ : «\ Cette somme de travail [le travail d'édition numérique et de diffusion d'Érudit] est reconnue par le FRQ, alors que la somme de travail sur le contenu n'est pas reconnue par le FRQ. Il reconnaît la somme de travail sur le contenant et fait comme si le contenu était gratuit à produire. En 5 ans on a eu le temps de s'habituer, ça a cessé d'être une découverte.\ »^[Entretien avec Élisabeth Nardout-Lafarge, directrice de la revue _Études françaises_, et Jean-Benoît Cormier Landry, secrétaire de rédaction, Montréal, 7 mai 2019].

 Pourtant, nous l'avons déjà évoqué, les effets du support d'écriture et de lecture sur les modes de pensée ne sont plus à démontrer. Les quarante ou cinquante années d'écriture sur ordinateur personnel, et les 25 années d'écriture en réseau depuis l'avènement du web ont nécessairement eu un impact sur les modalités de rédaction et d'édition des auteurs et des éditeurs. Pourtant les formes éditoriales de l'article ou de l'ouvrage, c'est-à-dire les préceptes de production et de structuration des textes sont supposées être restées les mêmes.

Et de fait, l’article, l’ouvrage, les actes, et même les colloques, ne sont pas pleinement passés au numérique. De manière très naturelle, les savoir-faire de l'édition papier ont continué à structurer les artefacts numériques de l’article ou de l’ouvrage.

<!-- 25aout2019 - ajout remédiation -->

On peut voir dans cette continuité une forme de remédiation de la revue papier dans la revue numérique, au sens de @bolter_remediation:_2000 [reference]{.ref}.

Dans son chapitre introductif à l'ouvrage collectif _Théatre et intermédialité_, Jean-Marc Larrue retrace l'évolution de la pensée intermédiale et distingue une première période dite médiatique, associée à l'idée traditionnelle du média dominante des années 70 à la fin des années 90, d'une période post-médiatique qui cherchera à de-essentialiser les médias comme unités discrètes.
[Développer les deux modèles]{.note} On s'inscrira dans le modèle postmédiatique, cad dans une pensée de l'entre, où la revue numérique ne doit pas être pensée comme un média essentialisé, mais toujours comme une dynamique se nourrissant autant des modalités techniques de médias antérieurs ou contemporains, que de pratiques éditoriales, scripturales, sociales, communautaires, qu'elles soient anciennes comme contemporaines.

C'est en pensant ainsi que l'on pourra se dégager des logiques réactionnaires que Larrue désigne comme «\ résistance médiatique\ »\ :

> «\ [L]es processus de remédiation entraînent parfois -- mais pas toujours -- des réactions plus ou moins violentes qui relèvent du principe général de "résistance médiatique" qu'on peut comprendre comme une tentative de contre-remédiation et qui est totalement absent, lui aussi, du champ d'analyse de Bolter et Grusin. Cette résistance est un mécanisme de défense qui se déclenche lorsque des fondements de la médiation sont ou, plus exactement, semblent mis en danger par l'intrusion d'un nouvel élément. Le mécanisme de résistance a pour effet d'empêcher, d'éviter, de retarder l'introduction de ce nouvel élément ou de le rejeter après qu'il se soit introduit.\ » [@larrue_du_2015 , p.34]

Dans le cas du numérique et de ses effets profonds sur les pratiques scripturales des chercheurs ou éditoriales des revues savantes, on peut relier les résistances médiatiques que ces effets suscitent avec la lutte entre «dominants» et «prétendants» que décrit Bourdieu lorsqu'il étudie le champ scientifique en 1976, bien avant que le numérique ne s'introduise dans l'édition.

> Dans la lutte qui les oppose, les dominants et les prétendants, c'est-à-dire les nouveaux entrants, comme disent les économistes, recourent à des stratégies antagonistes, profondément opposées dans leur logique et dans leur principe : les intérêts (au double sens) qui les animent et les moyens qu'ils peuvent mettre en oeuvre pour les satisfaire dépendent en effet très étroitement de leur position dans le champ, c'est-à-dire de leur capital scientifique et du pouvoir qu'il leur donne sur le champ de production et de circulation scientifique et sur les profits qu'il produit. Les dominants sont voués à des stratégies de conservation visant à assurer la perpétuation de l'ordre scientifique établi avec lequel ils ont partie liée. [@bourdieu_champ_1976, p.96]

Bourdieu associe très spécifiquement l'«\ ordre établi\ » à «\ l'ensemble des institutions chargées d'assurer la production et la circulation des biens scientifiques en même temps que la reproduction et la circulation des producteurs (ou des reproducteurs) et des consommateurs de ces biens\ » [-@bourdieu_champ_1976, p.96], comprenant le système d'enseignement d'une part comme transmetteur de la science officielle dans ses deux états, objectivé (les artefacts produits) et incorporé (les habitus scientifiques), et d'autre part les revues scientifiques.

> «\ Outre les instances spécifiquement chargées de la consécration (académies, prix, etc.), il comprend aussi les instruments de diffusion, et en particulier les revues scientifiques qui, par la sélection qu'elles opèrent en fonction des critères dominants, consacrent les productions conformes aux principes de la science officielle, offrant ainsi continûment l'exemple de ce qui mérite le nom de science, et exercent une censure de fait sur les productions hérétiques soit en les rejetant expressément, soit en décourageant purement l'intention de publication par la définition du publiable qu'elles proposent.\ » [-@bourdieu_champ_1976, p.96]

::: note
Citation complète de Bourdieu\ :

> Dans la lutte qui les oppose, les dominants et les prétendants, c'est-à-dire les nouveaux entrants, comme disent les économistes, recourent à des stratégies antagonistes, profondément opposées dans leur logique et dans leur principe : les intérêts (au double sens) qui les animent et les moyens qu'ils peuvent mettre en oeuvre pour les satisfaire dépendent en effet très étroitement de leur position dans le champ, c'est-à-dire de leur capital scientifique et du pouvoir qu'il leur donne sur le champ de production et de circulation scientifique et sur les profits qu'il produit. **Les dominants sont voués à des stratégies de conservation visant à assurer la perpétuation de l'ordre scientifique établi** avec lequel ils ont partie liée. Cet ordre ne se réduit pas, comme on le croit communément, à **la science officielle, ensemble de ressources scientifiques héritées du passé qui existent à _l'état objectivé_, sous forme d'instruments, d'ouvrages, d'institutions, etc., et à _l'état incorporé_, sous forme d'habitus scientifiques, systèmes de schemes générateurs de perception, d'appréciation et d'action**
> qui sont le produit d'une forme spécifique d'action pédagogique et qui rendent possible le choix des objets, la solution des problèmes et l'évaluation des solutions. Il englobe aussi **l'ensemble des institutions chargées d'assurer la production et la circulation des biens scientifiques en même temps que la reproduction et la circulation des producteurs (ou des reproducteurs) et des consommateurs de ces biens**, c'est-à-dire au premier chef le système d'enseignement, seul capable d'assurer à la science officielle la permanence et la consécration en l'inculquant systématiquement (**habitus scientifiques**) à l'ensemble des destinataires légitimes de l'action pédagogique et, en particulier, à tous les nouveaux entrants dans le champ de production proprement dit. Outre **les instances spécifiquement chargées de la consécration (académies, prix, etc.),** il comprend aussi **les instruments de diffusion, et en particulier les revues scientifiques qui, par la sélection qu'elles opèrent en fonction des critères dominants, consacrent les productions conformes aux principes de la science officielle, offrant ainsi continûment l'exemple de ce qui mérite le nom de science, et exercent une censure de fait sur les productions hérétiques soit en les rejetant expressément, soit en décourageant purement l'intention de publication par la définition du publiable qu'elles proposent** (20).

:::


L'analyse de Bourdieu sur le rôle de la revue dans la perpétuation d'un ordre établi insiste sur l'une des fonctions éditoriales classiques, à savoir la fonction de sélection, ici sous forme de filtre conceptuel et thématique. Comme le montrent nos entretiens, les éditeurs se targuent le plus souvent d'une certaine innovation conceptuelle, en particulier chez les jeunes éditeurs ou les jeunes revues, innovation mise en avant de manière assumée pour justement se démarquer des anciens et de l'ordre établi.

> -- M.N.\ : «je pensais qu'_Itinéraires_ était une bonne revue pour faire évoluer les études littéraires et que c'était quand même un outil d'agentivité des esprits. C'est-à-dire que même si c'est des petits articles, même si c'est des petits volumes, 3 par an, que ça pouvait peut-être changer progressivement l'idée qu'on se faisait des études littéraires et de ce que ça peut apporter globalement à la connaissance.»

> -- M.N.\ : «Historiquement, les revues [numériques] ont toujours été un espace exploratoire, plus que les éditions papier où il y a quelque chose de presque patrimoniale. Il y a un coté expérimental dans la revue [numérique] qui est possible.»


Mais, si certaines revues se sont données dans leur mission une volonté d'explorer des territoires conceptuels nouveaux, l'analyse de Bourdieu redevient pertinente lorsque l'on regarde de plus près le conservatisme éditorial de certaines revues, qui semble voir dans le modèle épistémologique établi celui qui leur donnera la plus grande légitimité auprès de la communauté scientifique. Ainsi, la directrice de la revue _Mémoires du livre_ avançait lors de son entretien

> -- M.P.\ : «Quand on a fondé la revue en 2009, il y avait encore un préjugé très fort à l’encontre des revues électroniques. Ce n’était pas un processus aussi rigoureux qu’une revue scientifique papier. Pour se battre contre ça, notre stratégie a été d’appliquer une grande rigidité, beaucoup de rigueur, et c’est pour ça que les évaluations sont au cœur de notre processus. Je pense qu’elles le sont en réalité, parce qu’il y a deux moments d’évaluation, la sélection des propositions d’abord puis la sélection des articles par deux membres externes et un membre interne, la revue a été pensée pour avoir toute cette rigueur et placer l’évaluation au cœur de son processus.»

> -- M.N.\ : «Ça a voulu dire dans un premier temps reproduire les mêmes manières de faire que les revues papier les plus exigeantes, mais le faire dans un contexte numérique, pour justement lutter contre ces préjugés. Ce n’était pas très original au début. Faire la même chose que ce que les revues papier font, sauf que c’est en ligne, c’est gratuit il n’y a pas d’abonnement, mais c’est les mêmes processus de sélection, les mêmes appels de texte, les mêmes comités scientifiques qui viennent donner du capital symbolique, c’est les mêmes pratiques. J’imagine que dans l’avenir il faudra évoluer.»

Il faut remarquer que cet attachement n'est pas sans lien avec la pression institutionnelle des financeurs.

> -- M.P.\ : «\ On est limités à reproduire les exigences que les pourvoyeurs de fonds nous demandent. Au niveau du processus d’évaluation, le CRSH va valoriser cette évaluation-là avec au moins deux experts externes à l’aveugle. À moins qu’eux changent leurs règles, on ne pourra pas les changer non plus. La question du libre-accès a toujours été fondamentale, nous n’avons jamais voulu être une revue sous abonnement, et la conséquence c’est que de faire ce que le patron, en l’occurrence le CRSH, nous dit de faire.\ »

Pour autant, cette éditrice, qui se trouvait être au moment de l'entretien en pleine passation de ses fonctions de directrice de revue, semble suggérer une piste d'évolution et de renouveau pour la revue.

> MP: Il y a ce paradoxe avec lequel on vit et peut-être que l’équipe va devoir se poser des questions et voir ce qu’ils veulent faire avec ça. Cette nouvelle idéologie que les revues peuvent établir un meilleur dialogue entre les auteurs, les évaluateurs, animer la recherche est peut être une deuxième étape dans la vie de la revue, mais je dirais que les 10 premières années on a surtout été occupés à avoir l’air crédible aux yeux des autres.

On sent dans ces propos une tension intéressante faite à la fois de méfiance et d'espoir, méfiance pour un modèle qualifié de «nouvelle idéologie», mais espoir tout de même, avec la prise de conscience qu'il y aurait là pour les revues l'opportunité d'«animer la recherche» et de faire ainsi évoluer une mission souvent réduite à la «fonction symbolique [d']alouer du capital académique»[^citLariviere] [@lariviere_oligopoly_2015], elle-même coincée dans ce que Bourdieu décrivait comme «une stratégie de conservation» [-@bourdieu_champ_1976, p.94] de ce même capital.

[^citLariviere]: Ma traduction. La suite de la citation originale est intéressante\ : "Unfortunately, researchers are still dependent on one essentially symbolic function of publishers, which is to allocate academic capital, thereby explaining why the scientific community is so dependent on ‘The Most Profitable Obsolete Technology in History’ [@schmitt_academic_2014]".

C'est en considérant la revue comme l'intersection toujours dynamique d'un faisceau de techniques et de pratiques que l'on pourra la penser dans la continuité, et non dans la rupture, et dépasser ainsi les résistances naturelles à toute disruption [@stiegler_dans_2016].

L'approche intermédiale en ce sens est pertinente à plusieurs titres. Concevoir la revue numérique comme une remédiation de la revue papier est une première piste à explorer pour inscrire l'édition savante numérique dans la continuité de l'histoire humaine [@jacob; @chartier] -- et non-humaine [@vitali, mvr à paraitre] \ ! -- des techniques et des pratiques d'écriture, d'édition, de publication et de diffusion. Mais il faudra concevoir cette remédiation au-delà de la théorie de Bolter et Grusin et du modèle _remédiant_, dont @larrue_du_2015 pointe les limites. Ce modèle occultait ainsi les échecs pour ne considérer que les remédiations réussies, c'est-à-dire supposées améliorer de média en média le principe d'«immédiateté». Il ne s'attachait qu'aux médias les plus visibles (le cinéma, la télévision, la radio, la vidéo numérique) et à leur généalogie «darwinienne» jugée trop «linéaire».

> «\ Cette vision optimiste et linéaire de l'évolution des médias est cependant contredite par la réalité. Si nous allons vers une fidélité -- ou une transparence sans cesse plus accrue, comme le répète d'ailleurs l'idustrie, comment expliquer le succès de médiations en basse fidélité tels les webdocumentaires réalisé[e]s à partir de téléphones portables ou les enregistrements  en format MP3\ ?\ »
> [...]
> «La réalité intermédiale [est] faite d'entrelacs, d'enchevêtrements, de retours en arrière, d'accidents -- dont la généalogie des médias, forcément linéaire, qui inspire les premiers intermédialistes, pouvait difficilement rendre compte.» [-@larrue_du_2015, p.37-38]

L'approche intermédiale présente l'intérêt de considérer la remédiation qu'est l'entreprise de revue numérique comme une opportunité pour y intégrer des pratiques éditoriales plus contemporaines, telles que celles que décrit Louise Merzeau. [Cette idée sera plutôt à développer lorsque je parlerai de la conversation, et de sa remédiation dans un modèle éditorial conversationnel pour la revue savante]{.note}

Sans prétendre que la revue peut devenir un espace théatrale, hypermédia par excellence dans les propos de Larrue, en tant que média «fédérateur» [développer]{.note}, on verra comment la production de connaissance peut être pensée comme un espace performatif. La problématique majeure de ma thèse sera d'ailleurs de vérifier si cette performance conversationnelle est _soluble_ dans un format éditorial susceptible à la fois de mémoire et de reenactment. [à rapprocher de la problématisation que fait chartier sur la tension entre ce qu'il appelle communication électronique et édition électronique, qui s'opposent notamment par les valeurs qui les sous-tendent, la première se rapprochant de l'immédiateté, gratuité, circulation libre, réalisant le projet des Lumières, et le second se rapprochant du maintient du modèle de la propriété intellectuelle, et de l'ordre des discours de l'imprimé (voir chartier sur l'édition numérique)]{.note}

 C'est en prenant en compte cette histoire longue de l'écriture et de l'édition que l'on peut comprendre la continuité des pratiques éditoriales perpétuant les ancrages épistémologiques [@chartier_crise_2014] dans cette période mouvementée qu'est la transition à une _épistémé_ numérique [@gras_les_2016]. L'attachement à l'évaluation par les pairs en est un exemple parmis d'autres. [voir les exemples de chartier, régime de la "preuve" qui se transforme]{.note} [+ citation d'entretien possible]{.note} Mais une telle continuité vient aussi gripper les possibilités et les opportunités qu’offre l’environnement numérique, pour fluidifier et enrichir la conversation scientifique, ou encore pour imaginer de nouveaux modèles épistémologiques.

<!-- 10juillet2019 - retravailler -->
Un exemple de ce déphasage réside dans le dossier de revue, qui continue d'être la forme éditoriale majoritaire dans les revues scientifiques. Qu'elle découle d'un colloque dont les organisateurs souhaitent rendre compte des communications, d'une démarche exploratoire de la part d'un chercheur pour avancer sur une question, à dresser un état de l'art d'un champs, ou encore de la part des rédacteurs de la revue, de pousser en avant une problématique, les dossiers jalonnent la vie éditoriale des revues. Pourtant, cette pratique de production ne correspond plus aux pratiques de lecture, comme le constatent l'ensemble des acteurs de l'édition scientifique. En effet, la publication numérique des revues a contribué à l'éclatement de l'unité éditoriale qu'offrait la publication papier en rassemblant dans un même objet une série de textes. Assurer une cohérence.
Or les statistiques de consulation montrent que désormais l'accès aux revues se fait essentiellement par le biais des articles,
[à développer: réduction de la granularité éditoriale, du numéro à l'article]{.note}.

Autre déphasage éditorial, illustrant l'absence de prise en compte de l'environnement numérique et de son "fonctionnement", la pauvreté des données censées alimenter les moteurs de recherche. De la production de contenus qualitatifs pour le lecteur humain à la production de données structurées qualitatives pour les robots et les algorithmes, il n'y a qu'un pas que les éditeurs se doivent de faire s'ils veulent conserver la légitimité dont il se targue.

Car, c'est bien dans ce "fonctionnement" que réside la production de l'autorité. Ce qui se joue dans le déphasage éditorial n'est pas donc simplement une méconnaissance de l'adresse à des pratiques de lecture nouvelles, mais une perte d'autorité et donc de légitimation des revues scientifiques face à des contenus dont la production aura su s'intégrer dans l'environnement.

[reprendre la problématisation de chartier sur la tension entre l'approche topologique des savoirs que produit l'édition imprimée, et l'approche encyclopédique des savoirs que produit l'environnement numérique (rapprochement thématique)]{.note}

[développer la question de l'autorité et de la légitimation qlq part (?)]{.note}

# L'institution
<!-- reprise de phd/Chps1/Chps1_alt_1problematique -->

Dans un tel contexte, la question qui est posée est celle d'un renouvellement des modalités d'élaboration du savoir au sein des institutions de savoir, de manière à envisager de nouvelles formes de communication scientifique, mais aussi la prise en compte dans ces formes de savoirs pluriels, non académiques. Nous allons dans un premier temps développer notre vision de l'_ethos_ des institutions de savoir dans leur rapport à l'écriture.

Puis[fera l'objet d'une autre partie]{.note}, à partir de nos études de terrains, nous montrerons en quoi la communauté scientifique gagnerait à s'inspirer des pratiques d'écriture et d'édition que l'on a pu observer dans d'autres communautés d'écriture comme celle des communs ou celle qui s'est constituée autour de la figure du Général Instin. Nous verrons que se dessine au sein de ces communautés un paradigme alternatif pour la production de connaissance, pour lequel ce qui se joue à travers l'écriture est d'abord la production d'un collectif, et non la production de documents. Notre enjeu sera alors de réconcilier dans un modèle conversationnel de communication scientifique une production à la fois sociale (_faire collectif_) et éditoriale (des ressources).

## [la mission de l'université sur la chaine de l'écrit]

> Prétendre que ce nouvel ensemble de compétences n’est que « technique » revient à refuser que la mise en réseau de milliers de travaux de recherche touche au métier, au savoir-faire, à la compétence des chercheurs. Ne pas en avoir conscience consiste à refuser de s’interroger sur la culture implicite du chercheur patenté, faite de « petits riens », de « trucs de cuisine », d’intuition, mais aussi de culture érudite et de réseaux sociaux. Tout comme la science du milieu du XXe siècle, et la manière de la faire, n’ont plus de rapport avec le joli catalogue qui décore la grande salle de la Sorbonne, la pratique scientifique d’aujourd’hui, avec les laboratoires, les méthodes, les axes de recherche contemporains, s’écartent de ce qui avait valeur de norme il y a 30 ans. À partir d’une transformation de l’outillage intellectuel du chercheur, on arrive logiquement à une transformation de ses pratiques intellectuelles 2. Et bien sûr, plus, tard, à une évolution de ses thèmes de recherche. [@guichard_linternet:_2002, III.5.1]


> Notre trentaine de pionniers^[chercheurs au département de littérature à l'ENS] découvre l’internet assez vite, entre 1991 et 1997. Très vite, pour eux, cet ensemble de protocoles est plus qu’un système de communication ou qu’un objet de consommation. C’est un instrument de travail, qui nécessite un savoir-faire, et à ce titre, ils ressentent le besoin d’acquérir une solide culture informatique, ou, à défaut, de s’entourer d’informaticiens.
> [...]
> C’est là qu’ils prennent le plus de risques: l’apprentissage de l’informatique leur coûte du temps, tout comme le travail de recherche sur les réseaux (web, listes de discussion, etc.) et celui de publication. Ce qui réduit leur participation à l’économie universitaire traditionnelle, dont ils ne respectent plus les règles, en négligeant d’accroître leur capital d’articles imprimés et en proposant d’autres modèles. Leur engagement leur coûte aussi de l’argent: achats d’ordinateurs, de modems, frais téléphoniques, etc. sont souvent engagés sur leur budget personnel. En effet, le plus souvent, leurs institutions ne les suivent pas, quand elles n’exercent pas de farouches résistances qui peuvent menacer la carrière des innovateurs.
> [...]
>  Alors que ces derniers développaient une réflexion sur le fonctionnement de l’enseignement et de la recherche, renouvellaient les thèmes et les méthodes de leurs disciplines suite à leur compréhension des potentialités de l’informatique et de l’internet, se lançaient dans des activités entrepreneuriales —tant pour institutionnaliser leurs pratiques que pour les financer, dans un cadre juridique et économique ad hoc—, et découvraient l’importance des défis pédagogiques à relever —élargissement de la base des « apprenants » et irruption des entreprises dans le marché éducatif—, leurs collègues en étaient à se familiariser avec le courrier électronique 8.
>
> En fait, les innovateurs restent condamnés à la solitude, principalement parce qu’ils se distinguent de leurs pairs et supérieurs sur un point: ils ont choisi d’adopter une attitude réflexive par rapport à leur outillage mental.
[@guichard_linternet:_2002, III.5.3]


> Pour autant, l’usage optimal de l’écriture ne va pas de soi. La maîtrise —et le développement— de l’outillage mental nécessite un long apprentissage, dont on sait qu’il peut prendre toute une vie. Cet apprentissage ne se fait pas en solitaire: il se construit dans un environnement collectif, propre à l’individu, que nous appelons laboratoire; mais il crée aussi des effets de sédimentation plus larges. En effet la transcription des savoirs, leur matérialité, comme leurs effets politiques —grâce aux avantages obtenus suite à une meilleure compréhension du monde— puis économiques, se réalisent dans une temporalité longue, qui est celle de la constitution des structures sociales. Ces effets peuvent aussi avoir des pendants pervers: valorisation d’une écriture archaïsante qui perd tout lien avec la langue parlée, constitution de castes attachées à leurs privilèges au point qu’elles en oublient leur mission de transmission de la connaissance, mise en place de monopoles dans la chaîne de production de l’écrit, etc. [@guichard_linternet:_2002, Introduction]


Les institutions en général existent par et pour les inscriptions qu'elles sont censées garantir\ : registres, rapports, mémoires, archives, catalogues, index, inventaires, nomenclatures, formulaires, etc. L'écrit est au cœur du fonctionnement de l'institution, c'est à la fois _ce sur quoi elle repose_ (sa condition d'existence), et _ce qu'elle certifie_ (sa raison d'être). À cela s'ajoute, pour l'institution scientifique, _ce qu'elle étudie_.

Là où certaines institutions trouvent leur stabilité et pérennité dans une nécessaire inertie vis-à-vis de ses écrits et inscriptions, l'institution académique a cette particularité de devoir adopter une position réflexive sur ses écrits, d'une part, mais aussi sur la chaîne de production de l'écrit, dont les processus contribuent à la légitimation. On retrouve ce projet scientifique dans les approches de certaines disciplines dont l'épistémologie des sciences, l'archéologie des savoirs, ou encore l'histoire du livre. Mais pourquoi reléguer à certaines spécialités un aspect aussi primordial de la légitimité des savoirs\ ?

Car assumer cette réflexivité participe à la consolidation de la légitimité des écrits, et par conséquent de l'institution qui les produit. Au contraire, abandonner cette dernière préoccupation équivaut en quelque sorte à couper son cordon d'alimentation. En tant que garante des écrits et de l'écrit, elle ne peut légitimement exister que si elle accompagne les pratiques et les techniques d'écritures, les analyse, et en prescrit de nouvelles.

Plus que pour toute autre institution, la recherche sur la chaîne de production de l'écrit scientifique constitue ensemble sa mission, sa raison d'être et la condition de sa survie.

C'est ainsi, en tout cas, que nous considérerons l'institution, dans cette vision idéale et paradoxale d'une institution garante des écrits qu'elle assure en les stabilisant, les analysant et les interprétant, mais qui dans le même temps ne peut exister que dans la dynamique d'une remise en question de l'écrit, de sa chaîne de production et des techniques intellectuelles qui lui sont associées. C’est la condition pour ne pas confondre la conservation _des_ écrits avec la conservation _de l_’écrit.

> Certes, notre outillage intellectuel se nourrit des interrogations que l’on porte à son sujet\ : l’analyse, explicite ou non, de la façon dont l’écriture agit sur elle-même accroît de façon surprenante nos capacités à comparer, imaginer, synthétiser. Mais, étudier cette réflexivité, profiter de sa dynamique, c’est évidemment aussi s’y engager soi-même: adopter une posture de recul face à l’instrument essentiel à la construction de notre pensée transforme notre représentation du monde, l’organisation de nos raisonnements.
[@guichard_linternet:_2002, Introduction]


La réflexivité intrinsèque de cette démarche est nécessairement une mise en danger de l'institution, dans la mesure où elle se doit de constamment réévaluer ce qui la structure. Cela nécessite la cohabitation complexe de deux approches antagonistes, une approche _conservatrice_ de protection[^guich], et une vision basée sur le soin («\ prendre soin\ »), le doute, la remise en cause. Cette vision tend nécessairement à l'évolution et l'innovation de l'écrit, de son milieu et de ses techniques.

[^guich]: Conservation qui tend parfois à l'_enclosure_ des écrits et de leurs techniques intellectuelles, voir à ce sujet [la conclusion cinglante](https://hyp.is/HtPIXMmnEemYnic4iIcZag/www-sop.inria.fr/axis/personnel/Eric.Guichard/theseEG/theseEGch9.html) de Guichard dans la seconde partie de sa thèse -@guichard_linternet:_2002

Cela constitue donc pour l'institution un projet continu, autrement dit un champ de recherche, qui ne peut se réaliser que dans l'expérimentation et la conception de nouveaux processus de production. Confronté à l'introduction de l'informatique au prestigieux département de Lettres de l'ENS^[l’École Normale Supérieure], Eric Guichard considère l'expérimentation -- dans son cas d'étude, la conception et l'édition d'un site web scientifique -- comme le moyen de «\ rapprocher le document [l'écrit] des outils et méthodes qui en ont permis la conception\ ». Pénétrant péniblement l'ENS en 1998, Internet, en tant que milieu technique numérique, venait en effet «\ réhabilite[r] les aspects obscurs, non-dits, de la production scientifique que sont les outils et méthodes\ ». [@guichard_linternet:_2002]


Finalement, c'est dans cet effort de réflexion et de conception d'une «\ chaine de production de l'écrit\ » que pourra s'esquisser l'élaboration d'un modèle épistémologique articulant l'édition, la publication, la recommandation (légitimation, certification), l'évaluation, l'écriture et la consultation.


## [constat de démission de l'université sur la chaine d'écriture]

<!-- début de chapitre article Stylo CRIHN -->
Les travaux scientifiques sur le document numérique [@pedauque_document_2011; @broudoux_auctorialite_2007], sur la communication scientifique [@beaudry_communication_2010; @beaudry_communication_2011; @oswald_formes_2015] et ses évolutions [@bourassa_devenirs_2018], ou encore sur les nouvelles chaines de publications [@fauchie_repenser_2018; @kembellec_reflexions_2017; @mourat_design_2018] -- TEI, markdown, R-notebook, python-notebook -- ne manquent pas, et se sont encore intensifiés avec le champs des humanités numériques [@ref].

Pourtant, malgré la diversité des initiatives et la multiplicité des outils à disposition, force est de constater que l'éditeur de texte _Microsoft Word_ demeure l'outil majoritaire dans les pratiques d'écriture des chercheurs et des étudiants, en particulier dans les sciences humaines [@kembellec_mediation_2013]. De surcroît, de l'administration à l'enseignement, en passant par la prise de note des étudiants ou l'évaluation de la recherche, tous les aspects de la vie universitaire sont concernés par cette situation de quasi-monopole.

On ne peut que s'étonner d'un tel paradoxe où une institution publique,  experte dans l'écrit, a pour principal support d'écriture un outil grand public, dont la conception, la réalisation et sa maintenance est l'œuvre d'une entreprise commerciale, qui plus est un des cinq GAFAM.

<!-- chp1 -->
Ce paradoxe nous révèle en fait qu'en abandonnant <!-- nécessiterait d'être justifié encore: en quoi les institutions ont abandonné ? 1. parce que le numérique n'a servi qu'à améliorer l'accès, 2. parce que les nvx dispositifs de lecture/écriture sont avant tout gafam. --> (aux GAFAM) la réflexion sur les supports et les techniques de l'écriture et de la lecture, au moment-même où les pratiques des chercheurs se transforment et s'adaptent au nouvel écosystème, l'institution s'est écartée de sa mission première, à savoir de prendre autant soin de l'écrit que de la chaîne de production de l'écrit.

Pour autant, l'institution académique n'est pas complètement démissionnaire, et plusieurs initiatives sont engagées dans l'évolution de l'édition scientifique, ou plus largement dans la construction de l'écosystème numérique qui héberge les savoirs et leur circulation.
Des chercheurs ou des laboratoires sont souvent parties prenantes, aux côtés de l'industrie, des instances décisionnaires sur l'innovation et l'évolution des protocoles et des formats qui régissent Internet et le web^[En premier lieu le W3C qui définit les standards du web, véritable entreprise de normalisation de l'écrit, de ses formats et de ses langages]. [Pousser plus en avant la réflexion nous amènera d'ailleurs à mesurer combien la définition de ces protocoles et formats reste décisive.]{.note}

Cependant, malgré ces initiatives, qu'elles soient expérimentales ou en voie d'institutionnalisation, les retards accumulés en matière de production et d'édition laissent penser que, dans cette phase de transition où les modèles papier et numérique cohabitent et s'hybrident, l'inertie institutionnelle empêche les différents acteurs de prendre la mesure du changement de paradigme déjà à l'œuvre dans l'édition. Par méconnaissance, méfiance ou résitance active, les processus de légitimation demeurent les mêmes, ne permettant de faire évoluer les processus et les formats traditionnels de communication scientifique, quand bien même ceux-là ralentissent la production et la circulation des idées. Plus problématique encore qu'un simple ralentissement, en délaissant des pratiques d'écriture relevant de la communication scientifique, l'Université accentue une crise d'autorité déjà bien réelle. La théorie de l'éditorialisation de Marcello @vitali_rosati_editorialization:_2018 nous est précieuse pour comprendre la nature de l'autorité dans un monde façonné par le numérique. Les structures spatiales se sont transormées, et avec elles les structures de l'autorité, rendant non pas caduques les principes sur lesquels reposent l'activité et la légitimité scientifique, mais nécessitant malgré tout une évolution radicale de la production des connaissances et de ses mécanismes de légitimation. Dans sa capacité à adopter ces nouveaux mécanismes, sur joue la pérennité des institutions de savoir.

Depuis la thèse d'Eric Guichard rendant compte en 2001 des résistances actives de l'institution -- l'ENS -- à un renouvellement de l'«ordre des discours» [@chartier_crise_2014], de nombreuses réflexions et initiatives poussent de l'intérieur les universités dans le sens d'une évolution nécessaire. C'est d'ailleurs l'un des objectifs motivant explicitement la constitution du champ des humanités numériques que de repenser les modalités de l'enseignement et de la recherche [reference]{.ref}. Pourtant, force est de constater que l'écriture des chercheurs elle-même, et l'édition et la publication de leurs écrits sont restées quelques peu figées dans des formats et des pratiques qui ne sont plus en adéquation avec les nouvelles pratiques d'écriture et de communication que l'on peut observer par ailleurs.

<!-- refaire une transition double déphasage impose une nécessaire ouverture à d'autre formes -->
Finalement, le fait que les lieux et les formes de la controverse et du consensus échappent de plus en plus à cette institution nous questionne sur la nature et les lieux même du savoir. L'université peut-elle élargir son champ de validation et de légitimation\ ? Peut-on envisager qu'elle se nourrisse et contribue à des formes alternatives de savoir\ ?  Entre le savoir autorisé et stabilisé dans les formes traditionnelles de l'édition scientifique, et les échanges non-institutionnalisés de communication et de collaboration propre à l'environnement numérique, on peut à juste titre se demander, si d'une plus grande fluidité dans les échanges, ne résultent pas une hybridation et une diversité susceptibles de mieux adresser la complexité du monde.

L'histoire nous enseigne que l'institutionnalisation de ces pratiques passe par l'innovation éditoriale, adressant du même coup ce double déphasage, institutionnel et éditorial.

# Au 17^ème^ siècle, la naissance d'un format éditorial


Cette partie revient sur la naissance au 17^ème^ siècle du périodique savant en tant que format, et met en évidence la conjonction entre formalisation éditoriale, légitimation et institutionnalisation.
S'imposant comme un pilier essentiel de la communication scientifique en quelques dizaines d'année, son émergence constitue un tournant épistémologique majeur pour la science moderne [@peiffer_les_2008, p.281].
Ce moment particulier du 17^ème^ siècle laisse transparaître une certaine similitude avec notre moment contemporain. En effet l'explosion des pratiques d'écriture et des formes de communication actuelle se traduit, comme à l'époque, par une innovation éditoriale mettant en tension à la fois une diversification des formes et leur institutionnalisation. Dans son étude approfondie du _Journal des savants_, Vittu met en évidence l'impact épistémologique du périodique, en tant que forme éditoriale innovante.

  > Notre problématique envisage le _Journal des savants_ comme une forme éditoriale dont les conditions de production et la matérialité même suscitèrent des positions stratégiques nouvelles dans les champs du savoir et induisirent des recompositions de leur configuration. [@vittu_formation_2002, p.181]

Mais la revue reste-t-elle, 350 ans après la naissance du _Journal des Savants_, un lieu de prédilection pour l'innovation éditoriale\ ? À l'heure de l'_édition en réseau_, existe-il d'autres espaces et instances susceptibles d'accueillir et de formaliser les pratiques communicationnelles émergentes\ ?

## le _Journal des Savants_

En 1665 naît le premier périodique scientifique, le _Journal des Savants_, lancé par Denis de Sallo. L'entreprise naît dans le contexte intellectuel hérité de la République des lettres dont la communauté de savants et de lettrés s'attachait à cultiver le savoir mais aussi à le communiquer, le transmettre et le diffuser [@volpe_dissemination_2013]. Jusqu'à la seconde moitié du 17^ème^, la _République_ perdure en Europe grâce à une intense correspondance personnelle entre les membres des cercles savants. Dans l'esprit de partage et d'ouverture caractéristique de cette communauté, l'information scientifique transitait alors essentiellement par voie postale, au travers des frontières linguistiques et géographiques, transgressant ainsi les frontières sociales, politiques et religieuses.[Parallèle à faire avec la transgression des disciplines voir celle des communautés de savoir (hors académie)]{.note}

L'initiative de Denis de Sallo est d'abord l'entreprise privée d'un magistrat et homme d'affaires évoluant «\ à la confluence des cercles administratifs, mondains et savants\ »
<!-- qui fréquentait différents cercles de savoir\ : lettrés, savants et «\ curieux\ »  -->
[@vittu_formation_2002, p.181-182]. L'initiative personnelle du magistrat va progressivement s'institutionnaliser au fil des années, parallèlement à l'essor de la légitimité du Journal.

Le pendant anglophone du _Journal des Savants_ est lancé quelques mois plus tard au sein de la Royal Society de Londres par Henry Oldenburg (1619-1677), diplomate et homme de sciences d'origine allemande. Le _Philosophical Transactions_ institue dès sa création, dans le prologue du premier numéro, quatre missions, dont les modalités évolueront avec le temps, mais qui resteront jusqu'à aujourd'hui les principales fonctions éditoriales des revues scientifiques dans l'élaboration des connaissances\ : l'enregistrement, la certification, la diffusion et l'archivage.


<!-- peut-être souligner ça: la raison pour laquelle tu fais l'histoire est que tu veux comprendre quelle était la mission originaire des revues savantes  -->

> Les rédacteurs qui se succédèrent à la tête du Journal des savants de 1665 jusqu'à la retraite de Jean-Paul Bignon, mirent en place par touches successives une nouvelle forme editoriale, celle du journal savant qui se caractérise par une publication régulière d'éléments, les articles, destinés à constituer un recueil enrichi de diverses annexes qui en facilitaient l'utilisation. [@vittu_formation_2002, p.189]

À ses débuts, c'est-à-dire avant que sa forme éditoriale soit établie et ne s'institutionnalise, le périodique apparait dans un premier temps à la communauté de la _République_ comme le moyen d'élargir ses correspondances privées à de très nombreux lecteurs.

> L'organisation du Journal comme chambre d'écho des lettrés et
des savants, la multiplication des périodiques inspirés de son modèle, enfin
la création d'outils facilitant sa consultation, témoignent de la construction
d'un instrument nouveau dont l'utilisation bouleversa sensiblement les
usages de la République des Lettres. [@vittu_formation_2002-1, p.349]

Rapidement, il devient aussi le moyen de s'assurer et de protéger la primeur d'une découverte ou d'une invention, et ce, bien plus efficacement que les actes des académies, dont la temporalité ne peut rivaliser avec la fréquence de publication des _périodiques_.

<!-- ajout juillet2019 pour souligner la raison pour laquelle je fais l'histoire des revues -->
Il y a alors un double mouvement à l'œuvre dans les missions que ce sont données les revues scientifiques. En effet, que ce soit l'élargissement du lectorat ou l'enregistrement et la certification des contenus, la revue devient une instance de légitimation des connaissances que cherchent à s'échanger une communauté^[[relation transductive (?) dans l'institutionnalisation de la revue et la normalisation du format éditorial. On retrouve cette relation dans les légitimations successives, celle des textes qui est aussi celle des auteurs, et aussi celle de la revue, et infine celle des revues scientifiques en général.]{.note}].

> Au-delà de ces activités de cabinet, le développement des journaux
savants suscita de nouveaux usages au sein de la République des Lettres.
Nouveaux instruments de divulgation des découvertes les périodiques
devinrent bientôt l'un des moyens d'acquérir une position dans la
communauté savante, ce que les académies constituées envisagèrent avec une
certaine réserve. Productions éphémères, volantes, au contenu mélangé, ils
se transformèrent en instruments privilégiés du travail savant grâce à des
prolongements, qui eux-mêmes induisirent des réorganisations des bibliothèques et de recompositions du savoir. [@vittu_formation_2002-1, p.367]

Le rôle de la revue n'est pas celui d'un simple intermédiaire. Elle est le vecteur matériel d'un contenu certes, mais plus important, elle en est le vecteur symbolique, car elle y adjoint la légitimité nécessaire à sa réception. Comme on le verra, cette légitimité s'est construite de concert avec le format de l'article de revue, et plus largement avec le format de la revue. En formalisant un modèle éditorial de communication scientifique, les revues se sont dotées d'une matérialité concrétisant le processus d'institutionnalisation, tant des contenus qu'elle est chargée de véhiculer, que de la revue comme modèle éditorial. On le voit notamment avec l'adoption du terme _article_ désignant une forme éditoriale de plus en plus normalisée comme en témoigne l'évolution du titre d'article. Ou encore, l'institutionnalisation est rendue visible dans la reconfiguration des disciplines, travaillées par la spécialisation des périodiques, au fur-et-à-mesure que ceux-ci se dotent d'éléments de repérage pour permettre au lecteur de s'orienter.

> [E]n soulignant la démarche d’un groupe de
savants, en créant des liens entre divers problèmes résolus par
les mêmes méthodes et en nommant ses membres, les journaux
font aussi émerger de nouvelles disciplines — l’analyse dans les
Acta eruditorum de la fin du 17 e siècle — dont la reconfiguration,
telle qu’on peut la suivre dans les index annuels, change sans
cesse. [@peiffer_les_2008, p.299]


::: note
Voir aussi les 3 "couches" du documents, donc la 3eme : «Enfin, la troisième couche, c’est la fonction communicationnelle du document : la trace sociale instituée par lui.» [@broudoux_evelyne_2018]. Repris de @pedauque_document_2003, «le document comme médium».
:::

## De la correspondance à l'article

L'apparition du terme _article_ pour identifier les parties de texte de périodique date du milieu des années 1680, lorsque Pierre Bayle intitula «\ article\ » chacun des segments numérotés de ses _Nouvelles de la République des Lettres_. Les libraires d'Amsterdam l'imitèrent rapidement pour leur contrefaçon du _Journal des savants_. Les rédacteurs du _Journal_ vont progressivement adopter une formalisation de l'article, mais sans en adopter le terme. Il faut cependant attendre 1711 pour que le terme rentre dans le langage courant et désigne une partie de périodique scientifique [@vittu_quest-ce_2001, p.148].

Dans un premier temps, les contributions aux périodiques sont définies par leur structure et leur composition, que ce soit un _extrait_[^extrait] ou un _mémoire_, ainsi que par une «\ autorité\ » venant cautionner la contribution de l'auteur. Les éléments éditoriaux qui accompagnent les contributions sont minimaux. Le périodique est alors une simple suite continue de textes, segments simplement séparés par un titre lui-même non normalisé.

L'étude des titres de segments dans le _Journal des savants_ est éloquente sur la progression vers une formalisation et une professionnalisation de la référence, intégrant par étapes tout ce qui constitue aujourd'hui une notice bibliographique\ : auteur de l'_extrait_, mention du lieu et de la date d'édition, indication du nombre de pages de l'ouvrage, son format et le nom de son éditeur. Des trois lignes de titre souvent accrocheur en 1665 et reflétant les pratiques épistolaires entre lettrés, on passe en 1714 à huit lignes en moyenne. Une première explication est d'ordre commercial, le rédacteur essayant de s'attirer les faveurs des libraires pour obtenir les derniers ouvrages. Une autre explication est d'ordre éditorial, la formalisation participant à la construction sur le temps long d'une matière scientifique mieux référencée et mieux exploitable.

[^extrait]: l'_extrait_ désigne un résumé ou une recension d'un ouvrage.

L’émergence du format «\ article\ » s'accompagne ainsi d'une explosion et d'une normalisation des éléments éditoriaux que l'on va progressivement retrouver dans les périodiques de l'époque.


## Contrefaçons

Témoin du succès des tous premiers titres, la contrefaçon, en particulier hollandaise[^Elsevier], permit notamment au _Journal des savants_ d'élargir considérablement sa diffusion en Europe centrale et en Europe de l'Est, dès la première année de parution. L'histoire de cette contrefaçon est intéressante à plus d'un titre.

Premièrement sur le plan juridique, où l'on comprend que le privilège royal accordé au rédacteur et au libraire ne pouvait protéger ces derniers que sur un territoire limité, puisque des contrefaçons apparaissent également en Aquitaine. Hors de la juridiction du Roi, à Amsterdam par exemple, tout contrefacteur était considéré dans son bon droit lorsqu'il entreprenait la réimpression et la vente de nouveaux textes. À tel point que le premier contrefacteur s'emparant d'une œuvre ou d'un périodique s'en assurait l'exclusivité, absolument respectée par ses confrères.

> Une édition hollandaise copiant une édition parisienne avec privilège peut ainsi, selon l'extension que l'on donne au concepte de contrefaçon, être réputée soit contrefaite soit seulement réédition, puisque le droit de copie français n'est pas opposable aux autorités des Pays-Bas. Une convention signée en 1710 par cinquante-quatre libraires hollandais accordait même au premier contrefacteur d'un ouvrage étranger une espèce de droit de copie moral reconnu par ses confrères\ ! [@moureau_plume_2006, p.141-142]

Deuxièmement sur le plan économique, les limites matérielles et financières de l'imprimeur^[L'imprimeur Cusson ne possède alors qu'un petit nombre de presses.] empêchent une plus large circulation, alors limitée aux grands centres universitaires d'Europe de l'Ouest et du Sud et aux canaux diplomatiques. Elles font également obstacle à la diversification des formes éditoriales telle que la pratiquait l'imprimeur hollandais. Ainsi, l'impression contrefaite du _Journal_ dans une ville marchande comme Amsterdam entraîne sa circulation sur des réseaux marchands bien plus vastes, moins érudits et à moindre coût. Par ailleurs, sur le plan éditorial, le format adopté pour la contrefaçon consistait en des recueils annuels, dans une édition plus petite (_in-douze_ habituellement au lieu des coûteuses éditions parisiennes _in-quarto_), transformant l'instrument d'information éphémère qu'était le périodique dans le Royaume en un ouvrage de références à l'extérieur [@vittu_formation_2002].

Cependant, la conversion éditoriale du format périodique en recueil annuel n'est pas le simple fait d'une contrefaçon différée. Elle vient s'inscrire dans le projet initial des fondateurs des revues pour une véritable construction de connaissances. Les travaux de Vittu montrent ainsi comment le _Journal des savants_ avait dès le début adopté une pagination continue d'un numéro à l'autre, préfigurant la constitution de recueils de numéro. Vittu décrit également l'instauration progressive d'instruments éditoriaux  entièrement tournés vers la structuration des connaissances facilitant la recherche et la découverte\ : index, différentes tables de matière, sommaires, formalisation des références. Les méthodes d'indexation utilisées par l'éditeur parisien et les contrefacteurs hollandais divergent d'ailleurs, le premier élaborant des tables analytiques qui reflètent une vision davantage encyclopédique d'accès aux savoirs, les second adoptant une approche bibliographique [@vittu_quest-ce_2001, p.148]. Vittu va d'ailleurs montrer que la réception du _Journal_ et des périodiques en général s'accompagne d'un renouvellement des pratiques savantes, de la lecture à l'écriture, outillées par ces nouveaux instruments[@vittu_quest-ce_2001, p.142], et d'un élargissement à de nouveaux publics [@peiffer_les_2008, p.299].

<!-- et finalement l'institutionalisation -->

[^Elsevier]: Par Daniel Elzevier (1626-1680), de la célèbre famille de typographes et d'imprimeurs néerlandais.

## Indexations

À ce stade, deux citations de Vittu font ressortir des éléments de continuité entre la naissance au 17^ème^ d'un nouveau dispositif de communication scientifique, et l'émergence aujourd'hui de nouvelles formes d'écriture et d'édition\ :

> «\ D'un point de vue éditorial, l'article est un segment d'un imprimé. Il est produit rapidement, soumis à la loi de la nouveauté, accède au marché de long terme par l'adjonction de plusieurs appareils d'indexation. L'ouvrage clos se transforme alors en un magasin de matériaux ouvert au choix du lecteur.\ »

> «\ le mot article rend bien compte de cette articulation d'une rhétorique acceptée par la communauté savante et d'un appareil offrant la possibilité d'une lecture aléatoire du journal savant en plus de sa lecture séquentielle.\ »

On a dans cette formulation tous les éléments d'une maîtrise des flux informationnels par l'indexation et le traitement de l'information\ : fragmentation, métadonnées, diversification des parcours de lecture. L'analogie avec les pratiques éditoriales actuelles est frappante et l'on pourrait rapprocher l’émergence de ces nouveaux objets éditoriaux que sont l'article et la revue au 17^ème^ siècle comme une réponse à la saturation attentionnelle consécutive de l'imprimerie. Alors qu'émergent aujourd'hui de nouvelles pratiques et formats éditoriaux, dans le sens notamment d'une fragmentation des artefacts institués, on peut légitimement envisager une institutionnalisation de ces formats, de la même manière que le périodique s'est imposé, a légitimé et institutionnalisé le format épistolaire caractéristique de la République des lettres.

## Fabrique d'une autorité
<!-- déplacer les deux citations de Vittu et ses commentaires (sans doute terminer dessus avec le dernier paragraphe ajouté hier), et voir pour fusionner "autorité et institutionalisation" et créer une nouvelle partie sur format/ingénierie éditoriale, etc.-->

Trop facilement attaquable (et régulièrement attaqué), le rédacteur du _Journal_ se voit _épaulé_ en 1687 par un «\ bureau de rédacteurs\ », composé de lettrés et de savants, suite à la décision du Chancelier pour répondre aux critiques de partialité dont faisait l'objet le _Journal_. Ce _bureau_ tente alors d'attribuer au périodique, d'un côté, une position plus neutre et moins controversée que celle du rédacteur unique, et de l'autre, une responsabilité éditoriale plus engagée et moins diffuse que celle de la «\ compagnie des gens de lettres\ » qui venait cautionner les contributions par l'intermédiaire d'un membre reconnu de la République des lettres. Neutralité et responsabilité, les ingrédients de la fonction éditoriale scientifique se mettent en place et s'_inscrivent_ dans le dispositif de la revue. [ajouter référence lecture sept2019 de vittu]{.note}

Avec la formalisation de l'article comme objet éditorial, à travers à la fois la normalisation de la référence bibliographique, mais aussi l'engagement de la responsabilité éditoriale, on assiste à une évolution de l'autorité, depuis la légitimation du travail de rédacteur et du journal lui-même dans un premier temps, puis une fois cette légitimité reconnue, la légitimation des auteurs et des articles eux-mêmes.

On le voit, le processus de normalisation va de pair avec celui de l'institutionnalisation. Cette imbrication très fine des processus permet aux marqueurs éditoriaux de _faire dispositif_. L'édition devient alors le lieu de fabrique d'une autorité, incarnée par les normes éditoriales et figurée par la fonction éditoriale.

Grâce à son dispositif éditorial, le _Journal des savants_ acquiert un statut d'autorité, en tant qu'acteur central dans le paysage savant de l'époque. Cette autorité est celle d'un format et d'une inscription normalisée, ayant démontré l'efficacité et la légitimité du périodique comme nouvel objet éditorial. Elle peut alors s'appliquer par extension à toute revue adoptant les mêmes principes éditoriaux.

À la fin du 17^ème^ siècle, le nombre de revues scientifiques explose. En 1684, vingt ans seulement après les premiers numéros du _Journal des Savants_, Pierre Bayle écrit dans la préface de la première édition du périodique _les Nouvelles de la république des lettres_ [^Bayle]\ :

[^Bayle]: Pierre Bayle (1647-1704) est philosophe et écrivain. Il crée _les Nouvelles de la république des lettres_ en 1684.

> «\ On a trouvé si commode & si agréable le dessein de faire sçavoir au Public, par une espèce de Journal, ce qui se passe de curieux dans la République des Lettres, qu’aussitôt que Monsieur Sallo, Conseiller au Parlement de Paris, eut fait paroître les premiers essais de ce Projet au commencement de l’année 1665, plusieurs Nations en témoignèrent leur joye, soit en traduisant le Journal qu’il faisoit imprimer tous les huit jours, soit en publiant quelque chose de semblable. Cette émulation s’est augmentée de plus en plus depuis ce temps-là ; de sorte qu’elle s’est étendue non seulement d’une Nation à une autre, mais aussi d’une science à une autre science. Les Physiciens, & les Chymistes ont publié leurs Relations particulières ; la Jurisprudence, & la Médecine ont eu leur Journal ; la Musique aussi a eu le sien ; les Nouvelles Galantes diversifiées par celles de Religion, de Guerre, & de Politique ont eu leur Mercure. Enfin on a vu le premier dessein de Monsieur Sallo executé presque par tout en une infinité de manières.\ » (Pierre Bayle, Nouvelles de la République des Lettres. Préface. mars 1684) [@vittu_formation_2002]

Le succès des premières initiatives éditoriales, bien que poursuivant des ambitions parfois personnelles, atteste du vide qu'il y avait alors à combler au sein de la communauté des lettrés. Car en densifiant les échanges et en les structurant en un format éditorial, les périodiques n'ont pas seulement prescrit de nouvelles pratiques d'écriture, ils ont amenagé un nouvel espace dont la communauté s'est emparé pour s'organiser. Parce qu'ils répondaient parfaitement à l'économie et aux pratiques de communication de l'époque, les formats du périodique et de l'article ont fournit une structure spatiale viable et opérante au service d'un collectif.

## Appropriations [ajouté v0.2, encore à développer]{.note}

Comment l'article et le périodique transforme le travail des savants, et notamment l'écriture. L'étude de @peiffer_les_2008 sur les écrits s'appropriant les périodiques montre cette capacité nouvelle de se répondre les uns les autres, de s'appuyer sur les précédents, de réinterpréter un tel à la lecture d'un autre, etc. Les auteurs identifient là «le cœur du travail que la forme périodique permet» [@peiffer_les_2008, p.294].

> Ainsi, une méthode est élaborée et publiée dans un journal, le mémoire est repris et critiqué, cette critique elle-même étant soumise à une nouvelle critique à laquelle d’ailleurs il y aura réplique.[-@peiffer_les_2008, p.295]

 Le périodique semble produire une unité, une consistance, un contexte favorable à la discussion dans la longueur. Finalement, c'est la constitution d'une communauté.

> Tous ces exemples illustrent la diversité des formes d’appropriation par les savants de ces journaux, que ceux-ci soient considérés comme des moyens d’information, des sites de production des savoirs ou des instruments stratégiques. L’ampleur du phénomène incite à formuler la question des effets que cette forme de communication périodique a pu avoir sur les modes mêmes de construction des savoirs. La brièveté des pièces publiées dans les journaux savants engage à traiter un seul aspect d’une question, à communiquer une seule observation, le récit d’une seule expérience, la solution d’un problème. La périodicité de la publication permet de réagir rapidement, de faire insérer des réponses, corrections, modifications et extensions, qui peuvent être à l’origine de débats et de controverses. Les savoirs publiés dans les périodiques savants sont ainsi comme des précipités prêts à se
recomposer sous la plume d’auteurs variés.[@peiffer_les_2008, p.297-98]


:::: note
[conclusion à retravailler]{.note}

L'histoire nous montre ainsi que les innovations de support s'accompagnent d'innovations éditoriales.

 Au 17^ème^ siècle, la mécanisation et la commercialisation du dispositif épistolaire aboutirent à la naissance de la revue et de l'article, deux formes éditoriales intimement liées, ... dont l'institutionnalisation rapide permit l'évolution  le modèle épistémologique

La revue est un de ces lieu d'innovation, et c'est dans ce contexte qu'il devient nécessaire de développer une réflexion critique sur les revues scientifiques, parce qu'elles sont à la fois un dispositif institutionnel par excellence (au sens où les revues focalisent les formes de légitimation et de certification reconnues par les institutions académiques), mais aussi parce qu'elles restent un lieu d'innovation éditoriale.
::::


# Institution et communauté, dialectique entre formes et pratiques

::: note
Pour le moment dans cette partie, je ne fais qu'amener deux cas concrets montrant la relation entre les pratiques d'une communauté et l'institution, soit à travers l'institutionnalisation d'une pratique (qui se traduit par une formalisation de la pratique), soit à travers l'adoption d'une norme/forme soumise par l'institution à la communauté.
:::

Ce qui se joue au 17^e^ siècle avec la normalisation d'un format de communication, c'est l'inscription de règles et de pratiques -- d'écriture et d'édition, qui sont celles d'une communauté. Au fil de l'éclatement des sciences et leur cloisonnement dans des disciplines de plus en plus constituées, la communauté scientifique va également se scinder et chaque sous-communauté disciplinaire va progressivement adopter des normes et des pratiques spécifiques.

Existant au travers de leurs pratiques, les communautés s'instituent au travers des modèles éditoriaux qu'elles adoptent. Ce sont en effet les artefacts éditoriaux qui permettent d'amener certaines pratiques vers leur institutionnalisation, c'est-à-dire leur reconnaissance par les institutions légitimantes.

[citer le cas de la TEI, et comment un format est venu structurer l'epistémologie des communautés qui l'utilisent (voir l'étude du chercheur qu'a rencontré Marcello à Utrecht-DH2019)]{.note}

[citer le cas inverse, où l'institution vient prescrire des pratiques, avec l'interview de Stephane sur l'émergence d'OpenEdition dans au début des années 2000 et comment OE a promu la non structuration des données, au profit d'interfaces de lecture]{.note}

<!-- Paragraphe repris de Q1_v2 - ### Formats et usages -->
Il y a là une tension particulière entre format et usage. Les actions de lecture, d'écriture, d'association tendent avec le temps à se formaliser par l'usage (et devenir des _pratiques_), mais nécessitent pour subsister une liberté d'évolution. La formalisation par l'usage ne relève pas de la même contrainte que la formalisation par un format. Par exemple, la grammatisation des langues vernaculaires [@auroux_revolution_1994] n'empêche ni des pratiques particulières ni leurs évolutions naturelles (on parle bien de langues _vivantes_). La grammatisation reste un processus ouvert, accompagnant les pratiques au fil de la mutation du langage. La relation qu'entretiennent usage et format, ou langue et grammaire, relèvent finalement de celle qui conjuge l'homme et la technique [@leroi-gourhan_geste_1964; @simondon_gilbert_1994]. L'indissociabilité de ces couples ne peut être pensée en termes d'opposition, mais de coévolution dont la tension vertueuse fournit à l'un et à l'autre une dynamique vitale.

## [cas du hashtag twitter]
Le cas du _hashtag_ sur la plateforme Twitter nous permet de saisir cette relation entre forme et usage, et d'identifier comment une _primitive_ d'usage se formalise dans un modèle. Lorsque Twitter est lancé en mars 2006, les conventions de mention («\ @\ »), de _hashtag_ («\ #\ ») ou encore de _retweet_ («\ RT\ @\ ») n'existent pas, ni dans les pratiques ni dans le système [@seward_first-ever_2013].
Hormis le _retweet_ dont la pratique s'est développée sur Twitter, ces éléments de langage proviennent de pratiques plus anciennes sur les forums ou les salons de discussions.
Ces pratiques vont progressivement émigrer[^migration] sur Twitter pour devenir bientôt des «\ conventions\ » correspondant en fait à des actions unitaires ou des _primitives_ de la conversation, respectivement\ : s'adresser à quelqu'un (souvent en réponse à un précédent message), signaler le sujet ou le point focal du message, relayer le message d'un tel.

> «\ While  retweeting  can  simply  be  seen  as  the  act  of
copying and rebroadcasting, the practice contributes to
a  conversational  ecology  in  which  conv
ersations  are
composed of a public interplay of voices that give rise
to an emotional sense of shared conversational context.\ » [@boyd_tweet_2010]


[^migration]: j'aurais simplement pu utiliser _migrer_, mais le terme _émigrer_ me semble mieux rendre compte des _territoires_ que sont les plateformes avec leurs repères, leurs usages et leurs utilisateurs. Sur l'usage du terme _territoire_, voir la conférence «Les enjeux du libre accès pour le Québec» [[video](https://youtu.be/Vm50iaR0a8s?t=1898)] de Jean-Claude Guédon au colloque [_La publication savante en contexte numérique_](https://www.acfas.ca/evenements/congres/programme/85/300/317/c), 2017, ACFAS (U. Mc Gill)]

Twitter les a implémentés dans le modèle de la plateforme par étapes\ : mai 2007 pour la mention, juillet 2009 pour le _hashtag_, novembre 2009 pour le _retweet_. L'implémentation de ces actions unitaires s'est matérialisée par des boutons d'action spécifiques, paratexte «\ pour l'action\ » remplaçant la frappe au clavier d'un ou deux caractère·s ou la succession des opérations d'un _copier-coller_, par un simple clic. Cette dialectique entre les éditeurs de la plateforme et ses usagers témoigne bien d'une co-conception du dispositif et des modalités de sa conversation. Les concepteurs et éditeurs de Twitter ont en quelque sorte grammatisé (en partie) ce qu'est une conversation Twitter, telle que les utilisateurs la pratiquaient.
Dans ce cas précis, une pratique massive ne pouvant être ignorée est venue déterminer de nouvelles fonctionnalités. [Principe du dispositif bienveillant, voir article autorité/anarchy]{.note}

## [cas de la plateforme OpenEdition]
Cette dialectique n'est cependant pas toujours aussi équilibrée ou bilatérale. Les plateformes peuvent ainsi profondément structurer les pratiques.
Dans un entretien^[Voir annexe [Entretien avec Stéphane Pouyllau](../index.html)] mené en octobre 2018 pour ce mémoire, Stéphane Pouyllau raconte comment Open Edition, la plateforme de publication scientifique, a pris lors sa création une direction radicale, à contre-courant d'un certain paysage informatique à la fin des années 90. Dans les milieux autorisés de l'informatique scientifique française, la tendance est à la base de données documentaires et au développement de moteurs de recherche performants, basés sur une indexation fine des données. [développer avec l'entretien]{.note}
 Marin Dacos, le fondateur d'Open Edition, vient d'un autre groupe de chercheurs, utilisateurs du CMS SPIP [lequel, cf. entretien]{.note}. Il souhaite mettre en place une plateforme de diffusion des revues scientifiques en SHS. Mais au lieu de considérer les articles comme de la donnée, il va d'abord s'intéresser à la mise en forme et à l'affichage des articles scientifiques, en considérant seule une expérience de lecture proche de celle de l'édition papier amènera les éditeurs de revue à passer à l'édition et à la diffusion numérique. Le choix technique pour implémenter la vision de Marin Dacos est celui d'un CMS (Content Management System, ou système de gestion de contenus), SPIP, qui a le mérite de modéliser un modèle éditorial et de le réaliser à l'écran. Pour les spécialistes de la recherche d'information à l'époque, ce choix est une abberation tant il néglige l'indexation des données, essentielle à la survie d'un contenu dans l'océan en expansion du web.

> On n'a pas vu venir google, qui a complètement remis en cause les principes de la recherche d'information, basée à l'époque sur une donnée très structurée. OpenEdition au contraire a fait le choix du CMS, au détriment de la donnée structurée, pour favoriser la présentation de l'article dans un affichage proche de ce que les revues produisaient en papier. [à réécrire avec la transcription de l'entretien]{.note}

Une telle orientation, qui peut sembler avant tout technique, a eu en fait des répercutions épistémologiques très concrètes. Elle a en effet perpétué une séparation des métiers entre l'éditeur et le diffuseur, là où l'environnement numérique portait la promesse d'une nouvelle répartition des fonctions de l'un et l'autre, comme le reconnaitra Marin Dacos, lorsqu'il publie avec Pierre Mounier, «\ L'édition électronique\ » [@dacos_edition_2010].

Fidèle à sa vision d'une plateforme au service des éditeurs avant de penser les usages potentiels d'une édition numérique, la plateforme Revues.org a confirmé l'éditeur de revue dans sa pratique éditoriale classique, se concentrant sur le texte et sa présentation, sans investir dans la production des données, pourtant essentielle dans l'écosystème numérique. C'est ce que nous a montré la série d'entretiens menés auprès des direct·rice·eur·s et secrétaires de rédaction de revues, dans le cadre du projet Revue 2.0.

> [citation ]{.note}

Installé dans cette distribution des rôles et des fonctions, le diffuseur, au contraire, se spécialise dans la production et la structuration des données, nivellant au passage les données [à développer avec la réflexion sur l'interopérabilité]{.note}.

[rappeler qu'openedition était parti sur une solution sans données, version SEO de base, avant de comprendre qu'il fallait structurer les données, et est allé revoir ses amis "data" négligés à l'époque.]{.note}

::: note

développer sur la structuration du paysage de la communication scientifique, ce qui revient à une des problématiques actuelles de revue 2.0, à savoir une pratique éditoriale qui est restée proche du texte et de sa présentation, et qui n'a pas investi dans l'enrichissement des données, pourtant essentielle dans l'écosystème du web. La séparation des métiers considère ainsi que c'est au diffuseur de gérer l'existence, ou la subsistance, des articles sur le web, là où l'éditeur devrait conserver une certaine maîtrise des données. Question de légitimation.

:::

# Communauté et construction du savoir scientifique

:::: note

Finalement, la question qui est posée plus haut est celle de la communauté scientifique et de la crise des savoirs des universités. Le "moment" que l'on identifie aujourd'hui remet en avant la question récurrente de ce qu'est la communauté scientifique, comment elle se constitue, comment elle se réinvente dans les marges des formes institutionnalisées.

Dans cette partie, nous discutons des types de communauté qui ont jalonné l'histoire scientifique récente.

- Umbolt
- La République des lettres
- Les Salons de Mélançon
- les conférences (voir Judith Scribnai)

::::

# Hypothèse : un modèle éditorial conversationnel

:::: note

Pour adresser les constats et la problématique posés plus haut, je fais l'hypothèse que la communauté scientifique peut et doit adopter ses formes de communication scientifique vers des formes conversationnelles.

Dans cette partie, nous pouvons introduire l'idée de milieu dans lequel une intelligence collective peut se révéler.

::::

## La nature environnementale du numérique

::: note

Repris de mon champ 1. Il y a un travail de sélection des notions, puis de développement des notions sélecitonner.

:::

### Le numérique comme milieu

#### Éditorialisation

La revue scientifique «\ nativement numérique\ » constitue un objet  d'étude idéal pour adresser la question de l'éditorialisation telle que définie par @vitali_rosati_quest-ce_2016 qui propose avec l'éditorialisation une conceptualisation des processus de production, de légitimation et de circulation des connaissances dans l'environnement numérique. Ce qui pourrait apparaître comme une théorie de l'édition numérique, dépasse en fait largement l'édition comme pratique ou comme secteur d'activité. L'éditorialisation prend ainsi une portée culturelle en s'appliquant potentiellement à tous les processus d'écriture dans l'environnement numérique, et une portée philosophique en proposant une pensée de l'espace numérique, de sa structuration et consécutivement de l'autorité.

L'éditorialisation me permet de dépasser la notion de dispositif et sa conception foucaldienne. On retrouve cette même conception de l'éditorialisation chez Louise Merzeau dont l'approche héritée de la pensée médiologique considère le numérique comme _milieu_ [@merzeau_ceci_1998; @merzeau_editorialisation_2013]. La notion de milieu nous sera particulièrement utile ici pour aborder ce glissement de valeurs depuis des entités bien identifiées dans le monde pré-numérique vers des entités qui tendent justement à devenir environnementales.

#### Écologie médiatique

Cette approche, qui rejoint celle de l'écologie médiatique [@bardini_entre_2016], intégre la nature environnementale du numérique et nous permet en effet de revisiter ces entités, support, dispositif ou encore technique intellectuelle, et de les considérer comme partie prenante de l'environnement ou du milieu.

##### Causalité circulaire

Le milieu nous amène notamment à sortir du déterminisme technologique associé généralement au support technique d'inscription, et à une conception linéaire de la causalité [@bardini_entre_2016] entre par exemple support et pensée, ou entre inscription et technique intellectuelle. De la causalité _formelle_ [@mcluhan_formal_1976] à la causalité _circulaire_ [@merzeau_mediologie_2006] ou _récursive_ [@levy_place_1998], l'écologie médiatique inscrit les supports dans une boucle rétroactive entre technique et usage, permettant d'envisager autrement les interactions entre des entités en partie dissoutes dans le milieu. Or, il se trouve que le numérique, caractérisé notamment par sa récursivité, adhère particulièrement bien à cette conception de la causalité. Parce qu'il est un milieu écrit (codes, protocoles, dispositifs), le numérique intègre de manière récursive des valeurs qui se reproduisent dans les écritures qu'il supporte et génère.

##### Écriture-milieu

Dans ce contexte théorique, l'étude de cas du corpus littéraire contemporain _Général Instin_^[cf. Méthodologie] viendra illustrer comment l'écriture littéraire elle-même devient constitutive du milieu ^[@rongier_general_2017 parle d'«\ écriture-milieu\ »] . Dans cette idée de glissement et de dilution des fonctions et des valeurs, on pourrait qualifier cette écriture de _dispositive_ dans la mesure où elle véhicule tout autant un discours qu'une série de valeurs transmises dans les formes et supports. Dans le cas d'Instin, ce n'est pas strictement le numérique qui produit cette récursivité de l'écriture sur elle-même, mais il la permet. En quelque sorte, il la _prédispose_.

#### Fonction éditoriale (dilution)

Avec la dilution du dispositif dans le milieu, s'opère un autre glissement notable, celui de la fonction éditoriale. Traditionnellement assurée par des acteurs humains, maîtrisant des opérations d'identification, de sélection, d'agencement et de fixation du texte, la fonction éditoriale se trouve distribuée dans le milieu numérique et répartie entre différents acteurs. En effet, le support d'écriture et de lecture s'imbrique avec des dispositifs intégrant en eux-mêmes de nouvelles instances de décision\ : les algorithmes, la communauté de lecteurs. Dans le sens d'une co-construction de l'espace de savoir, ces algorithmes de décision viennent outiller les acteurs humains, éditeurs et communautés de lecteurs. Avec la fonction éditoriale, c'est tout le processus de légitimation qui se trouve lui aussi dilué et réparti entre différents acteurs, humains, dispositifs et supports.

### Le (texte) numérique comme milieu

Le glissement de la fonction éditoriale se joue également dans la nature du texte numérique, devenu à la fois support, dispositif et milieu.

Pour @kittler_logiciel_2015, les inscriptions numériques se distinguent des inscriptions sur support physique par le fait qu'elles ne sont plus perceptibles par la vue humaine. Du visible, elles sont passées à un ordre de grandeur nanométrique hors d'atteinte des capacités sensibles de l'être pensant. Nous pouvons interpréter cela dans le sens de la médiologie en disant que le texte s'est simplement dissout dans son milieu, se confondant avec les autres écritures qui produisent ce même milieu (à savoir les codes, les protocoles, les inscriptions sur silicone - circuits imprimés et et puces, qui sont en fait des instructions). C'est en ce sens aussi que l'on peut concevoir le texte numérique comme une écriture-milieu.

Écriture-milieu également dans la dénaturation ultime du signe alphabétique dans un système de signes binaire, lui-même traduit par la machine par des impulsions électriques. Cette déconstruction radicale du texte [@kittler_logiciel_2015] revient à une dissolution de l'unité d'inscription (la lettre) en unités plus petites, le bit. Ces bits de nature binaire _supporte_ à la fois le texte inscrit, le code qui le manipule, la puce qui _traite_ (_process_) le code. Le bit, et son équivalent physique (l'impulsion électrique) constituent finalement les briques élémentaires du milieu numérique.

Pour autant, si l'inscription sort du domaine du visible, l'intelligibilité du texte n'est pas perdue. Sa dissolution est à tout moment réversible grâce au calcul, tant que les conditions sont réunies pour opérer ce calcul. C'est justement le milieu qui assure la faisabilité de ce calcul, où l'on voit bien ici comment le milieu est coproduit à la fois par les couches matérielles (à commencer par la présence d'énergie électrique), logicielles et environnementales^[on peut penser aux protocoles du réseau lorsque le calcul fait appel à des ressources externes]. Cette faisabilité est également dépendante des multiples standards sous-jacents à la production de texte\ : de l'encodage de caractère à l'encodage du texte, mais aussi de la police d'affichage, des feuilles de styles, etc. C'est l'ensemble de ces éléments qui permettent au texte numérique, doublement inintelligible, car encodé et imperceptible, de se laisser voir et lire par l'être humain alphabétisé.

_L'ensemble de ces éléments_ constituent d'après @bachimont_nouvelles_2007 une étape d'«\ interprétation\ » qui se glisse entre l'inscription et son intelligibilité par le lecteur. Cette interprétation calculée^[à considérer comme une médiation technique.] de l'inscription numérique échappe au lecteur, et révèle une autre conséquence directe du caractère computationnel du milieu numérique\ : ces écritures ou inscriptions sont capables de «\ lire et d'écrire par elles-mêmes\ » [@kittler_logiciel_2015, p.30]. Plus exactement, le milieu est susceptible de produire de nouvelles écritures à partir d'écritures existantes. On peut se demander alors s'il est possible (et nécessaire) de distinguer différents niveaux d'écriture selon sa provenance et sa fonction dans le milieu, qu'elle soit machinique ou humaine, mais aussi qu'elle soit code, données, métadonnées, discours, etc.

Le milieu numérique est en effet régi par une succession d'écritures programmatives (logiciels, protocoles, mais aussi pourquoi pas cartes imprimées et puces électroniques) qui élaborent ensemble un espace d'action (et d'écriture). Ces écritures sont en quelque sorte structurelles ou architecturales et procure un cadre rigide et structurée.

#### Liquidité

Or, la dissolution du symbole évoque la nature liquide du texte numérique. Cette métaphore de la liquidité vient illustrer l'instabilité de l'inscription numérique, par essence altérable et modifiable à tout moment, que ce soit du fait d'une action humaine ou d'un process informatique. Tout un chacun (humain ou machine) ayant le contrôle sur le processus d'écriture peut _éditer_, au sens de modifier, un texte numérique.
Cette liquidité est une métaphore potentiellement féconde pour penser l'inscription numérique, mais elle doit être nuancée pour plusieurs raisons. D'une part comme on l'a vu avec l'idée d'écritures architecturales, toutes les écritures informatiques n'ont pas le même niveau de liquidité.

C'est ce qui fait que malgré le fait que le texte soit par nature modifiable, les dispositifs d'écriture (éditeurs ou traitements de texte) sont programmés pour contraindre les droits et accès _en écriture_ à certaines inscriptions. Car si le support de mémoire numérique est effectivement réinscriptible à volonté, il n'en demeure pas moins qu'il est strictement contrôlé et maîtrisé par des couches logiciels qui s'assurent que les données soient correctement manipulées, et en premier lieu qu'elles soient conservées intègres.

<!-- [mvr fait remarquer que le milieu est architecturale, structuré et rigide. qu'il n'est pas liquide comme le texte. Il distingue donc les écritures discursives des écritures de code et protocole :) Ce qui me fait plaisir puisque c'est ce que je me tuais à lui dire : si tout est écriture (protocole, software, cartes imprimées, puces, etc.) et éditorialisation, tout n'est pas du même niveau d'écriture et d'éditorialisation. Il est nécessaire de distinguer les diff. formes d'écriture et leurs rôles respectifs dans le milieu, et notamment dans l'écriture du milieu. Ceci étant dit, oui, si les niveaux de fluidité sont moindre sur les écritures architecturales et supérieures pour les écritures discursives, toute deux participent de la construction de l'espace _et_ du milieu : écriture milieu, écriture _dans_ l'espace et _de_ l'espace.][*] [il y a des architectures qui sont très rigides : oui modifiable mais quand meme des infrastructures et archi principales sont définies.][*] -->

#### Invariant textuel

On peut avancer que l'invariant textuel [@de_biasi_papier_1997] que lui assurait le support papier demeure dans l'environnement numérique d'une certaine façon. Il n'est certes plus assuré par le support lui-même, mais par le dispositif, c'est-à-dire ici par tous les mécanismes garantissant le traçage des accès au texte notamment _en écriture_. Ce transfert fonctionnel est sans doute l'une des clés de l'épistémé numérique tant il porte à conséquence sur la fonction d'autorité du texte et par extension de son.ses auteur.s. et des institutions qui en sont responsables. Car dans la graphosphère, c'est sur la base de cette stabilité du support (le papier), que pouvait exister la stabilité du dispositif (le livre), sur laquelle reposait la stabilité de l'institution (la bibliothèque), et finalement tout le régime de sens depuis l'imprimerie d'après @de_biasi_papier_1997. Dans l'environnement numérique, les repères de stabilité (ou encore les signes d'autorité) se sont radicalement déplacés, produisant de fait une impression d'instabilité du texte, érodant la notion même de _référence_ et avec lui tout le système bibliographique qui s’est mis en place pour l’institutionnaliser. Or, cette référence et son institutionnalisation sont les conditions du partage d’un socle commun de connaissances au sein d’une communauté de savoir. La possibilité de s’y référer procure au texte stabilisé une autorité et une authenticité nécessaires à une réflexion commune.

#### Calculabilité et système de référence

Une autre approche pour questionner cette apparente liquidité est de la considérer comme une fluidification et accélération des processus d'écriture et de réécriture. Cette accélération est permise par le calcul, et c'est par le calcul que se résoud également la complexité de manipulation du texte et de ses états successifs. Il est en effet possible de mettre en place des dispositifs et des protocoles associés capable de gérer cette liquidité apparente du texte numérique, pour recréer des conditions de stabilité, ou tout du moins pour abaisser la complexité native à une complexité appréhendable par la cognition humaine, qu'elle soit individuelle ou collective.

Depuis les instructions informatiques élémentaires de gestion de fichiers, avec son nommage, son extension, son encodage, sa date de création ou de modification, etc., les dispositifs d'édition et de publication n'ont cessé d'améliorer leur gestion du texte numérique et de reproduire un tant soit peu une certaine stabilité, jusqu'à assurer aujourd'hui une panoplie de fonctions qui n'étaient pas envisageables avec le support papier, telles que le _versionning_, le multi-auteur (asynchrone), le collaboratif (synchrone), l'annotation, etc.

Le cas du wiki, donnant accès à toutes les versions antérieures du texte et aux modifications successives par auteur, en est l'exemple le plus emblématique. Github proposent un protocole de contribution différent à partir duquel émergent des dispositifs d’écriture collaborative^[voir notamment l'outil Penflip https://www.penflip.com/] (Burton). On peut également citer le principe de la blockchain^[sur Wikipedia https://fr.wikipedia.org/wiki/Cha%C3%AEne_de_blocs], conçue comme un registre distribué assurant la comptabilité des écritures et de leurs auteurs (machines et humains).

Ces exemples montrent bien qu'il serait possible, en théorie, de reconstruire un système bibliographique, c'est-à-dire un système fiable de référence, dans le sens d'un modèle épistémologoqie embrassant pleinement cette liquidité du texte. Or, on voit bien que les différentes fonctions traditionnellement assurées par l'institution, le dispositif ou le support (respectivement la bibliothèque, le livre ou le papier), ne sont plus distinctes et séparées, mais sont parfois transférées à d'autres entités, ou diluées entre elles, autrement dit, assumées par un milieu tout à la fois support, dispositif et institution.


## Vers une herméneutique collective

### Régime documentaire en environnement numérique

En 2013, Louise Merzeau publie un article analysant «\ l'éditorialisation collaborative d'un évenement\ » et le dispositif mis en place en marge d'une conférence scientifique pour la documenter [@merzeau_editorialisation_2013].

Dans cet article qui apparaît comme un jalon important de la «\ feuille de route\ » [@merzeau_nouvelle_2007] qu'elle s'était précédemment donnée, Louise Merzeau identifie des pistes de réflexion valides pour penser un nouveau régime documentaire propre à l'environnement numérique et à la fragmentation des écrits. La problématique que l'auteure poursuit alors est celle d'une connaissance dynamique caractérisée par un double processus de production documentaire (constitution de corpus) et d'une production mémorielle (remobilisation de ces corpus).

Pour Louise Merzeau, ce dispositif d'éditorialisation serait susceptible d'apporter une réponse aux inquiétudes soulevées dans plusieurs études du régime attentionnel en contexte Web et numérique. Que ce soit @carr_is_2008 qui considère que le «\ médium\ » Internet amenuise les capacités de concentration et de réflexion, ou @cardon_democratie_2010 qui s'inquiète de la prédominance de la logique affinitaire dans la recommandation des contenus, ou encore @rouvroy_gouvernementalite_2013 pour qui la gouvernementalité algorithmique réduit la subjectivité des individus (et leur devenir) à des profils prédictifs, les travaux ne manquent pas pour analyser un régime de communication condamnant «\ toute possibilité d’aménager des espaces communs de la mémoire et de la connaissance[^reff]\ ».

L'hypothèse avancée par Merzeau, et qu'elle ne cessera d'affiner progressivement [@merzeau_entre_2014; @merzeau_profil_2016], est celle d'un dispositif d'éditorialisation collaborative qui, en générant un processus vertueux de circulation et de réécriture, permet du même coup un processus mémoriel reposant «&nbsp;sur une production documentaire affranchie des logiques affinitaires au sein d'un même espace contributif.[^ref]&nbsp;»

[^reff]: voir [@merzeau_editorialisation_2013, p. 116]

[^ref]: _Ibid._

### L'hypothèse d'une herméneutique collective

Nous portons l'hypothèse plus loin\ : selon nous, le processus qu'elle décrit et analyse comme celui d'une redocumentarisation collective, pose en fait les fondements d'une dynamique interprétative, autrement dit d'une herméneutique collective.

L'analyse de Louise Merzeau fait en effet ressortir quatre propriétés constitutives de ce dispositif d'éditorialisation\ : 1)\ sa bienveillance, 2)\ sa réflexivité, 3)\ son appropriabilité et 4)\ sa distance. Ces quatre éléments assurent respectivement les fonctions 1)\ d'établir entre les individus et le dispositif la confiance nécessaire à tout engagement, condition pour ouvrir un espace où penser collectivement, 2)\ d'établir par la visualisation les conditions de l'élaboration d'une finalité commune, 3)\ de favoriser la circulation et la redocumentarisation des contenus catalysant des associations nouvelles, 4)\ d'aménager à l'interstice de ces associations «\ une glose critique et documentaire\ », lieu-même de l'interprétation.

De l'appropriation à l'interprétation, il n'y a qu'un pas. Et si l'appropriation se matérialise par des écritures et des réécritures, alors le dispositif conversationnel peut prétendre relever d'une activité herméneutique.

La question devient alors\ : comment profiter de cette dynamique herméneutique pour produire des connaissances, ou encore, comment formaliser ces réécritures dans un format éditorial favorisant à la fois la conversation et son archivage\ ? Autrement dit, comment réconcilier le régime social et conversationnel du dispositif avec un régime documentaire\ ?

#### Bienveillance

Louise Merzeau emprunte à Belin la notion de «\ bienveillance\ » pour caractériser le dispositif analysé. Chez @belin_bienveillance_1999, la notion relève d'un espace transitionnel _entre_ le dehors et le dedans, espace où l'individu se reconnaît et dans lequel il peut placer sa confiance. Pour Belin, le dispositif est cet espace dit «\ potentiel\ », et «\ repose moins sur l'édition d'une loi que sur la mise en place de conditions\ »\ : conditions d'un possible.

Louise Merzeau reprend le concept de bienveillance dispositive à son compte, insistant sur le caractère environnemental du dispositif, ou spatiale en lien avec «\ l'habiter\ »[^habiter] déjà évoqué.

[^habiter]: Ce rapprochement est également employé par Belin qui l'emprunte à «\ l'habiter\ » de Bachelard (Bachelard, Gaston, La poétique de l'espace, Paris, Presses Universitaires de France, 1957, p. 202-203.).

> «\ Pour négocier l’hétérogénéité des sollicitations extérieures, nous avons besoin de les rendre commensurables avec nos compétences mobilisables. C’est à cet arrangement d’un milieu transitionnel que concourt la médiation des dispositifs. Celle-ci autorise en effet "\ une suspension temporaire de la frontière entre l’intérieur et l’extérieur, frontière qui se trouve remplacée par une relation de rappel, d’assortiment ou de reconnaissance\ " (Belin, 1999, p. 256). Condition de _l’habiter_, cet accommodement avec l’environnement suppose que l’outil soit moins vécu comme instrument que comme augmentation prothétique.\ »

La bienveillance du dispositif relève alors de son appropriabilité, voire de son incorporabilité (jusqu'à la prothèse), à moins que ce ne soit le milieu dispositif qui incorpore l'usage.

Enfin, comme condition du possible, Louise Merzeau rapproche la bienveillance dispositive à la contrainte créative, c'est-à-dire propice à la création, telle que pratiquée dans la littérature «\ oulipienne\ »\ :

> «\ En ce sens, si le dispositif n’est plus à entendre ici dans le sens coercitif que lui donnait Foucault (1977), il désigne encore une contrainte. Moins panoptique que pragmatique, celle-ci instrumente l’autonomie des participants, comme une contrainte poétique guide et libère la créativité du poète.\ »

#### Réflexivité

Louise Merzeau rapproche le régime attentionnel du dispositif analysé du régime attentionnel de la co-création, que l'on retrouve par exemple dans les espaces de coworking, les hackathons, et finalement dans toute approche de co-design. Il s'agit de maintenir un niveau d'engagement général suffisamment élevé pour atteindre collectivement un objectif. Ce régime fonctionne sur un premier principe de fragmentation des tâches selon les compétences et appétences des participants, respectant également les temporalités et les degrés de participations de chacun. La modularité est en effet une des caractéristiques du dispositif tel que Louise Merzeau le décrit\ :

> «\ Chaque application privilégie un outil, une temporalité (avant, pendant, après), une forme sémiotique (image, texte, oralité) et une modalité participative spécifique (commentaire, annotation, documentation, témoignage, archivage…), sans chercher à les écraser sous une même logique ou les ordonner dans une arborescence unique.\ »

Bien entendu, le risque d'un tel régime est de verser dans un néo-fordisme, et de générer finalement une prolétarisation [@faure_proletarisation_2009] des individus. Louise Merzeau montre que le dispositif évite cet écueil de deux manières\ :

1. par la déhiérarchisation des processus, propre aux approches design.
2. par la capacité du dispositif à offrir des visualisations de son propre processus, jouant le rôle «\ de boussole et de régie\ ».

Autrement dit, ces visualisations permettent aux individus de conserver une _vision_ d'ensemble, de se repérer, se positionner, «\ se régler\ » sur les actions des autres. Elles donnent à voir et à lire les temporalités à l’œuvre, la communauté («\ sociabilité\ »), et finalement la finalité de l'ensemble.

Ce _don_ de données revient à rétablir une certaine symétrie entre la communauté productrice et les plateformes détentrices. Une symétrie dans le sens d'un rééquilibrage (relatif) de pouvoir.

Ces visualisations sont identifiées par Louise Merzeau comme un pivot essentiel du dispositif. D'une part, elles garantissent aux individus un accès et une maîtrise de la finalité partagée par la communauté, et d'autre part, puisqu'élaborées (calculées) par la machine sur la base des traces produites par les participants, elles permettent aux participants d'acquérir une réflexivité sur ces mêmes traces, première condition vers leur interprétation.

Finalement, ces visualisations sont d'un côté vecteur d'engagement (le moyen), et de l'autre vecteur d'interprétation (la fin). L'interprétation fonctionne ici à la fois sur la synthèse visuelle, vecteur réflexif par excellence, et sur l'association de fragments ou de ressources, dans l'interstice de laquelle elle peut se nicher.

#### Appropriation

La réflexivité constitue un premier vecteur herméneutique relativement classique. L'analyse de Louise Merzeau fait ressortir un second vecteur, celui de l'appropriation. Cette dernière se concrétise dans le dispositif analysé à travers les «\ actions dispositives\ » des individus lorsqu’ils sélectionnent, organisent et réécrivent les ressources, autrement dit lorsqu'ils les «\ éditorialisent\ » [@merzeau_editorialisation_2013, p111]. On le voit, ces actions s'emparent des fonctions éditoriales classiques\ : la sélection et la structuration des ressources constitutives d'un processus de légitimation ascendant. Les réécritures sont à proprement parler une forme d'appropriation.

En annonçant vouloir «\ interroger les réseaux sociaux [en tant que dispositifs conversationnels] comme agents d'une évolution de la fonction éditoriale et pas seulement comme moyens de socialisation\ », Louise Merzeau anticipait en fait, sans que ce soit explicite dans le texte, cette fonction d'appropriation ou _d'appropriabilisation_. Ce néologisme peut sembler barbare, il est pourtant relativement opérant pour désigner _le processus de mise à disposition en vue d'une appropriation_. Aux fonctions éditoriales traditionnelles de sélection, de mise en forme et de diffusion [@vitali_rosati_ledition_2017], s'ajoute cette fonction d'appropriation consistant pour les institutions et les éditeurs numériques à créer les conditions de possibilité de l'appropriation, c'est-à-dire d'élaborer les dispositifs d'éditorialisation pré_disposant_ les ressources à leur appropriation.

Pour Louise Merzeau, le succès du dispositif analysé en terme d'engagement des contributeur s'explique précisément par le fait qu'il a pleinement assuré cette fonction d'appropriation. C'est l'un des enjeux à poursuivre dans la mise en œuvre de la Conversation en tant que dispositif d'éditorialisation.

#### Distance

Louise Merzeau nous montre que, dans le milieu architectural qu'est le numérique [@vitali_rosati_editorialization:_2018, p. 38], l'espace est régi par «\ des repères, des références, des normes (lexicales) et des règles d’intelligibilité\ ». Le dispositif instaure en effet un «\ protocole\ » éditorial précis qui conditionne les réécritures.

Cette capacité des participants à suivre, ou plus exactement à jouer de ce protocole, constitue pour Louise Merzeau le «\ savoir-lire-et-écrire numérique\ », ou encore la translittératie. «\ En jouer\ », tant les actions dispositives sont indissociablement des écritures _dans_ le dispositif (éditorialisation des ressources), que des écritures _du_ dispositif (éditorialisation du dispositif). Un autre indice de translittératie transparaît dans le fait que les principaux participants aient également contribué à la conception même du dispositif, dans un processus amont de «\ médiation\ ».

Écritures et réécritures forment ensemble une «\ glose\ », contrainte vertueusement selon des normes et des règles (le protocole). Si la glose consiste par définition en une production de sens constitutive d'une herméneutique, Louise Merzeau cherche encore à mieux la caractériser, à la rattacher au milieu numérique d'où elle émane, afin sans doute de ne pas complètement l’amalgamer à la glose manuscrite. Négociation nécessaire pour penser le numérique entre rupture et continuité. Elle invoque ainsi la culture anthologique [@doueihi_grande_2008] pour préciser la nature particulière de cette glose, fonctionnant en effet par associations sans cesse reconfigurées de fragments et de ressources\ :

> «\ Au centre  de la «\ compétence numérique\ » [-@doueihi_grande_2008], il faut placer l’aptitude à extraire, transférer et recomposer des contenus au sein  d’agencements collectifs. Dans ce «\ partage anthologique \[…\], sélection subreptice de fragments pour les diffuser sous forme de  recueils signifiants \[…\], le sens dérive largement d’une association  des contenus\ : au lieu d’être nécessairement lié à des auteurs, avec leur identité ou leurs intentions, il l’est plutôt à une catégorisation  flexible\ » (*Ibid*., p. 70). Alors que dans la culture littérale, l’ordonnancement des unités s’opère dans des milieux homogènes (texte,  livre, bibliothèque), il s’effectue dans la culture translittérale à  l’intersection de systèmes hétérogènes interopérables.\ »

Ces _intersections_ sont des zones potentielles d'association, où deux unités en produiront une troisième (1+1=3). Mais l'association n'est pas fusion, et entre les fragments de sens se loge toujours un _espace interstitiel_, matérialisant à la fois le rapprochement, nécessaire à l'interrogation, et la distance, propice à la critique. Finalement, ces interstices sont les lieux mêmes de l'interprétation. Cette distance joue le rôle de _coupe_-circuit, inhibant les comportements réflexes et réhabilitant nos capacités cognitives et interprétatives _court_-circuitées par les logiques affinitaires, algorithmiques et probabilistes des réseaux sociaux\ :

> «\ Cette inquiétude n’est pas tant celle de l’accélération que de l’écrasement des distances sur des proximités toujours plus étroites\ : proximité affinitaire, promue au rang de principe d’autorité par les réseaux sociaux, proximité algorithmique, ramenant tous les  contenus au stade de données statistiquement corrélables, proximité  probabiliste, évacuant tout écart d’incertitude au profit d’une  prédictibilité des comportements.\ »^[@merzeau_editorialisation_2013, p. 115]

De ce point de vue, Louise Merzeau nous livre une critique fine des dispositifs conversationnels des grandes plateformes algorithmiques dont elle pointe elle aussi «\ l'aliénation attentionnelle\ ». À nouveau, loin de s'arrêter au constat largement consensuel [@morozov_save_2013; @cardon_democratie_2010; @ertzscheid_entre_2014], elle nous confie les clés d'un horizon dépassant l'état de fait que nous imposent les GAFAM\ : distance, appropriation, réflexivité et pour accueillir les trois premières, bienveillance.

# Thèse : faire collectif

:::: note
Ce que nous montre le développement de l'hypothèse précédente, c'est un changement de paradigme par lequel la communauté scientifique déplace ses efforts mis dans la production de connaissances vers la production du collectif. C'est ce déplacement du document vers le collectif qui me semble en effet être une piste prometteuse pour réinventer l'université.
::::

# Etudes de cas

:::: note
à venir
::::

# Bibliographie
