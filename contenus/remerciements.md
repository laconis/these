---
title: Remerciements
subtitle:
date: 2020-08-28
version: 1.0
previous: 0.1
tagprevious: th1.0
date-version: 2020-09-08
link-citations: true
chapitre: unnumbered
lang: fr
---


::: {#dedicace}

[à Louise Merzeau.]{.dedicace}

:::



::: {#remerciement}

## Remerciements

[J]{.lettrine}e pourrais résumer cette thèse à l'année de rédaction qui s'achève, mais ce serait alors sans compter les multiples rencontres et soutiens qui l'ont portée jusque-là.

C'est à Louise Merzeau que je dédie cette étude, profondément imprégnée de sa pensée et de son optimisme. Avec sa disparition l'été 2017, nous avons perdu une collègue et une amie. Si pour ma part j'ai perdu aussi une première lectrice, ses écrits n'ont jamais cessé de me diriger. Je leur dois d'ailleurs de m'avoir fait réaliser, au tournant de l'année 2014, que _ce que je faisais_ alors méritait peut-être _que j'y pense_ le temps d'un doctorat.

Je dois aussi cette impulsion vers le doctorat à Joëlle Le Marec, avec qui nous avons étroitement collaboré sur un projet ANR. Nos conversations m'ont largement ouvert à l'hypothèse d'une thèse. Je l'en remercie aujourd'hui très chaleureusement. Merci aussi à l'ami Samuel Huron pour la voie qu'il a ouverte avec tant de plaisir, me confirmant que l'aventure en valait la peine.

J'étais alors ingénieur à l'Institut de recherche et d'innovation, dirigé par Bernard Stiegler et Vincent Puig. Les projets et les réflexions qui s'y développent ont marqué ma pensée. Cette aventure intellectuelle commence en fait à Compiègne en 1998 lorsque Bernard Stiegler crée à l'Université de Technologie de Compiègne la filière _Ingénierie des industries culturelles_ que j'intègre avec passion. Il va sans dire que je suis encore aujourd'hui extrêmement redevable de cette ouverture critique et théorique offerte alors à des ingénieurs.

Je tiens à remercier tout particulièrement mes deux directeurs Marcello Vitali-Rosati et Manuel Zacklad pour leur confiance et leur soutien. Il n'était sans doute pas facile de reprendre la direction à la suite de Louise Merzeau, mais Manuel l'a fait avec enthousiasme et finesse. Je dois rendre hommage également à la force de volonté de Marta Severo qui a su reprendre au pied levé les charges de Louise à Nanterre et au Dicen-IDF, et maintenir pour moi le lien avec l'Université.

Les quatre années de recherche, d'expérimentations et de bricolage à la _Chaire de recherche du Canada sur les écritures numériques_ ont été aussi riches qu'intenses, tant sur le plan humain que sur le plan intellectuel.
En capitaine de corvette, Marcello mène son laboratoire avec une générosité sans égale. Notre collaboration de longue date, marquée par dix années de codirection de séminaire, a désormais établi une complicité et une amitié qui se sont révélées essentielles pour la thèse.
Ces années québécoises doivent beaucoup à son engagement et à celui de tous les membres de son équipe avec qui j'ai pu travailler et échanger. Je remercie plus particulièrement Margot Mellet pour son écoute et sa finesse, ainsi qu'Eugénie Matthey-Jonais, Enrico Agostini Marchese, Antoine Fauchié, Lena Krause, Marie-Christine Corbeil, et les autres qui ont embarqué et tenu le cap. Nos réalisations essaimeront j'en suis sûr davantage que ces milliers de signes encore à lire.

Parmi elles, je dois citer ici tous les participants aux expérimentations dont la thèse s'est nourrie.

Je pense à la communauté qui s'est rassemblée autour du dossier «\ Écrire les communs\ », et en particulier à Xavier Coadic, à Romain Lalande et à Muriel Amar.\
Je pense à l'équipe organisatrice de la _Publishing Sphere_, en particulier à Lionel Ruffel et à Michael Nardone, ainsi que tous les participants évoqués ou non dans cette thèse, et plus chaleureusement Patrick Chatelier.\
Je pense à la formidable équipe de l'ouvrage «\ version 0. Notes sur le livre numérique\ », et en particulier Julie Blanc pour son transfert technologique dont la thèse a grandement profité.\
Je pense enfin à tous les éditrices et éditeurs de _Sens public_ qui ont vaillamment bêta-testé nos rêves les plus fous, et auprès desquel·le·s j'ai tant appris. Tout comme auprès des développeurs et designeuse qui, de l'autre côté du miroir, se démenaient pour les réaliser. Je saluerai en particulier Arthur Juchereau pour sa créativité. _Sens public_ ne serait pas devenu ce réseau d'intelligences sans la vision de Gérard Wormser, que je remercie très chaleureusement.

Dans le cadre de cette recherche et du projet _Revue 2.0_, j'ai mené plusieurs entretiens auxquels se sont prêtés des chercheurs, des éditeurs et des éditrices. Je les remercie sincèrement pour leur témoignage et pour la confiance qu'ils m'accordent au moment où je partage cette matière, en particulier Stéphane Pouyllau pour son décryptage de l'histoire récente de l'édition française.

Je remercie le support du CRIHN et de tous ses membres pour la diversité de leurs contributions, et plus spécialement Emmanuel Château-Dutier pour son érudition de la technique. Je remercie très chaleureusement Michael E. Sinatra pour son amitié et son appui, toujours attentif à rendre disponible le réseau de pairs qu'il constitue si bien.

Je remercie tous les membres du Dicen-IDF pour leur soutien en 2017 et leur retours avisés à chaque fois que l'occasion s'est présentée, et en particulier Gérald Kembellec, ainsi que les membres du Labex _Les passés dans le présent_ que je vais mieux découvrir maintenant. Je remercie particulièrement Ghislaine Glasson-Deschaumes pour son intérêt et son soutien.

L'exercice de la thèse oblige à signer seul un travail pourtant éminemment collectif. Dans le monde à venir où l'institution encouragera enfin les écritures collectives, Servanne Monjour aurait sans aucun doute co-signé cette thèse. Je ne serais pas arrivé si haut dans cette ascension sans ses talents et son engagement de lectrice et d'interlocutrice. Ces derniers laissent présager de très belles choses pour l'enseignante-chercheuse et l'éditrice qu'elle est devenue.

Sylvia Fredriksson cosignerait aussi, pour nos multiples collaborations qui m'enrichissent depuis 2012, dont celle qui amena Louise Merzeau à écrire et à entrevoir le sentier que j'ai suivi ensuite. Dont celles aussi qui pendant la thèse m'ont rouvert à d'autres communautés, d'autres pratiques et d'autres savoirs. Je remercie sa confiance, sa générosité et son inspiration.

Un grand merci enfin à ma famille et à mes amis pour leur soutien et leur présence transatlantique, et à mes enfants pour les couleurs dont ils ont barbouillé mes années de doctorat. On y a grandit ensemble. Je reste ébahi aujourd'hui de la joie de vivre avec laquelle ils tournent cette page avec moi et s'apprêtent à accueillir leur nouvelle vie.

---


<!-- À l'issue de ces cinq années de recherche, il y a bien sûr cette archive, mais les fruits en sont ailleurs.

 j'en retiendrai plus certainement le processus


de pensée, d'écriture et d'édition

Une thèse est une longue conversation avec soi-même

Cinq années de recherche s'achèvent, cinq années pendant lesquelles les rencontres auront été nombreuses -->

:::
