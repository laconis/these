---
title: Conclusion
subtitle:
date: 2020-07-07
version: 1.0
previous: 0.2
tagprevious: th1.0
dateversion: 2020-09-08
bibliography: contenus/conclusion.bib
link-citations: true
chapitre: unnumbered
lang: fr
---


# Faut-il en finir avec les revues\ ?

[C]{.lettrine}ette thèse s'est ouverte sur une polémique, une rébellion pourrait-on dire, menée par des chercheurs et des chercheuses\ -- parfois par les institutions elles-mêmes\ -- contre les modalités contemporaines de la publication scientifique. Pour nombre d'entre eux, dont on a pu citer quelques prises de parole radicales, le constat est clair\ : leur conception de la communication scientifique n'est pas respectée et les revues n'assument plus correctement leur fonction. La médiation des connaissances devrait donc s'émanciper des éditeurs et, pour reprendre la formule de Lionel Maurel, «\ l'édition peut bien aller mourir\ » [-@calimaq_lawrence_2017, [sur Hypothesis](https://hyp.is/Qc-IkuV1Eeq61Auo_CqxiA/scinfolex.com/2017/05/05/lawrence-lessig-les-derives-du-web-et-la-mort-des-editeurs/)].

Pour en avoir le cœur net, j'ai tout d'abord cherché à comprendre les ressorts de cette polémique et les raisons profondes de ce rejet. Le chemin parcouru n'aura été ni fluide ni linéaire. La thèse a ainsi emprunté plusieurs routes, parfois simultanées, dont l'enjeu ou l'aboutissement aura chaque fois été, je m'en rends compte, de déconstruire certaines des assertions et des positions initiales.

Dans un premier temps, je me suis confronté au constat généralement partagé d'une crise pourtant toujours «\ supposée\ » [@peres_ledition_2014]. J'ai identifié ainsi un double déphasage, éditorial et institutionnel, qui s'est ramifié par la suite en un déphasage épistémologique.
Le déphasage éditorial semblait sauter aux yeux\ : la conservation presque à l'identique des deux artefacts que sont l'article et la revue, hérités du papier et de l'imprimé, contraste terriblement avec la réinvention dans des formes proprement numériques de l'ensemble des activités humaines scripturales, documentaires ou communicationnelles depuis une vingtaine d'années.

J'ai établi ensuite le déphasage institutionnel, en partie responsable du premier déphasage et du manque d'innovation dans les formes éditoriales. Ce second déphasage est d'abord lié à la marchandisation croissante de l'édition scientifique corrompant de fait l'activité de recherche et de publication. Face à la concentration des éditeurs et à leurs pratiques commerciales et juridiques désastreuses pour les institutions de savoir, la réponse de ces dernières reste encore timide, hormis quelques exceptions locales. Ce constat est largement documenté, mais il peut être envisagé selon une autre perspective plus fondamentale dont j'ai tenté de rendre compte et qui nous livre une première clé pour réconcilier l'édition avec son temps.

J'ai montré ainsi que l'institution académique, pourtant garante de la culture écrite, avait en quelque sorte démissionné de ses responsabilités vis-à-vis de la chaîne de production de l'écrit. Elle laisse en effet aux GAFAM le _soin_ presque exclusif des outils et des pratiques scripturales dans l'environnement numérique, y compris au sein de l'université. Autrement dit, les universités et les chercheurs ne peuvent déjà plus prétendre prescrire les techniques intellectuelles de demain. Plus préoccupant encore, ils en ont perdu l'expertise. Or on l'a vu dans les chapitres 1 et 2, ce constat s'applique tout autant aux éditeurs scientifiques.

Dans un tel contexte, certes peu engageant, peut-on et doit-on encore sauver les revues\ ? C'est le pari que j'ai voulu faire dans cette thèse en avançant que les revues de LSH ont certainement encore un rôle à jouer dans l'écologie du savoir à venir. Mais le parti que je défends suppose une réinvention complète de la revue, nous engageant à une remise à plat de la fabrique des savoirs en LSH.
L'enjeu sous-jacent de ces deux chantiers, éditorial et institutionnel, se révèle donc épistémologique.

Quels sont les contours du régime d'autorité et de vérité du milieu numérique\ ? Ou encore, quels _devraient_ en être les contours pour assumer notre responsabilité en tant qu'éditeur, chercheur ou institution\ ? Autrement dit, de quel régime voulons-nous\ ? Et quel rôle les revues peuvent-elles jouer dans ce monde à inventer\ ?

Pour répondre à ces questions, j'ai travaillé dans plusieurs directions en redéfinissant progressivement ce qu'est la revue et en élargissant sa conceptualisation au-delà du modèle et de l'objet éditorial connu.

## Adopter de nouvelles perspectives (ch1)
[D]{.lettrine}ans le premier chapitre, j'ai pu montrer que la seule perspective du format et de son passage de l'imprimé au numérique ne permettait pas d'embrasser toute la complexité du sujet. Il m'a donc fallu écarter temporairement le format et sa modélisation pour adopter une approche communicationnelle et considérer finalement la revue scientifique en tant que _media_. Je me suis alors ancré dans les études médiatiques, et en particulier dans la pensée intermédiale dite de Montréal intégrant notamment les interrelations médiatiques me permettant de mieux saisir de quoi est fait le _media_ revue.

 De ce point de vue, le détour historique par la genèse des premiers périodiques savants a été utile pour identifier l'origine épistolaire de la forme article et du modèle périodique. La revue est donc née de la formalisation puis de l'institutionnalisation d'une pratique d'écriture et de communication adoptée par la communauté savante. Il a aussi permis de saisir l'impact épistémologique de la revue comme instrument de travail, comme instance de légitimation, et comme phénomène de structuration disciplinaire. Finalement, ce détour aura confirmé la revue comme un lieu d'innovation éditoriale, institutionnelle et épistémologique.

Par ailleurs, les théories médiatiques m'ont poussé à considérer la conversion numérique de la revue comme une _remédiation_ et ainsi à situer ma réflexion dans l'écosystème et le milieu numérique dont j'ai alors tenté d'identifier les conjonctures médiatrices. J'ai esquissé ces dernières en discutant la nature du texte numérique, ou en explorant les notions de fragments, de cristal de connaissances ou de performativité, toutes caractéristiques du milieu numérique et des processus qui le définissent.

Enfin, ce déplacement théorique vers le _media_ s'est finalement mieux accordé avec l'hypothèse de la conversation et avec mon approche méthodologique. Car la performativité du _media_ m'a incité à entreprendre l'étude des pratiques, ce qu'a fait notamment Louise Merzeau dans son analyse de l'expérimentation menée en 2012 sur un dispositif d'éditorialisation d'événement scientifique. En confrontant son héritage médiologique à la culture numérique, Louise Merzeau posait les pistes théoriques pour un nouveau régime documentaire et mémoriel. J'ai alors repris ces pistes à mon compte en les appliquant à la communication scientifique, ce qui m'a permis d'étayer l'hypothèse de la conversation. Sur le plan pratique, l'impasse d'une première modélisation de la conversation m'a incité à délaisser le format comme seule approche de la revue. Sur le plan conceptuel, j'ai affiné ma conception du modèle conversationnel pour l'envisager comme un processus d'éditorialisation et d'appropriation des ressources. Cette étape s'est révélée déterminante pour la réalisation et l'analyse des expérimentations suivantes.

Mais je crois que la première approche expérimentale a suscité une attention nouvelle vis-à-vis de mon sujet, jusque-là abordé avec peut-être trop de confiance. Car cette attention à la pratique n'exigeait rien d'autre qu'une attention aux praticien·ne·s de l'édition scientifique.

## L'édition avec les éditeurs (ch2)

[C]{.lettrine}e regard attentif aux pratiques constitue la seconde ouverture méthodologique et opère un glissement de mon objet d'étude depuis l’artefact éditorial, son format et son dispositif, pour replacer au centre de ma démarche les individus qui les produisent.
J'en rends compte dans le second chapitre, détaillant l'enquête de terrain menée auprès de six revues de LSH dans le cadre du projet _Revue 2.0_.
Or en me positionnant à l’écoute des éditrices et éditeurs de revue et en engageant avec elles·eux une conversation, j'ai recueilli une parole tout à fait précieuse et inédite, marquant un tournant dans mes recherches. La confiance qui a caractérisé ces échanges a certainement favorisé l'expression d'un enthousiasme sincère\ -- mais aussi inquiet, venant écorner l'image de conservatisme souvent attribuée aux éditeurs. La singularité des témoignages a certainement influencé ma prise de conscience d'une diversité vertueuse de pratiques. Enfin, ces entretiens m'ont permis de renouer avec une dimension humaine, installant la suite de la thèse dans le sillon du modèle collectif.

C'est ainsi que j'ai fait évoluer ma conceptualisation de la revue pour la considérer comme un espace\ : espace de la fabrique d'une part et espace de la collégialité d'autre part. Les conversations qui s'y tiennent dessinent plus nettement\ -- ou plus fidèlement\ -- que les protocoles éditoriaux la véritable structure de l'autorité, c'est-à-dire sa structure spatiale.
À ce sujet, les entretiens ont été édifiants concernant le paradoxe de l'édition savante en LSH tel qu'il est subit\ -- et défendu\ !\ -- par les praticiens. Mon analyse est la suivante\ : le travail éditorial se réalise dans une confrontation continuelle entre la subjectivité des textes et des idées d'une part et l'objectivité des protocoles éditoriaux d'autre part. J'ai découvert que cette tension ne se résout que dans sa _négociation_, c'est-à-dire dans la collégialité pragmatique des conversations, qui révèlent _in fine_ <!-- c'est très con mais depuis 2 ans in fine est très connoté Macron --> la dimension collective en puissance du processus et de la décision éditorial·e.

Ce collectif est le dernier prisme à travers lequel j'ai abordé la remédiation de la revue.

## Expérimenter et hybrider les revues  (ch3)

[À]{.lettrine} la suite de cette enquête de terrain menée sur un échantillon très ciblé, ma recherche s'est encore particularisée en menant une série d'expérimentations éditoriales. Ces dernières m'ont parfois conduit à m'extraire temporairement du champ strict de l'édition scientifique, opérant un pas de côté salutaire pour renouveler l'horizon de la revue.
Dans le troisième chapitre j'ai présenté les différents dispositifs et protocoles auxquels j'ai contribué en tant que concepteur ou éditeur, en cherchant à mettre en évidence les formes conversationnelles et les manifestations du collectif. Se révélant plus opérant que la notion de format, je me suis attaqué cette fois-ci à la notion\ -- et à sa pratique\ -- de protocole éditorial, déconstruisant au passage le modèle de scientificité responsable du déphasage institutionnel.

En étudiant par ailleurs des pratiques alternatives d'écriture et de publication, par exemple dans le champ littéraire, j'ai pu élaborer un cadre analytique me permettant de remettre en perspective les expérimentations que je menais moi-même. J'ai pu alors identifier et spécifier la dynamique conversationnelle récurrente dans les écritures numériques, ainsi qu'une pensée du collectif bien différente de la collégialité pratiquée dans l'édition scientifique. Cette pensée m'a été précieuse pour esquisser une alternative et envisager une remédiation de la revue scientifique l'inscrivant pleinement dans la culture et les savoirs contemporains. Cette alternative n'est rien de moins qu'une déconstruction de l'épistémologie actuelle basée sur l'économie et la pratique de l'imprimé. Prenant en compte la subjectivité et la pluralité des discours dans le champ des lettres et sciences humaines, c'est une proposition pour une scientificité émancipée basée sur une écriture ouverte et collective.

Au terme de ce travail de recherche qui associe des approches à la fois théoriques et pratiques\ -- pratiques de l'enquête ou de la création, je voudrais insister sur trois notions pour rassembler l'ensemble de l'appareil conceptuel mobilisé et tenter ainsi de synthétiser un modèle pour la revue scientifique.

# Concepts

## Conversation

[L]{.lettrine}e concept de conversation traverse l'ensemble de la thèse. Abordée selon différentes perspectives d'un chapitre à l'autre, la conversation est venue agencer ensemble des notions récurrentes, du fragment à l'éditorialisation, du mouvement à la performativité, ou encore de l'écriture au collectif. Avec la pensée de Lankes, le concept de conversation s'épanouit et s'accorde avec celui de la connaissance, conçue comme un «\ processus infini de transformation qui, à chaque fois, invite à l’action\ » [@lapointe_glossaire_2019].
Ce processus d'écriture et de réécriture est source à la fois d'interprétations et d'associations nouvelles, suggérant un régime herméneutique de production de connaissances.

Par ailleurs, considérant que sa nature fragmentaire, anthologique et performative épouse parfaitement les caractéristiques du milieu numérique, la conversation peut effectivement se concevoir comme une forme possible de production et de communication de connaissances.

En posant l'hypothèse d'un modèle conversationnel de communication scientifique, je n'imaginais pas que la conversation puisse se révéler aussi présente et décisive dans le processus éditorial des périodiques scientifiques. C'est justement en conversant avec les praticien·ne·s de l'édition que ceux·celles-ci se sont autorisé·e·s à dévoiler la constante négociation à l'œuvre, écornant au passage l'idéalité du modèle éditorial et du protocole censés garantir la scientificité des publications.

Convaincu par cette omniprésence de la conversation, j'ai pu alors explorer ses manifestations dans divers processus d'écriture, parfois en l'expérimentant directement dans des dispositifs originaux, parfois en l'extrapolant de discours et de pratiques alternatives d'écriture et de publication. J'ai qualifié ces écritures en mouvement d'_écritures dispositives_ dont j'ai montré d'une part la capacité à structurer le milieu d'écriture, et d'autre part la performativité vertueuse sur l'avènement d'un collectif.

À l'issue de ce travail, je définirai donc la conversation comme une forme particulière d'éditorialisation de fragments et de ressources, une écriture en mouvement, dispositive et collective, dont la performativité opère tant sur les idées qu'elle entrechoque que sur le milieu qui l'accueille, ou encore sur le collectif dont elle est la dynamique.

Car dans le modèle que je propose, la conversation n'est qu'un moyen, en premier lieu celui de la circulation des écritures et des idées, tout comme finalement l'article, dont la forme communicationnelle héritait directement de l'échange épistolaire. Mais à la circulation des connaissances, la conversation introduit leur appropriation, entendue à la fois comme une dynamique de réécriture\ -- en tant qu'appropriation des fragments, mais aussi du milieu\ --  et comme l'affirmation d'une _appartenance_. C'est par cette dernière que peut advenir la conscience collective, un _faire collectif_ qui déplace radicalement l'enjeu épistémologique de la communication scientifique. De la circulation à l'appropriation, la fonction éditoriale renouvelle finalement sa vocation devenant garante de la vitalité d'un _nous_ collectif.


## Fonction éditoriale

[S]{.lettrine}i la figure de l'éditeur s'étiole au point de s'éteindre, les éditeurs et les éditrices ne sont pas en voie de disparition, bien au contraire. Considérons plutôt que «\ nous sommes tous éditeurs\ ». Il ne s'agit pas là de soutenir symboliquement une figure effectivement obsolète\ -- bien que nullement sinistrée, mais plutôt de réaliser ce que le milieu numérique a fait de nous\ -- des éditeurs\ -- et de notre culture.
Écrire dans le numérique est toujours déjà un geste éditorial. Ainsi, ce qu'on appelle désormais _la culture numérique_ a en quelque sorte projeté notre «\ culture écrite\ » vers _une culture écrite et éditée_, où chacune de nos actions se traduit par une écriture et par son éditorialisation.

Pour mieux comprendre les effets et les enjeux de ce tournant, mieux vaut effectivement ne pas trop s'attarder sur l'évincement de la figure elle-même, et se concentrer plutôt sur le glissement de la fonction et sur ce qu'elle devient en se renouvelant.

Dans le modèle conversationnel précédemment synthétisé, je conçois la conversation comme un processus d'écriture favorisant des appropriations successives du sens et du milieu par lesquelles advient un collectif. Adopter ce modèle conversationnel comme forme éditoriale revient alors à considérer la revue comme collectif, et à en maintenir la dynamique collective.
Il ne s'agit pas seulement de _faire collectif_, mais de transformer\ -- conjuguer\ -- cette injonction en performance collective\ : _nous faisons collectif_.

La fonction éditoriale consisterait alors à créer les conditions de possibilité de cette conversation, c'est-à-dire finalement des appropriations collectives par lesquelles justement ce _nous_ se réalise.

Pour revenir à l'édition scientifique, l'application d'un tel modèle à la revue se joue sur le protocole éditorial, à même de _disposer_ les acteurs, _quels qu'ils soient_\ -- protocoles techniques, éditeurs et éditrices, contributeurs, formats, dispositifs, outils, etc., dans le sens d'une production collective d'écritures conversationnelles. Comme l'ont révélé les entretiens avec les praticiens de l'édition périodique, une certaine conversation est déjà bien présente au cœur des processus éditoriaux. Elle reste pourtant cloisonnée, aveugle, illégitime même parfois vis-à-vis du protocole éditorial censé garantir l'objectivité de la décision et la scientificité des contenus.

Au contraire, la conversation que j'envisage dépend précisément de l'ouverture et de la transparence du protocole éditorial. Une telle ouverture et une telle transparence ébranlent en profondeur les fondements épistémologiques de la scientificité, car elles supposent de revoir les principes mêmes de l'évaluation. En devenant collective et en l'émancipant d'une objectivité qui se révèle impraticable, la reconversion de cette dernière impose de redéfinir la notion même de scientificité. D'ores et déjà, cette approche de l'évaluation ouverte se fraie progressivement un chemin dans la communauté scientifique. Je la considère comme une preuve de concept d'un modèle conversationnel qui pourrait s'élargir bien au-delà de l'évaluation pour faire de la conversation le moteur même de la dynamique éditoriale. De ce point de vue, la fonction éditoriale consisterait alors à organiser et à assurer une véritable redistribution au collectif de l'autorité et de la légitimation.

Mais l'exemple du dossier «\ Écrire les communs\ » ou encore les pratiques de la gouvernance des communs m'incitent à penser que l'édition périodique scientifique pourrait encore accentuer ce tournant épistémologique. Car l'ouverture du protocole éditorial peut s'appliquer récursivement à lui-même. Il s'agit d'en partager la gouvernance en le soumettant lui aussi à la conversation. Ainsi, en définissant collectivement les règles, les outils et les modalités du protocole, c'est-à-dire de la conversation, c'est tout le processus de légitimation qui se _virtualise_ en même temps que le collectif. En suivant cette piste, la virtualisation du collectif coïnciderait avec la virtualisation de la revue. J'emploie ici la notion de «\ virtualisation\ » au sens philosophique du terme, envisageant ainsi la revue comme un espace, un _media_ mais aussi une forme toujours en devenir, toujours en puissance.

En conclusion, outre l'ouverture d'un espace conversationnel, la fonction éditoriale s'étend encore à garantir l'ouverture de la gouvernance, en instaurant les conditions de possibilité d'une co-construction continue d'un protocole et d'un dispositif éditorial. C'est d'ailleurs sans doute ainsi et seulement ainsi qu'une véritable confiance peut s'installer à la fois dans le dispositif et dans la conversation elle-même. Cette confiance est primordiale, elle donne la mesure de la légitimité des échanges et du collectif.

Cette co-construction bienveillante ouvre un horizon auctorial et éditorial nouveau, en affranchissant la revue des dualismes auteur/éditeur ou auteur/lecteur pour au contraire faire advenir le collectif et le conjuguer à la pluralité du _nous_.

J'appelle alors éditeur ou éditrice, celui ou celle qui s'engage dans cette démarche éditoriale.

## Savoir·s

[L]{.lettrine}e modèle que j'imagine s'écarte radicalement de l'objet revue tel que l'histoire de l'imprimé nous l'a légué. Pour autant, mon propos n'est pas de rejeter l'ancien modèle, mais plutôt d'engager les revues et ses praticiens dans un imaginaire vers de nouvelles formes d'appropriation et de production\ : collectives, fragmentaires, conversationnelles.
La thèse envisage une évolution de la pratique éditoriale dans le sens d'une ouverture des protocoles et dans le sens d'une conversation dont les formes et les temporalités coïncideront mieux avec la culture numérique.
Car l'obsolescence du format n'implique pas nécessairement l'obsolescence de la revue ou de ses éditeurs. Bien au contraire.

À ce sujet, le résultat marquant de mon enquête auprès des praticiens fait état de la pluralité des modèles éditoriaux et des modalités de la légitimation scientifique, au détriment du modèle idéal de scientificité telle qu'il est pourtant prescrit par l'injonction institutionnelle. Cette diversité éditoriale et cette pluralité épistémologique ne doivent pas être comprises comme un effondrement scientifique des LSH ou encore comme une crise de vérité. Ces subjectivités sont en fait la nature et la force de ces champs disciplinaires. Elles doivent être à tout prix entretenues.
Elles suggèrent des espaces inédits, encore à investir, et la possibilité d'une innovation éditoriale aussi ouverte que celle que l'on peut observer hors du champ scientifique.

À nouveau, chaque éditeur et éditrice poursuit une vision propre et singulière, souvent porteuse d'innovation intellectuelle ou éditoriale, susceptible de réconcilier l'édition scientifique avec les pratiques contemporaines d'écriture. Reste pour ces praticiens à acquérir une littératie nouvelle, celle d'écrire, de lire et d'éditer le milieu numérique pour mieux l'_occuper_.

Car face à la «\ prolétarisation\ »^[Je me réfère à nouveau à la définition qu'en donne @faure_proletarisation_2009, ou encore _Ars Industrialis_ [-@ars_industrialis_proletarisation_2008]\ : [«\ La prolétarisation est, d’une manière générale, ce qui consiste à priver un sujet (producteur, consommateur, concepteur) de ses savoirs (savoir-faire, savoir-vivre, savoir concevoir et théoriser).\ »]{.cite} Voir [sur Hypothesis](https://hyp.is/eb-0JuIdEeqHkWd_ZQahpw/arsindustrialis.org/prol%C3%A9tarisation).] réitérée au début de cette conclusion, il est effectivement urgent pour les institutions et pour l'ensemble de la communauté scientifique de _s'enlettrer_, d'acquérir une maîtrise minimale du milieu numérique, tant dans ses techniques que dans ses enjeux sociétaux et philosophiques, pour regagner la maîtrise de ses moyens de production. La littératie numérique constitue l'une des clés pour que les institutions de savoir à nouveau _prennent soin_ des modalités de production et de circulation du savoir.

Par ailleurs, un tel chantier institutionnel se joue sur une évolution nécessaire des modalités juridiques et économiques de la publication scientifique, dont j'ai présenté les dérives liées à la marchandisation croissante depuis la moitié du 20^ème^ siècle, exacerbée par la numérisation des contenus. Lorsque Bernard Stiegler rejoue le mythe du _pharmakon_ à propos du milieu numérique, ce n'est pas tant pour en dénoncer la toxicité ou pour en louer les vertus, mais pour nous désigner responsables de son orientation.

Ainsi, s'il émerge une nouvelle figure de l'éditeur, elle se définira peut-être par l'engagement et par le soin qu'il investira vis-à-vis de sa communauté de savoir et de son milieu. De la même manière que l'éditeur se devait de connaître son champ disciplinaire, il devra désormais savoir aussi pratiquer son milieu. Car cette pratique\ -- ce _savoir-lire, écrire et éditer_\ -- est la condition pour que se déploie au cœur des revues une créativité autant éditoriale qu'institutionnelle. L'innovation éditoriale qu'a été le périodique savant a bien redistribué les cartes de l'institution et de l'autorité scientifique au 17^ème^ siècle. De la même manière, la remédiation des revues scientifiques vers des pratiques en phase avec la culture numérique s'accompagnera d'un renouvellement des institutions de savoir.

Ce tournant épistémologique est finalement une proposition pour _faire science autrement_, selon les termes d'Isabelle Stengers. L'attention et le soin au milieu renvoient à son idée d'écologie politique que l'éditeur ou l'éditrice pourrait faire sien·ne.
La conversation, l'ouverture du protocole et l'ouverture de sa gouvernance profilent ensemble la remédiation d'une revue _inscrite_ dans les _inquiétudes_ de son collectif, au-delà de la communauté scientifique et au-delà de son objectivation du monde. On le comprend, pour faire face à la crise de légitimité que traverse l'institution scientifique, celle-ci doit envisager de nouveaux critères de scientificité, de manière à déhiérarchiser les savoirs et à s'ouvrir à des communautés de savoirs alternatifs.
Or le modèle conversationnel que j'ai défendu admet une approche intégrative de la pluralité des savoirs, en reconnaissant la nécessité de leur diversité.

C'est ainsi qu'il faut comprendre les _brouhahas_ de la culture numérique, qui portent en eux les moyens d'une pensée écologique et contre-hégémonique.
Réconcilier la revue scientifique avec la culture contemporaine ne signifie aucune soumission à un ordre numérique.
Elle signifie que la production de savoir sera collective. Voilà l'horizon du régime de vérité à inventer. Voilà désormais la mission que les revues peuvent résolument embrasser.

J'apporterai enfin une ultime remarque sur le cheminement de ma pensée pendant ces cinq années de thèse.
De mon point de départ focalisé sur le passage des revues au numérique, d'aucuns auraient pu soupçonner un certain solutionnisme positiviste. Je crois heureusement que la pensée critique des chercheur·e·s qui m'accompagnent depuis plus de vingt ans m'en a toujours défendu. Je dirais plutôt, puisque me voilà arrivé au bout de ma peine, que c'est en fait un certain optimisme écologique qui me guette désormais.



# Bibliographie {.unnumbered}
