---
title: De la revue au collectif
subtitle: La conversation comme dispositifs d'éditorialisation des communautés savantes en lettres et sciences humaines
author: Nicolas Sauret
date: 2020-08-28
version: 1.0
previous: 0.1
tagprevious: th1.0
date-version: 2020-09-08
link-citations: true
chapitre: numbered
lang: fr
---


::: {#avert}

## Avertissement

La présente thèse a été co-dirigée par Marcello Vitali-Rosati, professeur au département des littératures de langue française de l'Université de Montréal et titulaire de la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca) et par Manuel Zacklad, professeur de Sciences de l'information et de la communication au CNAM et directeur du laboratoire [Dicen-IDF](https://www.dicen-idf.org/). Louise Merzeau, anciennement professeure de Sciences de l'information et de la communication à l'Université Paris Nanterre et co-directrice du laboratoire Dicen-IDF, a initié la co-direction en 2015 avant de nous quitter en juillet 2017.

La thèse a été réalisée dans le cadre du labex [_Les passés dans le présent_](http://passes-present.eu/) et a donc bénéficié de l’aide de l’Etat gérée par l’ANR au titre du programme _Investissements d’avenir_ portant la référence ANR-11-LABX-0026-01.
Elle a également bénéficié du soutien de la _Chaire de recherche du Canada sur les écritures numériques_, du FRQSC (Programme international) et du [CRIHN](https://www.crihn.org).

Elle a été déposée le 1^er^ septembre 2020 et est diffusée depuis en libre accès sur le site [https://these.nicolassauret.net](https://these.nicolassauret.net).

### Conversation

Dans la mesure du possible, les citations ont été référencées en utilisant _Hypothesis_. Vous pouvez accédez aux textes cités en suivant les liens «\ sur Hypothesis\ ». Auparavant, il vous faudra vous créer un compte sur _Hypothesis_ et vous inscrire sur le groupe d'annotation dédié en cliquant sur [Thèse N.Sauret](https://hypothes.is/groups/JJ786xJj/these-n-sauret).

Au plaisir d'y poursuivre la conversation.

<!-- [TheseNico](https://hypothes.is/groups/3yDdEXPW/thesenico). -->


:::license

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>\  
La thèse est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International</a>. <a xmlns:dct="http://purl.org/dc/terms/" href="https://these.nicolassauret.net/" rel="dct:source">https://these.nicolassauret.net/</a>
:::

:::
