---
title: La revue espace
subtitle: conversation avec les éditeurs de revue
date: 2019-12-25
version: 1.0
previous: 0.4
tagprevious: th1.0
date-version: 2020-09-08
bibliography: contenus/revue20.bib
link-citations: true
chapitre: numbered
lang: fr
---

# Introduction

[U]{.lettrine}ne revue n'existe pas sans ses éditeurs. Dans la partie précédente, ma première approche par le format m'a amené à réévaluer la conceptualisation de la revue, pour envisager cette dernière comme un _media_, au-delà de sa seule dimension informationnelle. De ce point de vue, la remédiation des revues passerait nécessairement par la prise en compte des conjonctures médiatrices qui la produisent. Or entre autres conjonctures, chaque revue est le résultat continûment actualisé d'un travail collectif, mené par une équipe de chercheur·e·s dont l'investissement est souvent sous-estimé.

La figure de l'éditeur s'est considérablement altérée depuis l'avènement du web comme espace de publication émancipé. La position d'intermédiaire que tenait l'éditeur se trouve largement en porte-à-faux dans le contexte de la culture numérique. Les tenants de cette culture considèrent en effet l'éditeur comme un représentant de l'ancien monde, chien de garde\ -- par nécessité ou par idéologie\ -- de la propriété intellectuelle, dont les principes ont été copieusement bousculés par ceux du web et de l'internet^[Voir par exemple «\ Une Déclaration de l'indépendance du cyberespace\ », de John Perry @barlow_declaration_1996.].

Aussi, dans ce moment numérique rebattant les cartes des modalités de la publication, la question mérite d'être posée\ : a-t-on encore besoin des éditeurs\ ? Les auteur·e·s, créateurs et créatrices ont-ils·elles besoin d'un tiers pour décider de la publication de leurs créations\ ? Ce principe n'est-il pas devenu obsolète\ ?

La question et sa réponse ne sont pourtant pas si simples. Après avoir été un pourfendeur de la propriété intellectuelle et de ses partisans, Lawrence Lessig a opéré en 2017 un revirement inattendu au profit de la figure de l'éditeur, réhabilitée comme garante d'une société démocratique, basée sur une information vérifiée et validée [@tellier_lawrence_2017; @calimaq_lawrence_2017]. Je reformulerais volontiers ce que Lessig déplorait lorsqu'il déclarait\ : «\ nous nous sommes trompés\ ». Car ce n'est pas tant les éditeurs d'hier qui nous font défaut dans le contexte de la post-vérité, mais plutôt la fonction éditoriale elle-même. Celle-ci doit manifestement renaître et se renouveler pour assurer certains garde-fous, non pas pour rétablir la propriété intellectuelle et protéger les ayant-droits, mais pour réorganiser la validation des informations et des connaissances et assurer un _régime minimal_ de vérité face au régime algorithmique^[Je désigne avec ce terme le «\ régime de vérité numérique\ » [@rouvroy_regime_2015] issu de la «\ gouvernementalité algorithmique\ » définie par @rouvroy_gouvernementalite_2013.]. La question initiale doit donc être reformulée. De quels éditeurs a-t-on besoin\ ? Ou plus exactement, comment renouveler la fonction éditoriale\ ? En particulier dans le domaine de l'édition scientifique, quels aspects du métier sont amenés à évoluer, et dans quel sens\ ? Pour quel régime de vérité\ ?

En l'état actuel, le régime algorithmique de la sélection et de la recommandation que nous fournissent les grandes plateformes sociales n'est pas satisfaisant et subit de vives critiques qui dépassent\ -- enfin\ -- le cercle des initiés. Le discernement critique des individus demeure encore un rouage essentiel dans la circulation des informations et des connaissances, et cette assertion ne peut que se confirmer pour le discours scientifique relevant des domaines et des registres des lettres et sciences humaines.

Il faut donc penser la remédiation de la revue en même temps que la fonction éditoriale et les éditeurs et éditrices qui l'assureront. Ainsi, il apparait indispensable d'interroger directement ces praticien·ne·s et de les inclure dans la réflexion. Quelle est leur vision de la revue et de l'édition scientifique\ ? Quel est leur horizon\ ? Comment appréhendent-ils·elles le numérique et les injonctions institutionnelles qui y sont liées\ ? En comprennent-ils·elles les enjeux épistémologiques ou sociétaux\ ?

Pour aborder ces questions, j'ai complété mon travail de recherche théorique par une recherche de terrain auprès d'un panel d'éditeurs et d'éditrices impliqué·e·s dans le projet de recherche _Revue 2.0_, mené à la CRC-EN et dirigé par Marcello Vitali-Rosati. Je consacre l'ensemble de ce chapitre à l'analyse des résultats que j'ai pu obtenir auprès de ce terrain très particulier. Avant de présenter en détail le projet et ses enjeux, je dois faire remarquer qu'il s'agit d'une ouverture méthodologique assez radicale par rapport à mon approche initiale. Car, à l'approche expérimentale déjà introduite dans le chapitre précédent, j'ai ajouté l'enquête et l'entretien, auxquels je me suis formé pour l'occasion. Mes activités passées de réalisation documentaire se sont certainement révélées utiles, et auront peut-être aussi contribué à enrichir l'enquête classique de SIC avec une démarche à la fois ethnographique\ -- descriptive\ -- et autoréflexive, ayant amené les enquêtés à s'interroger sur leurs propres pratiques.

Au contact des éditeurs et des éditrices, j'examine ici la fabrique éditoriale dans sa dimension humaine, venant éclairer et enrichir les enjeux théoriques et les concepts déjà évoqués dans le premier chapitre.
Après avoir présenté le projet, je proposerai une problématisation des sujets abordés lors de cette observation de terrain, notamment des notions d'éditorialisation, d'autorité, d'énonciation éditoriale et enfin de conversation. J'introduirai ensuite la méthodologie basée sur une dialectique d'observation entre des éléments _inscrits_, dont on verra qu'ils relèvent d'abord de la construction institutionnelle, et des éléments verbalisés relevant de l'entretien et de la conversation. Je proposerai enfin une synthèse analytique des résultats autour d'une série d'axes thématiques structurants pour la suite de ma réflexion.


# Le projet _Revue 2.0_\ : une enquête auprès des éditeurs

<!--
Tu gardes ici ta description du projet telle quelle + présentation du terrain.

Dire que la conversation n'est pas exactement l'objet du projet revue20, mais que ta participation s'est concentrée là-dessus. Faire la distinction entre l'objectif du projet selon le labo et le tien (qui serait une sorte de sous-objectif).

Historique de ma démarche\ : implication dans l'écriture du projet (1ere année de la thèse, balisage historique de la revue, participation à la conception et écriture, choix du terrain - permet d'en faire une meilleure justification), coordination et action (recherche-action)

-->

[L]{.lettrine}e projet _Revue 2.0_ a démarré en octobre 2018, soit en début de troisième année du doctorat. Il est le résultat d'une précédente réflexion d'équipe tenue depuis 2016 et s'est ainsi inscrit pleinement dans le doctorat et dans ma démarche de recherche entamée au début de la thèse.

Le projet engage une étude et des expérimentations sur l'évolution du modèle éditorial des revues en LSH, avec pour objectif de proposer de nouveaux formats éditoriaux alternatifs à l'article scientifique. La conversation n'est pas spécifiquement au cœur du projet comme elle peut l'être pour ma thèse. Pour autant, mon intention de concevoir un format éditorial conversationnel a tenu lieu de sous-projet mettant en œuvre plusieurs expérimentations de conversation au sein de revues partenaires. _Revue 2.0_ a donc coïncidé de près avec mon projet de thèse.

J'ai par ailleurs contribué à l'écriture du projet (automne 2017) et à l'élaboration de ses principales problématiques de recherche, en lien avec les travaux déjà initiés au sein de la _Chaire de recherche du Canada sur les écritures numériques_ (CRC-EN). Le projet s'appuie notamment sur le balisage historique effectué en deuxième année de thèse et présenté lors de mon _examen de synthèse_^[L'examen de synthèse est une épreuve écrite et orale réalisée à mi-parcours de la scolarité doctorale au Québec à l'issue de laquelle le doctorant rentre officiellement en rédaction.] (automne 2017). Cette étude de l'émergence au 17^ème^ siècle des périodiques savants et de l'article comme format de communication scientifique^[Voir dans le chapitre 1 [«\ Au 17^ème^ siècle, la naissance d’un format éditorial\ »](../vrt/revueformat.html#au-17ème-siècle-la-naissance-dun-format-éditorial).] m'a servi à avancer l'hypothèse d'un _moment médiatique_ similaire entre d'une part la création du périodique et la formalisation de l'article savant dans le contexte d'une imprimerie dont la maturité aura permis la mécanisation de la pratique épistolaire de la République des lettres, et d'autre part la formalisation en cours de nouvelles pratiques d'écriture, de publication et d'échange de connaissances propre à l'espace du web.

Enfin, j'ai participé au montage du partenariat avec Marcello Vitali-Rosati, directeur du projet, et Servanne Monjour, co-chercheuse sur le projet. Le projet est en effet financé par le programme _Développement partenariat_ du CRSH^[Le _Conseil de recherches en sciences humaines_ est l'organisme fédéral canadien du financement de la recherche en sciences humaines.], imposant une forte dimension collaborative au sein du projet. En invitant plusieurs revues en sciences humaines à devenir partenaires, j'ai eu la chance de constituer un terrain de recherche _ad hoc_ pour observer et étudier les pratiques éditoriales et les techniques de l'édition scientifique en sciences humaines. Nous verrons par ailleurs comment la méthodologie de recherche adoptée pour aborder ce terrain et récolter des données qualitatives a pris une tournure conversationnelle, en phase avec mon objet d'étude, la conversation comme mode de production de connaissances.

D'une durée de 3 ans, le projet rassemble un partenariat constitué de six revues savantes francophones dans différentes disciplines ou champs de recherche, ainsi que trois partenaires institutionnels de l'édition scientifique, _OpenEdition_ et _Érudit_ en tant que plateformes de diffusion en ligne des revues de sciences humaines et sociales, et _Huma-Num_ en tant qu'infrastructure nationale de recherche pour les sciences humaines, menant notamment un travail de moissonnage et d'agrégation de ressources en ligne.

Lancé en 1998, _Érudit_ est un consortium québécois interuniversitaire dont la mission est de produire et de diffuser les versions numériques de plus de 150 revues savantes et culturelles, et d'agréger les thèses et mémoires de sept universités québécoises. _Érudit_ est «\ reconnu depuis 2014 comme l'unique Initiative Scientifique Majeure (Fondation canadienne de l’innovation) en sciences humaines et sociales au Canada\ »^[Voir la _lettre de soutien_ déposée par _Érudit_ pour s'associer au partenariat _Revue 2.0_ et à sa demande de financement.]. En s'imposant comme diffuseur scientifique francophone en Amérique du Nord et en se positionnant très tôt sur le libre accès, le consortium est devenu de fait une référence au-delà des sciences humaines québécoises.
La publication numérique des revues s'est appuyée sur le schéma XML _Erudit Article_^[Voir la documentation du schéma XML ([retro.erudit.org/xsd/article/3.0.0/](http://retro.erudit.org/xsd/article/3.0.0/en/doc/schemas/eruditarticle_xsd/schema-summary.html)) et son historique ([retro.erudit.org/documentation](http://retro.erudit.org/documentation/doc_erudit.htm)).], développé par le consortium pour l'édition scientifique numérique. Le modèle de données a été adopté notamment en France par la plateforme Persée^[Voir la page «\ Modèles de données\ » sur [Persee.fr](https://www.persee.fr/modeles-de-donnees).]. Si le modèle de données a pu, au début des années 2000, être considéré comme une approche exemplaire du point de vue documentaire, contrairement aux premiers pas de _Revues.org_ [^revuesorg], il présente aujourd'hui quelques lacunes ou obsolescences, par exemple pour les contenus nativement numériques.

> L’impression de copies imprimées d’une revue est de moins en moins usuelle alors que l’outil d’_Érudit_ reste calqué sur son mode de production.^[Voir la _lettre de soutien_ d'_Érudit_ au projet _Revue 2.0_.]

 Conçu initialement dans une perspective de remédiation des contenus papier, le schéma souffre en effet d'une certaine obsolescence au regard de l'évolution des pratiques éditoriales ces dernières années. Mais outre le biais de conception de son schéma documentaire, _Érudit_ est lié à une plateforme technique et à un protocole de production des contenus l'obligeant à maintenir une rupture nette entre le travail d'édition mené par les éditeurs de revue et le travail de structuration opéré par les documentalistes du diffuseur. C'est une des problématiques que le diffuseur cherchait à adresser en s'associant au partenariat de recherche. Dans le cadre de mes travaux, la plateforme _Érudit_ et son modèle documentaire incarnent parfaitement la tension entre institution, format et pratiques.

[^revuesorg]: Lancée en 1999, le portail de diffusion _Revues.org_ est devenu en 2017 _OpenEdition Journal_. Stéphane Pouyllau, déjà très impliqué en 1999 dans les réflexions et les initiatives françaises qui viendront structurer l'édition numérique scientifique en France, considère aujourd'hui que les fondateurs de Revues.org ont fait le choix d'une édition esthétique plutôt que d'une édition documentaire. Voir dans les annexes [le transcript de l'entretien](../annexes/revue20/transcript_entretienSPouyllau_20181026.html) du 26 octobre 2018 avec Stéphane Pouyllau.

_OpenEdition_ se définit comme un portail de publication en sciences humaines et sociales. Lancé en France en 1999 avec la plateforme _Revues.org_ dédiée à la diffusion numérique des revues scientifiques en SHS, _OpenEdition_ s'est diversifiée en proposant la publication de monographies (*OpenEdition Books*), de carnets de recherche (*Hypotheses.org*), un portail d'annonces d'événements scientifiques (*Calenda*). _Revues.org_ est devenu  _OpenEdition Journal_ en 2017. OpenEdition s'appuie aujourd'hui sur le système de gestion de contenu (CMS) _Lodel_ développée _in house_, évoluant régulièrement depuis la première version publiée sur Github en 2003, et offrant ainsi une plateforme plus récente et plus homogène que la plateforme d'_Érudit_. _Lodel_ permet également de conférer aux éditeurs une partie du travail de structuration des contenus. Son format pivot est le _TEI OpenEdition_, dérivé du format _TEI Journals_, inscrivant potentiellement les contenus dans un large écosystème aux modes d'appropriation différenciés. Malgré cela, et malgré une position dominante dans le monde francophone de l'édition scientifique, les marges de manœuvre pour l'évolution de ses composantes techniques et éditoriales n'en demeurent pas moins réduites. Pour les revues, se font sentir les mêmes contraintes d'homogénéisation liées à une nécessaire normalisation de la structuration des contenus. Par ailleurs, l'histoire (technique et éditoriale) de _Revues.org_, telle que racontée par Stéphane Pouyllau dans son entretien^[Voir en annexe [le transcript de l'entretien](../annexes/revue20/transcript_entretienSPouyllau_20181026.html) du 26 octobre 2018 avec Stéphane Pouyllau.], révèle les choix techniques à l'origine de Revues.org et leurs implications profondes sur l'édition scientifique. Cette histoire met en lumière une certaine distribution des fonctions éditoriales qu'il conviendra d'analyser au regard des potentialités de l'environnement numérique.

À travers son portail [_Isidore.science_](https://isidore.science), Huma-Num propose aux chercheurs et au public plusieurs services dont le plus important est un moteur de recherche spécialisé, permettant d'effectuer des requêtes précises sur plus de six millions de ressources en ligne venant de différentes institutions. Isidore propose également un service permettant de constituer des collections de ressources, et de les partager auprès de la communauté Huma-Num, ouvrant la voie à des scénarios de collaboration dans le champ des sciences humaines. Malgré une mission orientée sur l'infrastructure outils destinée aux chercheurs et aux laboratoires de SHS, Huma-Num complète le développement et la maintenance de ces outils par une approche expérimentale sur de nouveaux services au plus près des besoins des équipes. De ce point de vue, Huma-Num constitue un partenaire très engagé dans la réflexion sur les évolutions possibles en matière d'édition numérique. Positionnée logiquement à la fin de la chaîne de l'édition pour l'exploitation des données diffusées à travers le portail _Isidore.science_, l'équipe d'Huma-Num s'interroge ainsi sur les modalités de production des données par les chercheurs, rejoignant de ce fait la philosophie de l'éditeur sémantique _Stylo_ développé par la CRC.EN.

Enfin, les six revues francophones forment un panel certes non-représentatif de l'édition périodique scientifique, mais incarnant cependant une diversité de champs de recherche, d'origines et de rapports au numérique. Ce groupe nous ouvre une première exploration de ce qu'est une revue scientifique aujourd'hui, de son fonctionnement, de sa mission et de ses valeurs dans le paysage de la connaissance.

La revue _Études françaises_, lancée en 1965 au Département d'études françaises^[Le département est aujourd'hui nommé «\ Département des littératures de langue française\ ».] de l'Université de Montréal, est la plus ancienne des revues partenaires. Comme son nom l'indique, la revue s'occupe d'études littéraires francophones, champ pour lequel la revue a acquis une renommée certaine. Diffusée en numérique par _Érudit_ dès 2003, _Études françaises_ a été l'une des premières revues prestigieuses à tenter le passage à la diffusion numérique. Elle est aujourd'hui en accès libre numérique avec une barrière mobile de 12 mois, et a conservé parallèlement une diffusion papier publiée par les Presses de l'Université de Montréal. Pour autant, sa place dans la discipline n'est plus une évidence dans ce contexte de transformation des modes de diffusion des revues, comme en témoigne le non-renouvellement des financements fédéraux et régionaux ces dernières années. _Études françaises_ étant ancrée dans une histoire relativement ancienne pour une revue, il était intéressant d'étudier dans le cadre du projet l'adéquation des pratiques éditoriales de la revue avec celles de son diffuseur.

La revue _Itinéraires. Littérature, textes, cultures_ étudie également la littérature à travers une approche exploratoire visant à «\ repérer des phénomènes d’émergence littéraires et culturels\ »^[Voir la page de présentation de la revue sur la plateforme _OpenEdition Journals_\ -- [journals.openedition.org/itineraires/](https://journals.openedition.org/itineraires/).]. La revue est née en 1976 au sein du centre de recherche _Pléiade_ de l'Université Paris 13. Elle est devenue exclusivement numérique en 2015, diffusée par _OpenEdition Journals_. Son passage tardif au numérique présente plusieurs intérêts pour le projet. En particulier, le coût d'entrée pour rejoindre la plateforme du diffuseur a nécessité un effort de formalisation, tant du protocole éditorial que des formes publiées.

La revue _Cybergéo_ se présente comme «\ une revue numérique européenne de géographie\ ». Nativement numérique dès son lancement en 1996, la revue est depuis 2007 publiée et diffusée en libre accès (Freemium) sur _OpenEdition Journals_. Son ancienneté dans l'environnement numérique lui a permis de développer une approche éditoriale spécifique avec un modèle de publication en flux continu, l'exploration de nouveaux formats éditoriaux (*data papers*, ouverture d'un «\ espace de conversation informelle\ »^[Voir la présentation sur [cybergeo.hypotheses.org](https://cybergeo.hypotheses.org/).] sur hypotheses.org), ou encore la publication multilingue. Fort de ces avancées sur le plan éditorial, la revue n'a malheureusement pas souhaité prendre part à la phase d'observation du projet, qu'elle a de ce fait dû quitter. Il est évident, au regard de leur expérience en matière de communication scientifique numérique, que les éditeur·rice·s de la revue auraient apporté une réflexion pertinente à mon terrain et au projet lui-même. On pourra retenir de cette abstention qu'il n'est ni aisé ni évident, même pour les tenants d'une science ouverte, de partager et de transmettre les process et les méthodologies à l'origine des données et des publications ouvertes.

La revue _Intermédialités\ : histoire et théorie des arts, des lettres et des techniques_ s'inscrit dans la théorie de l'intermédialité telle qu'elle s'est développée à l'Université de Montréal. La revue a été fondée en 2003 au sein du _Centre de recherche sur l'intermédialité_, avec une approche fortement interdisciplinaire et une ouverture particulière sur la recherche création. La revue s'est forgé une marque de fabrique en éditant une mise en forme soignée de travaux artistiques, dans une perspective critique et/ou créative. _Intermédialités_ étant passée en 2009 à une diffusion exclusivement numérique chez _Érudit_ pour des raisons budgétaires, il nous semblait intéressant d'évaluer comment la revue avait négocié une transition numérique caractérisée par une normalisation de la mise en forme.

_Mémoires du livre_ est une revue nativement numérique diffusée en libre accès sur la plateforme _Érudit_. Lancée en 2009 sous l'impulsion du _Groupe de recherches et d'études sur le livre au Québec_ de l'Université de Sherbrooke, la revue s'intéresse à l'histoire du livre à travers une approche interdisciplinaire. Son âge récent explique logiquement le choix du tout numérique. Ces deux caractéristiques la positionnent de fait par rapport aux autres revues, lui conférant une place de témoin pour mon terrain.

La _Revue internationale de photolittérature_ a été créée au sein du CELLAM (Université Rennes\ 2) dans le sillage du projet de recherche _PHLIT_ et de son répertoire des œuvres photolittéraires. La revue, lancée en 2017 et encore en phase de développement, répond au besoin d'incarner le champ de recherche de la photolittérature encore récent et en pleine structuration. Dans un premier temps, la création en 2012 du corpus et répertoire _PHLIT_ avait l'ambition de proposer un premier espace et outil de recherche aux chercheurs internationaux engagés dans le champ de la photolittérature. Dans le même temps, la plateforme de publication _PHLIT_ proposait d'agréger des articles déjà publiés ailleurs, afin de centraliser un corpus critique en libre accès. Mais l'afflux d'articles a confirmé le projet d'une revue à part entière, organisée autour d'un numéro annuel thématique.
En se dotant d'une revue dédiée, le champ entreprend en quelque sorte son institutionnalisation, dont le processus coïncide d'ailleurs avec celui de l'intégration de la revue à la plateforme _OpenEdition Journals_. En termes d'éditorialisation, la _Revue_ propose une démarche originale pour exploiter le répertoire _PHLIT_ et la base de données associée, en les faisant dialoguer avec les articles publiés.

Avec un tel partenariat, le projet a cherché à couvrir la partie institutionnelle du cycle de vie des contenus édités par les revues, à savoir la production avec les éditeurs, la diffusion avec les diffuseurs et l'appropriation avec l'agrégateur.

En tant que coordinateur scientifique du projet, j'ai mené la première phase du projet consacrée à l'observation des pratiques éditoriales des revues partenaires. Si la conversation n'est pas en soi une finalité pour le projet, elle constitue par contre l'un des horizons que j'ai explorés en vue de revitaliser les revues.


# Problématiques adressées

[L]{.lettrine}a rédaction du projet _Revue 2.0_ s'est appuyée sur les problématiques générales étudiées par la CRC-EN en lien avec l'édition numérique\ : la production, la circulation et la légitimation des contenus en contexte numérique. À l'origine de plusieurs projets et expérimentations auxquels j'ai pris part au sein de la Chaire, ces problématiques rejoignent de près les interrogations de la présente thèse, en particulier la question de l'autorité dans l'environnement numérique et la conception de dispositifs d'éditorialisation. La conversation comme format de communication scientifique s'inscrit comme une piste exploratoire au croisement de ces trois axes de réflexion.

## Éditorialisation et production de l'autorité\ : de l'ouverture du texte à l'ouverture de la conversation

<!-- réintégrer l'encart sur l'autorité (dans Interne/Externe). Redistribution d'une autorité - co-construction vertueuse à la fois du dispositif éditorial et des connaissances qui s'y élaborent, sortir d'une dualité éditeur/auteurs et aller vers un collectif dont les conditions de possibilité émergent avec le dispositif éditorial\ : c'est la nouvelle fonction de l'éditeur.

éditorialisation = ouvrir le texte ==> ouvrir la conversation.

TITRE\ : Éditorialisation et production de l'autorité\ : de l'ouverture du texte à l'ouverture de la conversation
 -->
[A]{.lettrine}vec le transfert de l'ensemble de l'écosystème scientifique dans l'environnement numérique, les structures de l'autorité régissant la production et la diffusion des connaissances demeurent-elles inchangées ou sont-elles au contraire profondément transformées\ ? Que peut-on dire par exemple du fait que les chercheurs et les étudiants se tournent d'abord vers le moteur de recherche _Google_ pour trouver de l'information [@bonino_lautorite_2017], plutôt que vers des moteurs spécialisés\ ? Si une entreprise privée est devenue de fait le premier prescripteur de contenus scientifiques, cela n'indique-t-il pas que l'autorité aurait changé de main et de nature\ ? Quelles en sont les conséquences pour l'autorité conférée traditionnellement à l'article scientifique et à la revue en général\ ?

La problématique de la construction de l'autorité dans l'environnement numérique réside au cœur des travaux de la CRC-EN. En lien avec cette problématique, Marcello Vitali-Rosati développe une théorie de l'éditorialisation qui a l'ambition de caractériser les modalités de production de l'espace à l'ère numérique, dont la structure détermine les relations d'autorité. À ce rapprochement entre espace et autorité s'ajoute l'éditorialisation comme processus de production d'espace\ :

> «\ Si l'éditorialisation est ce qui structure l'espace numérique et si la structure d'un espace est ce qui détermine l'autorité, alors l'autorité de l'espace numérique est déterminée par l'éditorialisation.\ » [@vitali_rosati_quest-ce_2016]

L'ère numérique suppose ici que l'espace numérique ne se distingue plus d'un espace dit physique, tant nos actions et nos écritures s'y trouvent intimement mêlées. Dans cet espace d'action et de vie, les protocoles qui régissent les réseaux et le web, les logiciels qui définissent les dispositifs de communication, de publication et d'écriture, et finalement les structures de l'environnement numérique, établissent ensemble un nouveau régime d'autorité qui dépasse le périmètre de nos actions et de nos présences _en ligne_. C'est ce régime que la théorie de l'éditorialisation cherche à définir à partir d'une analyse structurelle et critique des dynamiques et des interactions à l'ère numérique.

On retrouve chez Louise Merzeau la même idée lorsqu'elle articule espace structuré (l'architecture) à une fonction «\ autorisante\ »\ :

> «\ Bien que structurée, l’architecture dispositive est peu
contraignante, autorisant une multiplicité de modes d’utilisation, des
plus systématiques aux plus aléatoires, sans imposer un enchaînement
obligatoire d’étapes.\ » [@merzeau_editorialisation_2013]

Merzeau désigne pour sa part la structure spatiale produite par les éditorialisations comme une «\ architecture dispositive\ ». Comme je le développerai dans le dernier chapitre^[Voir au chapitre 3 la partie [«\ Quel milieu pour la conversation\ ? Les "écritures dispositives" du collectif littéraire Général Instin\ »](../vrt/revuecollectif.html#quel-milieu-pour-la-conversation-les-%C3%A9critures-dispositives-du-collectif-litt%C3%A9raire-g%C3%A9n%C3%A9ral-instin).], je m'inscris pleinement dans cet ancrage «\ dispositif\ » et dans une approche où l'éditorialisation s'incarne au travers de dispositifs et de processus d'éditorialisation, en particulier ceux de l'édition savante en pleine remédiation.

Si l'éditorialisation, en tant que théorie de l'espace et de l'autorité, est particulièrement stimulante, elle s'élève à un niveau d'abstraction qui en rend l'appréhension parfois difficile. Mon objectif est de la confronter à des dispositifs concrets.
Cette approche par le dispositif me permet de m'appuyer sur des expérimentations concrètes d'outils, de protocoles ou de formats éditoriaux comme celui de la conversation.

  <!-- , à un objet éditorial précis, la conversation, avec ses propres enjeux, sa propre histoire.  -->

Il en va de même avec l'étude de terrain que j'ai pu mener auprès des revues savantes partenaires, consistant à décrire le processus éditorial, puis à le discuter avec ses acteurs. De ce point de vue, ma méthodologie de recherche peut se concevoir elle-même comme une conversation avec les éditeurs.

<!-- ici entend réaliser ce travail d'éditorialisation avec les éditeurs. -->

L'une des principales caractéristiques de l'éditorialisation est l'ouverture du texte à différents intervenants. Cette ouverture suggère une évolution de la fonction auctoriale, désormais redistribuée. Dans ce changement de cadre, que devient la fonction éditoriale\ ? L'hypothèse que je poursuivrai pour ma part est celle d'une redistribution de l'autorité savante qui nécessite d'être accompagnée par l'avènement de nouveaux formats éditoriaux de communication scientifique.

En considérant que la fonction éditoriale évolue, et avec elle le rôle de l'éditeur, j'avance que la responsabilité de ce dernier n'est plus d'assurer l'entièreté des fonctions traditionnelles (sélection, structuration, légitimation), mais d'en assurer les conditions de possibilité. L'exemple de YouTube est emblématique d'un glissement de la fonction éditoriale.
La plateforme de vidéos, en tant qu'éditeur, n'assure pas elle-même la fonction éditoriale de sélection, mais a mis en place un dispositif opérant pour chaque utilisateur une sélection de contenus. Ainsi, se soustrayant à la sélection opérée traditionnellement chez les grands médias par leurs directeurs de programme, YouTube a proposé un modèle de recommandation distribuant la responsabilité de la sélection entre les mains des usagers et d'un algorithme prenant en compte ces usages.
Ce glissement de la fonction engage clairement un transfert de l'autorité, puisque l'instance légitimante se voit redistribuée aux participants à travers leurs usages et, dans le cas de YouTube, au dispositif algorithmique lui-même.

Que se passerait-il si YouTube s'occupait d'articles scientifiques plutôt que de vidéos grand public \? Derrière cette projection aux conséquences hasardeuses, j'ai voulu poser la question de la redistribution de l'autorité savante dans un cadre d'écriture et de publication -- d'éditorialisation -- plus ouvert et plus participatif. C'est cette conversation que j'ai entamée avec les éditeurs partenaires du projet. Qu'est-ce qu'ils·elles pensent du système d'autorité actuel, et qu'envisagent-ils·elles pour le faire évoluer ou pour adopter les structures spatiales de l'autorité dans le numérique\ ?

<!--
> «\ Each authority is the result of a particular spatial structure. A space is a configuration that generates the possibility of trust. Trust is the result of the particular structure of the relationships between objects. Changing space means also changing the kind of authority we can trust.\ » [@vitali_rosati_editorialization_2018] -->



## Ruptures dans la chaîne de production de l'édition scientifique\ : de la perte de l'énonciation éditoriale

<!--
Cette partie est top (juste quelques annotations) Je reformulerais juste le titre et je dirais de manière un peu plus claire ce qui t'intéresse du point de vue de ta démarche\ : la question de l'énonciation éditoriale

TITRE\ : Ruptures dans la chaîne de production\ : vers perte de l'énonciation éditoriale


-->

[C]{.lettrine}omment les pratiques éditoriales des revues se sont-elles renouvelées avec l'avènement du numérique\ ? Quelle est la nature des transformations opérées sur les chaînes éditoriales\ ? Le projet _Revue20_ est parti du constat notoire de l'omniprésence de la suite _Microsoft Office_ et de son logiciel _Word_ comme outil d'écriture et d'édition[^word]. Ce constat est problématique à plusieurs égards.

Le premier aspect concerne le principe éditorial sur lequel _Word_ a bâti son succès, à savoir une interface d'écriture et d'édition dite *WYSIWYG*^[«&nbsp;Ce que vous voyez est ce que vous obtenez&nbsp;» (*"What you see is what you get"*).], offrant à l'écran et à l'utilisateur une représentation homothétique d'un document imprimé, c'est-à-dire structurée en pages, ou plutôt en feuilles de papier, conforme à ce que l'imprimante produira à la fin du processus. Cette représentation est parfaitement adaptée à la structuration mentale de notre littératie, issue de trois siècles d'imprimé. Cette littératie de l'imprimé et le modèle mental des connaissances qui en découle héritent en effet d'un savoir-faire rigoureux en matière d'édition et de structuration qui consistait en l'organisation des contenus dans une forme contrainte, un recueil de feuilles, chacune offrant un certain nombre de pages. Cette organisation du contenu mobilisait alors des compétences graphiques impliquant la disposition spatiale des contenus et le choix des caractères typographiques, selon des conventions établies au fil des siècles et dont la hiérarchisation graphique des éléments du texte en assurait la lisibilité et l'intelligibilité. Ainsi, _Word_ comme logiciel de *traitement de texte* offre aux auteurs de textes la possibilité de s'improviser éditeurs de documents. Cette situation nouvelle, à première vue susceptible d'entrainer une démocratisation de l'édition, a pourtant deux conséquences.

 1) Les auteurs travaillant avec un traitement de texte comme _Word_ sont amenés à exprimer graphiquement des éléments relevant en fait de l'information, et qui devraient donc être formalisés en tant que tel. Cette méthode de travail est particulièrement problématique dans le cas de l'édition scientifique qui demande rigueur et précision, notamment en levant toute ambiguïté sur l'interprétation de ces éléments. Or l'expression graphique est par nature sujette à interprétation, là où une expression sémantique permet au contraire de *déclarer* la nature ou la valeur d'une information. L'une des fonctions de l'éditeur, notamment à travers le métier de secrétaire d'édition, est bien de composer le contenu, c'est-à-dire de proposer une hiérarchisation de ses différents éléments, et de faire ainsi le lien entre le travail de rédaction de l'auteur, et la mise en page finale du contenu. Or ce travail de l'éditeur se poursuit sur _Word_, s'appuyant sur les mêmes marqueurs graphiques, livrés ensuite au diffuseur ou aux presses pour la structuration ou la mise en forme finale. C'est justement là que s'installe une potentielle rupture de sens, entre le document produit par l'éditeur et celui produit par les presses ou le diffuseur. Car le diffuseur, sur la base du marquage graphique produit par l'éditeur via un traitement de texte, engage une nouvelle structuration du contenu, soit dans un logiciel professionnel de PAO^[Pour publication assistée par ordinateur.] pour une diffusion papier, soit dans une édition structurée pour une édition numérique. Par exemple, dans le cas du diffuseur _Érudit_, ce travail de structuration, pris très au sérieux^[Ce travail peut constituer en moyenne 21 heures de travail par dossier de revue. Voir en annexe [la chaîne de production d'*Érudit*](../annexes/revue20/erudit-workflow-prod.pdf).], est assuré par un professionnel de l'information, qui est amené à interpréter les différents éléments du document édité sous Word, et à leur donner une certaine valeur sémantique. Le même travail est donc effectué deux fois\ : la première fois l'information est exprimée graphiquement par l'éditeur, la seconde fois elle est exprimée sémantiquement par un documentaliste. Seuls les multiples allers-retours entre le diffuseur et l'éditeur, puis entre l'éditeur et l'auteur, pourront venir à bout des différentes ambiguïtés ou erreurs d'interprétation, et valider enfin un document final *bon à tirer*. Une telle rupture, reflétant sans doute une nécessaire distribution et spécialisation du travail, perd clairement son sens dans le cas d'une édition numérique. En effet, dans un environnement d'écriture nativement sémantique (potentiellement) tel que l'environnement numérique, il n'est plus justifié de déléguer le travail de structuration à la fin de la chaîne éditoriale, quand le sens, c'est-à-dire la valeur scientifique des éléments du contenu, peut être déclaré dès l'étape de rédaction, par le chercheur lui-même, c'est-à-dire la personne la plus à même de définir et de caractériser l'information dans son texte.

[^word]: Si ce constat s'applique au grand public, il se vérifie en particulier au sein de l'université, dans les pratiques des chercheurs comme des étudiants.

C'est cette rupture de sens que le projet a souhaité cibler en proposant des pratiques éditoriales susceptibles de redonner aux auteurs et aux éditeurs la capacité de structurer sémantiquement leurs contenus, et d'assurer ainsi la continuité des données tout au long de la chaîne de production des revues.

 <!-- 2) les éditeurs sont devenus des experts de Word, là où pourtant aucun imprimeur n'accepterait d'imprimer du word. Un corps de métier intermédiaire s'est donc construit chez les diffuseurs ou les presses, pour la mise en forme des contenus.

Un point aveugle de l'édition scientifique réside dans le constat d'une déprofessionalisation des éditeurs de revue scientifique en ce qui concerne la production et la structuration des documents, que ces derniers soient destinés à l'impression ou à une diffusion numérique. En effet, l'essentiel du travail de structuration est laissée dans les mains du diffuseur, de l'imprimeur ou parfois d'intermédiaires spécialistes en édition scientifique. Dans cette sous-traitance assumée qui semble régir -->

2) Cette rupture de sens est en effet symptomatique de la perte de contrôle des éditeurs sur les contenus qu'ils prétendent publier. Si les éditeurs se sont concentrés sur la production et la légitimation du texte et des auteurs à travers un processus d'évaluation, de relecture et d'édition du texte, ils ont aussi largement capitulé sur les compétences nécessaires pour assurer le simple accès aux contenus dans l'environnement numérique. En déléguant à un intermédiaire, que ce soit une plateforme de diffusion ou à des prestataires dits techniques le soin de structurer les contenus et les métadonnées des revues dans les formats de diffusion, l'éditeur a en conséquence perdu la maîtrise sur l'existence et la circulation de ses contenus dans l'environnement numérique. Mais l'a-t-il *perdu*, ou bien ne l'a-t-il jamais eu\ ? La question est légitime si l'on considère les modes de diffusion sous forme d'ouvrages et d'imprimés, à travers les réseaux de bibliothèques et les abonnements. L'indexation des contenus imprimés était assurée essentiellement par les bibliothèques lors de l'acquisition d'un ouvrage.

La théorie de l'«\ énonciation éditoriale\ » proposée par Emmanuel @souchier_image_1998 nous donne quelques éléments pour saisir la nature du pouvoir de l'éditeur sur ce qu'il produisait.

> Il convient donc de considérer le texte à travers sa matérialité (couverture, format, papier…), sa mise en page, sa typographie ou son illustration, ses marques éditoriales variées (auteur, titre ou éditeur), sans parler des marques légales et marchandes (ISBN, prix ou copyright)…, bref à travers tous ces éléments observables qui, non contents d’accompagner le texte, le font exister. Ces marques visuelles qui permettent de décrire l’ouvrage ont été mises en œuvre par les acteurs de l’édition. Élaborées par des générations de praticiens dont le métier consistait à «\ donner à lire\ », elles sont la trace historique de pratiques, règles et coutumes. [@souchier_image_1998]

Avec ce concept, Souchier nous incite à analyser les conditions de matérialité des textes et de leur circulation, c'est-à-dire à rendre visible le média ou, pour aller plus loin, à en «\ interroger l'évidence\ ». Le savoir-faire de l'éditeur tel qu'il travaillait *en vue* d'une édition papier consistait justement à anticiper et à prendre en compte la matérialité du texte qu'il donne à lire. Cette matérialité s'inscrit dans ce que Souchier appelle le «\ texte second\ »^[Souchier reprend ici le découpage utilisé par Roland Barthes dans sa sémiologie du mythe [@barthes_mythologies_2005, p.187].] dont la fonction est de «\ donner à lire le "texte premier"\ », c'est-à-dire en assurer l'intelligibilité et la lisibilité graphique. Cette lisibilité s'adresse bien sûr au lectorat, mais prépare aussi la réception qui en sera faite par les bibliothécaires dont une des tâches sera de procéder à l'indexation de l'ouvrage à partir de ses marques éditoriales.
Dans ce mode de diffusion papier, l'éditeur conservait une maîtrise de l'«\ image du texte\ », notamment par la commande et la validation d'une maquette graphique, et ainsi par une proximité graphico-sémantique entre le document produit par l'éditeur et livré au graphiste pour le mettre en page. Car à ce moment précis du processus éditorial, l'éditeur et le graphiste emploient finalement le même langage, graphique, et se réfèrent à un même modèle, la maquette graphique.
L'«\ énonciation éditoriale\ » relève pleinement de la fonction de l'éditeur, qui saura donner à l'ouvrage sa place dans l'écosystème de circulation des écrits, que ce soit en anticipant son indexation et son catalogage, voire l'espace de librairie qui l'accueillera, ou en établissant les conditions de sa lecture. En maîtrisant l'architexte qui enchâsse le texte, c'est-à-dire «\ la matérialité du support et de l’écriture, l’organisation du texte, sa mise en forme, bref par tout ce qui en fait l’existence matérielle\ »[@souchier_image_1998], l'éditeur assume une bonne partie de la construction de l'autorité du texte.

> Un texte ne tisse donc pas uniquement des relations «\ intertextuelles\ » avec les autres textes qui constituent l’horizon culturel dans lequel il se meut, au sens où l’entendait Julia Kristeva, il est également le creuset d’une énonciation collective derrière laquelle s’affirment des fonctions, des corps de métier, des individus…, et où fatalement se nouent des enjeux de pouvoir.

Or, cette énonciation éditoriale et cet architexte ne s'établissent plus de la même manière dans l'environnement numérique, modifiant par là même les rapports d'autorité. Sept années après l'introduction du concept d'énonciation éditoriale par Emmanuel Souchier, ce dernier revient avec Yves Jeanneret en 2005 sur l'utilité de ce concept dans l'environnement numérique [@jeanneret_lenonciation_2005]. Leur approche prend en compte les conditions de matérialité des textes et de leur circulation insiste sur l'impact politique de ces conditions d'existence. Leurs travaux résonnent aujourd'hui avec l'archéologie des médias qui entend elle aussi déconstruire et mettre à jour les dynamiques de médiation. Mais il est intéressant de noter que l'approche sémiotique semble avoir enfermé leur analyse dans le cadre restreint de l'écran et ce qui s'y affiche. Ils rappellent à juste titre que l'écran et les pages qu'il présente ne sont pas sans matérialité et que cette matérialité puise dans une histoire longue de l'écrit et de la mise en page, invoquant pour l'occasion l'histoire et l'anthropologie pour en analyser les sens et les dynamiques de pouvoir. Mais leur discours de rappel est d'abord une tentative pour contrer un discours peut-être trop prégnant à l'époque qui entendait opposer «\ texte matériel/texte virtuel, pouvoir de l'éditeur/pouvoir du lecteur, intermédiation/auto- publication, etc.\ » [@jeanneret_lenonciation_2005, p.4] dans une perspective radicale de «\ table rase\ ». Souchier et Jeanneret s'attaquent ici à une certaine idéologie de l'horizontalité et de l'ouverture, associée notamment aux prémisses de l'Internet puis du Web^[On en retrouve les racines dans un certain libéralisme, ainsi que @turner_aux_2012 a pu le démontrer.]. Mais il est permis d'aller plus loin en reconnaissant les changements profonds qui s'opèrent sur l'écriture lorsque celle-ci appartient à l'espace numérique. Car il est évident, comme le notent Souchier et Jeanneret, que les représentations de cette écriture numérique rentrent en résonnance avec l'histoire de l'écrit et de l'imprimé, ainsi qu'avec un savoir-faire et une énonciation éditoriaux, l'inscrivant davantage dans une continuité qu'une rupture. À cela, il faut ajouter la matérialité même du signe, qui n'est plus ontologiquement une encre sur un support solide, mais plutôt une série de variations électriques sur un support manipulable et calculable. Cette électrification du signe en augmente aussi l'abstraction, réduisant la lettre à un encodage, ajoutant certes une couche de médiation^[Le calcul nécessaire pour passer du binaire "0100 0001" au caractère correspondant "A", selon l'encodage ASCII 7bit.], mais dénuée pour le coup de toute énonciation.

En insistant sur les effets de détermination des logiciels de traitement de texte, Souchier et Jeanneret pointent une partie du problème, à savoir l'illusion d'une désintermédiation là où s'installent au contraire des médiations de plus en plus complexes sur le plan technique. Mais la perte de pouvoir ne s'arrête pas à celle de la maîtrise de la page et de son organisation, perte qui serait consécutive à la surdétermination d'un logiciel dominant. La perte provient plutôt de la méconnaissance de la véritable nature de l'écriture numérique, qui se joue davantage dans les couches *non-visibles* que dans ses représentations [@petit_lecriture_2017]. Et c'est sans doute à ce niveau d'écriture que l'éditeur a perdu une partie de son autorité. La littératie et son savoir-faire de l'«\ image du texte\ » ne lui servent plus à grand-chose lorsque le texte est organisé et structuré par une série de marqueurs sémantiques, selon le ou les langages de balisage de texte que constituent par exemple le XML ou le HTML. À ces marqueurs, pourront être associées des feuilles de styles et des règles de mise en forme, dans l'optique de générer effectivement une image du texte. Mais la véritable structure se joue dans une syntaxe programmatique, selon le principe général de séparation du texte et de sa mise en forme. Dans ce cas, le _texte_ s'enrichit en fait d'un balisage, si possible sémantique, venant déclarer sa structure de manière expressive, indiquant ses éléments de sens et ses éléments éditoriaux. Plus important encore, les bibliothécaires, s'ils n'ont pas disparu, ne procèdent plus à l'indexation des contenus et des ouvrages à la main sur la base d'un architexte qui lui fournirait des données. Les index et les champs du catalogue sont renseignés automatiquement sur la base des données et des métadonnées livrées avec le texte, en général dans le même langage de balisage que le texte lui-même. Autrement dit, l'énonciation éditoriale d'un document se déploie dans l'environnement numérique au travers de balises dédiées, sémantiquement intelligibles et non plus graphiquement, permettant à la fois aux machines et aux humains d'identifier sans équivoque la nature d'un contenu. Cette écriture programmatique, *compréhensible* par la machine, a ouvert la voix au classement automatique et à la recherche de document, modifiant profondément les modalités d'existence d'un contenu, et par là même les dynamiques de pouvoir.
Ainsi, l'autorité dans l'environnement numérique se définit largement à travers les formats et les données que ceux-ci exposent, et aux moteurs de recherche et aux agrégateurs spécialisés exploitant ces données.
C'est pourquoi le seul savoir-faire de l'éditeur concernant l'énonciation éditoriale de l'imprimé ne permet plus de construire l'autorité d'un texte. Emprisonné dans le logiciel de traitement de texte, l'éditeur est cantonné à une écriture et à une structuration graphique, déléguant la structuration réelle au diffuseur.
Cette distribution des tâches et la spécialisation qui pouvaient fonctionner dans l'environnement papier révèlent plutôt aujourd'hui les lacunes profondes des éditeurs de revue en matière de littératie numérique, présumant d'une déprofessionnalisation du secteur. En adressant cette problématique, le projet _Revue 2.0_ entendait proposer des modèles de production susceptibles d'encapaciter les éditeurs sur le plan technique et éditorial, afin de renforcer la légitimité déjà largement estompée des revues.

## De nouveaux modèles éditoriaux\ : la conversation comme modèle heuristique

<!--
TITRE\ : De nouveaux modèles éditoriaux\ : la conversation comme modèle heuristique

Expliquer que le projet revue20 encourage de nouveaux modèles édito. De ton côté, c'est celui de la conversation qui t'intéresse car il te semble vertueux. C'est ce que tu vas d'ailleurs démontrer ici. Ta méthodo a été une mise en pratique de ta thèse. Est-ce que la conversation ne peut pas en effet nous amener à découvrir des aspects inattendus de la revue à l'époque de sa remédiation\ ? de comprendre autrement les problématiques qui sont attachées à cette remédiation\ ? Ta méthodo s'appuie sur le modèle conversationnel - c'est une conversation avec les éditeurs - de manière à renouveler la connaissance que nous avons sur les revues.

l'écriture qui modélise de novuelles formes de pensée.

-->



[P]{.lettrine}uisque la nature même du support d'écriture est modifiée, écrit-on encore de la même façon à l'ordinateur\ ? Que se passe-t-il lorsque cette écriture informatisée se noue avec le réseau de publication qu'est le web\ ?

Avec l'avènement de l'Internet et du Web comme espaces de savoir et d'échange, de nouvelles pratiques d'écriture ont émergé et rapidement évolué. De concert avec ces pratiques, les formats et les protocoles qui les accueillent et les régissent se sont eux aussi transformés. Que l'on pense aux formes les plus anciennes, comme le courriel ou la messagerie instantanée, fonctionnant sur des protocoles et des formats spécifiques, aux formes associées aux débuts du Web, comme le blogging, le wiki ou le forum de messageries, ou encore à des formes plus récentes, héritières des précédentes, comme le micro-blogging ou les éditeurs de texte collaboratifs synchrones^[*Collaborative real-time editor*.], les écritures numériques ont épousé les différents registres\ : communication, expression, collection, circulation, collaboration. Avec ces écritures et ces formes ont émergé de nouveaux contenus, dont les modèles éditoriaux ont su hybrider des modèles reconnus avec des modèles inédits. C'est le cas de la forme encyclopédique, renouvelée par _Wikipédia_ selon des principes de collégialité, de contribution et de transparence. _Wikipédia_ est un exemple déjà largement reconnu [@barbe_wikipedia_2015] d'une forme nativement numérique de connaissance. Une nouvelle écriture du savoir suggère aussi de nouveaux savoirs.

Qu'en est-il dans le milieu de l'édition scientifique\ ? Puisque le support modèle la raison, puisque le mode d'écriture et d'édition affecte le mode de pensée [@goody_raison_1979], ne serait-il pas nécessaire que les supports de communication scientifique évoluent pour assimiler les pratiques d'écriture numérique\ ?
Une telle question pointe en fait le déphasage éditorial qui subsiste aujourd'hui entre les formats de communication scientifique institutionnalisés et les pratiques d'écritures déjà adoptées par les chercheurs.

Le projet _Revue 2.0_ fait le pari que les revues, qui n'ont encore que très peu évolué dans leurs formes et objets éditoriaux, ont devant elles un espace éditorial à investir. Si leur forme est aujourd'hui fortement figée notamment par les injonctions institutionnelles, qui s'incarnent notamment dans les prérequis demandés par les organismes subventionnaires[^frq], bras armés de l'institution, les revues n'en demeurent pas moins des lieux d'innovation éditoriale. L'effort est conséquent, tant les éditeurs ont perdu dans la transition numérique la maîtrise de leurs objets et de l'environnement dans lequel ces objets circulent. Mais l'innovation ne viendra pas uniquement d'une littératie numérique et d'un savoir-faire éditorial à se réapproprier. Elle viendra également d'une vision à renouveler et de valeurs à reconstruire. Avec les évolutions de la fonction éditoriale déjà décrites, le rôle des éditeurs et des revues est encore à inventer. Je soutiens que, comme à la fin du 17^ème^ siècle lorsqu'elles ont su formaliser l'échange épistolaire, les revues pourraient se positionner pour légitimer des pratiques d'écriture et de lecture déjà présentes, et institutionnaliser des formes de communication nouvelles.

[^frq]: Pour illustrer, voir les propos de la directrice de la revue _Mémoires du livre_\ : [«\ On est limités à reproduire les exigences que les pourvoyeurs de fonds nous demandent. Au niveau du processus d’évaluation, le CRSH va valoriser cette évaluation-là avec au moins deux experts externes à l’aveugle. À moins qu’eux ne changent leurs règles, on ne pourra pas les changer non plus. La question du libre accès a toujours été fondamentale, nous n’avons jamais voulu être une revue sous abonnement, et la conséquence c’est de faire ce que le patron, en l’occurrence le CRSH, nous dit de faire.\ »]{.cite} (Marie-Pierre Luneau\ -- Entretien du 24 avril 2019)

La conversation comme potentiel modèle de communication scientifique se révèle un objet d'expérimentation fécond pour penser ce renouvellement de vision. Le registre conversationnel est au cœur des nouvelles sociabilités issues de la culture numérique. Implémentée par différentes plateformes sociales, la conversation n'a pas les mêmes fonctions d'un dispositif à l'autre. Elle peut être utilisée pour partager des ressources, pour commenter des contenus, pour résoudre collectivement des problèmes, pour transmettre des connaissances ou encore pour organiser les actions d'une communauté. Elle s'inscrit en fait pleinement dans la culture participative décrite par Jenkins et constitue l'un des vecteurs de circulation des ressources. Plus ou moins organisée et outillée selon les plateformes, elle est le lieu d'une production de connaissances, comme en témoigne par exemple la plateforme _Stackoverflow_ dédiée à la résolution de problèmes, ou la plateforme _Hypothes.is_ dédiée à l'annotation de contenus Web. Mais si la conversation se révèle fertile à la production de connaissance, quels en sont les mécanismes de légitimation\ ?

La conversation telle qu'elle se déroule en ligne sur les plateformes sociales relève d'une écriture collective, se déroulant selon des règles sociales particulières d'une plateforme à l'autre. C'est l'un des aspects que l'éditeur peut investir afin d'organiser l'écriture collective, ou autrement dit de _disposer_ la conversation, ses règles, y compris éditoriales. On retrouve dans ce principe l'idée du dispositif d'éditorialisation tel que je l'ai introduit en relatant l'expérience des ENMI12. De ce point de vue, le dispositif est un agencement entre des ressources, les acteurs et la structure technique et éditoriale qui les supportent. Cet agencement dépasse la seule question du format, comme nous avions tenté de le concevoir.
L'enjeu est également institutionnel. Le processus d'institutionnalisation des nouvelles manifestations de la connaissance n'est pas un long fleuve tranquille, comme en témoignent les critiques^[Avec un peu d'ironie, je source ces critiques avec la notice _Wikipédia_ dédiée aux [«\ critiques de Wikipédia\ »](https://fr.wikipedia.org/wiki/Critiques_de_Wikip%C3%A9dia) #auto-critique.] dont peut encore être victime _Wikipédia_ dans une certaine communauté scientifique. L'institutionnalisation se joue dans la capacité d'une institution à reconnaître des pratiques existantes, puis à accompagner leur formalisation dans des modèles et protocoles éditoriaux adéquats. Une telle adoption institutionnelle dépasse largement le cadre temporel d'une thèse, mais il est permis de poser quelques éléments susceptibles d'encadrer positivement un processus d'institutionnalisation.
<!-- je pourrais reprendre ici l'interview de Stéphane P. sur OpenEdition -->

Dans le cadre du projet _Revue 2.0_, j'ai tenté d'identifier et de localiser les processus conversationnels au sein des revues scientifiques. Dans le cadre plus large de la thèse, j'ai tenté de concevoir et de réaliser des dispositifs conversationnels, dont je rendrai rendre compte dans le chapitre 3.

<!-- #CONVERSATION le déplacement de la fonction suppose une co-construction vertueuse du dispositif éditorial et des connaissances qui s'y élaborent. Un tel dispositif est fondé sur des caractéristiques précises que j'ai pu identifier (voir partie X) [finaliser lien interne]{.note}, capable de générer une dynamique susceptible de sortir d'une dualité éditeur/auteur·s et de faire advenir un collectif. -->

# De l'inscription à la conversation\ : méthodologie dialectique d'observation

<!--
TITRE\ : De l'inscription à la conversation\ : méthodologie d'observation / méthodo dialectique

réexpliciter la méthodo conversationnelle\ : converser (avec les éditeurs) pour produire de nouvelles connaissances,

réexpliciter en identifiant ton processus\ : 1) les inscriptions de la revue (une archive + un questionnaire fondé sur un modèle idéal édito) 2) la mise en conversation de la revue. Ce processus s'appuie sur la tension entre modèle idéal et réalité pratique de la revue. Dans le projet revue20, il y avait cet effort de virtualisation (). Voir si on ne peut pas penser ici une distinction entre modélisation et virtualisation. Les enquêtes traditionnelles (comme le questionnaire) impliquent une modélisation. La conversatioon permet d'Aller vers la virtualisation - retour aux problèmes. On ne modélise plus une revue ou un modèle idéal, on **ouvre** la revue à ses potentialités (c'est le sens de virtuel).

Dire que du coup tu as un glissement qui s'opère, de la revue comme objet vers la revue faite par les éditeurs.

Récursivité du modèle/virtuel avec la modélisation/standardisation nécessaire à la diffusion numérique.

-->


[L']{.lettrine}une des ambitions du projet _Revue 2.0_ a consisté à associer une recherche pratique ancrée dans le terrain de l'édition scientifique, à travers l'observation et la collaboration des revues et diffuseurs partenaires, et une recherche théorique ancrée dans les connaissances et les concepts émergents en matière d'édition numérique. Ce principe, également au cœur du fonctionnement de la CRC-EN, s'est manifesté pendant le projet à travers un travail minutieux de récolte de données sur les pratiques éditoriales au sein des revues scientifiques.

La première année du projet (2018-2019) a ainsi été consacrée à une phase d'observation des différents partenaires acteurs de l'édition scientifique, dans l'objectif de saisir le fonctionnement effectif des revues partenaires, et d'identifier leurs besoins.

Un protocole d'observation a été mis en place, d'abord de manière collégiale avec l'ensemble des partenaires lors de deux journées d'ateliers qui ont marqué le lancement du projet^[Les comptes-rendus des ateliers sont à retrouver sur [le wiki du projet](http://wiki.revue20.org/index.php/Journ%C3%A9es_de_lancement).]. Puis au sein de l'équipe du projet, le protocole a été affiné lors d'une série de réunions impliquant certains des chercheurs associés. Ces réunions ont abouti à la définition d'un protocole structuré autour de trois activités\ :

1. _un questionnaire en ligne_ consistant en une trentaine de questions courtes sur le fonctionnement de la revue, d’un point de vue éditorial et scientifique,
2. _une collecte d'archives_ qui vient compléter le questionnaire pour renseigner et décrire le fonctionnement éditorial de chaque revue partenaire,
3. _un entretien semi-directif_ avec les éditeurs et les éditrices, centré sur les aspects d’autorité et de scientificité propre à chaque revue scientifique.

Chaque étape incarne un changement de perspective sur la fabrique de la revue, afin d'en dresser un portrait à plusieurs facettes. Le questionnaire cherchait à modéliser les chaînes éditoriales de chaque revue, sur la base d'un «\ modèle idéal\ ». La collecte d'archives témoigne des pratiques réelles en termes de communication, d'évaluation et de validation. Les entretiens, enfin, ont été utiles pour décaler le regard sur le travail éditorial centré initialement sur la chaîne, et se concentrer sur les éditeurs et les éditrices eux·elles-mêmes.

En amont de l'observation, une série de documents préparatoires ont été produits.

Document·s | Objectif·s | Aperçu |
:--------- | :--------------------- | :---------: |
Argumentaires ([extrait ci-dessous](#extraitArgumentaire)) | - _Présenter_ le projet aux comités scientifiques ou éditoriaux des revues participantes, ainsi qu'aux auteurs potentiellement concernés par les données récoltées,<br/>- _Établir_ un consensus sur les modalités du projet. | ![Argumentaire Comités](../media/argumentaire-comites.png) |
Liste de documents | - _Récolter_ une série de documents internes génériques présentant le fonctionnement et le protocole éditorial\ : gabarit de la revue, formulaire d'évaluation, etc.,<br/> - _Constituer_ l'archive d'un article ou d'un dossier\ : échantillons aux différentes étapes du protocole éditorial (exemple d'appel à communication, évaluations, etc.), un suivi d'article au complet | ![Liste de documents](../media/liste-de-documents.png)|
Questionnaire | - _Obtenir_ des données précises sur le protocole détaillant le fonctionnement de chaque revue, et en identifier les étapes fondamentales,<br/> - _Modéliser_ la chaîne éditoriale des revues. | ![Questionnaire en ligne](../media/questionnaire-en-ligne.png)|
Consentement | - _Obtenir_ le consentement des participants, conformément aux règles du comité éthique de l'Université de Montréal (CERAH). | ![Formulaire d'information et de consentement](../media/formulaire-d-information-et-de-consentement.png) |

Table: Documents préparatoires à l'observation


::: {.focus #extraitArgumentaire}
> Il s'agira notamment d'étudier, au cœur même des rédactions, le fonctionnement des revues savantes d'un bout à l'autre de la chaîne éditoriale\ : de l'écriture de l'article à sa publication et ses réappropriations, en passant par le processus d'évaluation, d'édition, etc.
Nous souhaitons ainsi documenter et comprendre la réalité du terrain éditorial, en identifiant les besoins qui sont propres aux revues, les solutions innovantes mises en place par les éditeurs, et les verrous qui restent encore à lever. Tout cela en tenant compte, bien sûr, des singularités de chaque revue et de leur discipline de rattachement.
>
> Concrètement, durant cette première phase, notre équipe va\ :
>
> - suivre la publication de plusieurs articles ou de dossiers complets.
> - mener des entretiens avec les membres de la rédaction (directeurs, éditeurs, etc.)
>
> Notre étude sera basée sur un protocole d'analyse précis, discuté avec l'ensemble de nos partenaires. Concernant l'enquête sur le suivi des articles, nous nous engageons à garantir l'anonymat des données étudiées (auteurs et versions des articles, évaluations, etc.). Par ailleurs, aucun article ne fera l'objet de notre suivi sans l'accord préalable de son ou ses auteur(s).

:::

[Extrait de l'argumentaire destiné aux comités scientifiques ou éditoriaux des revues.]{.legendeFocus}

Ces éléments préparatoires ont été mis à disposition sur le wiki du projet et communiqués aux participants via une liste de diffusion, en même temps que le déroulé suivant\ :

:::{.fulltable #deroule}

+--------------------+--------------------------------------------------+
| période            | étapes                                           |
+====================+==================================================+
| 21 décembre 2018   | Soumission du présent protocole,                 |
|                    | accompagné de\ :                                 |
|                    |                                                  |
|                    |  1) une liste de                                 |
|                    | documents/traces que nous                        |
|                    | souhaitons récolter (date butoir                 |
|                    | de livraison\ : 1^er^ mars),                     |
|                    | 2) les                                           |
|                    |  "argumentaires"                                 |
|                    | présentant le projet, à                          |
|                    | destination de vos comités et                    |
|                    | de vos auteurs.                                  |
+--------------------+--------------------------------------------------+
| 25 janvier 2019    | Envoi des questionnaires aux                     |
|                    | revues, (3 semaines étaient                      |
|                    | données pour y répondre).                        |
+--------------------+--------------------------------------------------+
| février-mars 2019  | 1) Prise de rendez-vous pour deux                |
|                    | entretiens en avril: avec le·s                   |
|                    | direct.rice.eur·s de la revue et                 |
|                    | le·s secrétaire·s de rédaction,                  |
|                    | 2) Analyse des formulaires en                    |
|                    | préparation aux entretiens.                      |
+--------------------+--------------------------------------------------+
| avril 2019         | Entretiens                                       |
+--------------------+--------------------------------------------------+
| mai-juin 2019      | 1) Transcription et analyse des                  |
|                    | entretiens,                                      |
|                    |  2) Livraison des                                |
|                    | premiers résultats,                              |
+--------------------+--------------------------------------------------+

Table: Déroulé de l'observation communiqué aux participants et aux partenaires

:::

<!-- Ma démarche de recherche a intégré une forte dimension pratique avec la conception et la réalisation d'outils, de protocoles et d'objets éditoriaux. Dans cette démarche, la mise en œuvre d'artefacts me sert de support pour développer une pensée critique sur mon sujet d'étude. Une telle approche par l'expérimentation relève d'une démarche de recherche-action qui s'est retrouvée naturellement dans le protocole d'observation mis en place collégialement pour le projet _Revue 2.0_. -->

Une particularité de la méthodologie adoptée pour le projet est d'avoir délaissé l'approche quantitative qui aurait permis de dresser un portrait de l'édition scientifique, approche préconisée par exemple en France par\ -- entre autres\ -- le _Comité de suivi de l'édition scientifique_ du Ministère de l'Enseignement, de la Recherche et de l'Innovation[^etudes]. Ce comité a pour mission d'identifier à grande échelle les phases accomplies en matière d'accès ouvert, ou encore de saisir l'état de l'économie de l'édition scientifique [@noauthor_etude_2020]. Pour ce qui est du projet _Revue 2.0_, il s'agissait davantage d'identifier des besoins très précis dans un échantillon de revues que nous pouvions considérer représentatif, afin de réaliser avec ces revues des expérimentations ciblées.

[^etudes]: Voir la présentation du Comité sur [le site du Ministère](https://www.enseignementsup-recherche.gouv.fr/cid136723/le-soutien-a-l-edition-scientifique.html) ou sur le site [Ouvrirlascience.fr](https://www.ouvrirlascience.fr/le-soutien-a-ledition-scientifique-le-comite-de-suivi-de-ledition-scientifique/).

Ainsi, en intégrant six revues de LSH en tant que partenaires de recherche^[Six revues auxquelles s'est ajoutée la revue _Sens public_, déjà engagée dans un travail de recherche avec la CRC-EN.], de part et d'autre de l'Atlantique (France et Québec), le projet se donnait les moyens d'une relation durable, établie sur trois années^[Un projet de type «\ Partenariat\ » d'une durée de sept ans a depuis été déposé auprès du CRSH pour poursuivre les objectifs du projet _Revue 2.0_ tout en élargissant le partenariat actuel et le panel de revues.], nous engageant sur un suivi au plus près des besoins de chacune des revues. C'est l'installation de cette relation de confiance, en tant que coordinateur scientifique du projet, qui m'a permis d'obtenir un accès privilégié à la fabrique éditoriale et aux positions de ses acteurs et actrices, et d'en faire un terrain particulièrement stimulant.

La méthodologie d'observation déjà décrite ci-dessus s'est avérée fonctionner sur le mode de la dialectique entre ce que j'appellerai les _inscriptions_ et les _conversations_.

Les _inscriptions_ désignent tous les éléments scripturaux produits par la revue, soit avant son engagement dans le projet (les archives), soit pendant le projet (à travers le questionnaire). Que ce soit les documents génériques, ou les réponses au questionnaire, ces inscriptions revêtent un aspect déclaratif, dont l'adresse diffère -- les premiers s'adressant aux auteurs, directeurs de dossier ou évaluateurs, tandis que les seconds s'adressent aux chercheurs du projet, mais dont l'effet recherché est, consciemment ou non, d'_instituer_ le fonctionnement de la revue. Par exemple, rendre public (*publier*) son protocole éditorial dans un document de référence revient à donner à la revue toutes les marques de la normalisation institutionnelle (format, consignes éditoriales, modalités d'évaluation). Le questionnaire a mis les éditeurs dans une certaine position réflexive par rapport à leur fonctionnement interne, mais nous verrons que les réponses se sont également attachées à préciser, mais surtout à confirmer un modèle éditorial déclaré par ailleurs. Ces inscriptions ont de fait endossé une valeur de légitimation nécessaire à la revue.

En regard de ces _inscriptions_ instituantes et légitimantes, les _conversations_, c'est-à-dire les entretiens menés avec éditeurs et éditrices, ont jeté une nouvelle lumière sur la fabrique éditoriale. Le dialogue établi avec les éditeurs s'est basé en premier lieu sur les inscriptions, mettant en conversation les éléments déclarés (inscrits) et des éléments énoncés (exprimés en entretien).
C'est par cette dialectique que j'ai pu formuler une première conclusion selon laquelle le modèle éditorial idéal, identifié et modélisé sur la base des inscriptions, relevait davantage de l'archétype idéalisé, inconciliable avec la réalité pratique des rédactions. Je développerai ce point ultérieurement dans la partie _[Pluralité de modèles]_.

Mais ces _conversations_ suggèrent aussi un glissement de mon objet d'étude, de la revue en tant qu'artefact éditorial à celles et à ceux qui la font. Elles ont en effet remis en avant la dimension humaine, pragmatique et sans doute performative de l'article savant en tant que prise de position subjective, travaillée et validée en tant que telle lors du processus éditorial. La prise en considération de cette dimension humaine a permis de rouvrir ma problématique sur la dimension collective de la fabrique éditoriale, dimension qui se révèlera centrale dans la suite de ma réflexion.

Les entretiens ont enfin instauré une écoute attentive des acteurs de l'édition scientifique, recentrant nos intérêts de chercheurs sur les problématiques d'éditeurs. Croiser de cette manière les attentes respectives des uns et des autres a permis de rouvrir la revue à ses potentialités, et peut-être de dépasser la modélisation du processus éditorial initialement recherchée pour investir sa virtualisation. Cette approche a sans doute ouvert une piste pour résoudre la tension informationnelle dans laquelle se trouvent nécessairement pris les contenus numériques. Entre diversification et standardisation des formes, le numérique et ses technologies de l'information entretiennent en effet une indétermination paralysante pour des acteurs isolés.
Du modèle au virtuel, l'approche conversationnelle et expérimentale du projet a manifestement élargi l'horizon des possibles.


## Inscriptions

### Archives

[L]{.lettrine}es documents demandés aux revues ont été de deux types\ :

  1. les documents publics ou génériques régissant l'activité éditoriale de la revue, depuis les *Consignes aux auteurs*, le *Gabarit* (ou modèle *Word*), les consignes aux évaluateurs ou encore le *formulaire d'évaluation*, etc.
  2. des échantillons de *données* aux différentes étapes de l'activité éditoriale, d'un appel à communication jusqu'à la version *BAT* d'un article, en passant par les différentes versions et états d'un article, ou ses évaluations. Dans ces échantillons de données, nous avons également recueilli les échanges de courriel entre l'équipe éditoriale, le directeur de dossier, les auteurs, ou les évaluateurs.

Cette dernière archive revêt une valeur particulière au regard de la sensibilité des auteurs et des éditeurs à révéler la fabrique d'un texte et la nature des communications qui l'entourent.
En effet, un de ses objectifs aura été de reconstruire la génétique d'un article, établie à partir de l'ensemble des inscriptions effectuées sur et autour du texte de l'auteur. *Sur* le texte, avec par exemple les suggestions de modifications du secrétaire de rédaction, les réécritures de l'auteur ou encore les commentaires d'un évaluateur en marge du texte. *Autour* du texte, avec notamment les évaluations délivrées sur un formulaire d'évaluation, les échanges de courriels, ou encore les procès-verbaux du comité éditorial discutant de l'article.

Les premières inscriptions\ -- documents génériques\ -- participent de la légitimation institutionnelle de la revue. Celle-ci y organise un discours par lequel elle affiche\ -- c'est-à-dire rend public ou officialise --\ son protocole éditorial et son énonciation éditoriale. Elle échafaude déjà ainsi l'autorité par laquelle passeront les textes d'auteurs. Bien entendu, les secondes inscriptions remettent en perspective cet effort d'institutionnalisation déployé par la revue, puisqu'elles viennent documenter la pratique réelle du processus éditorial.

:::{.focus #listedocuments}

**Documents publics/génériques :**

- Document explicatif sur la soumission d'un dossier ou d'un article
- Protocole éditorial ou Consignes aux auteur·e·s
- Gabarit modèle Word (et autre·s) pour les auteur·e·s
- Protocole d'évaluation
- Formulaire d'évaluation (vide)
- Documents utilisés pour les rapports avec l'éditeur·rice et les diffuseurs\ : par exemple, consignes de l'éditeur·rice, protocole éditorial, protocole BAT, protocole mise en page, document du ou des diffuseurs.
- Contrats d'auteur·e·s, cession de droits
- Tout autre document qui vous semble pertinent à partager.

**Échantillons aux différentes étapes :**

- Exemple·s de dossier.s proposé.s qui a/ont été accepté·s par la suite (à comparer avec le suivant)
- Exemple·s d'appel·s à communication
- Exemple·s de document·s à différentes étapes de la chaîne (si possible, précisez dans le sommaire quels sont les formats, outils utilisés, comment l'interaction se passe) :
  - article qui vient d'être soumis
  - article commenté
  - article révisé à différentes phases
  - BAT (Bon à tirer)
  - si possible, formulaires d'évaluation des évaluateur·rice·s et résumés par la revue si différents lors du retour à l'auteur·e
  - échanges sur OJS – si cela est possible, nous donner un accès pour un article donné, sinon des captures d'écran des différentes étapes et échanges.
- Toutes traces de communication qui accompagne l’édition d’un article depuis sa soumission à sa publication\ : échanges mails, formulaire de plateforme, etc.
- Fiches d'évaluation dans ses différentes occurrences selon les étapes d’évaluations.

**Suivi d'article**

Dans la mesure du possible, un suivi complet sur un article, y compris courriels et autres formes de communication, pourrait constituer un cas d’étude particulièrement riche, que l’article ait été jugé « problématique » ou non.

:::

[Liste des documents demandés aux revues]{.legendeFocus}


### Questionnaire

Pour cette observation, le questionnaire avait pour objectif d'obtenir des données précises sur le protocole de chaque revue et d'en détailler le fonctionnement. Ces données permettaient de modéliser chronologiquement la chaîne éditoriale des revues, étape par étape. Les répondant·e·s étaient également amené·e·s à désigner les étapes qu'ils·elles considéraient comme fondamentales.

La structure du questionnaire suit la chronologie d'une chaîne éditoriale, selon un modèle idéal se déroulant séquentiellement en 4 étapes\ : Appel à communication, Évaluation, Production, Diffusion. Chacune de ces étapes se décline en sous-étapes.

:::{.focus #sousetapes}

- A. Appel à communication
  -  A1. Conception d'un dossier
  -  A2. Appel à communications
  -  A3. Réception des abstracts
  -  A4. Évaluations des abstracts
  -  A5. Retour à l'auteur
  -  A6. Écriture du texte
- B. Évaluation
  -  B7. Soumission 1^ère^ version de l'article
  -  B8. Évaluation interne
  -  B9. Évaluation externe
  -  B10. Préparation des évaluations pour retour à l'auteur
  -  B11. Retour à l'auteur
  -  B12. Réécriture de l'article
  -  B13. Soumission 2^ème^ version
  -  B14. Relecture par le comité
  -  B15. Relecture par les évaluateurs
  -  B16. Révision linguistique
  -  B17. Retour à l'auteur
- C. Production
  -  C18. Structuration
  -  C19. Mise en forme
  -  C20. Soumission épreuve
  -  C21. BAT auteur
  -  C22. BAT
  -  C23. Publication
- D. Diffusion
  -  D24. Diffusion numérique

:::

[Liste des étapes soumises au questionnaire]{.legendeFocus}

Pour chacune des étapes, le participant était invité à indiquer qui était concerné par l'étape, quels étaient les outils d'édition et les outils de communication utilisés, et à laisser un commentaire.

![Questions posées pour une étape](../media/questionparetape.png){width=50%}

Une fois les 24 étapes renseignées, le questionnaire proposait de sélectionner les 5 étapes déterminantes, et de justifier de la sélection\ :

> Toutes ces étapes sont fondamentales dans le cadre de la publication savante. Cependant, chaque revue, selon ses valeurs, ses spécificités et sa mission a tendance à donner davantage d'importance à certaines d'entre elles.
>
> Au regard des valeurs et spécificités de votre revue, choisissez les 5 d'entre elles qui vous semblent les plus importantes.

Cette étape de l'observation relevait encore du déclaratif, c'est-à-dire de ce que les revues déclarent comme modèle éditorial, au plus près de leur protocole officiel. Le questionnaire a donc été utile sur le plan théorique à la modélisation des protocoles de chacune des revues.
Le découpage très fin en étapes et sous-étapes qui structure le questionnaire a notamment permis de facilement comparer les modélisations et d'identifier les singularités des revues.

Par ailleurs, la comparaison des deux _inscriptions_ que sont les réponses au questionnaire d'un côté et les archives récoltées de l'autre, a permis de relever des points de tension entre l'image que se font les éditeurs de leur travail, et la réalité de leur pratique. Alors que le questionnaire était orienté sur la production des artefacts éditoriaux, cette nouvelle matière inférée par la comparaison des inscriptions est venue clairement interroger la production de l'autorité, en mettant à jour les modalités de la décision. Sur un plan pratique, le questionnaire a donc été utile pour réaliser cette première analyse, sur la base de laquelle j'ai pu préparer les entretiens avec les éditeurs.

## Conversation/Entretiens

[D]{.lettrine}es documents récoltés et des réponses aux questionnaires ont émergé plusieurs points à clarifier, ainsi qu'une série de thématiques à discuter en entretien. Ces points et thématiques ont permis de construire un carnet d'entretien, légèrement adapté pour chaque revue en fonction des réponses au questionnaire.

Les entretiens ont cherché à questionner plus particulièrement les trois thématiques suivantes, en lien avec les enjeux du projet\ : 1) la légitimation, 2) la matérialité de la revue et 3) les valeurs ou la vision auxquelles les participants étaient attachés. La question de la légitimation (1) se place au cœur des enjeux du projet et de son cadre théorique. Partant du présupposé d'un profond renouvellement des dynamiques d'autorité dans l'environnement numérique, il semblait nécessaire d'engager une discussion avec les éditeurs de revue pour comprendre, à travers leurs propos, comment l'autorité se construit, où elle se situe et quels en sont les acteurs. Il s'agissait de déceler où et comment se prennent les décisions, autrement dit ce qui, dans un processus éditorial faisant intervenir des pratiques, des outils et des acteurs humains, participe à la légitimation du texte, d'un auteur, ou plus généralement de la revue elle-même. Il apparait dans les entretiens que cette thématique de la légitimation et de l'autorité reste un sujet central pour les éditeurs de revue, ce qui vient confirmer leur attachement à leur fonction éditoriale d'autoriser les contenus et les connaissances qu'ils publient. Le second aspect concerne la matérialité de la revue (2), et plus précisément le rapport des éditeurs au numérique. Sur ce sujet, les questions pouvaient porter sur la transition d'une édition papier à une édition numérique, transition que plusieurs revues ont été obligées d'entreprendre et qui n'a pas toujours été très bien vécue\ ; sur les usages de certains outils ou plateformes utilisées\ ; sur les rapports au diffuseur, engagé dans l'élaboration de la forme finale de l'article. En filigrane, il s'agissait de comprendre ce qu'avait changé le numérique dans les pratiques, si la scientificité ou la qualité s'en étaient trouvées améliorées, ou encore de questionner les participants sur les effets supposés d'une édition numérique sur le processus de légitimation. Le dernier aspect, concernant les valeurs portées par l'éditeur et sa vision de l'édition (3), avait pour but de saisir à la fois les réflexions et les orientations envisagées par les revues, mais aussi de comprendre ce que veut dire «\ publier\ » pour les praticiens de l'édition scientifique, les significations qu'ils ont adopté et le sens qu'ils trouvent à leur activité.

<!-- [peut-être mettre ça comme premier résultat général]{.note} Les entretiens se sont avérés par ailleurs particulièrement enrichissant pour les participants eux-mêmes, tant le dialogue engagé entre le·s chercheur·s, le·s directeur·s et le·s secrétaire·s de rédaction a acquis une dimension auto-réfléxive sur leurs pratiques éditoriales, révélant une réalité de l'édition plus éloignée d'un modèle idéal qu'il en paraissait. Les éditeurs ont pu parfois adopter une position critique sur leurs protocoles. -->


# Résultats d'observation\ : le paradoxe des revues savantes

<!--ANCIEN TITRE\ : Résultats de l'observation -->
::: {#tableobs}

| revue             | documents | questionnaire | date entretien | durée entretien |
|:------------------|:---------:|:-------------:|:--------------:|:---------------:|
| Itinéraires       |    oui    |      oui      |   04/04/2019   |     122 min     |
| Mémoires du livre |    oui    |      oui      |   24/04/2019   |     110 min     |
| Intermédialités   |    oui    |      oui      |   02/05/2019   |     120 min     |
| Études françaises |    oui    |      oui      |   07/05/2019   |     102 min     |
| Photolittérature  |    oui    |      oui      |    à venir     |        -        |
| Sens public       |     -     |      oui      |       -        |        -        |
| Cybergéo          |    non    |      non      |       -        |        -        |

Table: Synthèse de l'observation

:::

&nbsp;

## Les inscriptions du modèle idéal

[L]{.lettrine}es _inscriptions_ que nous avons pu récolter dans le cadre du projet reflètent les valeurs que les revues souhaitent afficher. J'ai déjà introduit la valeur de légitimation et d'institutionnalisation des documents génériques adressés aux auteur·e·s et indirectement à toute la communauté scientifique, y compris les instances institutionnelles de financement ou de validation scientifique. Dans les gabarits _Word_ ou _Open Document_, les revues formalisent par exemple les marques éditoriales qui participent de la scientificité des contenus. Dans les consignes aux auteurs, le protocole d'évaluation est détaillé selon les normes scientifiques de la discipline, ou du moins selon les normes reconnues comme telles par les institutions scientifiques. De ce point de vue, ces inscriptions confèrent à la fabrique éditoriale un certain déterminisme sur la forme d'écriture, et donc sur le modèle de pensée. C'est le propre du modèle éditorial que de normaliser les pratiques pour établir une structure commune de communication.

En se basant sur un modèle idéal de chaîne et de protocole éditorial, le questionnaire a eu pour effet de renforcer cette formalisation, présentant le processus éditorial dans une forme discrétisée, identifiable et analysable. Les réponses obtenues ont permis de reconstruire l'image mentale sur laquelle s'appuient les éditeurs pour penser leur processus éditorial et communiquer sur leur revue\ -- et sa scientificité. Mais cette image reste de l'ordre du modèle, dont les contours parfois grossiers ne reflètent pas nécessairement la réalité. Elle n'en demeure pas moins essentielle au fonctionnement de la revue, puisqu'elle permet d'un côté à l'équipe de partager une organisation, et de l'autre de justifier de la scientificité de la revue, en répondant aux injonctions des financeurs et des instances de la discipline.

Dans le même temps, ce déterminisme se révèle intenable, et le modèle, on le verra, se disloque plus ou moins lorsqu'on le confronte au terrain de la fabrique éditoriale. Le modèle est donc un canevas dans lequel évoluent l'écriture et l'édition, mais que la pensée\ -- collective\ -- n'hésite pas à déborder lorsque la nécessité le demande, c'est-à-dire lorsque des subjectivités s'expriment. Ce n'est pas tant que le format de l'article ou de la revue ne permet pas d'accueillir toutes les idées, mais que le protocole doit parfois être enfreint pour que ces idées soient autorisées\ -- c'est-à-dire légitimées\ -- par la revue.

### Questionnaire

Les réponses au questionnaire ont été utiles pour produire des modélisations du processus éditorial mettant à jour les interactions entre les différents acteurs, les documents et les transformations d'état (versions d'articles) ou de statut (anonymisé, commenté, évalué, etc.),

L'ensemble des réponses au questionnaire a été compilé dans le tableau des réponses (aperçu ci-dessous et en annexe le tableur [Questionnaire observation du protocole editorial des revues (ODS)](../annexes/revue20/questionnaire_observation_du_protocole_editorial_des_revues.ods)) qui facilite l'analyse comparative des protocoles éditoriaux des revues. Cette première analyse a ainsi consisté à identifier les spécificités de chaque revue en matière de protocole ou de chaîne éditoriale.

![Aperçu du tableau des réponses au questionnaire](../media/tableau-des-reponses-au-questionnaire.png){width=50%}

#### Les étapes déterminantes selon les revues

Parmi les questions posées, la dernière[^cinqetapes] demandait aux répondants de sélectionner 5 étapes déterminantes pour le travail éditorial parmi les 24, et de justifier leur choix. À travers cette question semi-ouverte, l'idée poursuivie était d'identifier ce qui, en tant qu'éditeur, était porteur de valeurs dans leur protocole éditorial. On peut tirer plusieurs leçons des explications données par les répondants^[Voir toutes les réponses [dans les encadrés ci-dessous](#tab-cinqetapes).].

[^cinqetapes]: [«\ Toutes ces étapes sont fondamentales dans le cadre de la publication savante. Cependant, chaque revue, selon ses valeurs, ses spécificités et sa mission a tendance à donner davantage d'importance à certaines d'entre elles. Au regard des valeurs et spécificités de votre revue, choisissez les 5 d'entre elles qui vous semblent les plus importantes.\ »]{.cite}

En premier lieu, l'étape [A1] de conception du dossier revient pour 4 des 6 revues, identifiée comme «\ cruciale\ » (*Mémoires du livre*), car elle détermine «\ la dimension scientifique de la revue\ » (*Itinéraires*), ou encore «\ les orientations éditoriales\ » (*Sens Public*). Cette étape est celle d'une réflexion macroscopique sur le positionnement de la revue dans son champ disciplinaire, portant une attention particulière sur «\ l'émergence de sujets ou d'approches scientifiques\ » (*Itinéraires*).
Pour la *Revue internationale de Photolittérature* cependant, l'étape de rédaction de l'appel à communication [A2] est primordiale dans la mesure où le texte va «\ orienter la rédaction des abstracts, laquelle souvent se présente comme une première mouture des articles, et par conséquent détermine les suites\ ». Du point de vue de l'éditeur ici, davantage que le positionnement de la revue, c'est l'adresse aux auteurs qui prime, premier pas d'une relation encore à établir.

Sans exception, toutes les revues érigent le processus d'évaluation [A4, B8, B9, B10] comme une étape essentielle. Cependant les éditeurs ne vont pas attacher la même qualité à ses différentes manifestations dans le processus. *Itinéraires* perçoit d'abord dans l'évaluation un *travail*, en cela qu'elle reste «\ ce qui demande le plus de temps, d'énergie, d'attention, de vigilance\ ». Pour *Mémoires du livre*, les évaluations sont garantes de qualité («\ garde-fous de la qualité de la revue\ »), tandis que *Sens Public* valorise davantage l'évaluation interne [B8] plus «\ constructive\ » que l'externe [B9]. Ce dernier point est révélateur d'une certaine conception de l'évaluation qui sera discutée [ci-après](#résultats-généraux). Ainsi, l'évaluation, liée traditionnellement à une certaine scientificité, revêt différentes significations aux yeux des praticiens. En adoptant leur point de vue, il n'est pas étonnant de remarquer que l'étape [B10 Préparation des évaluations pour retour à l’auteur] a été sélectionnée comme déterminante pour 3 des 6 revues, témoignant d'un attachement particulier à la relation privilégiée que les éditeurs entretiennent avec les auteurs. Pour *Mémoires du livre*, cette étape joue «\ un rôle considérable pour la teneur et la tenue des articles\ », idée que l'on retrouve dans les propos de la *Revue internationale de Photolittérature*, pour qui «\ il faut faire preuve à la fois de finesse et de fermeté pour éviter soit de froisser un auteur, soit de rester trop vague quant aux attentes réelles envers lui\ ». On entrevoit ici un certain savoir-faire qui relève autant du discernement critique pour «\ améliorer [le] texte\ » que de la diplomatie. C'est là que se joue selon *Sens Public* «\ la capacité de l'éditeur de produire des bons contenus\ ». Ce retour à l'auteur est en fait au cœur de la fonction éditoriale, dont l'une des tâches consiste effectivement à accompagner l'auteur dans la réécriture du texte, vers son amélioration ou  son adaptation pour un destinataire.

Parmi les spécificités de chaque revue, on peut noter pour la revue *Itinéraires* le choix de l'étape [B14 Relecture par le comité] qui en donne la justification suivante\ : «\ pour vérifier que les évaluations ont bien été prises en compte et pour aussi relire l’intégralité en interne des numéros publiés - expertise générale\ ». Il y a là l'idée d'une contre-expertise vis-à-vis des évaluations et de ses effets, mais aussi d'un regard exercé bénéficiant de l'ancienneté de la revue. De ce point de vue, l'activité de l'éditeur ne s'arrête pas à l'édition et la publication d'un article. Il a la charge également de cultiver la revue et de lui donner, par l'accumulation réfléchie de textes, une certaine forme intellectuelle et scientifique. La _forme_ semble d'ailleurs constituer une constante chez les différents éditeurs. On la retrouve dans l'étape de révision [B16], de structuration [C18] et de mise en forme [C19], ou encore la soumission des épreuves [C20]. Ainsi, la révision assure «\ l'uniformité typographique des articles\ » et en «\ améliore la lisibilité\ » pour _Mémoires du livre_. L'éditeur s'arrête ici sur la forme du texte lui-même (typographie), dont la constance est gage de qualité. La mise en forme est valorisée notamment par la _Revue internationale de photolittérature_ dont l'usage de l'image nécessite une réflexion particulière sur l'arrimage entre le fond et la forme.

> La mise en forme est évidemment essentielle, surtout pour une revue nécessitant l’intégration d’images qui ne doivent ni être de simples illustrations ni masquer l’argumentation développée dans le texte.^[Extrait de la réponse au questionnaire pour la _Revue internationale de littérature_.]

![Capture d'un article de Gyongyi Pal dans la Revue Internationale de photolittérature[^gyongyipal]](../media/capture-article-phlitt.png){width=50%}

[^gyongyipal]: Gyongyi Pal . «\ La pantomime sous le prisme de la photographie – les œuvres photolittéraires de Károly Gink\ », *Revue internationale de Photolittérature* n°2 [En ligne], mis en ligne le 15 décembre 2018, consulté le 18 novembre 2019. URL\ : http://phlit.org/press/?articlerevue=la-pantomime-sous-le-prisme-de-la-photographie-les-oeuvres-photolitteraires-de-karoly-gink

L'éditeur de _Mémoires du livre_ parle encore de «\ forme aboutie et consentie\ » pour l'étape [C20 Soumission des épreuves], évoquant encore cette relation éditeur-auteur. La forme ici est celle de l'«\ image du texte\ », proposée par Souchier, et dont la validation (le consentement) par l'auteur révèle un contrat tacite définissant la relation entre l'éditeur et son auteur.

La revue _Intermédialités_ pour sa part met l'accent dans son commentaire sur l'évaluation, la validation scientifique, et la communication («\ soutenue\ ») avec l'auteur. Cette réponse laisse entrevoir la valeur de scientificité que se sont donnée les éditrices, engagées dans une rigueur certaine quant aux règles que la revue s'impose en matière d'évaluation et de validité scientifique. Cela nous sera confirmé par l'entretien mené avec l'équipe éditoriale.

Les encadrés suivants recompilent les réponses originales des revues à la question des «\ étapes déterminantes\ ».

:::{.focus .fulltable #tab-cinqetapes}
**Les 5 étapes déterminantes de la revue _Itinéraires. Littérature, textes, cultures_**

- A1. Conception d'un dossier
- B9. Évaluation externe
- B14. Relecture par le comité
- B17. Retour à l'auteur
- D24. Diffusion numérique

**Explications fournies**

> 1/ Conception du dossier\ : dimension scientifique de la revue, ligne éditoriale, contenus, émergence de sujets / approches scientifiques (dans la conception du dossier, l'appel à communications est évidemment un point d'attention corrélé fondamental) 2/ les évaluations internes / externes\ : c'est ce qui demande le plus de temps, d'énergie, d'attention, de vigilance 3/ Relecture par le comité et la direction\ : pour vérifier que les évaluations ont bien été prises en compte et pour aussi relire l'intégralité en interne des numéros publiés - expertise générale. 4/ Retour à l'auteur\ : implique la révision linguistique, le travail éditorial, mais au-delà, des questions plus larges sur la structure de l'article, la formulation et les références - c'est une ultime expertise effectuée sur la forme et le contenu par le secrétaire de rédaction 5/ La visibilité, mais aussi faire en sorte que les articles soient lus / cités, que les sujets / thèmes / approches contribuent à la discussion scientifique disciplinaire et interdisciplinaire.
:::

:::{.focus .fulltable}
**Les étapes déterminantes de la revue Mémoires du livre / Studies in Book Culture**

- A1. Conception d'un dossier
- A4. Évaluations des abstracts
- B8. Évaluation interne
- B9. Évaluation externe
- B10. Préparation des évaluations pour retour à l'auteur
- B11. Retour à l'auteur
- B16. Révision linguistique
- C20. Soumission épreuve

**Explications fournies**

> D'abord soumise à l'approbation du comité éditorial, la conception des dossiers, qu'elle soit assurée par un membre dudit comité ou par un-e responsable attitré-e, s'avère cruciale à la bonne marche de chaque numéro. Entre les diverses propositions suscitées par l'appel lancé, il importe ensuite de faire un choix avisé, d'une part pour garantir l'unité thématique d'un numéro, d'autre part pour limiter les risques d'alourdissement et de complication liés au processus de production. Les évaluations, internes et externes, sont en quelque sorte les garde-fous de la qualité de la revue. À ce chapitre, la préparation des rapports et le retour à l'auteur-e jouent un rôle considérable dans la teneur et la tenue des articles. À cela s'ajoute l'étape de la révision, dont la fonction est d'assurer l'uniformité typographique des articles, mais aussi d'en améliorer la lisibilité. Enfin, les échanges entre les auteur-es et la secrétaire de rédaction, depuis la soumission de l'article jusqu'à la validation des épreuves, permettent aux articles de parvenir au terme du processus dans une forme aboutie et consentie.
:::

:::{.focus .fulltable}
**Les 5 étapes déterminantes de la revue Études françaises**

- A1. Conception d'un dossier
- B8. Évaluation interne
- B9. Évaluation externe
- C23. Publication
- D24. Diffusion numérique

**Explications fournies**

> En plus des 5 étapes fondamentales, nous attachons la plus grande importance à la rétroaction avec l'auteur et les responsables de dossier durant l'édition des articles (révision linguistique et stylistique par l'équipe de direction) de même qu'à la relecture des 1^e^ et 2^e^ épreuves.
:::

:::{.focus .fulltable}
**Les 5 étapes déterminantes de la revue Revue internationale de Photolittérature**

- A2. Appel à communications
- A4. Évaluations des abstracts
- B7. Soumission 1^ere^ version de l'article
- B10. Préparation des évaluations pour retour à l'auteur
- C19. Mise en forme

**Explications fournies**

> La rédaction de l'appel à communication me semble plus importante encore que la conception d'un dossier ou d'un thème de numéro, car elle va orienter la rédaction des abstracts, laquelle souvent se présente comme une première mouture des articles, et par conséquent détermine les suites. La préparation des évaluations pour retour à l'auteur est également une phase déterminante, car il faut faire preuve à la fois de finesse et de fermeté pour éviter soit de froisser un auteur, soit de rester trop vague quant aux attentes réelles envers lui. Enfin la mise en forme est évidemment essentielle, surtout pour une revue nécessitant l'intégration d'images qui ne doivent ni être de simples illustrations ni masquer l'argumentation développée dans le texte.
:::

:::{.focus .fulltable}
**Les 5 étapes déterminantes de la revue Sens public**

- A1. Conception d'un dossier
- B8. Évaluation interne
- B10. Préparation des évaluations pour retour à l'auteur
- B11. Retour à l'auteur
- C18. Structuration

**Explications fournies**

> 1 conception du dossier doit représenter les orientations éditoriales. Les dossiers sont finalement les objets éditoriaux qui donnent le plus le sens de ce qu'est et que veut la revue. Un dossier bien conçu simplifie la chaîne (bons directeurs de dossiers, bons auteurs sollicités, bonne thématique, calendrier viable). 2. l'évaluation interne est tjs constructive - pas l'externe. En fait si on avait bcp de temps il faudrait avoir sulement une éval interne bien faite. 3. expliquer à l'auteur comment améliorer son texte est fondamental. c'est là que se joue la capacité de l'éditeur de produire des bons contenus. 4. du coup ici l'auteur prend - ou pas - en compte les suggestions de l'éditeur. 5. Inutile de diffuser si c'est pas bien structuré.
:::

:::{.focus .fulltable #last5etapes}
**Les 5 étapes déterminantes de la revue Intermédialités**

- B8. Évaluation interne
- B9. Évaluation externe
- B11. Retour à l'auteur
- B14. Relecture par le comité
- B17. Retour à l'auteur

**Explications fournies**

> Les étapes fondamentales sont toutes liées à l'évaluation des articles (1^ere^ soumission et 2^eme^ soumission) et à la validation scientifique (vérification de toutes les références du texte et recherche de plagiat et d'autoplagiat), impliquant toujours une communication soutenue avec l'auteur.
:::


#### Modélisation des protocoles

Le découpage en 24 étapes d'un protocole éditorial idéal a servi de canevas que chaque revue est venue adapter et détailler selon le modèle éditorial que les éditeurs et éditrices se faisaient de leur travail en termes de communication et de production. Sans poser directement la question de la décision ou des décisionnaires, les participants étaient invités à désigner les acteurs (humains) impliqués dans telle ou telle étape. Ce simple renseignement de présence en dit long sur le processus de décision, par exemple en révélant l'absence d'un acteur à une étape où on l'attendait. C'est ce qu'ont mis à jour les modélisations schématiques des protocoles ainsi décrits sur la base des réponses au questionnaire. Ces modélisations représentent les acteurs humains, les différents documents et leurs états, ainsi que les actions qui relient les uns aux autres.

Une première représentation permet de visualiser le réseau des individus impliqués dans le processus. Se sont dégagés assez naturellement quatre groupes distincts\ : l'auteur (en bleu), le directeur de dossier (en vert), l'évaluateur (ici en pourpre), et l'équipe de la revue (ici en couleur prune).

![Réseau des acteurs de la revue _Itinéraires_ ([live source](http://renkan.iri-research.org/renkan/p/4b655d31-40f5-11e9-a258-366562363433?cowebkey=6bcc210c3773c455e953a2603cacf850bd40d6ba695a77fa44a21603427bf347#?view=1023.5,512.5,1))](https://codimd.s3.shivering-isles.com:443/demo/uploads/upload_7831b5f6dca24d5e1d952a79562a73f6.png)

D'une revue à l'autre, les interrelations entre chacun des groupes n'interviennent pas de la même façon. Les communications sont plus ou moins formalisées, plus ou moins compartimentées. Les mêmes rôles peuvent être distribués différemment. Pendant un temps à la revue _Intermédialités_, une fois sa proposition de dossier acceptée, le directeur de dossier était pratiquement exclu du processus^[Ce principe a été récemment revu pour une plus grande inclusion du directeur de dossier dans les décisions.], là où pour la revue _Études françaises_, le directeur de dossier joue littéralement le rôle d'intermédiaire entre l'équipe de la revue et les auteurs. Ces différences révèlent en creux une tension entre ce qui relève de l'intérieur et de l'extérieur de la revue. <!-- Partant des réponses aux questionnaires et à la lumière des entretiens, nous pouvons répartir chaque groupe d'acteurs sur un axe _Interne &harr; Externe_. Nous discuterons de cette tension _interne-externe_ dans l'analyse générale^[voir [Interne et externe](#interneexterne)].-->

<!-- [Ajouter ici les représentations de chaque revue sur un axe, soit [Interne <-> Externe], soit [Externe <-> Interne <-> Externe].]{.note}-->

Les modélisations suivantes détaillent pour la revue _Itinéraires_ les trois premières phases du processus telles que présentées dans le questionnaire\ : Appel à communication, Évaluation et Production.

![Phase A - Appel à communication ([live source](http://renkan.iri-research.org/renkan/p/pub/4b655d31-40f5-11e9-a258-366562363433?cowebkey=57d8ff7f6e44dc6e99952514e134beec01dfc659f9845177687b6715679b01dd))](../media/revue20-phaseA.png)

![Phase B - Évaluation ([live source](http://renkan.iri-research.org/renkan/p/pub/1623c5c8-40fc-11e9-a258-366562363433?cowebkey=18045f646c9353f769587cf7c96dbeff3067866035f09f24ba18be1d9d7772b8))](../media/revue20-phaseB.png)

![Phase C - Production ([live source](http://renkan.iri-research.org/renkan/p/pub/ca8e9b27-41da-11e9-a258-366562363433?cowebkey=5983c44c592609cae3924bb10afcbbd29648db7a4c50f0129230fb3a89e15426))](../media/revue20-phaseC.png)

Le premier schéma montre le rôle prépondérant du directeur de dossier lors de la première phase, qui successivement propose un dossier, conçoit le dossier (en collaboration avec le comité éditorial de la revue), réceptionne les propositions d'article, décide de leur acceptation ou non pour le dossier, répond aux auteurs, réceptionne la première version des articles, et enfin transmet les articles reçus au comité de la revue pour évaluation. Au contraire, dans les deux étapes suivantes d'évaluation et de production, le directeur de dossier se retire et reste absent du processus, la revue prenant en charge tous les échanges avec les auteurs, les évaluateurs, ainsi que les décisions éditoriales et scientifiques sur la teneur des articles.


### Récolte de documents {#tableaurecolte}

:::fulltable


| Documents       | Études françaises | Intermédialités | Itinéraires | Mémoires du livre | Photo-littérature |
|:---------------|:----:|:----:|:----:|:----:|:----:|
| _Documents publics/génériques_ |  |  |  |  |  |
| Document explicatif sur la soumission d'un dossier ou d'un article | oui (dossier et article) |  | oui (dossier et article) | oui |  |
| Protocole éditorial ou Consignes aux auteur·e·s | oui | oui (les 2) | oui | oui | oui |
| Gabarit modèle Word (et autre·s) pour les auteur·e·s | oui |  |  |  |  |
| Protocole d'évaluation | oui |  | oui | oui | oui |
| Formulaire d'évaluation (vide) | oui (dossier et article) | oui (article et recherche-création) | oui | oui | oui |
| Documents utilisés pour les rapports avec l'éditeur·rice et les diffuseurs |  |  |  |  |  |
| Contrats d'auteur·e·s, cession de droits |  | oui |  | oui |  |
| Tout autre document qui vous semble pertinent à partager |  |  | Bilan statistique de fréquentation du site 2008-2018 |  | Infos JE/colloque ayant servi à constituer le dossier |
| _Échantillons aux différentes étapes_ |  |  |  |  |  |
| Exemple·s de dossier.s proposé.s qui a/ont été accepté·s par la suite (à comparer avec le suivant) |  |  | oui | oui | oui |
| Exemple·s d'appel·s à communication |  | oui | Oui (3) | oui (plusieurs étapes) | oui |
| Exemple·s de document·s à différentes étapes de la chaîne\ : |  |  |  |  |  |
| -- article qui vient d'être soumis | oui |  | oui | oui | oui (5) |
| -- article commenté | oui |  | oui | oui | oui (5) |
| -- article révisé à différentes phases | oui |  | oui | oui |  |
| -- BAT (Bon à tirer) | oui |  | oui | oui |  |
| -- si possible, formulaires d'évaluation des évaluateur·rice·s et résumés par la revue si différents lors du retour à l'auteur·e | oui |  | oui | oui | oui (5) |
| -- échanges sur OJS – si cela est possible, nous donner un accès pour un article donné, sinon des captures d'écran des différentes étapes et échanges. |  |  |  |  |  |
| Toutes traces de communication qui accompagne l’édition d’un article depuis sa soumission à sa publication\ : échanges mails, formulaire de plateforme, etc. | oui (emails) |  | oui (emails) | oui (emails) | oui |
| Fiches d'évaluation dans ses différentes occurrences selon les étapes d’évaluations. |  |  |  |  | oui |
| Autre·s |  |  | Corrections en bloc des métadonnées d'un dossier |  |  |
| Suivi d'article | oui | non | oui | oui | oui |

Table: Documents récoltés par revue

![Tableau des documents récoltés par revue](../media/revue20-tableaurecolte.png){#imgTableaurecolte}

:::

#### Compte-rendu du cheminement d'un article

Cette archive, lorsqu'elle était disponible, a surtout été exploitée pour confronter les réponses aux questionnaires, et pour vérifier par exemple certaines assomptions que nous pouvions en faire. En mettant en lumière la tension entre la réalité de leurs pratiques et le modèle idéalisé que les éditeurs se forgent à propos de leur protocole éditorial, elles ont également servi de support pour préparer les entretiens et élaborer de questions spécifiques au fonctionnement de la revue.

Juliette de Mayer, chercheuse participante au projet, a par ailleurs proposé un récit intitulé «\ Comment naît un article\ ?\ », relatant le cheminement d'un article tout au long du processus éditorial sur la base de l'ensemble des documents relatifs à l'article (courriels, fiches d'évaluations, commentaires en marge, versions successives de l'article, etc.).

:::{.focus #commentjdm}

**Comment naît un article\ ?**

_par Juliette De Mayer_

La naissance de l'article prend presque neuf mois (267 jours, entre le 14 mars 2018 et le 6 décembre 2018). L'article circule sous la forme de onze documents différents entre l'auteure (Mélodie), la co-directrice (Marie-Pier), l'assistante de direction (Cécile) et [?] (Joanie). Echangés par courriel, les documents s'accompagnent d'une correspondance constituée de quatorze messages écrits par les personnes mentionnées ci-dessus.

L'auteure envoie l'article le 14 mars, et reçoit les retours des évaluateurs le 28 juin (106 jours plus tard). L'auteure reçoit alors les deux formulaires remplis par les évaluateurs anonymes, ainsi qu'une version annotée du texte. Dans le courriel qui accompagne ces documents, [Joanie] précise que les évaluations sont "très positives," et résume les deux changements demandés par les évaluateurs (retravailler l'introduction pour préciser la problématique ; faire la démonstration de l'originalité de l'étude).

Environ un mois plus tard (c'est-à-dire dans les délais attendus), l'auteure envoie une version révisée de l'article (le 4 août). Le document _Word_ rend visibles les changements grâce au suivi de modification. Il semblerait que l'article n'est pas renvoyé aux évaluateurs, c'est le comité de direction qui juge de l'adéquation des révisions.

Le 27 août, l'assistante de direction (Cécile) renvoie à l'auteure un document avec des révisions linguistiques et stylistiques. Cécile qualifie son style "d'interventionniste". Le document révèle un total de 468 interventions\ : 152 insertions, 149 suppressions, 64 modifications dans la mise en forme, et 103 commentaires.

Deux jours plus tard, l'auteure renvoie une nouvelle version du document\ : elle a accepté la plupart des suggestions de l'assistante de direction et a répondu aux questions contenues dans les commentaires. Le document révisé contient la trace de 432 changements. On voit encore la trace de certaines des interventions de l'assistante de direction, mais pas toutes\ : dans son courriel, l'auteure explique avoir _accepté_ certaines modifications (c'est-à-dire les avoir insérés dans le texte, les faisant disparaitre du suivi de modification) et supprimé les commentaires afin de «\ nettoyer un peu le fichier\ ». Trois jours plus tard (30 août), Cécile répond qu'il lui faudra un peu de temps pour «\ nettoyer le fichier\ » et produire les épreuves en PDF. C'est fait huit jours plus tard (7 septembre), l'assistante de direction demande à l'auteur de vérifier les épreuves, en lui précisant qu'il lui reste du temps pour effectuer cette tâche puisque «\ nous sommes encore loin du dépôt à _Érudit_\ ». La semaine suivante (14 septembre), l'auteure renvoie une version du PDF avec quelques annotations qui concernent principalement la mise en page des images et des légendes, ainsi que quelques précisions dans les notes de bas de page. L'assistante de direction envoie une autre version du pdf, avec les changements requis, que l'auteure approuve.

Le parcours de l'article se termine en décembre (donc près de trois mois après l'approbation des épreuves), quand l'assistante de direction annonce à l'auteure que «\ les fichiers sont zippés\ », et la confirmation que le dossier a été déposé chez _Érudit_. Pour ce qui concerne l'auteure, c'est donc le _zippage_ des fichiers et l'envoi à la plateforme de diffusion qui marque la fin du processus.
:::

<!-- [Je pourrais tenter ici un autre compte-rendu récit intégrant les échanges courriels, une discussion précise sur une phrase à retravailler avec les différentes versions successives, les commentaires en marge, etc. Qu'est ce que cela apporterait par rapport aux questions posées\ ? Peut-être mettre en évidence la conversation scientifique. Eventuellement à exploiter pour comparer comment la conversation se déroule dans un processus d'évaluation fermée en double aveugle, le rôle de la revue dans cette conversation, _vs_ dans un processus d'évaluation ouverte/collégiale (dossier Commun)]{.note} -->


## Paroles d'éditeurs\ : négociations avec la réalité éditoriale


<!-- Titre: réalité pratique, négociation, virtualité de la revue, pluralité des modèles

reformuler en annoncant déjà les résultats\ : souligner le paradoxe avec la notion de pluralité, brouhaha.

formalisation se joue sur les métadonnées (catégories, ) (peu intrusive), mais aussi sur les formats et leur structuration (très déterministe). Le html devrait permettre qlq chose de plus ouvert.
 -->


[L]{.lettrine}es entretiens ont permis d'édifier des espaces de parole inédits pour les participants qui ont tous exprimé leur satisfaction d'avoir pu raconter à un tiers leur métier et leurs pratiques de l'édition. Mais au-delà d'une expression gratifiante, ce temps d'échange a aussi été l'opportunité pour ces équipes de prendre un peu de recul sur leurs pratiques éditoriales. Pour leur avoir offert un moment et une posture réflexifs sur des pratiques souvent appliquées spontanément, la plupart des participants ont qualifié l'entretien d'utile et d'important.
Depuis la perspective des objectifs du projet _Revue 2.0_, on peut considérer que cette pause imposée et bénéfique aura été un véritable _temps de la recherche_ pour ces praticien·ne·s cumulant plusieurs fonctions ou activités au sein de l'université, qu'ils·elles soient chercheur·e·s, professeur·e·s, chargé·e·s de cours, directeur·rice·s de revue, secrétaires de rédaction, etc.

De ce point de vue, cette étape de l'observation des revues marque un tournant dans le projet, et probablement pour ma thèse. En me positionnant à l'écoute des éditrices et éditeurs de revue et en engageant avec elles·eux une _conversation_, j'ai très certainement opéré un glissement de mon objet d'étude depuis l'artefact éditorial, son format et son dispositif, pour me recentrer sur les individus qui les produisent. Bien entendu, ils·elles font partie intégrante du dispositif éditorial. Ce ne sont pas de simples opérateurs extérieurs au dispositif, dans la mesure où les éditeurs et éditrices, tout comme les auteur·e·s ou les évaluateurs et évaluatrices, co-existent dans l'agencement dispositif, aux côtés des protocoles, des outils, des normes institutionnelles, des textes et des documents et de tous les écrits qui participent de la fabrique éditoriale. Ils sont des _acteurs_ à part entière du dispositif, souvent pour l'avoir conçu, pour l'affiner, l'adapter, soit sans le subir, c'est-à-dire en en maîtrisant certains paramètres. Or cette composante humaine dessine déjà la dimension collective que j'investiguerai dans le chapitre final de la thèse. Pour le moment, nous retrouverons ce collectif en filigrane dans les notions de comité, de collégialité, ou encore de communauté scientifique, notions chères aux éditeurs et éditrices qui se sont entretenu·e·s avec moi.

De leur posture réflexive ont rejailli plusieurs éléments que les questionnaires ou les documents récoltés ne laissaient pas nécessairement entrevoir. Les entretiens ont ainsi fait ressortir des pratiques singulières, souvent non-réfléchies, venant parfois appuyer ou infirmer nos hypothèses initiales, mais aussi bousculer nos conceptions autant que celles des praticien·ne·s.

En premier lieu, derrière le cadre idéal du protocole éditorial classique tel que modélisé dans le questionnaire, il est apparu que chaque revue non seulement 1) disposait de son propre protocole, élaboré progressivement par stratification de plusieurs années de pratiques et évoluant au fil des renouvellements des équipes éditoriales, mais aussi qu'elle 2) négociait constamment avec ce protocole pour l'adapter et le plier à la réalité du terrain. Il ressort de ce premier constat une tension entre un modèle éditorial idéal et un modèle éditorial appliqué, que l'on pourra discuter ci-dessous.

Enfin, il est tout à fait frappant de constater à quel point la conversation transparait au sein des témoignages recueillis. Elle se révèle ainsi omniprésente dans la fabrique éditoriale. On la retrouve notamment au cœur de la décision éditoriale, c'est-à-dire de la construction de l'autorité. On la retrouve également dans l'édition des textes elle-même, qui s'appuie sur une intense communication entre l'auteur·e et le·la secrétaire de rédaction. On la retrouve enfin dans la forme dialectique que cultivent les revues avec leur discipline ou leurs objets, comme c'est le cas de l'image pour la revue _Intermédialités_. On pourra se demander quelle place\ -- ou espace\ -- occupent ces conversations dans la production des connaissances\ ? Quel rapport conscient ou inconscient entretiennent les éditeurs et les éditrices avec la conversation\ ? Y a-t-il un _espace_ conversationnel à développer au sein des revues, et peut-il accéder à un statut éditorial qui lui rendrait tout sa pertinence\ ?

Je développe ci-dessous cinq thématiques tenant lieu de _résultats_. À partir de la parole des praticien·ne·s, j'ai tissé une analyse qui d'un côté établit une grille d'observation pour saisir l'état de l'édition périodique, et qui d'un autre côté pose des axes de réflexion pour penser la remédiation de la revue scientifique.


:::focus

**Entretien avec l'équipe d'_Itinéraires_ (04/04/2019)**

- Magali Nachtergael (MN), en tant que co-directrice
- Christèle Couleau (CC), en tant que co-directrice
- François-Xavier Mas (FXM), en tant que secrétaire de rédaction

**Entretien avec l'équipe de _Mémoires du Livre_ (24/04/2019)**

- Marie-Pier Luneau (MPL), en tant que directrice sortante
- Anthony Glinoer (AG), en tant que nouveau directeur
- Cécile Delbecchi (CD), en tant que secrétaire de rédaction sortante
- Joanie Grenier (JG), en tant que secrétaire de rédaction
- Juliette De Mayer (JDM), en tant que chercheuse sur le projet _R2.0_

**Entretien avec l'équipe d'_Intermédialités_ (02/05/2019)**

- Marion Froger (MF), en tant que directrice
- Maude Trottier (MT), en tant que secrétaire de rédaction

**Entretien avec l'équipe d'_Études françaises_ (07/05/2019)**

- Elisabeth Nardout-Lafarge (ENL), en tant que directrice sortante
- Jean-Benoît Cormier Landry (JBCL), en tant que secrétaire de rédaction sortant

Je suis pour ma part intervenu dans chaque entretien en tant que chercheur-enquêteur, ci-dessous avec mes initiales NS.

:::

[Synthèse des entretiens et des intervenants]{.legendeFocus}

### Pluralité de modèles

Malgré les similarités d'outils, de dispositifs ou de protocoles, on se rend compte à l'écoute des praticien·ne·s de l'édition scientifique qu'il est illusoire de parler d'un modèle unique d'édition pour les sciences humaines. Tout au contraire, les disparités d'un protocole éditorial à l'autre témoignent de singularités très fortes.

#### Objets éditoriaux

Cependant, un certain nombre de constantes demeurent, laissant penser qu'il est possible de modéliser le processus éditorial des revues en sciences humaines. Sur le plan des objets éditoriaux tout d'abord, on constate que toutes les revues publient des numéros de manière périodique, chaque numéro proposant un dossier thématique, lui-même constitué d'une série de textes. Ces textes peuvent être soit des articles scientifiques, auxquels sont attachés des critères de production et d'évaluation bien spécifiques, soit des notes de lecture (ou comptes rendus, ou encore «\ exercices de lecture\ » chez _Études françaises_) présentant de manière critique ou factuelle un ouvrage, un (ou plusieurs) article·s ou un dossier de revue. Selon le champ disciplinaire des revues, on trouve également des textes de création [^creation], des entretiens, des chroniques, des _documents_[^tracts].

Outre les dossiers, les numéros laissent parfois la place à des _varia_, c'est-à-dire à des textes _variés_ dont le sujet n'est pas directement lié au dossier thématique du numéro, mais qui présentent un intérêt pour la revue et son lectorat. Sans nécessairement développer cette question, il n'est pas inintéressant de noter au passage l'embarras des éditeurs face à ces textes qui d'un côté ne trouvent pas tout à fait leur place dans le modèle éditorial, et qui d'un autre sont jugés suffisamment pertinents pour être publiés. Or la périodicité propre à l'imprimé impose une publication des varia soit en fin de dossier thématique, soit dans un numéro dédié aux varia qui ne permet aucune mise en valeur, ni des textes ni de la revue elle-même. Dans un modèle éditorial numérique, comme c'est le cas pour la revue _Sens public_, la revue est libre d'adopter une périodicité continue et de publier ainsi des textes _au fil de l'eau_. C'est une tout autre légitimité qui peut alors être offerte au texte qui, plutôt que d'être déclassé dans un espace informe\ -- soit non-éditorial, peut bénéficier au contraire d'une visibilité particulière, sans concurrencer pour autant les dossiers.

Au-delà de ces formes éditoriales partagées, on trouve chez certaines revues des formats plus singuliers. Par exemple, dans la revue _Études françaises_, le numéro\ 53, dont le dossier est consacré à Gilles Marcotte, présente une rubrique «\ Envois\ » constituée de huit textes courts, sur le ton du témoignage et de l'hommage. Le terme «\ envoi\ » est effectivement un synonyme du terme «\ dédicace\ ». Le TLFI en donne également la définition suivante\ :

> -- P. anal. Fait de faire parvenir, d'adresser à quelqu'un un signe ou la manifestation d'un sentiment.

Cette _adresse_, posthume dans le cas du dossier _Études françaises_, suggère cependant une forme à explorer dans la perspective d'objets éditoriaux conversationnels.

La revue _Intermédialités_ présente de son côté dans son numéro 33 «\ restituer (le temps)\ » deux rubriques consacrées à un artiste (Basinsky) dont les travaux adressent spécifiquement la thématique du numéro\ : une première rubrique «\ Dossier — Essais on William Basinski\ », constituée de deux articles, et une rubrique «\ Artiste invité\ » dont l'objet est une interview de l'artiste, mais qui se décompose en une courte introduction, l'entretien lui-même, et un podcast, chacun de ces éléments faisant l'objet d'une publication séparée. Récurrente dans chaque numéro, cette dernière rubrique constitue une marque de fabrique pour la revue. _Intermédialités_ semble ainsi avoir conservé une grande liberté quand aux rubriques utilisées. On trouve encore les rubriques «\ Notes de labo\ »^[_Intermédialités_, [numéro 30-31](https://www.erudit.org/fr/revues/im/2017-n30-31-im03868/).]
ou «\ Contrepoints\ », cette dernière se déclinant en «\ Contrepoints\ --\ archives\ »^[_Intermédialités_, [numéro 30-31](https://www.erudit.org/fr/revues/im/2017-n30-31-im03868/).] ou «\ Contrepoints - Supplément web radio / Podcast \ »^[_Intermédialités_, [numéro 26](https://www.erudit.org/fr/revues/im/2015-n26-im02640/).], ou tout autre artefact médiatique ayant fait l'objet d'un texte critique.

Ces deux exemples nous montrent que les revues ne se limitent pas aux formats communément adoptés. Elles mettent en place d'un numéro à l'autre des dispositifs éditoriaux spécifiques pour contextualiser de manière originale un contenu. On peut également citer l'entretien avec Peter Brook intitulé "Presence and Creation" sur la revue _Sens Public_^[Entretien réalisé par Pedro Pérez-Guillón (voir sur [sens-public.org/articles/1370](http://sens-public.org/articles/1370)).] dont l'auteur Pedro Pérez-Guillón avait souhaité créer un intertexte bien particulier entre l'entretien lui-même et une série de citations et de références illustratives ou suggestives. Dans ce cas précis, c'est le souhait d'un auteur qui a amené l'éditeur à adapter le format initial pour en respecter l'intention.

![Mise en forme particulière pour un article sur la revue _Sens Public_](../media/mise-en-forme-particuliere-pour-un-article-sur-la-revue-sens-public.png){width=70%}

Il est intéressant de noter que dans ce cas précis, l'éditeur _Sens Public_, parce qu'il maîtrise l'ensemble de la chaîne de production, est effectivement en mesure de répondre à ce type de demande.
Il n'en est pas toujours ainsi. L'entretien avec la revue _Intermédialités_ révèle notamment une certaine frustration vis-à-vis des contraintes imposées par le diffuseur _Érudit_.

> -- [22:07] MF\ : On a perdu aussi le lien d'une certaine manière avec le monde des arts, parce qu'on avait un dossier d'artistes qui était dans nos pages papier, et là c'est beaucoup plus compliqué sur _Érudit_, sur le PDF, parce qu'on ne peut pas garantir les qualités d'impression en marge de diffusion.
>
> -- [22:28] MT\ : Par exemple là on en a eu un et c'est très compliqué, l'artiste fournit un PDF très lourd, c'est du matériel visuel, photographique et sonore, et j'ai été obligée de fournir un PDF moins lourd, j'ai demandé la permission à l'artiste qui ne m'a jamais réécrit, donc je me suis permise quand même de le faire.
>
> -- [22:49] NS\ : Donc de réduire les images\ ?
>
> -- [22:50] MT\ : Oui mais encore c'était pas encore assez léger pour _Érudit_, donc on ne peut pas vraiment développer les dossiers d'artistes.
>
> -- [22:58] MF\ : Oui, on est très coincées avec ça, le plus c'est qu'on a du sonore par rapport au papier, mais je trouve qu'il y a du coup des contraintes techniques qui interfèrent lourdement sur les idées à la création.
>
> -- [23:31] MT\ : Et c'est là qu'on pourrait développer l'intermédialité, via internet. Mais pour _Érudit_ c'est compliqué de faire ça, et ils ont un protocole d'édition très fixe et très peu plastique, à chaque fois c'est un peu compliqué. Quand on fait le dépôt d'un numéro par exemple, on ajoute une rubrique, mais moi je ne savais pas comment leur dire, donc je leur ai demandé si je pouvais leur envoyer un formulaire, une fiche avec nos recommandations et ces choses-là, mais ils nous disent pas grand-chose. Ils nous disent de déposer les numéros dans leur état final, mais la discussion se fait ensuite, et on a un peu l'impression qu'on les dérange en leur demandant certaines choses, alors que ce serait tout à fait intéressant pour nous de développer plastiquement nos publications sur internet.
>
> -- [24:35] MF\ : Oui. Moi je trouve que, concernant les dossiers d'art c'est un problème. Ce sont des contraintes qui rentrent dans le corps même des artistes. Alors pour les revues scientifiques et les textes c'est un peu moins contraignant, mais encore là, je me rappelle de textes ou la question des images, de leurs formats, de la manière dont on les ordonne etc., devenaient des problèmes majeurs pour _Érudit_ et qu'il fallait se battre pour conserver l'idée d'un montage par exemple, et non pas une succession de photos, donc c'est très lourd à gérer parce que c'est des choses qu'ils n'ont pas prévu, puisqu'ils fonctionnent avec des revues généralement uniquement textuelles.

Dans ce cas précis, les contraintes s'appliquent autant à l'objet éditorial (le type de rubrique qui demande au diffuseur une attention particulière), qu'à la multimodalité des contenus qui devrait pourtant être simplifiée dans l'environnement numérique. Cette question renvoie finalement à la tension entre une nécessaire formalisation des données en vue de leur traitement automatique, y compris leur mise en forme graphique à l'écran, et la souplesse requise par la diversité éditoriale des revues au sein d'un même diffuseur. _Érudit_ a fait le choix dès sa création d'un formalisme strict concrétisé dans son schéma XML encore utilisé aujourd'hui malgré une certaine obsolescence. Pourtant, le problème ne vient pas tant du schéma, très complet par ailleurs, mais de son exploitation. La chaîne de traitement des articles encodés dans le schéma XML _Érudit_ ne permet pas de mettre à profit toute l'expressivité du schéma prévue par ses concepteurs. Par exemple, si le schéma prévoit bien l'encodage sémantique des métadonnées constitutives des références bibliographiques, dans la pratique, les références ne sont encodées par les équipes _Érudit_ qu'avec un balisage de type graphique (dans l'exemple suivant, deux styles graphiques, lettre capitale et italique, sont appliqués.

- Liste des éléments enfants de la balise `<biblioref>` telle que prévue par le schéma _Érudit_^[Voir la documentation de l'élément [_biblioref_](http://retro.erudit.org/xsd/article/3.0.0/en/doc/schemas/eruditarticle_xsd/elements/biblioref.html).]\ :


  ```
  author, bookmark, collno, colltitle, duration, editionno, highlight, idpublic, inlineequation, issueno, mediaobject, nbpage, nbvol, no, pagination, publisher, publocation, quotation, reference, serialno, simplelink, subscript, superscript, title, unicodechar, volume, year
  ```

- Exemple d'encodage d'une référence tel que pratiqué par les équipes d'_Érudit_\ :

  ```xml
  <refbiblio id="rb3">
    B<marquage typemarq="petitecap">aker</marquage>, Sue (2013):
    Representing Inclusivity and Diversity in Early Years.
    <marquage typemarq="italique">In</marquage>: Vanessa
    H<marquage typemarq="petitecap">arbour</marquage> and Andrew
    M<marquage typemarq="petitecap">elrose</marquage>, eds.
    <marquage typemarq="italique">Write4Children</marquage> 4(2):6-14.
  </refbiblio>

  ```

Par ailleurs, les feuilles de style transformant l'article XML en contenu HTML affiché à l'écran et offert à la lecture sur le portail _Érudit_ ne permettent pas de répondre à toutes les demandes particulières des revues en matière de mise en forme et d'agencement des contenus.

[^creation]: La revue _Intermédialités_ intègre régulièrement des textes de créations dans sa rubrique _Recherche-Création / Research Creation_.

[^tracts]: Voir par exemple [la réédition critique des tracts, affiches et bulletins du _Comité d'action d'étudiants-écrivains_](https://www.erudit.org/fr/revues/etudfr/2018-v54-n1-etudfr03363/1042871ar/) qui accompagne le dossier «\  Écritures de la contestation. La littérature des années 68 \ » dirigé par Jean-François Hamel et Julien Lefort-Favreau pour le numéro 54 de la revue _Études françaises_.



#### Acteurs humains

Sur le plan des acteurs humains et de la répartition des rôles, on retrouve pour chaque revue la structure classique composée d'une direction ou co-direction, d'un comité éditorial, d'un comité scientifique, d'un·e ou plusieurs secrétaire·s de rédaction. De la même manière, toutes les revues mobilisent des évaluateurs internes ou externes, des directeurs ou de responsables de dossier, des réviseur·se·s, traducteur·rice·s\ ; toutes les revues procèdent à des évaluations, vérifient les références, relisent, éditent, structurent et valident les textes. Ces tâches, invariablement constitutives du travail éditorial des revues, tendent vers le même objectif de produire et publier le meilleur article et la meilleure édition possible.

Mais ce modèle apparemment unique cache en fait de grandes disparités.
Les revues se distinguent les unes des autres par l'autorité qu'elles confèrent aux différents acteurs, ainsi que par la valeur qu'elles attribuent à chacun des aspects de la production. Les différences sont manifestes\ : selon les revues, la rigueur, l'autorité ou la scientificité ne se jouent pas au même endroit.

Les directrices de la revue _Itinéraires_ s'en amusent d'ailleurs, en comparant leurs pratiques et celles d'une autre revue.

> -- [1:13:42] MN\ : J'ai été amenée a présenter la revue _Itinéraires_ devant l'école doctorale, dans un séminaire de doctorants. Il y avait un collègue qui est lui dans le comité de rédaction de la revue d'histoire et littérature française. [...] On avait l'impression de pas parler des mêmes processus scientifiques en fait. Eux ils étaient tout papier avec un comité qui se réunissait un peu comme des moines pour lire collectivement les articles ensemble [rires]. Il y a avait un coté très anachronique entre les deux processus. On ne vivait pas dans la même communauté scientifique, on ne partageait pas les mêmes références.

Outre des territoires conceptuels manifestement éloignés, ce qui étonne ici l'éditrice, c'est la lecture collective d'un texte, faite en comité. Cette remarque révèle à quel point les pratiques éditoriales se distinguent, jusque dans leurs fondements épistémologiques. Il se trouve que la revue _Études françaises_ est très attachée à cette pratique de lecture collective. Chaque numéro de la revue fait ainsi l'objet d'une journée complète de travail réunissant tout le comité éditorial. Tous les articles ont été lus en amont du comité, puis sont minutieusement discutés un à un, jusque dans leur syntaxe.

> -- [1:14:41] ENL\ : Ce travail investi est une des valeurs à laquelle je tiens. Depuis le texte que quelqu'un nous envoie jusqu'à ce que nous allons en faire, je nous revoie dans le bureau relire tout haut une phrase en se demandant quel est le complément d'objet direct, où tout à coup en la lisant tout haut se dire «\ ça ne va pas, tu ne peux pas dire ça comme ça\ » et chercher si on lui réécrit pour lui dire qu'il faudrait une autre formulation ou est-ce qu'on n’est pas capables, nous tous seuls, de lui soumettre déjà une mini révision qui empêcherait l'aberration qu'on a entendue tout à l'heure. Ce travail-là d'édition et d'éditorialisation me tient à cœur.

_Études françaises_ produit à l'issue de cette journée de travail un procès-verbal complet de plusieurs pages à destination du directeur de dossier. La directrice de la revue insiste\ :

> -- [54:57] ENL\ : Les réunions ont une grande valeur scientifique. Ça fait partie des moments de la carrière où j'ai le plus de plaisir à travailler, le plaisir intellectuel est majeur dans ces réunions. Parce qu'on lit ensemble, on se confronte, on discute, les désaccords sortent, comme il faut arriver à quelque chose au bout du compte on est obligés de les analyser, donc les présupposés derrière les désaccords apparaissent. Je pense qu'on fait du vrai travail intellectuel.

La revue _Études françaises_ érige la qualité du texte au plus haut point en mobilisant l'ensemble du comité sur la construction intellectuelle du texte.

> -- [1:15:23] JBCL\ : Et c'est aussi l'idée que la collégialité et du travail en commun dépassent le simple fait de la réunion en personne. C'est-à-dire que lorsqu'on investit à notre tour le texte et sa propre langue, en sachant que ce texte sera signé par quelqu'un d'autre que nous, mais on y travaille.
>
> -- [1:15:57] ENL\ : Il faut à la fois le rendre conforme à une certaine idée que l'on a de la qualité de la langue, de sa précision mais aussi il faut qu'on essaye de comprendre la logique d'écriture de ce texte. Et ça c'est assez fabuleux. Je pense que ça aura changé ma façon de lire des thèses, des mémoires, des travaux, et probablement ma façon d'écrire aussi. Il faut qu'on repère des trucs au-delà des agacements, il faut comprendre comment ce texte fonctionne et ça c'est passionnant. Mais c'est ténu.

De ce point de vue, la revue se voit en premier lieu comme une instance d'édition, et non comme une instance de légitimation.

> -- [19:28] ENL\ : Et c'est l'exemple type aussi d'une négociation pénible, ce n'est jamais très agréable de demander à une collègue de retravailler, mais qui a vraiment donné un bon dossier. On en est très content, ça a augmenté considérablement sa portée théorique. On voit bien que le comité a un rôle scientifique qui n'est pas simplement d'avaliser ou de reconnaître ou de ne pas reconnaître, c'est plus qu'une instance de légitimation, c'est une instance d'édition.

<!-- à garder au cas où, citation EF sur la structuration et son rapport à la construction intellectuelle\ :

  > JB: La structuration renvoie à l'aspect intellectuel de la chose, c'est-à-dire quelle est l'idée derrière et la structure de pensée du dossier, alors que la mise en forme relève plus de l'aspect technique. On demande toujours que soit respecté un certain protocole de rédaction, mise en forme de situation, de références etc., donc la partie cohérence interne du dossier, intellectuelle, est idéalement faite, tandis que la mise en forme est faite de manière inégale selon les coordonnateurs.

  > EN: C'est un rôle de contrôle d'une certaine façon. Ce n'est pas un rôle scientifique, comme l'a dit Jean-Benoit, la cohérence intellectuelle est assurée par le coordonnateur, et quand on reçoit le dossier on part du principe que cela a été fait. Donc nous la mise en forme, c'est une espèce de contrôle général de la conformité des textes qu'on reçoit avec le protocole de la revue.

  > JB: S'assurer aussi que les références soient faites de manière rigoureuse, pour une question de légitimité. C'est une forme de légitimation d'adapter le contenu à un contenant qui soit stable.

  > EN: C'est une étape de polissage mais aussi une étape de levée de problèmes qui n'avaient peut-être pas été perçus à l'évaluation. Ce sont des problèmes de présentation mais qui en cachent généralement d'autre, des problèmes de rigueur\ : ce n'est pas le bon numéro de revue, de page etc. Ça soulève d'autres questions.

  > JB: On parlait de légitimation tout à l'heure, l'état matériel dans lequel nous arrive le dossier constitué par les coordonnateurs en disent très long sur le travail intellectuel qui est fait aussi. C'est-à-dire que lorsque les articles arrivent avec différentes polices ou tailles de caractères, des systèmes de note différentes, on en arrive à penser que les textes n'ont pas fait l'objet d'une lecture par les coordonnateurs aussi fine et donc ça nous amène à penser que le texte de présentation n'aura pas une portée théorique ou heuristique aussi intéressante. Ça forme un tout, c'est comme un travail qu'on corrige, lorsque la langue est mauvaise souvent on a une argumentation derrière qui est fautive.

-->


On retrouve cet attachement à la révision pour la revue _Mémoires du livre_. Mais ce «\ trait\ » semble être d'abord attribué à une personne en particulier, la secrétaire de rédaction CD., qui s'auto-qualifie d'«\ interventionniste\ » dans l'un de ses courriels à une auteure\ :

> Chère [auteure], 
>
> Revoilà finalement ton article, avec mes commentaires et ceux de MP.
>
> Tu connais mon style "interventionniste" (surtout en termes de répétitions)..! Surtout, qu'il ne t'oblige en rien et que tu ne te sentes pas non plus tenue de justifier quoi que ce soit (tu peux d'ailleurs me renvoyer ton texte sans les marques de suivi). 
>
> Comme ce fut le cas pour ton autre article, c'est toujours un plaisir de te lire!
>
> CD. [secrétaire de rédaction]

Interrogé·e·s sur cet _interventionnisme_, la directrice sortante MPL. et le directeur entrant AG. précisent\ :

> -- [23:54] MPL : Quand on monte une revue à partir de rien on doit miser sur la qualité des gens qu’on embauche, et c’est un trait de CD. Ce n’est pas une politique éditoriale de la revue. Ceci dit, les échos sont positifs vis-à-vis de la correction. Ce qu’on a toujours voulu c’est publier les meilleurs textes possible, avec le moins de fautes. Oui CD intervient même dans la syntaxe, mais les auteurs ne se corrigent pas toujours et d’avoir une réviseuse qui se décrit comme interventionniste c’est un gage de qualité. C’est coûteux, mais ce qui compte c’est la qualité de la publication.
>
> -- [25:23] CD : Ça a été un style qui s’est développé avec le temps, mais je me suis toujours permis de m’insérer dans le texte. Ça n’a jamais été un impératif.
>
> -- [26:33] MPL : Je n’ai plus à passer après CD dans les textes qu’elle corrige. On a l’intention de confier la révision des textes à CD en sous-traitance, car il n’y a qu’elle qui soit parvenue à ce niveau-là.
>
> -- [27:04] NS : Est-ce que ça reste un enjeu pour la nouvelle équipe ?
>
> -- [27:07] AG : Un enjeu de fournir les meilleurs articles possible, oui. Je suis ravi que ça puisse se faire dans une poursuite en qualité de travail. Ça a des conséquences financières, effectivement, on met l’accent sur le travail de révision, et il faut noter la difficulté ajoutée quand les articles sont en anglais.

D'autres revues au contraire font reposer la valeur de leur travail sur d'autres aspects. _Intermédialités_ consacre une grande partie du travail de révision à la vérification des références utilisées par l'auteur. Toutes les références et toutes les citations sont ainsi passées au crible, vérifiées une première fois par les évaluateurs, puis «\ contre-vérifiées\ » par la secrétaire de rédaction. Cette dernière, chargée également d'identifier les plagiats, mesure ainsi la scientificité d'un article à la qualité de ses références. Dans ce cas, la revue devient une instance de contrôle et de vérification, dont la fonction est de garantir la qualité des références bibliographiques utilisées par l'auteur.

Tout éditeur scientifique porte évidemment une attention particulière à la qualité du texte et aux références bibliographiques. Mais les revues se distinguent par la valeur qu'elles attribuent à chacun de ces aspects, concrétisée notamment dans l'énergie et la rigueur qu'elles vont y consacrer. La scientificité n'est donc pas une notion exacte ni équivalente d'une instance éditoriale à l'autre. Elle relève davantage du jugement de valeur, fondé sur ce que les éditeurs considèrent essentiel dans l'élaboration et l'édition d'un contenu scientifique.

On le voit, les modèles éditoriaux divergent tant par les objets publiés autant que par le système de valeurs qui s'échafaude autour d'une revue. Cette pluralité de modèles témoigne également d'une pluralité épistémologique qui doit être prise en compte dans les solutions apportées à l'édition scientifique. Car le processus de légitimation n'opère pas de la même façon d'une revue à l'autre. Au risque de défaire un certain mythe de scientificité et d'objectivité, on peut parler d'une multiplicité de régime épistémologique.

### Légitimation

Cette diversité de modèles se confirme au regard des conclusions que l'on peut tirer des entretiens en matière d'évaluation et de légitimation. La légitimation des textes est l'une des fonctions attribuées aux éditeurs scientifiques. Elle s'appuie en théorie sur un processus rigoureux d'évaluation reposant sur le principe de relecture par des pairs.

Ce principe prend racine dès les prémices des périodiques savants au 17^ème^ siècle. Attaqué pour ses _extraits_ trop partiaux, et qualifié par ses détracteurs de «\ critique illégitime, de tribunal autoproclamé\ », le _Journal des Savants_ est contraint d'adopter dans ses premières années[^1827] un fonctionnement plus collégial «\ en constituant une société de rédacteurs\ » [@vittu_quest-ce_2001, p.136]. Ce fonctionnement instituera une certaine «\ neutralité de ton\ » face aux textes proposés et aux ouvrages dont le périodique souhaite rendre compte. Autre élément témoignant d'une recherche de légitimité par l'intermédiaire d'un tiers, les textes sont publiés en y adossant une figure faisant autorité dans la communauté des lettrés.

> Le plus souvent ce titre [d'article] désigne aussi un auteur, qui en général est accompagné d'une garantie savante, comme le nom d'un intermédiaire établi dans la République des Lettres («\ Extrait d'une lettre écrite (...) par M. l'Abbé Boisot à M. l'Abbé Nicise...\ »), ou l'indication d'une position lettrée («\ Extrait d'une lettre écrite (...) par M. Bohn Prodesseur en l'Université de Leipsich...\ »)\ ; soit le recours à des garants ou à des autorités que nous avons déjà noté pour les intermédiaires composant le «\ bureau informel\ ». [@vittu_quest-ce_2001, p.137-138]

[^1827]: Les premières années du _Journal des Savants_ sont mouvementées en ce qui concerne la gouvernance et l'établissement de ce qu'on appellerait aujourd'hui une ligne éditoriale. Cette histoire, très bien documentée par Jean-Pierre Vittu, témoigne en fait du processus d'institutionnalisation du _Journal_ lui-même, mais aussi de l'institutionnalisation de son modèle éditorial qui s'imposera comme «\ nouvel instrument de savoir\ » et comme forme centrale de la communication scientifique.

:::note
autre citation possible:

> Ainsi, après une réforme des académies, de la censure, et du système des privilèges, Bignon assura la direction du Journal de 1701 à 1714. Il en confia la rédaction à une «\ Compagnie de Gens de Lettres\ » composée de six membres, pensionnés par le libraire, que le directeur avait recrutés dans deux viviers différents. [@vittu_formation_2002, p.188]
:::

Le travail d'édition scientifique est entièrement consacré à cette construction d'autorité. Il s'agit bien de construction puisque la fonction éditoriale valide la valeur d'un propos à travers un _échafaudage_ de relations et de communications, autrement dit un _dispositif_. C'est la reconnaissance collective de ce dispositif qui permet à ce dernier de légitimer un propos aux yeux de ce même collectif. Notre question centrale, lors de cette phase d'observation du projet, résidait ainsi dans les mécanismes du dispositif éditorial, afin de saisir où, dans les pratiques éditoriales, se manifeste et se construit l'autorité. Qu'est-ce qui la produit\ ? Qu'est-ce qui participe à la légitimation d'un texte, d'un auteur\ ? Et de manière récursive, comment la revue elle-même en tant qu'instance de légitimation, maintient-elle sa propre légitimité\ ?

Or à nouveau, malgré les principes partagés de la lecture par les pairs, de l'évaluation ou de la sélection éditoriale, la légitimation n'est pas une science exacte ni équivalente d'une revue à l'autre. Tout au long du processus éditorial, la légitimité d'un texte se joue entre les mains d'une multiplicité d'acteurs décisionnaires. Selon les revues, les mêmes acteurs n'useront pas de leur fonction de décision aux mêmes étapes, leurs décisions ne seront pas de la même nature ou n'auront pas la même valeur de validation.

Le cas de la revue _Études françaises_ est intéressant à cet égard.
La revue établit les dossiers de ses numéros sur la base de propositions de dossier préalablement constitué par un groupe d'auteurs et un recueil de résumés (*abstracts*). Lorsque la proposition est soumise au comité de la revue, le dossier présente ainsi une problématique déjà accompagnée des réponses (embryonnaires) des auteur·e·s.
Il ne s'agit plus pour le·s directeur·rice·s de dossier de s'engager auprès de la revue sur l'exploration d'une voie théorique ou d'un champ de recherche, mais bien d'affirmer un positionnement.
Pour la plupart des revues en SH au contraire, la problématique à explorer se travaille en amont en collaboration avec le comité de la revue, et fait ensuite l'objet d'un appel à communication. La première décision éditoriale consiste alors à valider une problématique encore en friche, soumise _in fine_ à la communauté scientifique. Dans le cas d'_Études françaises_, lorsqu'il lui est soumis un dossier, le comité a déjà sous la main la problématique et les réponses qui lui sont apportées, ainsi que l'argumentation finale («\ la synthèse du dossier\ ») articulant le dossier dans une pensée collective. On comprend dans ces conditions que la nature de cette première décision change du tout au tout. Cette dernière porte en effet sur l'acceptation du dossier dans son ensemble, tel que présenté par le·s directeur·rice·s de dossier, et non sur un projet en gestation dont les frontières et la teneur sont encore mouvantes. La revue tient fortement à ce fonctionnement qui, d'après la directrice et le secrétaire de rédaction interrogés, contribue largement à son identité\ :

> -- [41:46] JBCL\ : J'ai un peu l'impression que la modalité de travail, de publier par dossier, d'envoyer un dossier complet, que le coordonnateur va lui-même constituer soit avec invitations ou appel préalable mais non-identifié à notre revue, c'est un travail qui est quand même assez différent d'autres revues où on fonctionne par appel, et où les coordonnateurs ont une implication beaucoup moins grande avec les textes individuels. Donc le réflexe est plus rapide de délaisser le travail au comité de rédaction de la revue. Simplement, sur la base d'une expérience personnelle, je peux dire que les fois où j'étais dans des expériences de soumettre un article à une revue qui fonctionne sur le modèle d'appel, je n'ai pas l'impression d'avoir été lu de manière fine et rigoureuse par les évaluateurs. Après lorsque ça fait l'objet d'une publication, souvent les textes de présentation sont très factuels, voici l'inventaire des textes sur la base d'une problématique de base, aucun effet de parenté entre les textes, quelle serait la ou les conclusions du dossier, il semble peu y en avoir. 
>
> -- [43:35] ENL\ : C'est intéressant ce que tu dis, parce que la discussion me permet de mieux comprendre la spécificité d'_Études françaises_ avec les dossiers. Je crois que contrairement à d'autres revues, on croit très fort au dossier. On pense notre revue en ces termes. Ce qui fait qu'on a un fonctionnement, peut-être pas monographique, mais vraiment par question théorique. Et on veut qu'il y ait une vraie question.
>
> -- [44:00] JBCL\ : On a plus l'air d'un ouvrage collectif finalement.
>
> -- [44:04] ENL\ : Oui c'est ça. Et donc, décidément le dossier est quelque chose qui nous conditionne beaucoup. Ça nous donne aussi beaucoup de travail. Vraiment je crois qu'on a cette idée que ce n'est pas un ensemble de textes sur un thème, c'est un dossier, donc il y a une problématique.
>
> -- [44:24] JBCL\ : Ça guide aussi la composition du comité de rédaction, où il y a le souci d'avoir des spécialistes de différentes disciplines, de manière à pouvoir toujours équilibrer la visée généraliste de la revue, et puis avoir des dossiers qui forment des touts.
>
> -- [44:47] ENL\ : Oui, parce que le comité de rédaction est composé en fonction de champs de spécialité mais aussi d'approches théoriques. Et on veut qu'elles soient différentes. On veut qu'une approche sémiotique, ou largement féministe, ou de _cultural studies_ puissent dialoguer et que le même dossier soit vu à travers ces deux filtres. Mais oui, c'est vrai que le dossier ça nous tient quand même.

Les éditeurs partagent une vision très enthousiaste de leur métier et de leur fonctionnement particulier. On retrouve ce même enthousiasme chez toutes les personnes interrogées, et il ressort des autres entretiens que les dossiers constituent dans tous les cas l'objet éditorial central pour le projet théorique des revues. Pour autant, ce fonctionnement propre à _Études françaises_ révèle bien que les modèles éditoriaux, ni même les objets éditoriaux, n'endossent pas la même valeur d'une revue à l'autre.

Nous en sommes à la toute première décision prise, celle des éditeurs ou du comité d'accepter ou non une proposition de dossier. Les éditeurs jouent ici de leur fonction de sélection, constitutive des fonctions éditoriales classiques, telles qu'on les retrouve dans le champ élargi de l'édition. Par cette décision, le processus de légitimation du dossier, de ses articles et de ses auteur·e·s démarre donc appuyé par l'autorité déjà acquise de la revue et de son comité.

> -- [32:04] ENL\ : On joue un rôle de légitimation du dossier, on fait exister des dossiers, on fonctionne encore beaucoup comme ça. Donc on joue un rôle de légitimation de problématiques. Ce faisant on légitime des auteurs, à commencer par les coordonnateurs qui ont imaginé et monté ce dossier, et puis des auteurs d'articles.
>
> -- [32:36] NS\ : Et dans ce processus quelles sont les étapes qui participent de cette légitimation\ ?
>
> -- [32:45] ENL\ : Déjà d'accueillir la proposition de dossier. On en refuse des propositions de dossier quand même, ce qui est une manière de pas valider une problématique ou une manière de l'aborder. Donc accepter le dossier et les coordonnateurs.trices qui le proposent. C'est la première étape. Pour l'envoyer en évaluation, on estime que le dossier est suffisamment valable pour qu'on l'envoie, à la limite on pourrait imaginer qu'à ce moment-là on dise que ça ne va pas.

L'instance de légitimation suivante est liée directement à l'évaluation. Les revues procèdent traditionnellement à une évaluation _interne_ et à une évaluation _externe_. Mais la légitimité accordée à chacune de ces évaluations n'est pas la même d'une revue à l'autre. Ces différences deviennent visibles lorsque l'on considère comment et par qui sont validées les évaluations, si les évaluations sont transmises aux auteurs ou non, si elles sont filtrées et éditées par le comité pendant le processus.

La revue _Intermédialités_ tient en très haute estime le principe de l'évaluation et a mis en place pour cela un protocole très strict. En premier lieu, le choix des évaluateurs doit respecter des règles draconiennes de non-conflit d'intérêts, et les évaluateurs doivent attendre cinq ans avant de pouvoir à nouveau évaluer pour la revue. Les secrétaires de rédaction ont donc la tâche manifestement laborieuse de trouver des évaluateurs potentiels pour un article en fonction de la thématique adressée puis de vérifier que l'auteur n'a pu croiser ces évaluateurs au cours de sa vie de chercheur (jury, colloques, projets de recherche, publications antérieures, etc.). MT. interrogée lors de l'entretien parle de ce travail avec enthousiasme, car il lui permet de «\ cartographier\ » un champ de recherche au niveau international et d'en saisir les acteurs. Mais la revue ne s'arrête pas en si bon chemin. En effet, le protocole d'évaluation en double aveugle est scrupuleusement respecté. L'anonymat des évaluateurs est préservé tout au long du processus, y compris au sein du comité éditorial. Par ailleurs, les évaluations externes sont généralement prises au pied de la lettre, et les rapports d'évaluation des experts sollicités sont communiqués tels que le comité les reçoit. Dans ces conditions, les avis des évaluateurs externes font office de décision, et acquièrent par conséquent une valeur légitimante importante au regard des pratiques éditoriales observées par ailleurs.

L'exemple de la revue _Intermédialités_ est intéressant, car les éditrices expriment une grande confiance dans le processus éditorial. Elles transfèrent à ce dernier une forme d'autorité décisionnaire.


> -- [02:03] NS : Est-ce que ça vous est arrivé de publier un article, à cause de contraintes par exemple, qu'à postériori vous n'auriez pas voulu publier\ ?
>
> -- [02:16] MF\ : En fait j'ai une position assez humble de ce point de vue là, puisque nous sommes une revue tellement interdisciplinaire, je pense que tout ce que nous avons publié, nous l'avons publié dans la mesure où ça respectait des qualités scientifiques, c'était reconnu par les pairs etc., mais je n'ai pas de position tranchée. Une fois que s’est passé par tout ce processus-là, je n'ai pas de regret, même si je pense que certains articles sont moins bons, que certains me plaisent moins, on n'a pas une ligne éditoriale à ce point-là définie pour se dire « Ça on regrette de l'avoir publié ».

C'est la rigueur avec laquelle les éditrices appliquent le protocole qui confère à la revue sa valeur légitimante et qui assure (aux yeux des éditrices) la validité d'un article. Nous verrons que ce transfert ou cette délégation d'autorité n'est pas toujours aussi clair.

Ainsi, sur d'autres revues, le comité va pouvoir contrebalancer, voire annuler les évaluations externes. La diversité des pratiques est ici éloquente quant à la pluralité des modalités épistémologiques de l'édition scientifique en sciences humaines.
Les conditions dans lesquelles se déroulent les discussions internes changent du tout au tout. Par exemple, le comité peut connaitre ou non l'identité des évaluateurs, il peut prendre en compte ou non des évaluations internes, réalisées de leur côté soit en simple aveugle (l'auteur est anonymisé), soit de manière ouverte (l'auteur n'est pas anonymisé). Ainsi, selon les pratiques d'une revue à l'autre, sera considérée comme décisive soit l'évaluation externe, soit l'évaluation interne, soit encore la discussion interne en comité.
La neutralisation ou la négociation d'une évaluation externe peut aussi faire intervenir l'opinion du directeur ou de la directrice de dossier, ce qui complexifie encore le processus de décision. En effet, bien qu'il soit externe à la revue, le directeur de dossier est parfois intégré dans le processus de décision, lui donnant la possibilité au regard des évaluations de défendre tel ou tel auteur, tel ou tel texte, réduisant d'autant la valeur légitimante de l'évaluation externe.

Finalement, l'autorité légitimante n'est pas de la même nature d'une revue à l'autre, malgré le principe partagé de l'évaluation. Il apparait en fait que l'autorité s'exprime toujours au détour d'un processus conversationnel, que ce soit une négociation, une argumentation ou une discussion collégiale. De ce point de vue, la conversation n'est pas un simple moyen, elle est au contraire structurante pour le processus de légitimation. Elle établit ainsi un espace particulier, au sein duquel peuvent s'élaborer les autorités.


### Constante négociation

Un résultat notable issu des entretiens vient éclairer encore la pluralité des modèles éditoriaux. Il apparait en effet que le processus éditorial place ses praticiens dans une négociation constante, d'une part entre acteurs décisionnaires, et d'autre part avec le protocole lui-même. À ce sujet, je tenterai d'éclaircir le paradoxe qui veut que l'édition scientifique en sciences humaines cherche à légitimer, sur des critères d'objectivité, des énoncés par définition subjectifs. Je montrerai notamment qu'il existe un écart entre ce que j'ai appelé les _inscriptions_, dont les éléments de scientificité relèvent souvent du discours institutionnel pour légitimer la revue, et la pratique réelle, nécessairement confrontée à des décisions relevant de l'interprétation.

L'analyse des documents génériques récoltés et le dépouillement des questionnaires ont permis de mieux caractériser le procotole éditorial de chacune des revues et de les distinguer. S'en est dégagé pour chaque revue un modèle idéal, proche de celui déclaré par exemple dans le document «\ protocole éditorial\ » à l'intention des auteurs et des évaluateurs, dans lequel la revue énonce le cheminement des textes soumis. Les revues s'appuient sur ce protocole déclaré pour garantir la scientificité de la revue et légitimer ainsi la revue en tant qu'instance légitimante. Cet aspect est très clair pour la revue _Intermédialités_ respectant strictement le protocole d'évaluation, ou encore la revue _Mémoires du Livre_ qui considère avoir construit sa légitimité sur le modèle des revues «\ papier\ ».

> -- [54:44] MPL\ : Quand on a fondé la revue en 2009, il y avait encore un préjugé très fort à l’encontre des revues électroniques. Ce n’était pas un processus aussi rigoureux qu’une revue scientifique papier. Pour se battre contre ça, notre stratégie a été d’appliquer une grande sévérité, beaucoup de rigueur, et c’est pour ça que les évaluations sont au cœur de notre processus. Je pense qu’elles le sont en réalité, parce qu’il y a deux moments d’évaluation, la sélection des propositions d’abord puis la sélection des articles par deux membres externes et un membre interne, la revue a été pensée pour avoir toute cette rigueur et placer l’évaluation au cœur de son processus.

Les entretiens sont pourtant éloquents. Les équipes éditoriales sont en prise avec une réalité complexe et discursive avec laquelle ils doivent nécessairement négocier. Si les questionnaires ont permis de localiser _où_ étaient prises les décisions, les entretiens ont révélé _comment_. Ainsi, les décisions pour sélectionner ou refuser, autrement dit pour *autoriser* un texte font l'objet d'une série de discussions susceptibles de tordre plus ou moins le protocole déclaré. De ce fait, chaque étape du protocole, tel qu'il est déclaré dans les documents génériques des revues ou modélisé à travers les questionnaires, subit dans la pratique des écarts (de protocole) sur leurs modalités.

La rigueur affirmée par les éditrices de _Mémoires du livre_ sur l'évaluation peut facilement se fissurer, par exemple sur l'anonymat des évaluateurs\ :

> -- [11:52] MPL: Ce serait assez artificiel de dire qu’on essaye de maintenir l’anonymat. Ils [les directeur·rice·s de dossier] sont au courant comme le directeur ou la directrice de la revue sait qui a évalué quoi. Ça nous permet aussi de mieux juger, car au fil des années nous comprenons mieux les standards de certains évaluateurs.

Dans cette déclaration, la directrice de la revue ne justifie pas pourquoi l'anonymat n'est pas respecté, mais elle précise le bénéfice que la revue en tire, à savoir de pouvoir moduler l'appréciation faite des rapports d'évaluation en fonction de l'évaluateur et de ses «\ standards\ ». L'objectivité sous-jacente au modèle idéal, censée garantir un niveau de scientificité, s'en trouve clairement relativisée au profit de la subjectivité des évaluations et de leur appréciation.

> -- [43:23] MPL: L’idée de moduler un peu la parole d’évangile des évaluateurs, est venue à force d’avoir des discussions au comité de rédaction. On se réunit une fois par année, parfois plus, et on fait la liste des problèmes rencontrés dans l’année. L’insatisfaction vis-à-vis des rapports revenait souvent, on voyait que c’était irritant. Les autres membres de comité ont d’autres pratiques dans d’autres revues, et on s’est rendu compte qu’il fallait nuancer ça.
>
> -- [44:30] AG : Je dirais à titre personnel que je soutiens beaucoup cette vision-là, qui consiste à arrondir les angles en cas de besoin pour maintenir la cordialité de la conversation, parce qu'elle va permettre d'améliorer tout le processus y compris les retards, la prise en compte des remarques faites dans les évaluations.

La négociation se joue ici dans l'édulcoration des rapports d'évaluation. C'est une pratique courante, dont les modalités sont différentes d'une revue à l'autre, mais qui à nouveau améliore le processus dans son ensemble, puisqu'elle réintroduit de l'humain (la «\ cordialité\ » notamment) dans un protocole aux contraintes trop rigides.
De la même manière, l'anonymat ne semble pas une valeur absolue dans les pratiques de la revue _Itinéraires_ ...

> -- [20:12] MN\ : Oui, les responsables du numéro ne connaissent pas les noms des experts, ils ne les connaitront jamais, c'est le secret de fabrication je pense des comités, et les auteurs non plus évidemment, et nous entre nous pendant le processus d'évaluation on ne communique pas non plus au reste du comité. Après ça arrive qu'on demande.. Les chargés du suivi des évaluations, eux les connaissent...
>
> -- [20:48] CC\ : Pour éviter de demander des expertises à des auteurs déjà... donc c'est obligé.
>
> -- [20:55] NS\ : Donc dans le comité seuls les chargés des évaluations sont au courant\ ? Je pensais que tout le comité était...
>
> -- [21:08] MN\ :  Ils [les membres du comité] ont potentiellement accès aux fichiers où il y a tous les noms. 
>
> -- [21:13] CC\ : Voilà, c'est pas un secret absolu, mais disons que on essaye de faire sans, [inaudible] tout le monde fonctionnant un peu comme ça.

Justifié cette fois pour des questions pratiques («\ éviter qu'un auteur soit sollicité pour évaluer\ »), il apparait que la règle est bien là pour être détournée. D'ailleurs, les éditeur·rice·s de la revue assument très consciemment la nécessité de la négociation et rejettent l'adoption d'un modèle idéal.

> -- [39:19] CC: Je ne suis pas sûre qu'on idéalise le modèle justement. La séparation absolue, on essaye de jouer le jeu autant que possible parce que ça nous semble une façon d'essayer de prendre de la distance et d'objectiver un peu les choses. Mais je crois pas qu’on en fasse non plus un modèle idéal. Il y a plein de cas dans lesquels au contraire, arriver à se parler ou à échanger des points de vue entre ces différentes parties va au contraire permettre d'arriver à un meilleur résultat. 

L'éditrice met effectivement en tension l'objectivité du protocole et la subjectivité des «\ points de vue\ ». Subjectivités qu'il convient d'ailleurs de «\ moduler\ », quitte à ne pas tenir compte d'une évaluation, c'est-à-dire à lui enlever toute valeur légitimante (c'est l'évaluateur qui se trouve alors délégitimé par la revue).

> -- [17:30] MN: Alors on a beaucoup discuté en comité du «\ bon expert\ » et du «\ mauvais expert\ ». Ce n'est pas parce qu'un expert est recruté comme expert par nous, que son travail sera toujours de qualité égale. Il y a des experts très constructifs et il y a des experts de mauvaise foi, on en a rencontré qui aussi ne supportent pas des positions des chercheurs, donc on comprend qu'ils sont un peu concurrents en fait, ce qu'on n'identifie pas tout de suite au moment où on l'a sollicité, et dans ce cas-là, quand on sent qu'il y a quelque chose de pas forcément très constructif dans la critique, on sollicite un troisième expert qui arbitre. On le fait pas systématiquement. 

Finalement, c'est cette non-systématicité qui nous permet de dire que le protocole est constamment négocié pour mieux s'adapter à la réalité des rapports humains. Toujours chez _Itinéraires_, les rapports d'évaluation sont filtrés de différentes manières. Certains sont simplement reformulés avant d'être transmis aux auteurs, soit par les éditrices en interne, soit par les responsables de dossier lorsque ceux-ci sont jugés compétents. D'autres ne sont jamais transmis et font l'objet d'une synthèse plus «\ constructive\ » afin de ménager les susceptibilités.

> -- [50:08] CC\ : Puis au moment où on leur [les directeurs de dossier] transmet les expertises et parfois il y a besoin d’un petit accompagnement à la réception des évaluations. 
>
> -- [50:18] MN\ : Oui, parce que nous, les responsables de suivi d'évaluation^[Ces responsables sont internes à la revue et sont censés être les seuls à connaître l'identité des évaluateurs.] reçoivent les expertises, mais ils ne sont pas du tout tenus de les donner [inaudible], ils peuvent parfaitement les lire, voir même enlever les choses inutiles, par exemple «\ trop mal écrits\ », là le chargé d'évaluation peut tout à fait reformuler les phrases de façon autre. Moi je mets par exemple «\ voulez-vous reformuler ce paragraphe\ » à la place de «\ trop mal écrit\ ». Donc ça ce sont des interventions quand même ...
>
> -- [50:58] CC\ : j’ai le souvenir d’un dossier pour lequel j’avais fait une grosse synthèse, j’avais complètement [inaudible] les originaux qui étaient trop inutilement blessants.
>
> -- [51:06] FXM\ : Ça paraît dans notre réponse au questionnaire, nous on organise les expertises, mais après on les donne au responsable de numéro de manière brute, et c’est au responsable de numéro de se charger...
>
> -- [51:17] CC\ : Non, justement, là c’est pas ça qu’on avait fait... ça dépend.
>
> -- [51:21] MN\ : ça nous est arrivé de faire des préfiltres, parce que justement une fois on avait transmis directement, c'était pour le tisseur, un des premiers qu'on avait édité, et il y avait dedans des expertises qui étaient vraiment trop brutales ...
>
> -- [51:38] CC\ : ... et qui avaient été transmises telles quelles par les responsables de numéro, et les  auteurs ne voulaient plus écrire.
>
> -- [51:42] MN\ : Ils étaient vexés, ils étaient blessés.
>
> -- [51:45] CC\ : Et c’était dommage parce que c’était des textes qui étaient bons, donc il n’y avait pas lieu de se fâcher, d’avoir des tensions inutiles.
>
> -- [51:56] CC: Ça dépend beaucoup si on connaît les responsables de dossier. On sait que ce sont des personnes qui vont justement faire ce travail de filtre et qui n'auront pas de difficulté à prendre le temps de faire ça. On peut leur donner des expertises brutes, et les laisser se débrouiller. Surtout si c’est des gens avec qui on a déjà travaillé, qui ont l’habitude ou qui font partie du comité de rédaction, en revanche, dans des cas où soit les expertises, où il y a beaucoup d’expertises qui posent problème, soit dans des cas où on craint que ce travail ne soit pas fait par le responsable de numéro, à ce moment-là, on peut nous le faire. 

On voit dans cet échange entre les deux directrices (CC et MN) et le secrétaire de rédaction (FXM), que le protocole n'est jamais figé complètement, au point que le secrétaire de rédaction s'y perd. Comme pour la revue _Mémoires du livre_, les mesures prises à l'encontre du protocole le sont pour «\ accompagner la réception des évaluations\ ». Cet «\ accompagnement\ » revient en fait à prendre en compte la dimension humaine de la relation auteur-évaluateur censée être avant tout _scientifique_. Il évoque, il me semble, le paradoxe de l'édition scientifique en sciences humaines. Les revues se munissent d'un protocole dit scientifique, pour en fait le détourner à l'épreuve de la réalité (humaine notamment). Cette réalité est autant celle des relations humaines, que celle des sciences humaines, qui semblent vouloir échapper à l'objectivisation. Sans nier la nécessité d'une certaine objectivité dans les processus de sélection et de légitimation, ne serait-il pas malgré tout pertinent d'assumer pleinement la subjectivité des positions, et de modifier les dispositifs de légitimation en conséquence\ ? Il est permis de se demander en effet si la réintroduction de cette dimension humaine, que l'on peut associer au soin porté à son interlocuteur, n'est pas elle-même scientifique dans la mesure où, aux dires des praticiens, elle semble améliorer la production des connaissances.

Dans le cas de la revue _Études françaises_, nous avons vu que la première sélection du dossier complet par le comité de la revue est déterminante. L'évaluation externe n'en est pas moins décisive, ainsi que l'établit le document _Protocole d'évaluation.docx_\ :

> Finalement, l’acceptation d’une proposition de dossier ne constitue en aucun cas une garantie de publication, les dossiers étant intégralement soumis à une évaluation anonyme par au moins deux experts du domaine.

Les entretiens révèlent pourtant que les évaluations externes n'ont que très peu de poids dans les décisions qui seront prises. Elles viennent donner au comité une indication, mais peuvent aussi bien ne pas être prises en compte.

> -- [25:11] ENL\ : On leur demande longtemps à l'avance, on leur donne trois mois à peu près, on les remercie pour ce qu'ils nous ont dit et on garde ça pour nous. Si on fait ça, c'est parce qu'on compte sur l'intervention du comité pour interpréter ces évaluations et les trancher. Mais on n'a pas d'autres liens avec les évaluateurs.
>
> [...]
>
> -- [48:34] JBCL\ : J'ai envie de préciser que la relation qui est établie entre nous et les évaluateurs externes met assez rapidement de l'avant le fait que l'évaluation faite n'est pas adressée aux auteurs mais au comité de rédaction et à la direction. C'est à eux, pour leur travail à venir, que l'évaluation est faite, et c'est dans cette logique-là qu'on demande de la penser.

Ce dernier point est une autre particularité dans le protocole d'_Études françaises_ Il est demandé aux évaluateurs de s'adresser avant tout au comité et non à l'auteur. Ainsi, plutôt que d'édulcorer les remarques d'un évaluateur, _Études françaises_ préfère assumer une rupture nette entre l'évaluateur et l'auteur. À ce dernier sera transmise, via le directeur de dossier, une synthèse des discussions interne au comité, sous la forme d'un procès-verbal .
Interrogée sur l'opportunité d'ouvrir davantage les flux de communication entre les évaluateurs et les auteurs, la directrice de la revue réitère sa confiance dans le dispositif classique de l'évaluation. Elle réhabilite les principes de séparation (anonymat, interprétation) pour l'objectivité sous-jacente à ces principes. Cependant, il faut tout de même remarquer que cette objectivité se voit à nouveau largement battue en brèche tant l'interprétation des évaluations par le comité se fait en toute connaissance des personnes et de leurs affinités, théoriques et/ou humaines.

> -- [51:43] ENL\ : Ça nous semble pas fonctionnel parce qu'on n'est pas dans cette logique-là. Ce qui nous ramène à la légitimation. Je pense que pour nous les évaluations internes et externes, c'est vraiment un processus de légitimation. Et ces verrous qui font que les gens ne communiquent pas entre eux, ou à travers nous, sont sensés garantir que le procédé reste anonyme, externe, et que donc les évaluateurs externes évaluent bien des textes, des problématiques et non des gens, et qu'on soit dans un processus de légitimation. On croit que malgré les aléas, il y a de mauvais évaluateurs externes, bon. Mais on en a deux, en plus on n’est pas idiots et on a six personnes au comité. Je fais confiance à cette structure pour qu'au sein du comité on soit capables d'évaluer non seulement le dossier qu'on a sur la table, mais les évaluations qui en ont été faites, ce qui dans la pratique est vrai. Pour le coup on a le nom des gens, et si quelqu'un dans le comité dit "oui c'était une mauvaise idée de l'envoyer à telle personne parce qu'il est dans une optique totalement différente », bon à ce moment-là on est autorisés à passer par-dessus, en partie, cette évaluation. Je caricature, ce n'est pas vraiment arrivé à ce point-là, mais je crois qu'on est dans la logique d'une légitimation par les pairs à l'aveugle, dont les inconvénients évidents sont contrebalancés par le nombre de gens qui interviennent et qui portent un jugement.

Cette dernière phrase révèle le même paradoxe déjà cité ci-dessus relatif aux pratiques éditoriales\ : l'éditrice 1)\ adhère aux principes de l'objectivité\ -- séparation des intervenants, 2)\ en reconnait les limites et les «\ inconvénients évidents\ », 3)\ adapte le processus en conséquence, ce qui revient à en abandonner l'objectif premier. Il semble _in fine_ que le jugement éclairé, c'est-à-dire informé autant que possible notamment de l'identité des personnes engagées dans le processus, soit préférable à un jugement effectué dans une certaine obscurité.

À ce stade, nous pouvons avancer deux remarques.
La première concerne l'omniprésence de la collégialité au cœur du processus éditorial. Les entretiens témoignent d'une intense activité de conversation, que ce soit au sein des comités, entre la revue et les évaluateurs, entre le comité et le·s directeur·rice·s de dossier, entre la revue et les auteurs. Pourtant tout est fait, au nom de l'objectivité scientifique, pour contraindre voire empêcher la libre conversation. Si l'on s'amusait à conceptualiser le processus éditorial en un protocole de communication au sens de Shannon, on s'apercevrait sans doute de sa parfaite deféctuosité. Peut-on envisager d'émanciper l'édition scientifique de certains de ses principes, vers un élargissement\ -- qualitatif et pourquoi pas quantitatif\ -- de la collégialité\ ?

Ce qui m'amène à une seconde remarque, concernant la nature épistémologique des sciences humaines. Cette constante négociation de leur propre protocole dans laquelle sont engagées les rédactions des revues nous ramène au fait que le carcan rigide d'un protocole garantissant une stricte objectivité est manifestement une illusion. L'édition est un processus multiple de sélection, de légitimation, mais aussi d'écriture et de structuration, et met ainsi en place des conditions bien spécifiques d'émergence de la pensée. Or il n'est pas certain que la pensée, ou sa mise en conversation avec d'autres pensées, ne se laisse si facilement modéliser. Si les contraintes peuvent être parfois créatives, elles peuvent également entraver l'interprétation et le jugement critique.
En considérant que les sciences humaines élaborent des connaissances dans une dynamique conversationnelle et requièrent de ce fait davantage de pensée critique que de protocoles de recherche, il est permis alors d'envisager des modèles éditoriaux favorisant la conversation.

### Interne/externe

<!-- D'une revue à l'autre, les experts évaluateurs sont par exemple 1) des "amis", connaissant la politique éditoriale de la revue et donc au fait de ce que la revue attend (de l'auteur et de l'évaluateur), 2) ce peut être parfois des auteurs ayant déjà publié un texte dans la revue, ou pour d'autre 3) des étrangers, sans aucune relation antérieure avec la revue, comme c'est le cas pour Intermédialités.

[Axes sur les reviewers] -->

On perçoit parfois la revue comme une communauté, avec son comité scientifique, son équipe éditoriale, ses lecteurs, ses auteurs réguliers, ses évaluateurs, directeurs de dossier. Pourtant cette communauté idéalisée se scinde dans la réalité en deux groupes aux frontières patentes, la famille interne d'un côté, et le monde extérieur de l'autre. Les entretiens avec les directrices et secrétaires de rédaction ont montré que cette distinction est maintenue de manière délibérée, et justifiée par l'idée selon laquelle réduire la porosité entre la revue et l'extérieur améliorera la scientificité de la revue. Cette tension entre l'interne et l'externe, ou entre l'«\ espace intime\ » et l'espace dehors, révèle en fait une conception particulière de la publication et de l'espace public. Or ces deux notions ne sont-elles pas justement retravaillées de fond en comble par les nouvelles modalités de la publication sur le Web\ ?


La distribution des rôles inhérente aux protocoles éditoriaux maintient une tension très nette entre ce qui est considéré comme interne et ce qui est considéré comme externe à la revue. Par exemple, l'auteur dont la revue doit discriminer l'écrit est maintenu externe à la revue, tandis que le secrétaire de rédaction se trouve logiquement interne. Le directeur de dossier qui _vient_ soumettre une thématique _provient_ également de l'extérieur.
Comme les précédents extraits d'entretien l'indiquent, certains participants considèrent cette tension comme vertueuse, et l'entretiennent en conséquence. D'autres l'ont plutôt incorporée comme dualité allant de soi dans la pratique éditoriale. Mais est-ce nécessairement le cas\ ? Cette tension n'est-elle pas d'abord le résultat d'une naturalisation de pratiques éditoriales héritées d'une part de l'imprimé, et d'autre part des standards scientifiques en vigueur dans les sciences naturelles\ ? Est-ce que les pratiques communicationnelles propres au web seraient en mesure de remettre en cause ces dispositifs régissant actuellement les rédactions des revues\ ?

Car une telle dualité interne/externe détermine largement les relations mises en place entre les individus impliqués dans le quotidien de la revue d'un côté, et de l'autre, les directeurs de dossier, les évaluateurs ou les auteurs. Ces derniers acteurs engagés à part entière dans le processus éditorial d'un numéro sont maintenus plus ou moins proches ou éloignés, mais toujours hors du noyau interne de la revue, quand bien même ils·elles seraient amené·e·s à participer aux décisions éditoriales.
Le cas des directeurs·rices de dossier est particulièrement frappant. À quel point sont-ils·elles impliquées dans les processus d'évaluation et de décision\ ? Quel est leur degré d'autonomie et de collaboration avec le comité\ ? Leur positionnement sur un axe interne/externe en dirait long sur les modèles éditoriaux d'une revue à l'autre.
Or, au regard des discussions intenses qui entourent et _produisent_ réellement le contenu d'un numéro, il est permis de se demander si un autre dispositif ne pourrait pas améliorer les flux de communication entre les différents intervenants. Quelle forme cela pourrait-il prendre, et que deviendrait alors le processus éditorial en matière de légitimation\ ?

À la question ainsi posée, les réponses des praticiens sont souvent tranchées.

La directrice d'_Intermédialités_ insiste sur l'utilité pour le comité d'avoir un espace de discussion interne, protégé de toute «\ surveillance\ » des autres intervenants, car cet espace intime assure au sein de l'équipe des échanges libres et décomplexés.

> -- [54:26] MF\ : C'est toujours la même chose. Quand on discute, si tu fais toujours la surveillance de tout le monde, à un moment donné ça devient un peu ingérable. Nous par exemple, chacun d'entre nous fait des textes qui sont transmis aux auteurs, on leur transmet les rapports, qui sont très peu édulcorés, un adjectif qu’on enlève, une phrase redondante. On enlève des phrases blessantes qui ne sont pas forcément intentionnelles, d'ailleurs. On enlève les formules blessantes pour ne laisser que l'argument. On fait un travail d'édition minime, et tout le monde est au courant, tant de ce travail que de la transmission aux auteurs, donc les évaluateurs font eux-mêmes attention. Si on devait se mettre sous la surveillance de l'auteur, c'est-à-dire qu'en écrivant ce texte on se met à la place de l'auteur, donc tu l'écris dans le but d'être lu par. Si, quand tu discutes avec des collègues après, tu es aussi sous ce coup-là, il y a aussi une perte de liberté, qui inclut tes propres erreurs. Dans les comités par exemple, on avance des réflexions, et parce que c'est quelque chose donné dans la discussion, il se peut qu'on change d'idée dans le texte transmis à l'auteur. Ce moment-là, où on se donne le moyen de réfléchir à quelque chose ensemble, avant de le livrer à l'auteur, est important. Surtout dans des questions d'interdisciplinarité, on est tous un peu humbles face au texte. Combien de fois, où les membres du comité commencent par «\ je ne suis pas un spécialiste, mais cette petite chose là, qu'est-ce que vous en pensez\ ? Moi je réagis de cette manière mais comme amateur curieux\ ». Combien de fois les gens commencent leur tour de parole comme ça\ ? Parce que c'est quelque chose de très interdisciplinaire, et parfois on n’est pas sûr de nous, tout simplement, et on livre aux autres «\ qu'est-ce que vous pensez de mon avis\ ?\ » avant même de livrer cette réflexion à l'auteur. Ces espaces sont importants, peut-être plus pour une revue comme la nôtre que dans une revue où il n'y a que des spécialistes qui se réunissent et qui savent où ils vont, et qui se servent de cette revue comme un instrument de divulgation de leurs recherches, de leurs thématiques. Mais nous quand on doit discuter sur un texte, il faut bien un espace où l'on puisse compter les uns sur les autres sur un texte où l'on n'est pas spécialiste. On compte sur l'intelligence collective, entre nous, avant d’embêter un auteur avec ça et de négocier notre avis avec l'auteur sur un texte. On aurait du mal à prendre des décisions des fois.

On pourrait rétorquer cependant que davantage d'ouverture de la conversation n'empêche en rien de ménager des espaces intimes. Tous les échanges n'ont pas nécessairement à être transparents, le contraire serait en effet contre-productif. Mais ce qui transparait malgré l'argumentaire de l'éditrice, c'est l'aspiration et le recours à l'intelligence collective pour construire un point de vue critique sur un texte. Est-ce que cette capacité critique ne viendrait pas s'affiner en augmentant le collectif\ ?
D'autant que la conversation a bien lieu, elle est même omniprésente dans tout le processus éditorial. Elle est cependant cloisonnée, de manière paradoxale, puis qu'elle en exclut les évaluateurs supposés experts et les auteurs premiers intéressés. Il ne s'agit donc pas d'une résistance épistémologique, mais plutôt d'une résistance de culture.
L'obstacle principal à une plus grande transparence et inclusion de la conversation relève finalement de la pudeur (intellectuelle), «\ on est tous un peu humbles\ », loin du changement culturel amorcé avec le web, caractérisé notamment par une émancipation des autorités [@broudoux_outils_2003] et de la libération inhérente de l'écriture et de l'expression.

<!-- [qui délivrent un avis largement décontextualisé, et excluant les auteurs, qui reçoivent les avis de manière asynchrone.]{.note}
Est ce que la conversation ne gagnerait à être plus inclusive

on voit bien le paradoxe d'une rédaction qui a besoin de conversation pour affiner son approche du texte. -->

Les éditrices de la revue _Mémoires du livre_ observent effectivement une tendance à l'ouverture qui se manifeste par les demandes des auteurs à dialoguer directement avec les évaluateurs, ou tout du moins à leur répondre.

> -- [38:20] MPL : Entrer en dialogue avec les évaluateurs ça ne s’est jamais fait de mémoire, c’est une demande que j’ai eue de la part d’un auteur européen qui est lui-même très impliqué dans le monde de la revue, participant à plusieurs comités de revue. Je pense que c’est une pratique qui va tendre à se répandre, mais nous ne sommes pas encore rendus là. Je ne sais pas si c’est une chose souhaitable, ce sera peut-être discuté en comité de rédaction si les demandes d’auteurs à être mis en contact avec les évaluateurs se répètent. Ça peut être bon, mais ça peut soulever un certain nombre de problèmes aussi.
>
> -- [39:14] JDM : Donc ça voudrait dire que les évaluateurs ne seraient plus anonymes ? Ce serait un dialogue direct entre l’auteur et les évaluateurs\ ?
>
> -- [39:24] MPL : l’auteur m’avait demandé de maintenir l’anonymat de l’évaluateur, mais de transmettre un courriel et de lui transmettre la réponse. On voit d’emblée le problème quand on a des secrétaires débordés, ça rajoute une étape de plus et je ne suis pas sûre que ça rajoute à la qualité de la revue. Je sais que les revues ont aussi pour mandat de dynamiser la recherche et de favoriser l’avancement de la réflexion chez les auteurs aussi, mais je ne suis pas certaine que ce soit une pratique répandue chez les autres revues ?

L'obstacle au dialogue est ici un argument commode, mais est-il recevable\ ? Car l'éditrice craint avant tout une augmentation des ressources nécessaires pour gérer les échanges. Se considérant comme unique _intermédiaire_ envisageable entre les évaluateurs et les auteurs, elle imagine sans doute la multiplication des courriels échangés, «\ l'étape en plus\ ». Dans le protocole éditorial actuel, en effet, la _médiation_ du dialogue deviendrait rapidement chronophage en ressources humaines. Mais il est possible d'imaginer un dispositif capable de prendre en charge cette médiation, sans pour autant court-circuiter le rôle ni la responsabilité de la revue. Au lieu de passer par un·e secrétaire de rédaction transférant des courriels de l'un à l'autre, la fonction de médiation peut plus simplement être assurée par un dispositif conversationnel, sans pour autant que soit retirée à la revue l'autorité de sa fonction éditoriale. Il suffit pour cela qu'elle conserve la maîtrise des règles et des paramètres du dispositif (anonymat, confidentialité, ouverture, etc.). Cette maîtrise constituera la base de la confiance que la revue et son équipe attribueront à une telle plateforme. La revue, en tant qu'instance éditoriale, sera alors en capacité de produire un climat de confiance vertueux autour du dispositif, climat dans lequel s'inscriront les différents intervenants.

Mon hypothèse est que la dualité interne/externe peut évoluer et laisser la place à une certaine porosité à travers des dispositifs établissant une distribution nouvelle de l'autorité. Il s'agit en fait d'adopter une pratique alternative de l'espace public, plus ouverte et plus transparente. Les protocoles éditoriaux adoptés par l'édition scientifique laissent transparaître une méfiance vis-à-vis de l'espace public, comme si seuls des contenus finalisés et figés pouvaient être rendus publics. Il y a dans cette approche de la publication une certaine négation du processus éditorial pourtant passionnant aux dires de ses acteurs. Ouvrir ce processus suppose effectivement de renverser le paradigme de la publication et de considérer que la fabrique éditoriale est potentiellement aussi pertinente sur le plan scientifique, voire plus pertinente, que le texte final. L'enjeu d'un tel renversement réside dans la capacité des éditeurs à endosser une nouvelle conception de l'espace public. Or ne vivons-nous pas déjà dans un tel espace public\ ? La culture numérique telle qu'a pu la définir Jenkins par exemple n'a-t-elle pas déjà constitué un espace public plus ouvert et transparent\ ?
Nous verrons dans le chapitre suivant qu'il existe, dans d'autres communautés d'écriture, des formes et des pratiques de publication contribuant à la mise en place d'un tel espace public. Je détaillerai comment ces communautés collaborent à travers des instances éditoriales _bienveillantes_, renouant avec un principe de confiance.

<!-- [reprendre cette idée du transfert d'autorité dans la confiance dans le dispositif, idée du «\ dispositif de confiance\ ». Voir  ci-dessous (et reprendre\ ?) ce que j'écrivais, en lien avec la bienveillance]{.note}
-->

Cette dualité tout juste évoquée se joue aussi sur le plan scientifique comme le note l'une des directrices de la revue _Itinéraires_. L'engagement scientifique des revues se retrouve dans tous les entretiens, que ce soit pour structurer une discipline ou un champ, pour explorer des hypothèses ou de nouvelles approches. Pourtant, l'éditrice établit une distinction entre la dimension éditoriale de l'intervention de l'équipe de la revue, et la dimension scientifique de la proposition d'un auteur ou d'un directeur de dossier.

> -- [44:58] MN\ : Ce n'est pas parce qu'on est responsable de la revue qu'on est responsable scientifique. Il y a ce que Marie-Anne Paveau appelle la «\ charité épistémique\ ». On considère quand même à priori, par défaut, que la personne qu'on a en face de nous, qui est chercheur, enseignant-chercheur, est en capacité de faire le numéro de façon autonome intellectuellement, idéologiquement, etc. Nous, on lui explique qui on est, ce qu’on fait, ce qu’on veut. S'il vient, cela suppose qu’il est d’accord, sinon il ne vient pas. À partir de là, son travail est fait dans la confiance. Évidemment, c'est une confiance mesurée, puisqu'il y a tout le processus d'évaluation, de contrôle, etc.

Au-delà de l'évidence du propos qui considère que l'échange intellectuel se fait nécessairement dans une relation de confiance mutuelle, le terme de _charité_ renvoie aux notions de don et de partage. Il ne s'agit pas cependant d'une faveur ou d'une offrande gracieusement accordée à des auteurs dans le besoin. Le don renvoie ici à la fonction première (introductive) de l'éditeur d'établir les conditions de possibilité d'une relation, en ouvrant un espace et un dispositif où les responsabilités et les autorités se trouveront réparties et distribuées. L'éditrice d'_Itinéraires_ décrit la «\ charité épistémique\ » faite au directeur de dossier lorsqu'il soumet et élabore un dossier. Il me semble que cette charité, ou cet espace ouvert, pourrait s'élargir à un cercle plus inclusif selon différentes modalités susceptibles de refléter la diversité éditoriale des revues.

La confiance s'érige à nouveau comme une valeur primordiale pour espérer ouvrir des espaces publics\ -- des espaces de publication\ -- vertueux en matière de production de savoir. Voici sans doute l'une des clés de la remédiation des revues, dont on voit qu'elle passera nécessairement par une nouvelle relation de confiance, par de nouveaux _dispositifs de confiance_, capable de concrétiser cette «\ bienveillance\ » telle que je l'ai introduit à partir de la pensée de Louise Merzeau. Il se joue ici un certain transfert de valeur entre confiance et autorité, qui permet de mieux percevoir le régime d'autorité que l'environnement numérique esquisse en renouvelant l'espace public.

### Fracture numérique

Lors des entretiens, l'une des problématiques abordées questionnait l'impact _du numérique_ sur l'édition et la revue du point de vue des praticiens. Le sujet était volontairement abordé sur le plan du format, de la production éditoriale et de la diffusion, sans évoquer directement la question épistémologique pourtant au cœur du débat. Derrière une perception parfois étriquée du numérique et de ses potentialités pour l'édition scientifique, les praticiens conservent de la revue scientifique et de sa fonction une vision à la fois convaincue, volontaire et ambitieuse. Nous verrons que cette vision s'ancre dans des valeurs et des notions suggérant une émancipation possible par rapport à l'_artefact_ revue.

Mais revenons pour le moment à la thématique du numérique abordée en entretien. De mon point de vue de chercheur, il en ressort un premier résultat marquant. La grande majorité des personnes entendues ne considèrent l'impact du numérique qu'à travers ses effets sur la diffusion de leurs contenus. De ce point de vue, tout le monde s'entend sur le fait que la publication en ligne accroît le lectorat et étend la portée de la revue. Le numérique est donc conceptualisé essentiellement comme vecteur de distribution, facilitant voire démultipliant l'accès aux contenus.

> -- [1:02:22] ENL\ : Je dirais que la forme numérique est plus accessible, et je suis sûre que c'est par le numérique que passe la majeure partie de notre lectorat, et même probablement du lisible. Par exemple le flot d'articles libres que l'on a, c'est des gens qui voient la revue sur _Érudit_ et qui ont envie d'y publier. J'en suis à peu près sûre. Donc il ne fait pas de doute qu'il est nécessaire que nous soyons diffusés sous cette forme, et que c'est la condition d'existence d'_Études françaises_ dans le champ disciplinaire.


Dans le même temps, cet élargissement du lectorat est aussi vécu comme une perte de contrôle sur l'identité et la provenance des lecteurs. Avec la diffusion d'imprimés par abonnement, les éditeurs jouissaient d'un contact humain et institutionnel leur permettant de connaître et de cartographier leur lectorat. Les plateformes de diffusion telles qu'_OpenEdition_ ou _Érudit_ se sont positionnées comme des intermédiaires vendant ou délivrant un accès groupé par «\ bouquet de contenus\ ». Aucune information ne permet de distinguer à quelle·s revue·s les institutions clientes s'intéressent plus spécifiquement. La directrice de la revue _Intermédialités_ regrette ainsi l'amalgame consécutif à l'accès groupé négocié par son diffuseur _Érudit_\ :

> -- [17:19] MF\ : À ce moment-là, je pense que la FRQ^[Le _Fond de Recherche du Québec_, l'organisme régional financeur des revues savantes.] obligeait les revues à se numériser entièrement, et on a commencé à voir l'impact de cette numérisation sur nos lecteurs. À la fois il y a eu cette contrainte, et à la fois il y a eu cette découverte, cet espoir que la numérisation allait nous ouvrir un champ incroyable de nouveaux lecteurs. La seule chose c'est que c'est arrivé à un moment où la revue commençait à se faire très bien connaître. On était abonné avec des abonnements individuels avec des institutions, avec des bibliothèques, un peu partout dans le monde. Philippe Despoix^[Ancien directeur de la revue _Intermédialités_.] avait fait un travail extraordinaire là-dessus, il y avait beaucoup de bibliothèques et d'universités américaines, allemandes, italiennes, françaises qui avaient répondu à nos démarches, et ça, dans le passage au numérique à _Érudit_, c'est tombé à l'eau. Parce qu'_Érudit_ ne fait que des paniers d'abonnements et ne gère pas des abonnements de revues à institutions directement. Donc à la fois c'était bien parce qu'en termes de lecteurs on a eu beaucoup plus, mais en termes d'institutions on a eu une perte[^suiteMF].

Car si les plateformes ont dans le meilleur des cas la capacité de fournir des statistiques précises sur les usages de lecture, les éditeurs ne sont plus en mesure de savoir si, par exemple, la Bibliothèque Bodléeinne d'Oxford présente la revue dans ses rayonnages, ou si telle ou telle institution a bien acheté le dernier numéro.
Le lectorat s'est effectivement étendu, parfois d'un facteur 100, mais les revues n'en ont pas moins un sentiment de perte de valeur symbolique.

[^suiteMF]: Suite de la citation: [«\ -- [18:30] MF\ : On est plus qu'avec _Érudit_ maintenant, ou avec très peu de bibliothèques qui continuent à nous suivre. Et cette économie-là nous est rentrée dans le corps aussi, parce qu'on voit maintenant qu'on est reliés à des logiques qui nous dépassent, en termes d'abonnement, de coût, de rayonnement, tout passe par _Érudit_, on s'est relié dans le rayonnement d'_Érudit_ elle-même. On a perdu la maîtrise de ça tout en ayant gagné beaucoup de lecteurs. Au niveau de notre rayonnement purement institutionnel on y a perdu. D'autant que ce qui est intéressant dans ce rayonnement institutionnel avec le papier c'est qu'on touchait des universités anglophones, italiennes, espagnoles, qui en s'abonnant à nos numéros papier pouvaient rendre disponible le numéro à leurs lecteurs hors étudiants, alors qu'en passant par _Érudit_ avec le système de barrière mobile, finalement pendant un an tous nos numéros sont inaccessibles aux gens qui n'ont pas accès à ces institutions-là, ou ces gens qui travaillent dans ces institutions parce qu'ils ne se sont pas abonnés à _Érudit_, ils ne font pas partie du consortium _Érudit_.\ »]{.cite}

Cette appréhension du numérique par le seul prisme de la diffusion est attestée par le sentiment que la nature du travail éditorial lui-même n'a pratiquement pas évolué. Par exemple, pour la directrice de la revue _Études françaises_\ :

> -- [1:03:20] ENL\ : Pour nous, la version papier, au niveau des textes c'est la même chose. Que les textes soient papiers ou distribués sur la plateforme _Érudit_, notre travail est le même.

Ou pour le secrétaire de rédaction d'_Itinéraires_\ :

> -- [1:03:06] FXM:  Après, dans le travail éditorial finalement ça change assez peu de choses, si ce n'est peut-être que, pour la mise en page _print_, je passais quand même un peu plus de temps à faire un travail soigné, chose qu'on fait un peu moins avec la version en ligne. Mais sinon tout le reste du travail éditorial évidemment, c'est exactement la même chose. 

Ces déclarations sont relativement étonnantes au regard des profonds bouleversements de l'écosystème d'écriture, d'édition et de publication. Comment un professionnel de l'édition peut-il passer à côté d'une telle transformation de son environnement\ ? Il est possible d'interpréter ce sentiment de plusieurs manières, d'ailleurs probablement concomitantes.
En premier lieu, _Microsoft Word_ demeure l'unique outil d'édition^[Avant d'adopter l'outil d'édition _Stylo_ développé par la CRC-EN, la revue _Sens public_ travaillait principalement avec _Libre Office_, mais pouvait s'adapter selon les besoins ou les pratiques des auteurs et faire une partie de l'édition sous _Microsoft Word_ ou sur _Google Documents_.], comme le montrent les réponses au questionnaire. Or la production du document au format _docx_ est une étape intermédiaire de l'édition, focalisée sur la validation du texte lui-même.

Cette vision quelque peu restreinte de leur outil est révélatrice de la sur-attention portée à la textualité des contenus au détriment de leur structuration et de leur mise en forme. Face à l'environnement numérique, les éditeurs ont été contraints, faute de compétences et de moyens, de déléguer une grande partie de ce qui constituait leur métier aux diffuseurs[^memoire-informatique]. Ces derniers ont ainsi pris en charge les dernières étapes de l'édition, en particulier la structuration sémantique des contenus, la production des métadonnées, et la mise en forme\ -- c'est-à-dire la mise en image au sens de Souchier à propos du «\ texte second\ », soit finalement l'ensemble de l'énonciation éditoriale[^memoire-erudit].

[^memoire-erudit]: En témoignent les propos d'Anthony Glinoer, directeur de _Mémoires du livre_\ : [«\ En termes de matérialité, étant une revue 100 % électronique, 100 % sur _Érudit_, on est contraint par les modèles et par les approches visuelles d’_Érudit_, on n’a pas tant de prise sur ces questions-là.\ »]{.cite} (Anthony Glinoer -- Entretien du 24 avril 2019)

[^memoire-informatique]: Ce que confirme par exemple Marie-Pier Luneau, ancienne directrice de la revue _Mémoires du livre_\ : [«\ Mais notre revue a pu grandir aussi avec _Érudit_ parce qu’_Érudit_ grandissait, et que ça fonctionnait avec la vision qu’il y avait, le libre accès a toujours été encouragé par _Érudit_, mais sans le support technique d’_Érudit_, des professeurs de littérature qui ne connaissent rien à l’informatique, n’auraient pas pu arriver à ça aujourd’hui.\ »]{.cite} (Marie-Pier Luneau -- Entretien du 24 avril 2019)

Certes, certaines règles typographiques sont encore dictées par l'éditeur, mais ce dernier n'est plus en capacité technique de les réaliser. Si l'attention au texte peut se justifier sur le plan scientifique, la déprise de l'énonciation éditoriale est particulièrement problématique vis-à-vis de la mission que l'on attribue traditionnellement aux éditeurs. Cette mission s'est donc concentrée par défaut sur la gestion de l'évaluation et sur la validation de la textualité des contenus, composant l'essentiel de la fabrique éditoriale au sein des revues.

> -- [1:13:02] ENL\ : Je pense que notre grande valeur réside dans la qualité du texte, parce qu'on pense qu'écrire avec précision et élégance garantit notre rigueur scientifique.
>
> -- [1:13:21] JBCL\ : Ne serait-ce qu'à la révision linguistique, la qualité du texte mais aussi la valorisation du propos. On pourrait ancrer ça sur ce qu'on disait tout à l'heure, de l'organisation subventionnaire qui valorise le contenant plus que le contenu, mais la valeur que représente le travail de la lecture littéraire, le travail que ça représente d'investir un texte et une pensée et de la faire travailler et d'y inscrire sa propre subjectivité.

Ce glissement est tel que certains éditeurs ne comprennent plus les étapes finales de l'édition, pourtant devenues primordiales dans un écosystème numérique de publication.
On le sait bien aujourd'hui, l'accès à un article ou à une revue se fait principalement à travers les moteurs de recherche. Les métadonnées qui caractérisent les articles sont donc les principales portes d'entrée pour y accéder dans l'océan des publications scientifiques. Avec la structuration sémantique des contenus, la production des métadonnées permettra à ces derniers d'être visibles, repérables et requêtables, et finalement d'_exister_ en ligne.
Les diffuseurs ne s'y trompent pas et prennent ce travail d'étiquetage très au sérieux. À titre d'exemple, _Érudit_ y consacre 75% du temps de traitement d'un dossier de revue^[Voir en annexe [la chaîne de production d'*Érudit*](../annexes/revue20/erudit-workflow-prod.pdf). ![_Érudit_'s production workflow](../media/revue20-eruditworkflow.jpg) [_Érudit_'s production workflow]{.legendImage}]. Pour sa part, _OpenEdition_ a su réintégrer les éditeurs dans la production des métadonnées à travers le CMS _Lodel_.

> -- [1:10:07] ENL\ : Cette somme de travail est reconnue par le FRQ, alors que la somme de travail sur le contenu n'est pas reconnue par le FRQ. Il reconnaît la somme de travail sur le contenant, il a raison,  mais fait comme si le contenu était gratuit à produire. En 5 ans on a eu le temps de s'habituer, ça a cessé d'être une découverte.

La directrice d'_Études françaises_ n'a pas tort de s'offusquer lorsque le FRQSC refuse de reconnaître le travail éditorial des revues sur le plan de la textualité, au point de ne plus le financer. Le signal est pourtant clair. En redirigeant une partie des financements initialement alloués aux revues vers le diffuseur _Érudit_, le FRQSC entérine le transfert de certaines fonctions éditoriales dans les mains du diffuseur, et confirme la primauté des données face au texte.


> -- [1:19:26] ENL\ : Je crois que l'idée qu'en raffinant le processus d'écriture on raffine l'analyse, le travail d'artisan qu'on a beaucoup aimé, dont on s'est fait dire qu'il était inutile à deux reprises par l'organisme subventionnaire, je crois que ça, il n'y a pas de marche d'évolution. Je crois que c'est ce qui produit la revue telle qu'elle est et que ça va bien au-delà d'une facture de propreté au premier degré, ça va plus loin, ça va dans le fait de pousser la réflexion des contributeurs quels qu'ils soient aussi loin que possible, en améliorant leur texte. Ça je ne crois pas qu'il soit souhaitable que ça change. Je crois qu'il faut le porter, rappeler que dans un univers où on nous demande de publier beaucoup, nous on répond qu'on veut publier bien, quel que soit le support, que publier bien ça prend du temps, de la collégialité, ça ne peut se faire tout seul. Il faut tenir cela, même si les choses ne vont pas dans ce sens, même si les dictats plus ou moins implicites qu'on est obligés de déduire des réponses de nos organismes subventionnaires, je crois qu'il faut le tenir, garder ces valeurs qui sont l'identité de la revue.

Mais l'éditeur a-t-il les moyens de retrouver la maîtrise de son énonciation\ ? Le souhaite-t-il seulement\ ? Comment renouer avec une pratique éditoriale complète par laquelle peut s'épanouir le couple forme et pensée\ ?
Car la production du savoir ne s'arrête pas à l'écriture d'un texte\ ; elle se poursuit dans son édition, dans son éditorialisation, dans sa mise à disposition, c'est-à-dire aussi dans les modalités d'appropriation. Sans vision et sans maîtrise de ces modalités, quelle raison d'être reste-t-il à l'éditeur\ ?
La prise de conscience suppose au préalable une connaissance minimale de ce qu'est le numérique en tant que support d'écriture et de lecture, en tant que _media_, environnement et milieu d'écriture.


Pour autant, le constat n'est peut-être pas si sombre. Tout n'est peut-être qu'une question de perspective, et au regard de l'ampleur des mutations, il apparaît sans doute préférable d'adopter une lecture plus optimiste.
On a pu attribuer la méfiance des revues à l'égard du numérique à une forme conservatisme rejetant les progrès en matière de diffusion, de libre accès ou de nouveaux usages de lecture. Il est probable qu'une meilleure littératie numérique renverse cette attitude et inverse même la tendance. Car l'édition est aussi un lieu d'innovation, et les revues ont une large marge de création à investir. C'est effectivement la vision de la directrice de la revue _Itinéraires_\ :

> -- [1:23:33] MN\ : Historiquement, les revues ça a toujours été un espace exploratoire, plus que les éditions papier où il y a déjà quelque chose de presque patrimonial. Il y a un coté expérimental dans la revue qui est possible.

Dans un autre registre, les éditrices de la revue _Intermédialités_ ont plusieurs fois exprimé leur frustration face aux contraintes éditoriales imposées par la plateforme _Érudit_ et son format de diffusion. Les évolutions de forme qu'elles envisagent pour exploiter les potentialités de l'environnement numérique sont ainsi bien plus innovantes sur le plan scientifique que les ajustements du système d'information du diffuseur. L'extrême normalisation imposée par la concentration des revues sur quelques plateformes de diffusion constitue un véritable obstacle à l'innovation éditoriale.

> -- [23:31] MT\ : Et c'est là qu'on pourrait développer l'intermédialité, via Internet. Mais pour _Érudit_ c'est compliqué de faire ça, et ils ont un protocole d'édition très fixe et très peu plastique, à chaque fois c'est un peu compliqué. Quand on fait le dépôt d'un numéro par exemple, on ajoute une rubrique, mais moi je ne savais pas comment leur dire, donc je leur ai demandé si je pouvais leur envoyer un formulaire, une fiche avec nos recommandations et ces choses-là, mais ils nous disent pas grand-chose. Ils nous disent de déposer les numéros dans leur état final, mais la discussion se fait ensuite, et on a un peu l'impression qu'on les dérange en leur demandant certaines choses, alors que ce serait tout à fait intéressant pour nous de développer plastiquement nos publications sur internet.

Il faut alors entendre la parole des éditeurs et des éditrices lorsqu'ils·elles expriment leur vision de la revue et de leur fonction. La revue s'y définit comme un espace ouvert sur l'extérieur, dans l'accueil des idées qui viendront le structurer.

> -- [42:18] MF\ : J'aime ce côté qu'offre la revue parce qu'elle n’est nulle part, c'est un territoire sans concept, appropriable par qui en veut, et par rapport à ce qu'il en fait, c'est là que va se situer l'évaluation. Tu te l'appropries, montre-moi ce que tu en fais.

La fonction éditoriale consiste alors manifestement à entretenir ce territoire, en particulier pour qu'il demeure un espace d'appropriation, non pas dans le sens d'une accaparement, mais dans le sens d'une libre contribution à l'espace commun. Pour tenter de se représenter son rôle d'éditrice, la directrice de la revue _Intermédialités_ reprend la métaphore du jardin et du jardinier, dont la mission n'est pas tant de déterminer le paysage, mais d'en maintenir les conditions de possibilité.

> -- [1:03:55] MF: J'aurais du mal à exprimer le rôle exact que l'on a. Surtout s'il faut le penser en termes d'amont ou d'aval, ou de leader. Je dirais qu'on entretient un terrain. On ne le laisse pas à l'abandon, on fait en sorte que ça pousse. Après, dans quelle direction ça va aller, on n'en sait trop rien, mais on fait en sorte que ça pousse.

On s'en souvient, la conversation n'est, au sein du protocole éditorial, qu'un moyen d'ailleurs peu scientifique\ -- une «\ négociation\ »\ -- pour pallier l'injonction de l'objectivité, inconciliable avec la nature discursive des _sujets_ de LSH. Mais en prenant un peu de hauteur par rapport à la fabrique éditoriale elle-même, en considérant cette fois-ci la revue en tant qu'espace, alors les éditeurs et les éditrices renouent avec la conversation comme finalité.

  > -- [1:38:52] MN: Ça crée des espaces imaginaires de réflexion, des cadres imaginaires dans lesquels on est autorisé à aller, et je pense que ça c'est ouvrir des espaces de discussion possibles, c’est quand même fondamental.

Car dans cet espace ouvert et appropriable de la revue, la conversation est ce par quoi s'_anime_ -- littéralement _devient vivante_ -- la communauté. Espace, communauté et conversation. Les éditeurs ont bien conscience de prendre soin d'un triptyque vivant dépassant largement l'artefact éditorial.

> -- [1:38:09] FXM: Moi je dirais que pour l'instant on est dans ce rôle de passeur, et je dirais que c'est ce qui nous intéressait dans le projet _Revue 2.0_, c'était de pouvoir aller plus loin justement, la notion de communauté, c'est là-dessus qu'on doit et qu'on peut s'améliorer, c'est constituer une communauté, l'animer, et qu'en tant que revue, être visible dans la conversation -- parler en tant que revue, je sais pas si c'est possible, mais voilà. 

La fonction éditoriale s'élargit alors considérablement en réancrant la revue scientifique au cœur de la cité, c'est-à-dire en prise avec les problématiques politiques et sociétales. Les directrices de la revue _Itinéraires_ ont conscience du pas de côté nécessaire pour réaliser cette vision. _Remédier_ la revue suppose sans doute d'investir les marges, y compris de l'institution scientifique, là où les _formes_ se libèrent en même temps que la pensée et les imaginaires.

> -- [1:40:09] MN: Nous sommes la revue littéraire du 93^[La revue _Itinéraires_ est domiciliée à l'Université Paris 8 en Seine Saint Denis (93).]. [...] On a aussi pris cette option-là\ : on est en banlieue, il y a un numéro méta sur la banlieue, qui a été beaucoup discuté d'ailleurs, est-ce que parce qu'on est en banlieue on doit parler de la banlieue\ ? Est-ce qu'on doit se désigner comme ça\ ? Ok, on est marginaux, donc on va créer un espace de réflexion peut-être aussi marginale, mais plus libre et plus ouvert avec des approches pas forcément institutionnelles, ou attendues... 
>
> -- [1:41:07] CC\ : ... pas forcément conformes, avec une possibilité de voir les choses un peu différement, de laisser la parole à de gens qui veulent aborder soit des territoires nouveaux, soit le faire d'une façon moins habituelle. On essaie aussi d'être au cœur des questions actuelles, dans ce cas pas trop marginal non plus, mais... 
>
> -- [1:41:34] MN\ : La banlieue influence Paname, Paname influence le monde. C'est pas parce qu'on est en marge qu'on n'a pas un pouvoir d'influence. Notre position marginale, si on l'accepte et si on la construit que en tant que telle ... En fait on a un discours dessus. On part de la marge, mais on ne réduit pas forcément notre perspective.




# Conclusion


[L]{.lettrine}es conclusions présentées ici n'épuisent que très partiellement le jeu de données récolté lors de cette observation. Cette première analyse s'est volontairement orientée sur mes thématiques d'étude, à savoir la fabrique de l'autorité ou le rôle de la conversation dans la production de connaissances. L'enquête elle-même n'aura d'ailleurs pas non plus épuisé un terrain aussi dense que la revue, en tant que lieu de savoir singulier, avec ses propres outils, ses protocoles et ses pratiques. Enfin, l'enquête a mis au jour un biais révélateur dans l'un de nos postulats du projet _Revue 2.0_. Le premier constat d'un certain illettrisme numérique chez les éditeurs nous a en effet amenés à adopter une position de passeur pour une transition numérique raisonnée et éclairée. Mais ce que l'on pourrait considérer de l'extérieur comme un solutionnisme s'est heurté à une réalité plus complexe, tant sur les pratiques et les compétences de chaque équipe éditoriale que sur leur vision et leurs objectifs d'éditeurs. Les entretiens conduits dans la seconde phase de l'observation auront de ce fait enrichi notre enquête d'une dimension humaine essentielle à la compréhension de l'édition scientifique, tel qu'elle se pratique dans les rédactions de revue. En particulier, sont apparues au grand jour la singularité de chacun des projets éditoriaux et la pluralité des formes et des modèles, battant en brèche le format idéalisé de la revue institutionnelle et son modèle épistémologique. Ces deux traits témoignent de la tension caractéristique du milieu numérique où cohabitent d'un côté une tendance à la normalisation des formes, imposée par les standards, mais aussi par la logique algorithmique, et d'un autre une dynamique d'innovation éditoriale se traduisant par un foisonnement des formes communicationnelles. Au contraire des grandes plateformes ou des dépôts institutionnels, il me semble que la revue doit rester avant tout un espace d'innovation et continuellement expérimenter ses modes de production et de communication des savoirs. La «\ maîtrise de la déprise\ » ne se jouera pas sur la capacité à se fondre dans le décor informationnel, c'est-à-dire à maîtriser ses données pour _correspondre_ aux rouages des algorithmes. Toute littératie numérique doit au contraire servir à déjouer les attendus et à complexifier ainsi ce même décor. Il s'agit en fait, pour reprendre l'image des éditrices interrogées, d'enrichir l'espace de la revue comme on embellirait un jardin de nouvelles plantations.

De ce point de vue, l'enquête menée dans le cadre du projet _Revue 2.0_ a permis d'approcher la revue comme un _espace_ davantage que comme un _format_ institutionnalisé. Or en cherchant à caractériser le travail éditorial dans les périodiques de LSH, l'enquête a pu dévoiler certains aspects de la fabrique de l'autorité, en particulier ce trait récurrent que j'ai nommé «\ négociation\ ».
Il apparaît en effet que le processus éditorial établit un espace de négociation au sein duquel se joue déjà une conversation scientifique. Il s'agit d'une part des _aménagements_ réalisés par les éditeurs sur leur propre protocole éditorial. Confrontés à la réalité de la discursivité des sujets et des problématiques de LSH, les éditeurs n'ont d'autres choix que de _négocier_ avec les protocoles éditoriaux censés pourtant garantir la scientificité des revues. D'autre part, la négociation se joue dans les multiples conversations, rouvertes par nécessité pour finaliser la décision éditoriale. Chacune de ces _infractions au protocole_ participe d'une structuration de l'espace et de l'autorité propre à chaque revue. Elles expliquent en partie les bifurcations, spécifiques d'une revue à l'autre, par rapport au format institutionnalisé, avec pour conséquence cette diversité de modèles éditoriaux et de modalités de construction de l'autorité.

Cette négociation m'incite à penser que la revue gagnerait à ouvrir et rendre transparent ces aménagements, c'est-à-dire finalement à ouvrir à ses lecteurs un espace nouveau, conversationnel. Il y a ici une opportunité pour les revues de repenser leur mission en reconnaissant que la production de connaissances relève d'un processus collectif, tel que je le développerai au chapitre suivant.




# Bibliographie {.refs}
