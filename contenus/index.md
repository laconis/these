---
title-prefix: Thèse
title: De la revue au collectif
subtitle: la conversation comme dispositif d'éditorialisation des communautés savantes en lettres et sciences
author: Nicolas Sauret
version: 1.0
date: 2020-09-08
tagversion: th1.0
keywords:
  - conversation
  - scholarly publishing
  - collective
  - editorialisation
  - writing
  - common
  - scholarly journal
  - media
  - remediation
  - humanities
---

:::titre
De la revue au collectif\ : la conversation comme dispositif d'éditorialisation des communautés savantes en lettres et sciences humaines
:::

:::auteur
Nicolas Sauret
:::

:::lire
[Lire la thèse](./1.0/remerciements.html)
:::

## Avant-propos

La présente thèse a été co-dirigée par Marcello Vitali-Rosati, professeur au département des littératures de langue française de l'Université de Montréal et titulaire de la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca) et par Manuel Zacklad, professeur de Sciences de l'information et de la communication au CNAM et directeur du laboratoire [Dicen-IDF](https://www.dicen-idf.org/). Louise Merzeau, anciennement professeure de Sciences de l'information et de la communication à l'Université Paris Nanterre et co-directrice du laboratoire Dicen-IDF, a initié la co-direction en 2015 avant de nous quitter en juillet 2017.

La thèse a été réalisée dans le cadre du labex [_Les passés dans le présent_](http://passes-present.eu/) et a donc bénéficié de l’aide de l’Etat géré par l’ANR au titre du programme _Investissements d’avenir_ portant la référence ANR-11-LABX-0026-01.
Elle a également bénéficié du soutien de la _Chaire de recherche du Canada sur les écritures numériques_, du FRQSC (Programme international) et du [CRIHN](https://www.crihn.org).

Elle a été déposée le 1^er^ septembre 2020 et est diffusée depuis en libre accès ici et en [PDF](./1.0/pdf/Sauret_these-revue-collectif_2020.pdf).


## Résumé

Si l'on s'accorde à dire que les outils numériques ont modifié en profondeur nos pratiques d'écriture et de lecture, l'influence que ces nouvelles pratiques exercent sur les contenus d'une part, et sur la structuration de notre pensée d'autre part, reste encore à déterminer.

C'est dans ce champ d'investigation que s'inscrit cette thèse, qui questionne la production des connaissances à l'époque numérique\ : le savoir scientifique aurait-il changé en même temps que ses modalités de production et de diffusion\ ? Je traiterai ce sujet à travers le prisme de la revue savante en lettres et sciences humaines, dont le modèle épistémologique, encore attaché au support papier, se voit profondément questionné par le numérique dans sa dimension technique aussi bien que culturelle. Je fais l'hypothèse que les modalités d'écriture en environnement numérique sont une opportunité pour renouer avec les idéaux de conversation scientifique qui présidaient l'invention des revues au 17^eme^ siècle. La thèse propose une réflexion en trois temps, articulée autour de trois conceptions de la revue\ : la revue comme format, comme espace et, tel que je le propose et le conceptualise, comme collectif.

La revue comme format, d'abord, émerge directement de la forme épistolaire au 17^eme^, favorisant alors la conversation au sein d'une communauté savante dispersée. Mais les limites conceptuelles du format nous invite à considérer la revue davantage comme un _media_. Pour penser alors sa remédiation, je montrerai que cette conversation trouve son incarnation contemporaine dans le concept d'éditorialisation. La revue comme espace, ensuite, où s'incarnait jusque-là l'autorité scientifique, fait émerger de nouvelles possibilités conversationnelles, en raison des glissements de la fonction éditoriale des revues et de leurs éditeurs dans l'espace numérique. Enfin, la revue comme collectif émerge d'une écriture processuelle, en mouvement, propre à l'environnement numérique. Un des enjeux de cette thèse réside dans la mise en évidence des dynamiques collectives d'appropriation et de légitimation. En ce sens, la finalité de la revue est peut-être moins la production de documents que l'éditorialisation d'une conversation faisant advenir le collectif.

Au plan méthodologique, cette thèse a la particularité de s'appuyer sur une recherche-action ancrée dans une série de cas d'étude et d'expérimentations éditoriales que j'ai pu mener en tant que chercheur d'une part, et éditeur-praticien d'autre part. La présentation des résultats de cette recherche-action, ainsi que leur analyse critique, fournissent la matière des concepts travaillés dans la thèse.

### Mots-clés

Conversation, édition scientifique, collectif, éditorialisation, écriture, communs, revue scientifique, _media_, remédiation, lettres et sciences humaines.

## Abstract

Digital tools have profoundly modified our writing and reading practices. Yet the influence that these new practices exert on content and on the structuring of our thinking has to be determined.

This thesis falls within this field of investigation and questions the production of knowledge in the digital age: has scientific knowledge changed along the transformation of its production and distribution means? I will deal with this subject through the prism of the scholarly journal in the humanities, whose epistemological model, still attached to the paper medium, is profoundly questioned by the digital age in its technical as well as cultural dimension. I hypothesize that the modalities of writing in a digital environment are an opportunity to revive the ideals of scientific conversation that presided over the invention of journals in the 17^th^ century. The thesis proposes a reflection in three stages, articulated around three conceptions of the journal : the journal as a format, as a space and, as I propose and conceptualize it, as a collective.

The journal as a format, first of all, emerges directly from the epistolary form in the 17^th^ century, thus favouring conversation within a dispersed scholarly community. But the conceptual limits of the format invite us to consider the journal more as a _media_. In order to grasp its remediation, I will show that this conversation finds its contemporary incarnation in the concept of editorialisation. Then the journal as a space, where scientific authority was previously embodied, brings out new conversational possibilities due to the shifts in the editorial function of journals and their publishers in the digital space. Finally, the journal as a collective emerges from a processual and writing, in movement, peculiar to the digital environment. One of the challenges of this thesis is to highlight the collective dynamics of appropriation and legitimation. In this sense, the purpose of the journal is perhaps less the production of documents than the editorialisation of a conversation that brings the collective to life.

From a methodological point of view, this thesis is the result of a practice-based research anchored in a series of case studies and editorial experiments that I have been able to carry out as a researcher on the one hand, and as an editor-practitioner on the other.

### Keywords

Conversation, scholarly publishing, collective, editorialisation, writing, common, scholarly journal, _media_, remediation, humanities.

## Conversation

Dans la mesure du possible, les citations ont été référencées en utilisant _Hypothesis_. Vous pouvez accédez aux textes cités en suivant les liens «\ sur Hypothesis\ ». Auparavant, il vous faudra vous créer un compte sur _Hypothesis_ et vous inscrire sur le groupe d'annotation dédié en cliquant sur [Thèse N.Sauret](https://hypothes.is/groups/JJ786xJj/these-n-sauret).

Au plaisir d'y poursuivre la conversation.


<!-- [TheseNico](https://hypothes.is/groups/3yDdEXPW/thesenico). -->




---

:::licence
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>
<a xmlns:dct="http://purl.org/dc/terms/" href="https://these.nicolassauret.net/" rel="dct:source">https://these.nicolassauret.net/</a> --
<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">«\ De la revue au collectif : la conversation comme dispositif d'éditorialisation des communautés savantes en lettres et sciences humaines\ »</span> de <a xmlns:cc="http://creativecommons.org/ns#" href="https://nicolassauret.net/" property="cc:attributionName" rel="cc:attributionURL">Nicolas Sauret</a> est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International (CC BY-SA 4.0)</a>.
:::
