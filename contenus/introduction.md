---
title: Introduction
subtitle: champ, traitement et enjeux
date: 2020-02-04
version: 0.2
previous: r20-0.1
date-version: 2019-09-17
link-citations: true
lang: fr
---

Mon champ est celui d'un média, la revue scientifique en sciences humaines et sociales, telle qu'elle est travaillée par l'avènement de l'environnement et de la culture numérique. À travers la revue comme artefact communicationnel, nous regardons comment le numérique remodèle la communication scientifique, et plus largement l'épistémologie sous-jacente.

De cette formule «le média revue scientifique en sciences humaine et sociales», il faut en distinguer et discuter les 4 éléments constitutif. En tout premier lieu le média, dont on voudra étendre la portée au milieu, pour nous milieu d'écriture, environnement-milieu et écriture milieu, où nous suivrons MVR et la théorie de l'éditorialisation selon laquelle l'espace est produit par l'écriture.

En second lieu, nous avons la revue, qui doit être considérée cette fois en tant que format éditorial, c'est-à-dire une forme donnée à un discours, dont la matérialité co-construit le discours. Par la périodicité, le recueil de textes et d'auteurs, la conjonction de textes aux rubriques identifiées, la forme est déjà une adresse à un lectorat et à une communauté d'écriture. De ce point de vue, la revue joue le rôle de point focal au sein d'une communauté, plus ou moins large, davantage que le livre ou la monographie, dont les formes de circulation et d'appropriation sont par nature plus dispersés.

Par ailleurs, la revue scientifique ajoute à sa forme générale une double spécificité, d'une part celle d'introduire dans son format éditorial des marqueurs de scientificité que l'on pourra développer, et d'autre part celle d'adresser une communauté plus resserrée. Ce resserrement peut jouer à la fois sur l'adresse d'une discipline particulière, au sein d'une discipline ou d'une transdiscipline sur une spécialisation thématique (par exemple la _Revue internationale de photolittérature_), ou encore sur le choix d'un angle théorique différencié (par exemple la revue _Intermédialités_) qui viendra distinguer la revue par rapport à une autre traitant d'une même thématique ou d'une même discipline. À travers ce dernier resserrement, la revue joue alors un rôle fédérateur au sein d'une communauté partageant un même point de vue théorique.

Enfin, il s'agit de sciences humaines et sociales, vaste domaine rassemblant de nombreuses disciplines, mais dont on pourra malgré tout retrouver des constantes sur le plan des formes éditoriales et de leurs fondements épistémologiques. La bannière parfois décriée des Sciences humaines et sociales nous oblige malgré tout à reconnaître une circulation naturelle des idées entre disciplines, témoignant justement du rôle particulier des revues scientifiques dans cette circulation, et que cette thèse s'appliquera justement à questionner. Les SHS se définissent souvent en creux par rapport Sciences dures, toute aussi humaine et sociales que les SHS pourtant, mais dont les fondemements épistémologiques diffèrent en effet. On attribue la dureté des sciences à la rigueur de ses critères de scientificités, reposant sur l'observation, l'expérimentation et finalement à une culture de la preuve qui n'aura pas manqué de contaminer les sciences molles par ses méthodes. Malgré tout, les SHS restent ancrées dans le principe de l'interprétation, toujours sujette à discussion, ou plus exactement toujours assise sur un processus conversationnel dont les formes sont d'ailleurs multiples. C'est pour cette conversation justement que nous nous concentrons sur les SHS, car il y là des pratiques nouvelles de communication, favorisée par le milieu numérique, et dont nous faisons l'hypothèse qu'elles pourraient constituer les prémisses d'une nouvelle épistémologie à condition qu'elles soient institutionnalisées dans des formes éditoriales adaptées. Cette hypothèse s'articule à deux autres, que j'essaierai de démontrer, l'une consistant à penser que l'institutionnalisation de pratiques d'écriture et de lecture passe par l'innovation éditoriale, et l'autre attachée à l'idée que la revue, malgré une forme traditionnelle relativement figée aujourd'hui, demeure encore un espace d'innovation éditoriale tel qu'elle l'a été à l'émergence des premiers périodiques scientifiques.

Si la thèse prend comme point focal la revue scientifique en tant que média, c'est en fait pour explorer des questions plus vastes, aussi essentielles à adresser dans le cours terme que dans un long terme qui apparait en passant de plus en plus court au regard de l'urgence climatique. Car la résolution des problèmes propres à l'anthropocène passera nécessairement par un renouvellement des paradigmes qui ont participé son avènement, qu'ils soient économiques, politiques, juridiques, culturels ou scientifiques. Cette thèse vient discuter ces questions du point de vue epistémologique, à partir d'une réflexion sur ce que les nouvelles pratiques d'écriture et de lecture apportent et ajoutent au paradigme scientifique et culturel en place depuis l'avènement de la science moderne. En observant et en expérimentant de nouvelles formes de production et de circulation des connaissances, nous questionnons en fait la construction de l'autorité, en pleine mutation avec l'adoption massive de la culture numérique. L'écriture numérique dans sa continuité et sa rupture modifie les structures d'autorité et redistribue en profondeur les fonctions, en particulier la fonction éditoriale dont nous verrons les déplacements de nature et ses implications. C'est en prenant en compte ces déplacements que la revue scientifique peut subsister comme forme collective, poursuivant ses missions premières tout en en adoptant de nouvelles^[collectif, appropriation].

J'appréhende la revue scientifique selon un prisme classique en trois temps : historique, actuel et prospectif.

Le premier temps est celui de la naissance du périodique scientifique, moment particulier car il est celui d'une innovation éditoriale qui, en institutionnalisant des pratiques d'écriture et de lecture de la communauté lettrée et savante à l'époque, mettra en place une nouvelle matérialité de la production du savoir et des connaissances.

Deuxièmement, je regarderai la situation actuelle, en partant du constat d'un déphasage de la communication scientifique institutionnelle au regard des pratiques d'écriture et de lecture émergeant avec le numérique. Ce déphasage révèle un double paradoxe, institutionnel et éditorial[^Institution],[^entretienPouyllau], et nous montrerons qu'il en induit un troisième, intimement lié aux deux premiers. Ce constat, je le soutiens à partir du projet Revue 2.0^[entretiens], qui m'a permis notamment de mieux saisir la façon dont les revues envisageaient le numérique (notamment comme simple vecteur de diffusion), de comprendre comment survivent les fondements épistémologiques qui se sont mis en place progressivement avec la science moderne, fondements incarnés (matérialisés) par le modèle éditorial de la revue scientifique. La situation actuelle des revues met en lumière la nécessité de repenser la question de l'écriture dans le champ scientifique, notamment en sciences humaines et sociale.

Cette réflexion nous amènera au troisième temps consistant à porter un regard prospectif sur le futur de la revue scientifique, en posant l'hypothèse que de nouvelles formes éditoriales[^crystalKnowledge],[^ManifesteCCLA] doivent voir le jour pour la production et l'élaboration des connaissances, que ces formes peuvent fonctionner sur un modèle conversationnel^[Merzeau], et que d'autres communautés d'écriture[^lescommuns],[^GénéralInstin] ont beaucoup à apprendre à la communauté scientifique sur une écriture-milieu dont l'enjeu n'est plus tant de faire document, mais de _faire collectif_, dans un mouvement inclusif à même d'accueillir des savoirs non institutionnels susceptibles d'adresser la complexité du monde.

[^Institution]: Institution

[^entretienPouyllau]: entretien Pouyllau

[^crystalKnowledge]: Crystal of knowledge

[^ManifesteCCLA]: Manifeste CCLA

[^lescommuns]: Les communs notamment à travers le dossier Sens public _Écrire les communs_

[^GénéralInstin]: Général Instin


## Plan (sept 2019)

  titre  | intention
:-- | :--
**1. La revue naissante** | partie historique sur la naissance du périodique et de l'article
  - _Au 17ème siècle, la naissance d’un format éditorial_ |
  --- le Journal des Savants |
  --- De la correspondance à l’article |
  --- Contrefaçons |
  --- Indexations |
  --- Fabrique d’une autorité |
  --- Appropriations |
**2. La revue déphasée** | constat et analyse de la situation actuelle
  -  _Multiplication des espaces et des formes conversationnelles_ | Cette partie est un premier constat, celui d'un double déphasage institutionnel et éditorial entre les formes institutionnalisées de communication scientifiques et la diversification des pratiques d'écriture dans la communauté scientifique.
  ---  Constat | -
  ---  Déphasage | -
  ------  Un paradoxe institutionnel | -
  ------  Un paradoxe éditorial | -
  ------  Un paradoxe épistémologique (titre à ajouter) | -
  -  _L'institution_ | -
  ---  [la mission de l’université sur la chaine de l’écrit] | -
  ---  [constat de démission de l’université sur la chaine d’écriture] | -
  -  _Institution et communauté_, dialectique entre formes et pratiques | -
  ---  [cas du hashtag twitter] | -
  ---  [cas de la plateforme OpenEdition] | -
**3. La revue émancipée** | partie prospective
  -  _Communauté et construction du savoir scientifique_ | -
  -  _Hypothèse : un modèle éditorial conversationnel_ | -
  ---  La nature environnementale du numérique | -
  ------  Le numérique comme milieu | -
  ------  Le (texte) numérique comme milieu | -
  ---  Vers une herméneutique collective | -
  ------  Régime documentaire en environnement numérique | -
  ------  L’hypothèse d’une herméneutique collective | -
  - _Thèse : faire collectif_ | -
