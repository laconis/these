---
title: Sommaire de la thèse
author: Nicolas Sauret
date: 2019-09-07
---
:::note

Dans la mesure du possible, je référence les citations avec Hypothesis sur un groupe dédié [Thèse N.Sauret](https://hypothes.is/groups/JJ786xJj/these-n-sauret).

Pour annoter la thèse, merci d'utiliser plutôt le groupe [TheseNico](https://hypothes.is/groups/3yDdEXPW/thesenico).

:::

## Rédaction en cours

[Introduction](intro/intro-0.2.html)                                  -- <small>[[PDF]](https://framagit.org/laconis/these/-/raw/master/pdf/Introduction%20%E2%80%93%20(v0.2).pdf?inline=false) [[intro.md sur git]](https://framagit.org/laconis/these/-/blob/master/contenus/intro.md)</small>

Chapitre 1 : [La revue format](partie1/partie1-0.4.html)              -- <small>[[PDF]](https://framagit.org/laconis/these/-/raw/master/pdf/La%20revue%20format%20%E2%80%93%20(v0.4).pdf?inline=false) [[partie1.md sur git]](https://framagit.org/laconis/these/-/blob/master/contenus/partie1.md)</small>

Chapitre 2 : [La revue espace](revue20/revue20-0.4.html)              -- <small>[[PDF]](https://framagit.org/laconis/these/-/raw/master/pdf/La%20revue%20espace%20%E2%80%93%20(v0.4).pdf?inline=false) [[revue20.md sur git]](https://framagit.org/laconis/these/-/blob/master/contenus/revue20.md)</small>

Chapitre 3 : [La revue collectif](conversation/conversation-0.8.html) -- <small>[[PDF]](https://framagit.org/laconis/these/-/raw/master/pdf/La%20revue%20collectif%20%E2%80%93%20(v0.8).pdf?inline=false) [[conversation.md sur git]](https://framagit.org/laconis/these/-/blob/master/contenus/conversation.md) </small>

[Conclusion](conclusion/conclusion-0.2.html)                                  -- <small>[[PDF]](https://framagit.org/laconis/these/-/raw/master/pdf/Conclusion%20%E2%80%93%20(v0.2).pdf?inline=false) [[conclusion.md sur git]](https://framagit.org/laconis/these/-/blob/master/contenus/conclusion.md)</small>

[Voir l'historique ci-dessous pour les versions de travail.]{.note}

## Historique

:::fullwidth

|chapitre |version |date |diff |annotateur·s|date d'annotation|
|:---|:-:|:-:|:-:|:---------|:-:|
|**AOUT 2020**   |   |   |   |   |   |
|conversation   |[v0.8](conversation/conversation-0.8.html) | 24/08/20  | [[diff]](conversation/diffconversation_0.7-0.8.html)  | ---  | ---  |
|Introduction   |[v0.2](intro/intro-0.2.html)               |22/08/20  | [[diff]](intro/diffintro_0.1-0.2.html) | ---   |---   |
|Conclusion     |[v0.2](conclusion/conclusion-0.2.html)    |20/08/20  | [[diff]](conclusion/diffconclusion_0.1-0.2.html) |    |---   |
|Revue 2.0      |[v0.4](revue20/revue20-0.4.html)          |18/08/20  | [[diff]](revue20/diffrevue20_r20-0.2-0.3.html) | ---   |  ---   |
|Conclusion     |[v0.1](conclusion/conclusion-0.1.html)    |17/08/20  | [[diff]](conclusion/diffconclusion_0.0-0.1.html) |    |---   |
|**JUILLET 2020**   |   |   |   |   |   |
|Introduction   |[v0.1](intro/intro-0.1.html)               |11/07/20  | [[diff]](intro/diffintro_0.0-0.1.html) | soumise le 24/7/20   |---   |
|Revue 2.0      |[v0.3](revue20/revue20-0.3.html)           |07/07/20  | [[diff]](revue20/diffrevue20_r20-0.2-0.3.html) | soumise le 14/08/20   | annotations Marcello (17/08/20)   |
|**JUIN 2020**   |   |   |   |   |   |
|partie 1       |[v0.4](partie1/partie1-0.4.html)           | 27/06/20  | [[diff]](partie1/diffpartie1_0.3-0.4.html) | soumise le 6/7/20   |---   |
|partie 1       |[v0.3](partie1/partie1-0.3.html)           | 04/06/20  | [[diff]](partie1/diffpartie1_0.2-0.3.html) | Commentaires Marcello (27/6/20)   |---   |
|**MAI 2020**   |   |   |   |   |   |
|conversation   |[v0.7](conversation/conversation-0.7.html) | 25/05/20  | [[diff]](conversation/diffconversation_0.6-0.7.html)  | annotée par Marcello!  | ---  |
|conversation   |[v0.6](conversation/conversation-0.6.html) | 13/05/20  | [[diff]](conversation/diffconversation_0.5-0.6.html)  | ---  | ---  |
|**AVRIL 2020**   |   |   |   |   |   |
|conversation   |[v0.5](conversation/conversation-0.5.html) | 14/04/20  | [[diff]](conversation/diffconversation_0.4-0.5.html)  | Révision ling. Servanne 13-16/5/20 (branche conversation-revision)  | ---  |
|conversation   |[v0.4](conversation/conversation-0.4.html) | 05/04/20  | [[diff]](conversation/diffconversation_0.3-0.4.html)  | Servanne (13/4/20 Dossier Communs)  | ---  |
|**FEV 2020**   |   |   |   |   |   |
|articulation   |[v0.2](articulation/articulation-0.2.html) | 26/02/20  | [[diff]](articulation/diffarticulation_0.1-0.2.html)  |   |   |
|articulation   |[v0.1](articulation/articulation-0.1.html) | 04/02/20  | ---  |   |   |
|conversation   |[v0.3](conversation/conversation-0.3.html) | 09/03/20  | [[diff]](conversation/diffconversation_0.2-0.3.html)  | Servanne (21/3/20 faire advenir le collectif)  | ---  |
|conversation   |[v0.2](conversation/conversation-0.2.html) | 26/02/20  | [[diff]](conversation/diffconversation_0.1-0.2.html)  | Servanne (4/3/20 version0 - spAuteur), Marcello (9/3/20 v0, écriture-milieu - thesenico)  |   |
|conversation   |[v0.1](conversation/conversation-0.1.html) | 04/02/20  | ---  | Servanne (écriture-milieu)  | 25/02/20  |
| ---  | ---  | ---  | ---  | ---  | ---  |
| **OCTOBRE 2019**   |   |   |   |   |   |
|Revue 2.0   |[v0.2](revue20/revue20-0.2.html)   |24/12/19  | [[diff]](revue20/diffrevue20_r20-0.1-0.2.html) | ---   |---   |
|Revue 2.0   |[v0.1](revue20/revue20-0.1.html)   |13/10/19  | --- | servanne   |24/12/19   |
| ---  | ---  | ---  | ---  | ---  | ---  |
| **SEPTEMBRE 2019**   |   |   |   |   |   |
|partie 1    |[v0.2](partie1/partie1-0.2.html)   |26/02/20  | [[diff]](partie1/diffpartie1_0.1-0.2.html) | ---   |---   |
|introduction|[v0.2](introduction/introduction-0.2.html)   |02/04/20  | [[diff]](introduction/diffintroduction_r20-0.1-0.2.html) | ---   |---   |
|introduction|[v0.1](introduction/introduction-0.1.html)   |17/09/19  | --- | servanne   |18/09/19   |
|partie 1    |[v0.1](partie1/partie1-0.1.html)   |22/09/19  | --- | ---   |---   |
|partie 2    |[v0.1](partie2/partie2-0.1.html)   |22/09/19  | --- | ---   |---   |
|partie 3    |[v0.1](partie3/partie3-0.1.html)   |22/09/19  | --- | ---   |---   |
| ---  | ---  | ---  | ---  | ---  | ---  |
|**AOUT 2019**   |   |   |   |   |   |
|chapitre1   |[v0.2](chapitre1/chapitre1-0.2.html)   |12/09/07  | [[diff]](chapitre1/diffchapitre1_0.1-0.2.html) |           |           |
|chapitre1   |[v0.1](chapitre1/chapitre1-0.1.html)   |19/07/19  | --- |servanne   |24/07/19   |

:::


## Annexes

## Références publiées ici

- Nicolas Sauret. Design de la conversation : naissance d’un format éditorial. Sciences du design n°2/2018, (Vol.8), 2018. [[version soumise]](references/ScDe_articlev1.3.html) [[sur Cairn.info]](https://www.cairn.info/revue-sciences-du-design-2018-2-page-57.htm)
