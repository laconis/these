
::: {#pageGarde}

:::title
De la revue au collectif\ : la conversation comme\
dispositif d'éditorialisation des communautés savantes\
en lettres et sciences humaines
:::
:::subtitle
:::
:::author
Nicolas Sauret
:::

:::link
Septembre 2020 | [these.nicolassauret.net](https://these.nicolassauret.net/)
:::

:::date
:::



:::subsubtitle

Thèse présentée en vue de l’obtention du grade de docteur :

+---------------------------------------------------------------------------+---------------------------------------------------------------------------+
|en _Sciences de l'information et de la communication_                      | en _Littérature comparée_ -- option _Études littéraires et intermédiales_ |
+---------------------------------------------------------------------------+---------------------------------------------------------------------------+
|de l'Université Paris Nanterre                                             | de l'Université de Montréal                                               |
+---------------------------------------------------------------------------+---------------------------------------------------------------------------+
|sous la direction de *Manuel Zacklad* (CNAM)                               |sous la direction de Marcello Vitali-Rosati (Université de Montréal)       |
+---------------------------------------------------------------------------+---------------------------------------------------------------------------+

:::tableinstitution

+---------------------------------------------------+---------------------------------------------------+
|École doctorale 138                                |Département de littératures et de langues du monde |
+---------------------------------------------------+---------------------------------------------------+
|&nbsp;                                             |Faculté des Arts et des Sciences                   |
+---------------------------------------------------+---------------------------------------------------+
|Université Paris Nanterre                          |Université de Montréal                             |
+---------------------------------------------------+---------------------------------------------------+

:::

:::


::: {#pageJury}

Composition du jury

--------------------- ------------------------ --------------------------------------
Président du jury     Michael E. Sinatra       Université de Montréal
Directeur             Marcello Vitali-Rosati   Université de Montréal
Directeur             Manuel Zacklad           CNAM, Université Paris Nanterre
Membre                Marta Severo             Université Paris Nanterre
Examinateur externe 1 Joëlle Le Marec          Université Paris Sorbonne
Examinateur externe 2 Serge Bouchardon         Université de Technologie de Compiègne
--------------------- ------------------------ --------------------------------------
:::

:::




::: {#resume}

## Résumé


Si l'on s'accorde à dire que les outils numériques ont modifié en profondeur nos pratiques d'écriture et de lecture, l'influence que ces nouvelles pratiques exercent sur les contenus d'une part, et sur la structuration de notre pensée d'autre part, reste encore à déterminer.

C'est dans ce champ d'investigation que s'inscrit cette thèse, qui questionne la production des connaissances à l'époque numérique\ : le savoir scientifique aurait-il changé en même temps que ses modalités de production et de diffusion\ ? Je traiterai ce sujet à travers le prisme de la revue savante en lettres et sciences humaines, dont le modèle épistémologique, encore attaché au support papier, se voit profondément questionné par le numérique dans sa dimension technique aussi bien que culturelle. Je fais l'hypothèse que les modalités d'écriture en environnement numérique sont une opportunité pour renouer avec les idéaux de conversation scientifique qui présidaient l'invention des revues au 17^eme^ siècle. La thèse propose une réflexion en trois temps, articulée autour de trois conceptions de la revue\ : la revue comme format, comme espace et, tel que je le propose et le conceptualise, comme collectif.

La revue comme format, d'abord, émerge directement de la forme épistolaire au 17^eme^, favorisant alors la conversation au sein d'une communauté savante dispersée. Mais les limites conceptuelles du format nous invite à considérer la revue davantage comme un _media_. Pour penser alors sa remédiation, je montrerai que cette conversation trouve son incarnation contemporaine dans le concept d'éditorialisation. La revue comme espace, ensuite, où s'incarnait jusque-là l'autorité scientifique, fait émerger de nouvelles possibilités conversationnelles, en raison des glissements de la fonction éditoriale des revues et de leurs éditeurs dans l'espace numérique. Enfin, la revue comme collectif émerge d'une écriture processuelle, en mouvement, propre à l'environnement numérique. Un des enjeux de cette thèse réside dans la mise en évidence des dynamiques collectives d'appropriation et de légitimation. En ce sens, la finalité de la revue est peut-être moins la production de documents que l'éditorialisation d'une conversation faisant advenir le collectif.

Au plan méthodologique, cette thèse a la particularité de s'appuyer sur une recherche-action ancrée dans une série de cas d'étude et d'expérimentations éditoriales que j'ai pu mener en tant que chercheur d'une part, et éditeur-praticien d'autre part. La présentation des résultats de cette recherche-action, ainsi que leur analyse critique, fournissent la matière des concepts travaillés dans la thèse.

**Mots-clés\ :** conversation, édition scientifique, collectif, éditorialisation, écriture, communs, revue scientifique, _media_, remédiation, lettres et sciences humaines.

## Abstract

Digital tools have profoundly modified our writing and reading practices. Yet the influence that these new practices exert on content and on the structuring of our thinking has to be determined.

This thesis falls within this field of investigation and questions the production of knowledge in the digital age: has scientific knowledge changed along the transformation of its production and distribution means? I will deal with this subject through the prism of the scholarly journal in the humanities, whose epistemological model, still attached to the paper medium, is profoundly questioned by the digital age in its technical as well as cultural dimension. I\ hypothesize that the modalities of writing in a digital environment are an opportunity to revive the ideals of scientific conversation that presided over the invention of journals in the 17^th^\ century. The thesis proposes a reflection in three stages, articulated around three conceptions of the journal : the journal as a format, as a space and, as I propose and conceptualize it, as a collective.

The journal as a format, first of all, emerges directly from the epistolary form in the 17^th^\ century, thus favouring conversation within a dispersed scholarly community. But the conceptual limits of the format invite us to consider the journal more as a _media_. In order to grasp its remediation, I will show that this conversation finds its contemporary incarnation in the concept of editorialisation. Then the journal as a space, where scientific authority was previously embodied, brings out new conversational possibilities due to the shifts in the editorial function of journals and their publishers in the digital space. Finally, the journal as a collective emerges from a processual and writing, in movement, peculiar to the digital environment. One of the challenges of this thesis is to highlight the collective dynamics of appropriation and legitimation. In this sense, the purpose of the journal is perhaps less the production of documents than the editorialisation of a conversation that brings the collective to life.

From a methodological point of view, this thesis is the result of a practice-based research anchored in a series of case studies and editorial experiments that I have been able to carry out as a researcher on the one hand, and as an editor-practitioner on the other.

**Keywords:** conversation, scholarly publishing, collective, editorialisation, writing, common, scholarly journal, _media_, remediation, humanities.
:::
