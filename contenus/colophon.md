---
title: 'Colophon'
date: 2020-08-27
version: 1.0
previous: 0.1
tagprevious: th1.0
date-version: 2020-09-08
link-citations: true
chapitre: unnumbered
lang: fr
---

:::{#colophon}
Cette thèse a été réalisée avec une chaîne éditoriale libre et modulaire. Les textes, les métadonnées et les références bibliographiques sont édités respectivement dans les formats _markdown_, _yaml_ et _bibtex_, à partir desquels sont produits des fichiers HTML statiques. Le script de production est écrit en _bash_, et mobilise principalement les logiciels et langages suivants : _Pandoc_\ -- génération des contenus en html, et _Paged.js_\ -- pagination et génération du PDF. Les contenus sont versionnés sur un repo _Git_, hébergé par l'instance _Gitlab_ de _Framagit_. La mise en page du corps de texte est inspirée du style _Tufte_, utilisant les polices de caractère _Georgia_ et _Open Sans_.

Source\ : [framagit.org/laconis/these](https://framagit.org/laconis/these)
:::
