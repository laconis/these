#!/bin/sh

versthese="1.0"
echo "Build these version" $versthese

  ## Test nouvelle version ?
  [ -e html/$versthese ] && ls html/$versethese || mkdir html/$versthese




cd contenus
cp remerciements.md ./site-remerciements.md
cp intro.md ./site-introduction.md
cp intro.bib ./site-introduction.bib
cp partie1.md ./site-revueformat.md
cp partie1.bib ./site-revueformat.bib
cp revue20.md ./site-revueespace.md
cp revue20.bib ./site-revueespace.bib
cp conversation.md ./site-revuecollectif.md
cp conversation.bib ./site-revuecollectif.bib
cp conclusion.md ./site-conclusion.md
cp conclusion.bib ./site-conclusion.bib
cp bibliographie.md ./site-bibliographie.md
cp bibliographie.bib ./site-bibliographie.bib
cp annexes.md ./site-annexes.md
cp colophon.md ./site-colophon.md
cd ..


# Traitement chapitre
chapitre=("remerciements" "introduction" "revueformat" "revueespace" "revuecollectif" "conclusion" "bibliographie" "annexes" "colophon")

for i in ${!chapitre[@]};
do
  echo Processing ${chapitre[i]}
  ## Test nouveau chapitre ?
  ## [ -e html/th-${chapitre[i]} ] && ls html/th-${chapitre[i]} || mkdir html/th-${chapitre[i]}

  cd contenus
  cp site-${chapitre[i]}.md temp.md
  awk '/---/{x=++i;next}{if (x == 1){split(FILENAME, A, "."); print > A[1]".yaml";}if (x == 2){print >FILENAME}}' temp.md
  title=$(yq r temp.yaml title)
  version=$(yq r temp.yaml version)
  previous=$(yq r temp.yaml previous)
  tagprevious=$(yq r temp.yaml tagprevious)
  rm temp.md
  rm temp.yaml

  ## Transformation du chapitre > html
  echo "transformation du chapitre $title version $version"
  cd ..
  pandoc --standalone --section-divs --ascii --filter pandoc-citeproc --filter pandoc-sidenote --variable=title-prefix:'Thèse « De la revue au collectif »' --variable=versthese:$versthese --variable=file:th-${chapitre[i]} --variable=url:${chapitre[i]} --css=../css/chapitre.css --template=./templates/th-chapitre.html5 --lua-filter=./templates/date.lua --table-of-contents ./contenus/these.yaml ./contenus/site-${chapitre[i]}.md -o ./html/$versthese/${chapitre[i]}.html


# tentative d'utiliser un script lua pour remplacer un placeholder %versthese% avec une variable
# pandoc --variable=versthese:1.0 --lua-filter=./templates/meta-vars.lua ./contenus/intro.md -o coucou.html

  ## ajout de la navigation
  current=$((i + 1))
  echo l61
  prec=${chapitre[i-1]}
  echo l63
  if [ $((i + 1)) -eq ${#chapitre[@]} ]
  then
    next=${chapitre[i-i]}
    echo l67
  else
    next=${chapitre[i+1]}
    echo l70
  fi

  cp ./contenus/site-$prec.md tempprec.md
  awk '/---/{x=++i;next}{if (x == 1){split(FILENAME, A, "."); print > A[1]".yaml";}if (x == 2){print >FILENAME}}' tempprec.md
  precTitle=$(yq r tempprec.yaml title)
  rm tempprec.md
  rm tempprec.yaml

  cp ./contenus/site-$next.md tempnext.md
  awk '/---/{x=++i;next}{if (x == 1){split(FILENAME, A, "."); print > A[1]".yaml";}if (x == 2){print >FILENAME}}' tempnext.md
  nextTitle=$(yq r tempnext.yaml title)
  rm tempnext.md
  rm tempnext.yaml




  # currentTitle=$(yq r ./contenus/site-${chapitre[i]}.md title)
  # echo l74
  # precTitle=$(yq r ./contenus/site-$prec.md title)
  # echo l76
  # nextTitle=$(yq r ./contenus/site-$next.md title)
  # echo l78

  echo "précédent: ${precTitle} (${prec}) < courant (${i}): ${title} (${chapitre[i]}) > suivant: ${nextTitle} (${next})"

  # footer>prec & next
  sed -i -e "s/chapitreprec/$prec/g" ./html/$versthese/${chapitre[i]}.html
  sed -i -e "s/chapitrenext/$next/g" ./html/$versthese/${chapitre[i]}.html
  sed -i -e "s/PRECTITLE/$precTitle/g" ./html/$versthese/${chapitre[i]}.html
  sed -i -e "s/NEXTTITLE/$nextTitle/g" ./html/$versthese/${chapitre[i]}.html
  # gestion structure lien selon version
  sed -i -e "s/vrt/$versthese/g" ./html/$versthese/${chapitre[i]}.html
done

rm contenus/site-*

cp -r media ./html/
cp -r annexes ./html/

ls html/$versthese

./makeindex.sh
